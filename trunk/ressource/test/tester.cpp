#define BOOST_TEST_MAIN
#define BOOST_AUTO_TEST_MAIN
#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE tester

#include <boost/test/unit_test.hpp>
#include <boost/test/auto_unit_test.hpp>
#include <boost/test/floating_point_comparison.hpp>
#include <boost/lexical_cast.hpp>
#include <vle/vpz.hpp>
#include <vle/manager.hpp>
#include <vle/value.hpp>
#include <vle/utils.hpp>
#include <vle/utils/PackageTable.hpp>
// for 'operator+=()'
#include <boost/assign/std/vector.hpp>
#include <boost/assert.hpp>
#include <boost/config.hpp>
#include <boost/program_options.hpp>

using namespace std;
using namespace boost::assign;
using namespace vle;
using namespace utils;

namespace po = boost::program_options;
namespace utf = boost::unit_test::framework;

struct F
{
   F () {
      vle::manager::init ();
   }
   ~
   F () {
      vle::manager::finalize ();
   }
};

BOOST_GLOBAL_FIXTURE (F) BOOST_AUTO_TEST_CASE (tester)
{
    string pkgname, vpzpathname;
    // Declare the supported options.

    po::options_description desc("Allowed options");
    desc.add_options()
        ("usage,H", "produce help message")
        ("package,P", po::value<string>(), "set the package")
        ("vpz,V", po::value<string>(), "set the vpz")
        ;

    po::variables_map vm;
    po::store(po::parse_command_line(utf::master_test_suite().argc,
                                     utf::master_test_suite().argv,
                                     desc), vm);

    po::positional_options_description p;
    p.add("vpz", -1);
    po::store(po::command_line_parser(utf::master_test_suite().argc,
                                      utf::master_test_suite().argv).
              options(desc).positional(p).run(), vm);
    po::notify(vm);

    if (vm.count("usage")) {
        cout << desc << "\n";
        return;
    }

    if (vm.count("package")) {
        pkgname = vm["package"].as<string>();
    } else {
        cout << "the name of the package was not set.\n";
        return;
    }

    if (vm.count("vpz")) {
        vpzpathname = vm["vpz"].as<string>();
    } else {
        cout << "the name of the vpz was not set.\n";
        return;
    }

    vle::utils::Package::package ().select (pkgname);

    vpz::Vpz file (utils::Path::path ().getPackageExpFile (vpzpathname));

    manager::RunQuiet r;

    r.start (file);

    BOOST_REQUIRE_EQUAL (r.haveError (), false);
}
