/**
 * @file
 * @author RECORD Team,
 *   http://record.toulouse.inra.fr ,
 *   Applied Mathematics and Informatics Research Division &
 *   Environment and Agronomy Research Division,
 *   French National Institute for Agricultural Research
 * @version $Id: $
 */

/*
 * Copyright (C) 2011 - INRA
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#include <vle/extension/fsa/Statechart.hpp>
#include <vle/extension/DifferenceEquation.hpp>
#include <vle/utils/i18n.hpp>


using namespace std;
using boost::lexical_cast;
using boost::bad_lexical_cast;
namespace ve = vle::extension;
namespace va = vle::extension::fsa;

namespace vd = vle::devs;
namespace vv = vle::value;
namespace vu = vle::utils;

namespace Ressource
{
/**
 * @brief Modèle d'Opération de Semis
 *
 */

class ASemis: public va::Statechart
{
public:

    enum State { IDLE, SOWING, SOWED};

    /**
     * @brief Charge le paramètre Densité de semis à partir du port de
     * condition expérimentale nomFichierSemis qui doit contenir le
     * nom relatif du fichier.
     *
     * Définie aussi le graphe d'état du modèle.
     */
    ASemis(const vd::DynamicsInit& atom,
           const vd::InitEventList& events)
        : va::Statechart(atom, events), mSemis(false)
    {

        // construction de l'automate à 3 états

        states(this) << IDLE << SOWING << SOWED;

        transition(this, IDLE, SOWING)
            << guard(&ASemis::isStarted)
            << send(&ASemis::out);

        transition(this, SOWING, SOWED)
            << after(vu::DateTime::aDay())
            << action(&ASemis::stop);

        eventInState(this, "Start", &ASemis::start) >> IDLE;

        initialState(IDLE);
    }

    virtual ~ASemis()
    {
    }

private:

    /**
     * @brief une méthode de type guarde qui assure la transition vers
     * l'état SOWING.
     */
    bool isStarted(const vd::Time& /*julianDay*/)
    {
        return mSemis;
    }

    /**
     * @brief une méthode de send qui réalise l'action sous forme de
     * l'envoi d'un évènement de Semis.
     *
     * Cette méthode envoie aussi un accusé de réception.
     */
    void out(const vd::Time& julianDay , vd::ExternalEventList& output) const
    {
        output << (ve::DifferenceEquation::Var("Sowing") = mDensite);  // parle a STICS   ("Sowing" -> voir SticsCrash)
        cout << vu::DateTime::toJulianDay(julianDay) << " mDensite: " << mDensite << endl;
        vd::ExternalEvent* order = new vd::ExternalEvent("ack");
        order->putAttribute("name", new vv::String(mActivity));
        order->putAttribute("value", new vv::String("done"));
        output.addEvent(order);
    }

    /**
     * @brief une méthode de type action dans un état qui modifie
     * l'état du modèle quand il se trouve dans l'état IDLE.
     */
    void start(const vd::Time& /*julianDay*/, const vd::ExternalEvent* evt)
    {
        mSemis = true;
        mActivity = evt-> getStringAttributeValue("activity");
        mDensite = evt-> getDoubleAttributeValue("densite");
    }

    /**
     * @brief une méthode de type "action de transition" qui modifie
     * l'état du modèle lors de la transition SOWING->SOWED
     */
    void stop(const vd::Time& /*julianDay*/)
    {
        mSemis = false;
    }

    /**
     * @brief observation du port Operation
     */
    virtual vv::Value* observation(const vd::ObservationEvent & event ) const
    {
        if ( event.onPort ("Operation")) {
            if (mSemis) {
                return vv::String::create("Semis");
            } else {
                return vv::String::create("");
            }
        }
        return va::Statechart::observation(event);
    }

    // Variables d'états
    bool mSemis;

    // Paramètres
    double mDensite;
    std::string mActivity;
};

} //namespace Ressource

DECLARE_DYNAMICS(Ressource::ASemis)
