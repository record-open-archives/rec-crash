/**
 * @file
 * @author RECORD Team,
 *   http://record.toulouse.inra.fr ,
 *   Applied Mathematics and Informatics Research Division &
 *   Environment and Agronomy Research Division,
 *   French National Institute for Agricultural Research
 * @version $Id: $
 */

/*
 * Copyright (C) 2011 - INRA
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#include <vle/extension/fsa/Statechart.hpp>
#include <vle/extension/DifferenceEquation.hpp>
#include <vle/devs/DynamicsDbg.hpp>

#include <vle/utils/i18n.hpp>

#include <boost/algorithm/string.hpp>
#include <boost/algorithm/string/split.hpp>

using namespace std;
using namespace boost::algorithm;

using boost::lexical_cast;
using boost::bad_lexical_cast;

namespace ve = vle::extension;
namespace va = vle::extension::fsa;

namespace vd = vle::devs;
namespace vv = vle::value;
namespace vu = vle::utils;

#define NB_JOURS 5

namespace Resource
{
/**
 * @brief a tractor resource vle::extension::fsa::Statechart model
 *
 */

class Tractor: public va::Statechart
{
public:
    enum State {IDLE, BUSY};

    Tractor(const vd::DynamicsInit& atom,
            const vd::InitEventList& events)
        : va::Statechart(atom, events),  mIsIDLE(true), mToBeNotAcked(false),
        mFinished(vd::Time::infinity)
    {
        // construction de l'automate

        states(this) << IDLE << BUSY;

        eventInState(this, "Request", &Tractor::inRequestIDLE) >> IDLE;

        transition(this, IDLE, BUSY)
            << guard(&Tractor::ifNotIDLE)
            << send(&Tractor::outAck);

        eventInState(this, "Request", &Tractor::inRequestBUSY) >> BUSY;

        transition(this, BUSY, BUSY)
            << guard(&Tractor::isToNoAck)
            << action(&Tractor::doDoneToNoAck)
            << send(&Tractor::outNoAck);

        transition(this, BUSY, IDLE)
            << when(&Tractor::isDone)
            << action(&Tractor::doIDLE);

        initialState(IDLE);
    }

    virtual ~Tractor()
    {
    }

private:

    /**
     * @brief store the name of the activity to be acknowledged...
     *
     * set the date the operation is finished, and ...
     *
     */
    void inRequestIDLE(const vd::Time& julianDay, const vd::ExternalEvent* evt)
    {
        //std::cout << "inRequestIDLE "<< vu::DateTime::toJulianDay(julianDay) << std::endl;

        mActivity = evt->getStringAttributeValue("activity");
        mField = evt->getStringAttributeValue("field");

        mFinished = julianDay +  NB_JOURS * vu::DateTime::aDay();

        mIsIDLE = false;
    }

    /**
     * @brief a guard to change the state of the resource
     *
     */
    bool ifNotIDLE(const vd::Time& julianDay)
    {
        //std::cout << "ifNotIDLE "<< vu::DateTime::toJulianDay(julianDay) << std::endl;

        return !mIsIDLE;
    }

    /**
     * @brief mention to the agent the ressource can be used
     *
     */
    void outAck(const vd::Time& julianDay, vd::ExternalEventList& output) const
    {
        //std::cout << "outAck "<< vu::DateTime::toJulianDay(julianDay) << " " << mActivity << std::endl;
        //output << (ve::DifferenceEquation::Var(mTractor) = 1.0);

        vd::ExternalEvent* response = new vd::ExternalEvent("response");
        response->putAttribute("name", new vv::String("response"));

        vv::Set *responseValue = new vv::Set();
        responseValue->addString(mField);
        responseValue->addBoolean(true);
        response->putAttribute("value", responseValue);
        output.addEvent(response);

        vd::ExternalEvent* order = new vd::ExternalEvent("ack");
        order->putAttribute("name", new vv::String(mActivity));
        order->putAttribute("value", new vv::String("done"));
        output.addEvent(order);
    }

    /**
     * @brief store the name of the activity to be rejected ...
     *
     * and mention that the job has to be rejected
     *
     */
    void inRequestBUSY(const vd::Time& julianDay, const vd::ExternalEvent* evt)
    {
        //std::cout << "inRequestBUSY "<< vu::DateTime::toJulianDay(julianDay) << std::endl;

        mActivity = evt->getStringAttributeValue("activity");
        mToBeNotAcked = true;
    }

    /**
     * @brief to mention the ressource can be released
     *
     */
    bool isToNoAck(const vd::Time& julianDay)
    {
        //std::cout << "isToNoAck "<< vu::DateTime::toJulianDay(julianDay) << std::endl;

        return mToBeNotAcked;
    }

    /**
     * @brief to mention the ressource can be released
     *
     */
    void doDoneToNoAck(const vd::Time& julianDay)
    {
        //std::cout << "doDoneToNoAck "<< vu::DateTime::toJulianDay(julianDay) << std::endl;

        mToBeNotAcked = false;
    }

    /**
     * @brief mention to the agent the ressource can not be used
     *
     */
    void outNoAck(const vd::Time& julianDay, vd::ExternalEventList& output) const
    {
        //std::cout << "outNoAck "<< vu::DateTime::toJulianDay(julianDay) << std::endl;

        vd::ExternalEvent* order = new vd::ExternalEvent("ack");
        order->putAttribute("name", new vv::String(mActivity));
        order->putAttribute("value", new vv::String("fail"));
        output.addEvent(order);
    }

    /**
     * @brief to mention the ressource can be released
     *
     */
    vd::Time isDone(const vd::Time& julianDay)
    {
        //std::cout << "isDone "<< vu::DateTime::toJulianDay(julianDay) << std::endl;

        return mFinished;
    }

    /**
     * @brief to mention the ressource can be released
     *
     */
    void doIDLE(const vd::Time& julianDay)
    {
        //std::cout << "doIDLE "<< vu::DateTime::toJulianDay(julianDay) << std::endl;

        mIsIDLE = true;
    }

    /**
     * @brief observation du port Operation
     */
    // virtual vv::Value* observation(const vd::ObservationEvent & event ) const
    // {
    //     return va::Statechart::observation(event);
    // }

    // Variables d'états
    bool mIsIDLE;
    bool mToBeNotAcked;

    vd::Time mFinished;

    std::string mActivity;
    std::string mField;

};

} //namespace Resource

DECLARE_DYNAMICS(Resource::Tractor)
DECLARE_NAMED_DYNAMICS_DBG(TractorDbg,Resource::Tractor)
