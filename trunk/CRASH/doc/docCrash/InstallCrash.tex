\makeatletter\def\input@path{{latex}}\makeatother
\documentclass[12pt]{article}
\usepackage{amsmath}
% ----  pour les accent français dans la biblio-----
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}

\usepackage{verbatim}
\usepackage{listings}

\usepackage[usenames,dvipsnames]{color}
\definecolor{light-gray}{gray}{0.95}
\usepackage{xcolor}

\definecolor{colorlink}{rgb}{0.6,0.0,0.0}
\usepackage[pdftex,
hyperindex=true,
pdfpagelabels,
colorlinks=true,  linkcolor=colorlink, citecolor=colorlink, urlcolor=blue]{hyperref}

\usepackage {graphicx}
\usepackage{subfigure}


\lstset{ % general style for listings
float=htpb
, numbers=left
, tabsize=1
, frame=single
, breaklines=true
, basicstyle=\footnotesize\ttfamily
, numberstyle=\tiny\ttfamily
, framexleftmargin=5mm
, backgroundcolor=\color{light-gray}
, xleftmargin=5mm
, frameround={tttt}
, captionpos=b
}
\lstset{language=bash}
\renewcommand{\thesubsubsection}{\alph{subsubsection})}


\title{Crash installation instructions}
\author{Jérôme Dury}
\date{\today}

\begin{document}
\maketitle
\tableofcontents
\clearpage
% -------------------------------------------
\section{Introduction}
This document describes the GNU/Linux installation of the different components of the CRASH simulation environment, i.e. Crash-VLE, Crash-DB and rCrash.\\
CRASH, \textit{\textbf{\underline{C}}rop \textbf{\underline{R}}otation and \textbf{\underline{A}}lloaction \textbf{\underline{S}}imulator using \textbf{\underline{H}}euristics}, is a model to simulate cropping plan decision-making at farm scale. The model should help technical advisors in supporting farmers in their choices and anticipating agricultural water needs depending on irrigated crop acreage. CRASH is developed under the RECORD-VLE, \textit{\textbf{\underline{V}}irtual \textbf{\underline{L}}aboratory \textbf{\underline{E}}nvironment}. RECORD-VLE is a multimodeling, simulation platform based on the discrete event formalism DEVS (Discrete Event System Specification).

% -------------------------------------------
\section{Installation procedure}
\subsection{Pre-Installation check list}
\subsubsection{VLE environment}
To install vle and rvle, please refer to the \href{http://www.vle-project.org/wiki/Download}{vle-project} website.
\subsubsection{Other require software}
\emph{subersion} is required to download the updated versions of the different RECORD-VLE packages and \emph{make} is required if you wish to use the different make file provided  within the CRASH packages. \\
\begin{lstlisting}[float=h]{}
	sudo apt-get install subversion make
\end{lstlisting}
% ------------
\subsection{vleCrash and littleCrash}
Four RECORD-VLE packages are required for the CRASH system-based modelling framework. The  packages have to be installed in the \emph{``../vle/pkgs/''} directory and can be downloaded from the  \href{https://mulcyber.toulouse.inra.fr/projects/record/}{repository} of the RECORD project using subversion:
\begin{lstlisting}{}
	svn checkout svn://scm.mulcyber.toulouse.inra.fr/svnroot/rec-crash/trunk/CRASH
	svn checkout svn://scm.mulcyber.toulouse.inra.fr/svnroot/rec_next_stics/branches/Crash-dev
	svn checkout svn://scm.mulcyber.toulouse.inra.fr/svnroot/record/trunk/pkgs/meteo
	svn checkout svn://scm.mulcyber.toulouse.inra.fr/svnroot/record/trunk/pkgs/glue
	ln -s Crash-dev StiVen
\end{lstlisting}{}

Then all the RECORD-VLE packages must be configured and compiled. For that we used the
\begin{lstlisting}{}
	vle -P CRASH configure build
	vle -P StiVen configure build
	vle -P meteo configure build
	vle -P glue configure build
\end{lstlisting}{}

% ------------
\subsection{CRASH-DB}
\subsubsection{Install Postgresql and Postgis}
To install postgresql, pgadmin3 and postgis, type the following code in the terminal:\\

\begin{lstlisting}[float=h]{}
	sudo apt-get install postgresql-8.4 postgresql-client postgresql-contrib pgadmin3 postgresql-8.4-postgis libpq-dev
\end{lstlisting}

Change superuser password\\
\begin{lstlisting}{}
	sudo passwd postgres
	# ... set the admin password
\end{lstlisting}{}

Postgres (8.4), pgadmin3 and PostGIS (1.5.2-2) will now be installed. For more help about installation and configuration, see the \href{http://postgis.refractions.net/docs/}{PostGIS Manual}.

\subsubsection{Create a PostGIS database template}
Creating a PostGIS database template is the way to go if you want to make it easy the creations of many GIS database on the same server. Without creating this template, you would need to repeat this steps every time you need to create a PostGIS database.\\

From the terminal, type this to create the template:\\
\begin{lstlisting}[float=h]{}
	sudo su postgres
	createdb postgistemplate
	createlang plpgsql postgistemplate
	psql -d postgistemplate -f /usr/share/postgresql/8.4/contrib/postgis-1.5/postgis.sql
	psql -d postgistemplate -f /usr/share/postgresql/8.4/contrib/postgis-1.5/spatial_ref_sys.sql
\end{lstlisting}

The template is now ready (a lot of functions and two tables - geometry\_columns and spatial\_ref\_sys - were created in it). Now we can test of postgistemplate we just created:\\
\begin{lstlisting}{}
	psql -d postgistemplate -c "SELECT postgis_full_version();"

	------------------------------------------------
	POSTGIS="1.4.0" GEOS="3.2.2-CAPI-1.6.2" PROJ="Rel. 4.7.1, 23 September 2009" USE_STATS
	(1 ligne)
\end{lstlisting}

\subsubsection{Create group role and user}
Generally the best way is to create the GIS data in the PostGIS database by using a different role and user than using the postgres one, that should be used only for administrative tasks. Typically I use to create a GIS role and user for managing data in the PostGIS database. You can even create more GIS users with different rights (SELECT, INSERT, UPDATE, DELETE on the different GIS feature classes), to generate a more safe environment. This depends on the configuration of your GIS scenario.\\

Connect to postgres (with postgres user) and create the group role, that here is name gisgroup (choose less permissions if needed for security reasons):\\
\begin{lstlisting}{}
	sudo -s -u postgres
	psql
	CREATE ROLE gisgroup NOSUPERUSER NOINHERIT CREATEDB NOCREATEROLE;
\end{lstlisting}

Type this to create the login role, here named user:\\
\begin{lstlisting}{}
	CREATE ROLE user LOGIN PASSWORD 'password' INHERIT;
\end{lstlisting}

Assign the gis login role to the gisgroup group role:\\
\begin{lstlisting}{}
	GRANT gisgroup TO user;
\end{lstlisting}


\subsubsection{Assign permissions}
We need to assign permissions for the postgistemplate tables (geometry\_columns and spatial\_ref\_sys will be owned from the gis user). Exit from the previous connection (type $\backslash$q), and connect to the postgistemplate database as the postgres user:\\
\begin{lstlisting}{}
	psql -d postgistemplate
\end{lstlisting}

Assign the permissions:\\
\begin{lstlisting}{}
	ALTER TABLE geometry_columns OWNER TO user;
	ALTER TABLE spatial_ref_sys OWNER TO user;
\end{lstlisting}

Exit from the connection ($\backslash$q)

\subsubsection{Database creation}
Now we are ready to create the database (or more databases) where to load the data (named crash\_test), using the createdb command, from the postgistemplate we just have created:\\
\begin{lstlisting}{}
	createdb -T postgistemplate -O user crash_test
	createdb -T postgistemplate -O user cas1
	createdb -T postgistemplate -O user casWCSPsimple
	createdb -T postgistemplate -O user casWCSPcomplete
\end{lstlisting}

\subsubsection{Parameterization of  PgAdminIII}
To access to the database we need to add a server (Figure \ref{PgAdmin1}) and to  parametrize it in PgAdmin (Figure \ref{PgAdmin2}).

\begin{figure}[h]
	\centering
	\subfigure[]{\includegraphics[scale=0.4, trim = 0mm 0mm 0mm 0mm,clip]{figures/PgAdmin1}
	\label{PgAdmin1}}
	\subfigure[]{\includegraphics[scale=0.4, trim = 0mm 0mm 0mm 0mm,clip]{figures/PgAdmin2}
	\label{PgAdmin2}}
	\caption{Adding and parameterizing a server in PgAdminIII}
\end{figure}

To test our crash-db installation, we can load a sample database.

\begin{lstlisting}{}
	cd data/farm_db
	make casWCSPsimple
\end{lstlisting}

The crash\_test database is now fill in with a case study with the following virtual farm (Figure \ref{WCSPsimple}).

\begin{figure}[h]
	\centering
	\includegraphics[scale=0.4, trim = 0mm 0mm 0mm 0mm,clip]{figures/WCSPsimple}
	\caption{Simple and virtual case study.}
	\label{WCSPsimple}
\end{figure}

% ------------
\subsection{rCrash}
Prior to install the rCrash package, some needed other packages have to be installed first. From R:
\begin{lstlisting}{language=R}
	install.packages("ggplot2", "lubridate", "maptools", "SQLiteMap", "RPostgresSQL")
	q()
\end{lstlisting}

Then to install rCrash from the terminal, type:\\
\begin{lstlisting}
	R CMD rCrash_1.0.tar.gz
\end{lstlisting}

If you wish to compile rCrash from sources you need to install \emph{roxygen} from R running in a root mode:
\begin{lstlisting}
	sudo R
\end{lstlisting}
then:
\begin{lstlisting}{language=R}
	install.packages("roxygen")
	q()
\end{lstlisting}
then, compile rCrash in the terminal using the makefile:
\begin{lstlisting}
	make package
\end{lstlisting}



% -------------------------------------------
\section{Getting started}


\section{Little Crash} % (fold)
\label{sec:littleCrash}
\subsection{List of predicates} % (fold)
\label{sub:pred}

\subsubsection{Soil} % (fold)
\label{ssub:Soil}

\begin{description}
	\item[Bearing capacity]:
		\begin{itemize}
			\item [Definition:] soil water status as a proportion of of readily availble water (-)
			\item [Function name:] \emph{p\_bearingCapacity\_sol}
			\item [Parameters]:
				\begin{itemize}
					\item \emph{d\_hucc\_sow} (crashdb: expertknowlege.soiltype.d\_hucc\_sow), value: double between 0-1 (-)  \end{itemize}
		\end{itemize}
	\item [Water depletion]:
		\begin{itemize}
			\item [Definition:]  water deficit between surface and root deep (mm) (Soil: waterDepletion)

			\item [Function name:] \emph{p\_waterDepletion\_sol}
			\item [Parameters]:
				\begin{itemize}
					\item \emph{waterDepletion}, value: double (mm)
				\end{itemize}
		\end{itemize}
	\item [Predicate]:
		\begin{itemize}
			\item [Definition:]
			\item [Function name:]
			\item [Parameters]:
				\begin{itemize}
					\item
				\end{itemize}
		\end{itemize}
\end{description}

% subsubsection Soil (end)

\subsubsection{Crop} % (fold)
\label{ssub:Crop}
\begin{description}
	\item [Harvested organe stage]:
		\begin{itemize}
			\item [Definition:] Phenological stage: harvested organe filling.
			\item [Function name:] \emph{p\_harvestedOrganeStage\_crp}
			\item [Parameters]:
				\begin{itemize}
					\item \emph{harvestedOrganeStage}, value: int (1) sown, (2) begining of the critical phase for grain number onset, (3) flowering (start of fruit sensitivity to frost), (4) onset of filling of harvested organs, (5) onset of water dynamics in fruits, (6) physiological maturity, (7) harvested
				\end{itemize}
		\end{itemize}
	\item [Vegetative stage]:
		\begin{itemize}
			\item [Definition:] phenological stage: vegetative dynamics.
			\item [Function name:] \emph{p\_VegetativeStage\_crp}
			\item [Parameters]:
				\begin{itemize}
					\item \emph{vegetativeStage}, value: int (1) sown, (2) germination, (3) emergence of budding, (4) maximum acceleration of meaf growth, end of juvenil phase, (5) maximum leaf area index, end of leaf growth, (6) harvested

				\end{itemize}
		\end{itemize}
	\item [H2O for harvesting]:
		\begin{itemize}
			\item [Definition:] Water content of reserves
			\item [Function name:] \emph{p\_h2o\_hrv}
			\item [Parameters]:
				\begin{itemize}
					\item \emph{h2oHrv}, value: double between 0-1 (-)
				\end{itemize}
		\end{itemize}
\end{description}
% subsubsection Crop (end)


\subsubsection{Weather} % (fold)
\label{ssub:Weather}
\begin{description}
	\item [Rain fall]:Predicat that check the sum of rain (limRain) over a number of day.
		\begin{itemize}
			\item Function name: \emph{p\_rain\_wth}
			\item Parameters:
				\begin{itemize}
					\item \emph{nDay}, value: int (day)
					\item \emph{limRain}, value: double (mm)
				\end{itemize}
		\end{itemize}
	\item [Minimun temperature for sowing]:
		\begin{itemize}
			\item [Definition:] Minimum temperature for sowing, depend on the crop.
			\item [Function name:] \emph{p\_tmn\_sow}
			\item [Parameters]:
				\begin{itemize}
					\item \emph{tmin\_sow} (crashdb: expertknowlege.crop.tmin\_sow), value: double (\°C)
				\end{itemize}
		\end{itemize}
\end{description}
% subsubsection Weather (end)

\subsubsection{date and time} % (fold)
\label{ssub:date and ti}
\begin{description}
	\item [date]:
		\begin{itemize}
			\item [Function name:] \emph{p\_at\_date}
			\item [Parameters]:
				\begin{itemize}
					\item \emph{date}, value: (day/month)
					\item or
					\item \emph{date+1}, value (day/month)
				\end{itemize}
		\end{itemize}
\end{description}
% subsubsection date and ti (end)

% subsection i (end)
% section section (end)


\end{document}

