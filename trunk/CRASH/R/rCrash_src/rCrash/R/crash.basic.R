##' crash_db   is a function that return the crahs db connexion parameter
##' @title Function: crash_db
##' @author Jerome Dury
##' @return The function returns a list
##' @export
crash_db <- function()
{
	lst = list(dbname = "test", user = "user", password = "psd")
	return(lst)
}

##' crash_basic is a function that retunr a list of basic value
##' @title Function: crash_basic »
##' @author Jerome Dury
##' @param dbname (string) crash database name
##' @param user (string) crash database user name
##' @param password (string) crash database user password
##' @return The function returns a list
##' @export
crash_basic <- function(dbname = dbname, user = user, password = password)
{
	sql="SELECT * FROM systemknowledge.landunit"
	lu = utils_dbimport(dbname, user, password, sql)
	lu.nb = nrow(lu)
	lst = list(lu.nb = lu.nb)

	return(lst)
}

##' date2Julian is a function convert day, month, year into Julian day
##' @title Function: date2Julian
##' @author Jerome Dury
##' @param integer day
##' @param integer month
##' @param integer year
##' @return The function returns a integer (julian day)
##' @export
date2Julian <- function(day, month, year)
{
	require(chron)
	origine = c(month=11, day=24, year=-4713)
	date=julian(month,day,year, origin. = origine)
	return(date)
}

##' julian2Date is a function convert julian day into date
##' @title Function: julian2Date
##' @author Jerome Dury
##' @param integer jday
##' @return The function returns a Date
##' @export
julian2Date <- function(jday)
{
	require(chron)
	dte=as.character(chron(jday, out.format =  "m/d/y", orig =c(month=11, day=24, year=-4713)))
	temp.dte= as.data.frame(month.day.year(dte))
	dte=with(temp.dte, paste(year, month, day,sep = "-"))
	dte =as.Date( dte,"%Y-%m-%d")
	return(dte)
}

##' getMonth is a function convert day, month, year into Julian day
##' @title Function: getMonth
##' @author Jerome Dury
##' @param character of "day/month"
##' @return The function returns a integer (month)
##' @export
getMonth <- function(x)
{
	out = as.integer(substr(x, regexpr("/",x)+1, regexpr("/",x)+3))
	return(out)
}

##' getDay is a function convert day, month, year into Julian day
##' @title Function: getDay
##' @author Jerome Dury
##' @param character of "day/month"
##' @return The function returns a integer (day)
##' @export
getDay <- function(x)
{
	out =  as.integer(substr(x,0, regexpr("/",x)-1))
	return(out)
}

##' cleanRvleResults is a functionto clean col names of results from rvle simulation
##' @title Function: cleanRvleResults
##' @author Jerome Dury
##' @param dataframe dataframe to clean
##' @return The function returns a data with cleaned column names
##' @export
cleanRvleResults <-function(data)
{
	z = colnames(data)
	clean <- function(x)
	{
		out = unlist(strsplit(x,"\\."))
		return(out[length(out)])
	}

	for(i in 1:length(z))
	{
		z[i]=clean(z[i])
	}
	colnames(data)=z

	if("Operations" %in% colnames(data))
	{
		cleanOperations <- function(x)
		{
			out1 = unlist(strsplit(as.character(x$Operations), "\\|"))

			out=data.frame(lu=NA,Operation=NA,type=NA,Var=NA)
			for(i in 1:length(out1)){
			out[i,] = t(unlist(strsplit(out1[i], ";")))
			}
			return(out)
		}
		data=ddply(data,z,cleanOperations)
		data$Operations=NULL
	}

	data$date = julian2Date(data$time)
	return(data)
}
