##----------------------------------------------------------------------------##
##----------------------------------------------------------------------------##
# CRASH #

## file= runLC_prod.R
## author = Jérôme Dury
## date = 21/06/2011

##---------------------------------------------------------------------------##
##---------------------------------------------------------------------------##

#initialisation
PATH.ini="~/vle/pkgs/CRASH/"
PATH.out ="~/vle/pkgs/CRASH/output"

PATH.datastics="data/stics_data"
PATH.R="R"
setwd(PATH.ini)

## Library
#detach("package:rCrash", unload=TRUE)
library(rCrash)
library(rvle)

## Define Crash db connection
crash_db <- function()
{
	lst = list(dbname = "cas_1", user = "crash", password = "crashtest", host ="localhost") 
	return(lst)
}
db=crash_db()

## -----------------------------------------------------------------------
# automatique 
##--------------------------------------------------------------------------
# make plan
setwd(PATH.ini)
plan_lst = isim_makePlan(file="data/stics_data/MP_1.CLI", rep=21, seed=100, yr_mn=1988, yr_mx=2008, db=crash_db())

plan_rep = plan_lst[["plan_rep"]]
#plan_rep = plan_rep[plan_rep$id_crop == 'CH'& plan_rep$itk == 'CH_3']
yr = plan_lst[["year"]]
plan = plan_lst[["plan"]]
#plan= plan[plan$id_crop == 'CH'& plan$itk == 'CH_3']

#execution
setwd(PATH.out)
df = isim_runPlan(plan_rep)

## analyse 
stics_var=df[["res.var"]]

#yield
isim_getYield<-function(maf, hrec, norm=NULL)
	{
		mf = as.numeric(tail(maf[maf>0.0],n=1))
		hc = as.numeric(tail(hrec[ maf>0.0],n=1))
		if(length(mf)==0){
			return(y=0)
		}

		y = mf
		nr = norm * 90
		if(!is.null(hrec)) y =mf * 100 / (100-hc)
		if(!is.null(norm)) y =  mf * 100.0 / (100.0-nr)
		return(y)
	}

	yd=ddply(stics_var,.(sim, index),summarize, yd_hrec = isim_getYield(Mafruit,H2Orec, norm = NULL), yd_norm = isim_getYield(maf= Mafruit, hrec=NULL, norm = 0.145), yd_dry = isim_getYield(Mafruit,NULL,NULL))

crpp = merge(plan[,c("index","code_soiltype","code_crop","id_variety","code_itk")], yd, by= "index")

#irrigation
wi = ddply(stics_var,.(sim, index), summarise, wi = sum(Airg))
pyld=ggplot(data=crpp)+geom_histogram(aes(x=yd_norm), binwidth =1)

##output
 crpp = merge(crpp,wi[,c("sim","wi")],by= "sim")
x=mean(crpp$yd_hrec)
y=mean(crpp$wi)
z=median(crpp$yd_hrec)
a=median(crpp$wi)
act =df[["res.act"]]

# Analyser les activités



# Calcul economique



# -----------------------------------------

file="data/market_data/Market.csv"
vectData<-read.csv(file = file, head = TRUE, sep = " ")

# export
cropproduction=crpp[, c("sim","code_soiltype","code_crop","id_variety","id_itk","yd","wi")]
colnames(cropproduction)  = c("id","code_soiltype","code_crop","ref_variety","ref_itk","yd","wi")
utils_dbexport( data = cropproduction , schema= "expertknowledge", dbname=db[["dbname"]], user=db[["user"]],password=db[["password"]])

# summary statistics
summary_var = ddply(crpp,.(index), summarize, yd_m = mean(yd),yd_sd=sd(yd), wi_m = mean(wi),wi_sd=sd(wi) )

######################
## data synthesis: activity
#######################
act =df[["res.act"]]

# en sortie une list:   

# Make graph of operation per itk


# export climate
library(RPostgreSQL)
drv <- dbDriver("PostgreSQL")
con <- dbConnect(drv, dbname="cas1", user="dury", password="viefolie")
dbListTables(con)
dbGetQuery(con, "SET search_path TO systemknowledge")
dbWriteTable(con,"climate",climate, row.names=FALSE)



#####################
## Final dataframe to be load in the dbCrash
#######################
systemknowledge.croproduction =data.frame(code_crop=character(0),ref_variety=numeric(0), ref_itk=numeric(0),  code_soiltype=numeric(0), yd_mn=numeric(0))

