/**
 * @file
 * @author RECORD Team,
 *   http://record.toulouse.inra.fr ,
 *   Applied Mathematics and Informatics Research Division &
 *   Environment and Agronomy Research Division,
 *   French National Institute for Agricultural Research
 * @version $Id: $
 */

/*
 * Copyright (C) 2010 - INRA
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#include <vle/devs/Dynamics.hpp>
#include <vle/utils.hpp>
#include <vle/devs.hpp>
#include <vle/vpz.hpp>
#include <boost/numeric/conversion/cast.hpp>

namespace vd = vle::devs;
namespace vv = vle::value;
namespace vz = vle::vpz;

namespace crash {
		/**
		 * @brief Modèle d'observation des Opérations
		 *
		 * D'un point de vue général, c'est un modèle d'observation
		 * d'évènements de type variables des Equations aux différences.
		 * A chaque fois qu'il reçoit un bag d'évènements, il les concatène,
		 * tous les évènements, dans une chaine de caractère.
		 * L'état du modèle est donc l'information composée des derniers
		 * évènements reçu.
		 *
		 * Le port d'observation à utiliser est "Opérations"
		 *
		 * @remarks N'importe quel nom de port peut-être utilisé en entrée.
		 * @remarks Dans le cadre de MOuSTICS, le modèle sert à observer le déroulement
		 * des opérations techniques.
		 * @remarks Utilisable en particulier avec une Vue de type évènementiel.
		 */
		class Observator: public vd::Dynamics
		{

				public:
						Observator(const vd::DynamicsInit& mdl,
										const vd::InitEventList& evts)
								: vd::Dynamics(mdl, evts), mTime(vd::Time::infinity)
						{};

						virtual ~Observator() { }

						virtual vd::Time init(const vd::Time& /*time*/)
						{
								return vd::Time::infinity;
						};

						vd::Time timeAdvance() const
						{
								return vd::Time::infinity;
						};

						void externalTransition(
										const vd::ExternalEventList& events,
										const vd::Time& time)
						{
								// pour les plugins qui n'accepte qu'une seule date par ligne

								if (time != mTime) {
										mOperations.clear();
										mTime = time;
								}

								for (vd::ExternalEventList::const_iterator it = events.begin();
												it != events.end(); ++it) {
										std::string type = "none";

										if((*it)->getStringAttributeValue("name")=="Sowing"){
												type = "densite";
										}

										if((*it)->getStringAttributeValue("name")=="FertilizingN"){
												type = "dose N";
										}

										if((*it)->getStringAttributeValue("name")=="Harvesting"){
												type = "done";
										}

										if((*it)->getStringAttributeValue("name")=="Irrigating"){
												type = "dose water";
										}

										mOperations += str(boost::format(" %d;%s;%s;%d |")
														% (*it)->getIntegerAttributeValue("landunit")
														% (*it)->getStringAttributeValue("name")
														% type
														% (*it)->getDoubleAttributeValue("value"));
								}
						};

						vv::Value* observation(const vd::ObservationEvent& event) const
						{
								if (event.onPort("Operations")) return buildString(mOperations);

								return 0;
						}

				protected:

						vd::Time mTime;

						std::string mOperations;
		};

} // namespace Moderato

DECLARE_DYNAMICS(crash::Observator)
