/**
 * @file
 * @author RECORD Team,
 *   http://record.toulouse.inra.fr ,
 *   Applied Mathematics and Informatics Research Division &
 *   Environment and Agronomy Research Division,
 *   French National Institute for Agricultural Research
 * @version $Id: $
 */

/*
 * Copyright (C) 2011 - INRA
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#include <vle/extension/fsa/Statechart.hpp>
#include <vle/extension/DifferenceEquation.hpp>
#include <vle/utils/i18n.hpp>


using namespace std;
using boost::lexical_cast;
using boost::bad_lexical_cast;
namespace ve = vle::extension;
namespace va = vle::extension::fsa;

namespace vd = vle::devs;
namespace vv = vle::value;
namespace vu = vle::utils;

namespace crash
{
/**
 * @brief Harvesting operation
 *
 */

class AHarvesting: public va::Statechart
{
public:

    enum State { IDLE, HARVESTING};

    /**
     * @brief Charge le paramètre Densité de semis à partir du port de
     * condition expérimentale nomFichierHarvesting qui doit contenir le
     * nom relatif du fichier.
     *
     * Définie aussi le graphe d'état du modèle.
     */
    AHarvesting(const vd::DynamicsInit& atom,
           const vd::InitEventList& events)
        : va::Statechart(atom, events), mHarvesting(false)
    {

        // construction de l'automate à 2 états

        states(this) << IDLE << HARVESTING;

        transition(this, IDLE, HARVESTING)
            << guard(&AHarvesting::isStarted)
            << send(&AHarvesting::out);

        transition(this, HARVESTING, IDLE)
            << action(&AHarvesting::stop);

        eventInState(this, "Start", &AHarvesting::start) >> IDLE;

        initialState(IDLE);
    }

    virtual ~AHarvesting()
    {
    }

private:

    /**
     * @brief une méthode de type guarde qui assure la transition vers
     * l'état SOWING.
     */
    bool isStarted(const vd::Time& /*julianDay*/)
    {
        return mHarvesting;
    }

    /**
     * @brief une méthode de send qui réalise l'action sous forme de
     * l'envoi d'un évènement de Harvesting.
     *
     * Cette méthode envoie aussi un accusé de réception.
     */
    void out(const vd::Time& julianDay , vd::ExternalEventList& output) const
    {
	for (unsigned int i=0 ; i < mListLU.size(); i++ ){
	    std::string harvesting_name = str(boost::format("harvesting_%1%") % mListLU.at(i));
		vd::ExternalEvent* harvest = new vd::ExternalEvent(harvesting_name);
		harvest->putAttribute("name", new vv::String("Harvesting"));
	        harvest->putAttribute("value", new vv::Double(1.0));
		harvest->putAttribute("landunit", new vv::Integer(mListLU.at(i)));
		output.addEvent(harvest);
	    }
		//vd::ExternalEvent* order = new vd::ExternalEvent("ack");
            //order->putAttribute("name", new vv::String(mActivity));  // accusé de reception
		//order->putAttribute("value", new vv::String("done"));
		//output.addEvent(order);
	    cout<<"\nHARVESTING\n"<<endl;
    }

    /**
     * @brief une méthode de type action dans un état qui modifie
     * l'état du modèle quand il se trouve dans l'état IDLE.
     */
    void start(const vd::Time& /*julianDay*/, const vd::ExternalEvent* evt)
    {
        mHarvesting = true;
		mActivity = evt-> getStringAttributeValue("activity");
	mListLU.clear();
		for (unsigned int i=0 ; i < (evt-> getSetAttributeValue("lstlu")).size(); i++ ){
		    mListLU.push_back((evt-> getSetAttributeValue("lstlu")).getInt(i));
		}
    }

    /**
     * @brief une méthode de type "action de transition" qui modifie
     * l'état du modèle lors de la transition SOWING->SOWED
     */
    void stop(const vd::Time& /*julianDay*/)
    {
        mHarvesting = false;
    }

    /**
     * @brief observation du port Operation
     */
    virtual vv::Value* observation(const vd::ObservationEvent & event ) const
    {
        if ( event.onPort ("Operation")) {
            if (mHarvesting) {
                return vv::String::create("Harvesting");
            } else {
                return vv::String::create("");
            }
        }
        return va::Statechart::observation(event);
    }

    // Variables d'états
    bool mHarvesting;

    // Paramètres
    std::string mActivity;
    vector<int> mListLU;

};

} //namespace Ressource

DECLARE_DYNAMICS(crash::AHarvesting)
