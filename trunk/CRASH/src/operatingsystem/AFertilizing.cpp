/**
 * @file
 * @author RECORD Team,
 *   http://record.toulouse.inra.fr ,
 *   Applied Mathematics and Informatics Research Division &
 *   Environment and Agronomy Research Division,
 *   French National Institute for Agricultural Research
 * @version $Id: $
 */

/*
 * Copyright (C) 2011 - INRA
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#include <vle/extension/fsa/Statechart.hpp>
#include <vle/extension/DifferenceEquation.hpp>
#include <vle/utils/i18n.hpp>


using namespace std;
using boost::lexical_cast;
using boost::bad_lexical_cast;
namespace ve = vle::extension;
namespace va = vle::extension::fsa;

namespace vd = vle::devs;
namespace vv = vle::value;
namespace vu = vle::utils;

namespace crash
{
/**
 * @brief N fertilizing operation
 *
 */

class AFertilizing: public va::Statechart
{
public:

    enum State { IDLE, FERTILIZING};

    /**
     * @brief Charge le paramètre Densité de semis à partir du port de
     * condition expérimentale nomFichierSowing qui doit contenir le
     * nom relatif du fichier.
     *
     * Définie aussi le graphe d'état du modèle.
     */
    AFertilizing(const vd::DynamicsInit& atom,
           const vd::InitEventList& events)
        : va::Statechart(atom, events), mFertilizing(false)
    {

        // construction de l'automate à 2 états

        states(this) << IDLE << FERTILIZING;

        transition(this, IDLE, FERTILIZING)
            << guard(&AFertilizing::isStarted)
            << send(&AFertilizing::out);

        transition(this, FERTILIZING, IDLE)
            << action(&AFertilizing::stop);

        eventInState(this, "Start", &AFertilizing::start) >> IDLE;

        initialState(IDLE);
    }

    virtual ~AFertilizing()
    {
    }

private:

    /**
     * @brief une méthode de type guarde qui assure la transition vers
     * l'état SOWING.
     */
    bool isStarted(const vd::Time& /*julianDay*/)
    {
        return mFertilizing;
    }

    /**
     * @brief une méthode de send qui réalise l'action sous forme de
     * l'envoi d'un évènement de Sowing.
     *
     * Cette méthode envoie aussi un accusé de réception.
     */
    void out(const vd::Time& julianDay , vd::ExternalEventList& output) const
    {
	for (unsigned int i=0 ; i < mListLU.size(); i++ ){
	    std::string fertilizing_name = str(boost::format("fertilizing_%1%") % mListLU.at(i));
		vd::ExternalEvent* fert = new vd::ExternalEvent(fertilizing_name);
		fert->putAttribute("name", new vv::String("FertilizingN"));
		fert->putAttribute("value", new vv::Double(mDose));
		fert->putAttribute("landunit", new vv::Integer(mListLU.at(i)));
		output.addEvent(fert);
	    }
		//vd::ExternalEvent* order = new vd::ExternalEvent("ack");
            //order->putAttribute("name", new vv::String(mActivity));  // accusé de reception
		//order->putAttribute("value", new vv::String("done"));
		//output.addEvent(order);
	    cout<<"\nFERTILIZING, dose: "<<mDose<<endl;
    }

    /**
     * @brief une méthode de type action dans un état qui modifie
     * l'état du modèle quand il se trouve dans l'état IDLE.
     */
    void start(const vd::Time& /*julianDay*/, const vd::ExternalEvent* evt)
    {
        mFertilizing = true;
        mActivity = evt-> getStringAttributeValue("activity");
	mDose = evt-> getDoubleAttributeValue("dose");
	mListLU.clear();
		for (unsigned int i=0 ; i < (evt-> getSetAttributeValue("lstlu")).size(); i++ ){
		    mListLU.push_back((evt-> getSetAttributeValue("lstlu")).getInt(i));
		}
    }

    /**
     * @brief une méthode de type "action de transition" qui modifie
     * l'état du modèle lors de la transition SOWING->SOWED
     */
    void stop(const vd::Time& /*julianDay*/)
    {
        mFertilizing = false;
    }

    /**
     * @brief observation du port Operation
     */
    virtual vv::Value* observation(const vd::ObservationEvent & event ) const
    {
        if ( event.onPort ("Operation")) {
            if (mFertilizing) {
                return vv::String::create("Fertilizing N");
            } else {
                return vv::String::create("");
            }
        }
        return va::Statechart::observation(event);
    }

    // Variables d'états
    bool mFertilizing;

    // Paramètres
    double mDose;
    std::string mActivity;
    vector<int> mListLU;

};

} //namespace Ressource

DECLARE_DYNAMICS(crash::AFertilizing)
