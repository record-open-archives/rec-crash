/**
 * @file
 * @author RECORD Team,
 *   http://record.toulouse.inra.fr ,
 *   Applied Mathematics and Informatics Research Division &
 *   Environment and Agronomy Research Division,
 *   French National Institute for Agricultural Research
 * @version $Id: $
 */


/*
 * Copyright (C) 2009 - INRA
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */


#include <iostream>
#include <fstream>
#include <string>
#include "boost/tuple/tuple.hpp"
#include <boost/numeric/conversion/cast.hpp>

// VLE
#include <vle/devs/Executive.hpp>
#include <vle/utils.hpp>
#include <vle/devs.hpp>
#include <vle/vpz.hpp>

//Crash file
#include <utils/CrashDBS.hpp>
using boost::lexical_cast;
using boost::bad_lexical_cast;

using namespace boost;
using namespace std;

namespace vd = vle::devs;
namespace vv = vle::value;
namespace vz = vle::vpz;
namespace vu = vle::utils;

namespace crash {

/**
 * @brief Un modèle d'instanciation d'une exploitation en fonction
 * d'un parcellaire.
 *
 * Le parcellaire est instancié à partir d'une table postgreql "db_Farmland" 
 * L'assolement est instancié à partir d'une table postgreql "croppingplan" 
 */
    class ModelManager: public vd::Executive

    {
    public:
	ModelManager(const vd::ExecutiveInit& mdl,
			const vd::InitEventList& events) :
	    vd::Executive(mdl, events),
	    nb_LandUnit(0)
	{   
	    /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	     * Simulation tools
	     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
	    bool toolSimulation = vle::value::toBoolean(events.get("simulation"));
       
	    if(toolSimulation == TRUE) {
		cout << "\n----------------------------------------------"<< endl;
		cout << "\tExecutive: db_Farmland instanciation" << endl;
		cout << "----------------------------------------------\n"<< endl;
		CrashDBS cdb(events);

		//Getdb_Farmland
		PGresult* db_Farmland = cdb.QueryDB("SELECT id_landunit as id_lu, code_soiltype as code_st from systemknowledge.farmland");
		nb_LandUnit = PQntuples(db_Farmland);
		cout << "The farm has: "<<endl;
		cout <<"\t" << nb_LandUnit << " land unit(s) " << endl;
		cout <<"\t" << nb_LandUnit << " STICS crop model will run" << endl;

		//      Getdb_CroppingPlan       
		PGresult* db_CroppingPlan = cdb.QueryDB("SELECT DISTINCT id_landunit, id_plotunit, crop.code_crop, crop.type_crop, cropplan.year_h, crop.start_sowingdate as st_sdate, crop.end_harvestingdate as ed_hdate, cropplan.code_itk as code_itk FROM systemknowledge.plotunit, systemknowledge.landunit, systemknowledge.cropplan , expertknowledge.crop  WHERE intersects(plotunit.the_geom, pointonsurface(landunit.the_geom)) AND  ref_plotunit=id_plotunit AND crop.code_crop=cropplan.code_crop ORDER BY id_landunit, id_plotunit, year_h"); 
		nb_CroppingPlan =  PQntuples(db_CroppingPlan);

		cout << "\nThe initial cropping plan concerns: "<< endl;
		cout <<"\t" << nb_CroppingPlan/nb_LandUnit << " cropping season(s)" << endl;

		map<int,map<int, vector<string> > > mCropPlan;
		for(int i=0; i<nb_CroppingPlan; i++){
		    int id_lu = lexical_cast<int>(PQgetvalue(db_CroppingPlan, i, PQfnumber(db_CroppingPlan, "id_landunit")));

		    // CropInSequence
		    vector<string> mCropInSeq;
		    mCropInSeq.clear();
		    mCropInSeq.push_back(lexical_cast<string>(PQgetvalue(db_CroppingPlan, i, PQfnumber(db_CroppingPlan, "code_crop"))));
		    mCropInSeq.push_back(lexical_cast<string>(PQgetvalue(db_CroppingPlan, i, PQfnumber(db_CroppingPlan, "type_crop"))));
		    mCropInSeq.push_back(lexical_cast<string>(PQgetvalue(db_CroppingPlan, i, PQfnumber(db_CroppingPlan, "st_sdate"))));
		    mCropInSeq.push_back(lexical_cast<string>(PQgetvalue(db_CroppingPlan, i, PQfnumber(db_CroppingPlan, "ed_hdate"))));
		    mCropInSeq.push_back(lexical_cast<string>(PQgetvalue(db_CroppingPlan, i, PQfnumber(db_CroppingPlan, "code_itk"))));

		    map<int,map<int, vector<string> > >::iterator it = mCropPlan.find(id_lu);
		    if (it!= mCropPlan.end()) {
			map<int, vector<string> > mCropSequence = mCropPlan[id_lu];
			int pos = 1 +  mCropSequence.size();
			mCropSequence[pos]=mCropInSeq;
			mCropPlan[id_lu]=mCropSequence;
		    } else {
			map<int, vector<string> > mCropSequence;
			int pos = 1 +  mCropSequence.size();
			mCropSequence[pos]=mCropInSeq;
			mCropPlan[id_lu]=mCropSequence;
		    }
		}

//              Display CroppingPlan
		cout << "\nInitial croppingPlan: " << endl;
		for (map<int,map<int, vector<string> > >::iterator iter = mCropPlan.begin(); iter != mCropPlan.end(); iter++) {
		    cout <<"\t" << "LandUnit: " << iter->first <<  endl;
		    cout << "\t\tCropPlan: "<< endl;
		    cout <<"\t\t\tPos" << "\tCrop " << "\ttype" << "\tstart" << "\tend" << "\tITK" <<  endl;

		    map<int, vector<string> > mCropSeq = iter->second;
		    for (map<int, vector<string> >::iterator iter = mCropSeq.begin(); iter != mCropSeq.end(); iter++) {
			cout <<"\t\t\t" << (iter->first) << "\t" << (iter->second).front() << "\t" << (iter->second)[1] << "\t" << (iter->second)[2] << "\t" << (iter->second)[3] << "\t" << (iter->second)[4] <<  endl;
		    }
		}

		/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
		 * ClassLandUnit
		 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
		vz::Conditions& cond_l = conditions();
		vz::Condition& cond_LU = cond_l.get("cond_rotation");

		for(int i=0; i< nb_LandUnit; i++) {	     
		    //  Set id_LandUnit
		    std::string id_lu = lexical_cast<string>(PQgetvalue(db_Farmland, i, PQfnumber(db_Farmland, "id_lu")));
		    cond_LU.del("id_LandUnit");
		    cond_LU.addValueToPort("id_LandUnit",vv::String::create(id_lu));
	
		    //  Set file folder for stics
		    std::string code_st = PQgetvalue(db_Farmland, i,PQfnumber(db_Farmland, "code_st"));
			
		    vv::Set& rotation(cond_LU.getSetValues("rotation").getSet(0));
		    rotation.clear();
		    map<int, vector<string> > mCropSeq = mCropPlan[lexical_cast<int>(id_lu)];
		    int pos=0;

		    for (map<int, vector<string> >::iterator iter = mCropSeq.begin(); iter != mCropSeq.end(); iter++) {             
			rotation.addSet();
			vv::Set& mCropInSeq(rotation.getSet(pos));
			std::string code_crop =(iter->second)[0];
			std::string code_itk =(iter->second)[4];
			std::string crop_stics;
 			if ((iter->second)[1]=="Winter"){
			    crop_stics = "stics_data/default_winter";
			}else{
			    crop_stics = "stics_data/default_summer";
			}	
			std::string fplt = str(boost::format("stics_data/crop/%1%1.plt") % code_crop );
			std::string fsol = str(boost::format("stics_data/soil/%1%.sol") % code_st);
			std::string ftec = str(boost::format("stics_data/itk/%1%.tec") % code_itk);

			mCropInSeq.addString(crop_stics);
			mCropInSeq.addString(fplt);
			mCropInSeq.addString(fsol);
			mCropInSeq.addString(ftec);
			mCropInSeq.addString((iter->second)[2]);   //date_start
			mCropInSeq.addString((iter->second)[3]);  //date_end
			cout<<(iter->second)[3]<<endl;
			mCropInSeq.addString((iter->second)[1]);  //Type

			pos++;
		    }
     
		    // Add port to the agent
		    addInputPort("CrashAgent", "Lai_" + id_lu);
		    addInputPort("CrashAgent", "H2Orec_" + id_lu);
		    addInputPort("CrashAgent", "INN_" + id_lu);
		    addInputPort("CrashAgent", "Swfac_" + id_lu);
		    addInputPort("CrashAgent", "Mafruit_" + id_lu);
		    addInputPort("CrashAgent", "VegetativeStage_" + id_lu);
		    addInputPort("CrashAgent", "AvgHur_" + id_lu);
		    addInputPort("CrashAgent", "SAWC_" + id_lu);
		    addInputPort("CrashAgent", "MaxSAWC_" + id_lu);
		    addInputPort("CrashAgent", "Airg_" + id_lu);

		    //  Create LandUnit Atomic Model
		    createModelFromClass("classLandUnit", "LandUnit_" + id_lu);
     
		    // Add connection between the LandUnit and the CrashAgent Atomic Models
		    addConnection("LandUnit_" + id_lu, "Lai", "CrashAgent", "Lai_" + id_lu);
		    addConnection("LandUnit_" + id_lu, "H2Orec", "CrashAgent", "H2Orec_" + id_lu);
		    addConnection("LandUnit_" + id_lu, "INN", "CrashAgent", "INN_" + id_lu);
		    addConnection("LandUnit_" + id_lu, "Swfac", "CrashAgent", "Swfac_" + id_lu);
		    addConnection("LandUnit_" + id_lu, "Mafruit", "CrashAgent", "Mafruit_" + id_lu);
		    addConnection("LandUnit_" + id_lu, "VegetativeStage", "CrashAgent", "VegetativeStage_" + id_lu);
		    addConnection("LandUnit_" + id_lu, "AvgHur", "CrashAgent", "AvgHur_" + id_lu);
		    addConnection("LandUnit_" + id_lu, "SAWC", "CrashAgent", "SAWC_" + id_lu);
		    addConnection("LandUnit_" + id_lu, "MaxSAWC", "CrashAgent", "MaxSAWC_" + id_lu);
		    addConnection("LandUnit_" + id_lu, "Airg", "CrashAgent", "Airg_" + id_lu);

		    addConnection("meteo", "Tmin","LandUnit_" + id_lu, "Tmin");
		    addConnection("meteo", "Tmax","LandUnit_" + id_lu, "Tmax");
		    addConnection("meteo", "Rain","LandUnit_" + id_lu, "Rain");
		    addConnection("meteo", "RG", "LandUnit_" + id_lu, "RG");
		    addConnection("meteo", "ETP", "LandUnit_" + id_lu, "ETP");
		    addConnection("meteo", "Co2", "LandUnit_" + id_lu, "Co2");
		}
	
		// Connection between the CrashAgent and the model meteo
		addInputPort("CrashAgent", "Rain");
		addConnection("meteo", "Rain", "CrashAgent", "Rain");
		addInputPort("CrashAgent", "ETP");
		addConnection("meteo", "ETP", "CrashAgent", "ETP");


		/*********************************************************************************************************
		 *
		 * The model Market
		 *
		 *********************************************************************************************************/	    

		PGresult* db_crop = cdb.QueryDB("SELECT * from expertknowledge.crop");
		nb_crop = PQntuples(db_crop);

		// Dynamique connection between the CrashAgent and the model Market
		for(int i=0; i <nb_crop; i++) {
		    string _codeCrop = lexical_cast<string>(PQgetvalue(db_crop, i, PQfnumber(db_crop, "code_crop")));
		    addInputPort("CrashAgent", _codeCrop);
		    addConnection("Market", _codeCrop, "CrashAgent", _codeCrop);
		}

		cout << "--------------------STICS run-------------------\n"<< endl;
	    } else {
		cout << "\n---CRASH TOOL: -------------------------------"<< endl;
		cout << "\t Crash simulation tool is not activated" << endl;
		cout << "----------------------------------------------\n"<< endl;
	    }
	}

	/**
	 * @brief Default destructor
	 */
	virtual ~ModelManager() { }

	int nb_LandUnit; //number of LandUnit
	int nb_PlotUnit; //number of PlotUnit
	int nb_CroppingPlan; //number of Cropping plan
	int nb_crop; //number of crop
    };
} // namespace Crash

DECLARE_EXECUTIVE(crash::ModelManager);

