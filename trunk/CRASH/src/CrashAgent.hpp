/**
 * @file
 * @author RECORD Team,
 *   http://record.toulouse.inra.fr ,
 *   Applied Mathematics and Informatics Research Division &
 *   Environment and Agronomy Research Division,
 *   French National Institute for Agricultural Research
 * @version $Id: $
 */

/*
 * Copyright (C) 2010 - INRA
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
#ifndef CRASHAGENT_H
#define CRASHAGENT_H

#include <sstream>
#include <numeric>
#include <iterator>
#include <boost/algorithm/string.hpp>

#include <iostream>
#include <fstream>
#include <ostream>

//VLE
#include <vle/extension/Decision.hpp>
#include <vle/utils.hpp>
#include <vle/utils/Tools.hpp>
#include <vle/utils/DateTime.hpp>
#include <vle/devs.hpp>

//Crash utils
#include <utils/Utils.hpp>

//Crash files
#include <belief/structuralknowledge/expertknowledge/ExpertKnowledge.hpp>

#include <belief/structuralknowledge/systemknowledge/farmland/FarmLand.hpp>
#include <belief/structuralknowledge/systemknowledge/cropproductionsystems/CPS.hpp>
#include <belief/structuralknowledge/systemknowledge/offfarm/Weather.hpp>
#include <belief/structuralknowledge/expertknowledge/CropSuccessions.hpp>
#include <belief/structuralknowledge/systemknowledge/offfarm/Market.hpp>
#include <belief/structuralknowledge/systemknowledge/cropproductionsystems/CroppingPlanCandidates.hpp>
#include <belief/structuralknowledge/systemknowledge/farmland/DoneOperation.hpp>

// Crash proceduralKW
#include <belief/proceduralknowledge/DecisionProfile.hpp>
#include <belief/proceduralknowledge/WCSPConstraintFactory.hpp>
#include <belief/proceduralknowledge/Filter.hpp>
//#include <belief/proceduralknowledge/CropSeqCandidates.hpp>

#include <utils/System.hpp>
#include <belief/proceduralknowledge/Filter.hpp>

//namespace
using namespace std;
using boost::lexical_cast;
using boost::bad_lexical_cast;
namespace ve = vle::extension::decision;
namespace vd = vle::devs;
namespace vu = vle::utils;
namespace vv = vle::value;

namespace crash {
  /**
   * @brief CrashAgent: Decision Agent of the Crash model.
   */
  class CrashAgent: public ve::Agent {

  public:

    /** @brief Default constructor */
    CrashAgent(const vd::DynamicsInit& mdl, const vd::InitEventList& evts);

    /** @brief Default destructor */
    virtual ~CrashAgent();

    bool isAlwaysFalse() const {
      return 0;
    }

  private:
    /** @brief file path for sowing decision parameters */
    string filepathSowing;
    /** @brief Object ExpertKnowledge */
    ExpertKnowledge mExpertKnowledge;
    /** @brief Object FarmLand */
    FarmLand mFarmLand;
    /** @brief Object CroppingPlan */
    CPS mCPS;
    /** @brief Object CroppingPlan */
    Weather mWeather;
    Filter mft;

  };
}  // namespace crash

#endif //CRASHAGENT_H
