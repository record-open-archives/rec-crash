#include <vle/extension/DifferenceEquation.hpp>
#include <vle/extension/DifferenceEquationDbg.hpp>
#include <vle/devs/DynamicsDbg.hpp>

namespace crash {

class DisCont : public vle::extension::DifferenceEquation::Multiple
{
public:
    DisCont(const vle::devs::DynamicsInit& init,
             const vle::devs::InitEventList& events) :
	vle::extension::DifferenceEquation::Multiple(init, events)
    {
        mMode = PORT;

        const vle::graph::ConnectionList& list =
            getModel().getOutputPortList();

        for (vle::graph::ConnectionList::const_iterator it =
                 list.begin(); it != list.end(); ++it) {
            mVars[it->first] = createVar(it->first);
        }
    }

    virtual ~DisCont() { }

    virtual void compute(const vle::devs::Time& /* time */)
    {
        for (std::map < std::string, Var >::iterator it = mVars.begin();
             it != mVars.end(); ++it) {
            it->second = 0;
        }
    }

private:
    std::map < std::string, Var > mVars;
};

} // namespace crash

DECLARE_NAMED_DYNAMICS(DisCont, crash::DisCont)
//DECLARE_NAMED_DYNAMICS_DBG(DisContDynamicsDbg, crash::DisCont)
//DECLARE_NAMED_DIFFERENCE_EQUATION_MULTIPLE_DBG(DisContDbg, crash::DisCont)
