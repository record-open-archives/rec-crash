#include <belief/proceduralknowledge/Itk.hpp>

namespace crash {
	Itk::Itk() :
		id_itk(0),
		code_crop("ZZ"),
		type_crop("NA"),
		pre_crop("NA"),
		suc_crop("NA")
	{}


	Itk::Itk(int _id,
			std::string _code_crop,
			std::string _type_crop,
			std::string _pre_crop,
			std::string _suc_crop,
			t_map_variety map_v,
			t_lst_string vect_ds) :
		id_itk(_id),
		code_crop(_code_crop),
		type_crop(_type_crop),
		pre_crop(_pre_crop),
		suc_crop(_suc_crop),
		map_variety(map_v),
		domain_soiltype(vect_ds)
	{}


	Itk::~Itk() {}

	int Itk::getId() {
		return id_itk;
	}
	int Itk::getId() const {
		return id_itk;
	}


	std::string Itk::getCodeCrop() {
		return code_crop;
	}
	std::string Itk::getCodeCrop() const {
		return code_crop;
	}

	std::string Itk::getTypeCrop() {
		return type_crop;
	}
	std::string Itk::getTypeCrop() const {
		return type_crop;
	}

	Itk::t_lst_string Itk::getDomainSoiltype() {
		return domain_soiltype;
	}
	Itk::t_lst_string Itk::getDomainSoiltype() const {
		return domain_soiltype;
	}


	Itk::t_lst_op & Itk::getLstOperation() {
		return lst_operation;
	}
	const Itk::t_lst_op & Itk::getLstOperation() const {
		return lst_operation;
	}

	CropOperation & Itk::getOperationByType(std::string _type) {
		for(unsigned int j=0; j < lst_operation.size(); j++) {
			if(lst_operation.at(j).getType()==_type){
				return lst_operation.at(j);
			}
		}
		throw std::runtime_error("ITK:: getOperationByType, operation not found");
	}

	std::string Itk::getStSow(std::string _var){
		t_map_variety::const_iterator it = map_variety.find(_var);
		if(it !=map_variety.end()){
			return  it->second.st_sow;
		}else{
			throw std::runtime_error("ITK:: setStSow, variety not found");
		}
	}
	std::string Itk::getStSow(std::string _var) const {
		t_map_variety::const_iterator it = map_variety.find(_var);
		if(it !=map_variety.end()){
			return it->second.st_sow;
		}else{
			throw std::runtime_error("ITK:: setStSow, variety not found");
		}
	}

	std::string Itk::getNdSow(std::string _var){
		t_map_variety::const_iterator it = map_variety.find(_var);
		if(it !=map_variety.end()){
			return  it->second.nd_sow;
		}else{
			throw std::runtime_error("ITK:: setNdSow, variety not found");
		}
	}
	std::string Itk::getNdSow(std::string _var) const {
		t_map_variety::const_iterator it = map_variety.find(_var);
		if(it !=map_variety.end()){
			return it->second.nd_sow;
		}else{
			throw std::runtime_error("ITK:: setNdSow, variety not found");
		}
	}

	std::string Itk::getStHrv(std::string _var){
		t_map_variety::const_iterator it = map_variety.find(_var);
		if(it !=map_variety.end()){
			return  it->second.st_hr;
		}else{
			throw std::runtime_error("ITK:: setStHrv, variety not found");
		}
	}
	std::string Itk::getStHrv(std::string _var) const {
		t_map_variety::const_iterator it = map_variety.find(_var);
		if(it !=map_variety.end()){
			return it->second.st_hr;
		}else{
			throw std::runtime_error("ITK:: setStHrv, variety not found");
		}
	}

	std::string Itk::getNdHrv(std::string _var){
		t_map_variety::const_iterator it = map_variety.find(_var);
		if(it !=map_variety.end()){
			return  it->second.nd_hr;
		}else{
			throw std::runtime_error("ITK:: setNdHr, variety not found");
		}
	}
	std::string Itk::getNdHrv(std::string _var) const {
		t_map_variety::const_iterator it = map_variety.find(_var);
		if(it !=map_variety.end()){
			return it->second.nd_hr;
		}else{
			throw std::runtime_error("ITK:: setNdHrv, variety not found");
		}
	}

	double Itk::getDensity(std::string _var) {
		t_map_variety::const_iterator it = map_variety.find(_var);
		if(it !=map_variety.end()){
			return it->second.density;
		}else{
			throw std::runtime_error("ITK:: setDensity, variety not found");
		}
	}
	double Itk::getDensity(std::string _var) const {
		t_map_variety::const_iterator it = map_variety.find(_var);
		if(it !=map_variety.end()){
			return it->second.density;
		}else{
			throw std::runtime_error("ITK:: setDensity, variety not found");
		}
	}

	void Itk::addCropOperation(CropOperation _operation) {
		lst_operation.push_back(_operation);
	}


	CropOperation::t_map_rules Itk::getRules(){
		CropOperation::t_map_rules map_all_rules;
		for(unsigned int j=0; j < lst_operation.size(); j++) {
			CropOperation::t_map_rules map_rule = lst_operation.at(j).getRules();
			map_all_rules.insert(map_rule.begin(),map_rule.end());
		}
		return map_all_rules;
	}

	CropOperation::t_map_rules Itk::getRules()const {
		CropOperation::t_map_rules map_all_rules;
		for(unsigned int j=0; j < lst_operation.size(); j++) {
			CropOperation::t_map_rules map_rule = lst_operation.at(j).getRules();
			map_all_rules.insert(map_rule.begin(),map_rule.end());
		}
		return map_all_rules;
	}

	void Itk::setYrInd(){
		int harv_month =  Utils::getInstance().getVectInt(getOperationByType("harvesting").getNd(), "/")[1];
cout<<"setYrInd"<<endl;
		for(unsigned int j=0; j < lst_operation.size(); j++) {
			if(type_crop=="Summer"){
				cout<<"Summer"<<endl;				
				lst_operation.at(j).setYrIndex(pair<int,int>(0,0));
			}
			if(type_crop=="Winter"){
				cout<<"Winter"<<endl;
				int st = Utils::getInstance().getVectInt(lst_operation.at(j).getSt(), "/")[1];
				int nd = Utils::getInstance().getVectInt(lst_operation.at(j).getNd(), "/")[1];
				int stind = 0;
				int ndind = 0;
				if(st<harv_month) stind=1;
				if(nd<=harv_month) ndind=1;
				cout<<"st "<<stind<<"  nd "<<ndind<<endl;
				lst_operation.at(j).setYrIndex(pair<int,int>(stind,ndind));
			}
		}
	}

} //namespace crash
