#include <belief/proceduralknowledge/ItkCrop.hpp>

using namespace std;

namespace crash {


  ItkCrop::ItkCrop() {}

  ItkCrop::ItkCrop(const int & _indexCrop, const std::string & _codeCrop,
		   const int & precedCrop,
		   const int & succedCrop,
		   const t_vectInt & domainVariety,
		   const t_vectInt & domainSoil) : IdCrop(_indexCrop),
						   codeCrop(_codeCrop),
						   preceding_crop(precedCrop),
						   succeding_crop(succedCrop) {
    domain_cropvariety = domainVariety;
    domain_soil = domainSoil;
  }


  ItkCrop::~ItkCrop() {}

  void ItkCrop::setIdCrop(const int & _indexCrop) {
    IdCrop = _indexCrop;
  }
  
  void ItkCrop::setCodeCrop(const string & _code) {
    codeCrop = _code;
  }
  
  void ItkCrop::setPrecedCrop(const int & precedCrop) {
    preceding_crop = precedCrop;
  }

  void ItkCrop::setSuccedCrop(const int & succedCrop) {
    succeding_crop = succedCrop;
  }

  int ItkCrop::getIdCrop() {
    return IdCrop;
  }

  int ItkCrop::getIdCrop() const {
    return IdCrop;
  }

  string ItkCrop::getCodeCrop() {
    return codeCrop;
  }

  string ItkCrop::getCodeCrop() const {
    return codeCrop;
  }

  int ItkCrop::getPrecedCrop() {
    return preceding_crop;
  }

  int ItkCrop::getPrecedCrop() const {
    return preceding_crop;
  }

  int ItkCrop::getSuccedCrop() {
    return succeding_crop;
  }

  int ItkCrop::getSuccedCrop() const {
    return succeding_crop;
  }

  int ItkCrop::getDomainVariety() {
    for(ItkCrop::size_type i=0; i != domain_cropvariety.size(); i++) {
      return domain_cropvariety.at(i);
    }
  }

 int ItkCrop::getDomainVariety() const {
    for(ItkCrop::size_type i=0; i != domain_cropvariety.size(); i++) {
      return domain_cropvariety.at(i);
    }
  }

  ItkCrop::t_vectOp ItkCrop::getVectOperation() {
    return cp_operation;
  }

  ItkCrop::t_vectOp ItkCrop::getVectOperation() const {
    return cp_operation;
  }

  void ItkCrop::addCropOperation(const t_vectOp & _operation) {
    cp_operation = _operation;
  }
 
  bool ItkCrop::concatenateRules(ofstream & _rulePredicates, const int & id_plot) {
    try {
      _rulePredicates << "'" << "\t\t ## Rules" <<  "'" << std::endl;
      _rulePredicates << "'" << "\t\t\t rules {"  << "\n'" << std::endl;
      ItkCrop::t_vectOp _operationItk = getVectOperation();
      ItkCrop::t_vectString temp;
      for(int j=0; j < _operationItk.size(); j++) {
	std::map < string, vector <string> > _mapRule;
	_mapRule = _operationItk.at(j).getMapRule();
	for(std::map < string, vector <string> >::const_iterator 
	      it = _mapRule.begin(); it != _mapRule.end(); ++it) {
	  //on associe à une règle un plot unit
	  std::string _rule = it->first;
	  _rule.append("_");
	  string idPlot = lexical_cast < string > (id_plot);
	  _rule.append(idPlot); 

	  vector < string >::const_iterator it = find(temp.begin(), temp.end(), _rule);
	  if(it == temp.end()) {
	    temp.push_back(_rule);
	    _operationItk.at(j).setRulePredicates(_rulePredicates, _rule, id_plot);
	  }
	}	
      }
      _rulePredicates << "'" << "\t\t\t } " << "\n'" << std::endl;
    }
    catch(ofstream::failure e ) {
      throw ArgError("ItkCrop: Unable to write the rules!");
    }
    return true;
  }

  bool ItkCrop::concatenateActivities(ofstream & _activities, const int & year,
				      const int & plotU) {
    try {
      _activities << "'" << "\t\t activities {" << "'" << std::endl;
      ItkCrop::t_vectOp _operationItk = getVectOperation();
      for(int j=0; j < _operationItk.size(); j++) {
	_operationItk.at(j).setActivities(_activities, year, plotU);
      }
      _activities << "'" << "\t\t }" << "'" << std::endl;
    }
    catch(ofstream::failure e) {
      throw ArgError("ItkCrop: Unable to write the activities");
    }
    return true;
  }

  bool ItkCrop::concatenatePreceedingConstraint(ofstream & pc, const int & id_plot,
						const int & year) {
    try {
      pc << "'" << "\t\t precedences {" << "'" << std::endl;
      ItkCrop::t_vectOp _operationItk = getVectOperation();
      for(int j=0; j < _operationItk.size(); j++) {
	_operationItk.at(j).setPreceedingConstraint(pc, id_plot, year);
      }
      pc << "'" << "\t\t }" << "'" << std::endl;

    }
    catch(ofstream::failure e) {
      throw ArgError("ItkCrop: Unable to write the preceeding constraint");
    }
    return true;

  }


  void ItkCrop::buildItk(const string & _charList, const int & year, const int & id_plot) {
    std::ifstream list(_charList.c_str());
    std::stringstream buff;
    buff << list.rdbuf();
    string buffer(buff.str());
    list.close();
    ofstream chaine;
    chaine.open(_charList.c_str(), ios::app);
    concatenateRules(chaine, id_plot);
    concatenateActivities(chaine, year, id_plot);
    concatenatePreceedingConstraint(chaine, id_plot, year);
    chaine << buffer;
    chaine.close();
  }
  
} //namespace crash
