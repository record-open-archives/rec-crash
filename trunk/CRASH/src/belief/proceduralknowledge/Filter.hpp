/**
 * @author Dragan AMENGA MBENGOUE
 * @date 20 avril 2011
 * @version 1.1
 */
#ifndef FILTER_HPP
#define FILTER_HPP

#include <vector>

#include <belief/structuralknowledge/systemknowledge/cropproductionsystems/CroppingPlanCandidate.hpp>
#include <belief/structuralknowledge/systemknowledge/cropproductionsystems/CroppingPlanCandidates.hpp>

#include <fstream>
#include <ostream>
#include <iostream>

using namespace std;

namespace crash {

  class Filter {

  public:

    /**
     * @brief Constructor default
     */
    Filter();

    virtual ~Filter();

    /**
     * @brief This function selected above stock solution to a file
     * @param _dirName the name of directory
     * @param _fileName the name of file
     * @param _nbSolution the solustion stocked
     * @param newFile is a boolean option
     * if(newFile == TRUE) {
     * open for writing with deletion of the open file
     * } else {
     * opening addition of data (write end of file)
     * }
     */
    void saveSolution(const std::string & _dirName, 
		      const std::string & _fileName,
		      const int & _nbSolution,
		      const bool & newFile);

    /**
     * @brief This function select one solution
     * @param indexSol index of the solution
     * @return the solution selected
     */
    std::string selectSolution(const int & indexSol);


  private:
 
  };

} //namespace crash

#endif //FILTER_HPP
