/*
 * @file record/pkgs/decision/planning/algos/
 *
 * This file is part of VLE, a framework for multi-modeling, simulation
 * and analysis of complex dynamical systems
 * http://www.vle-project.org
 *
 * Copyright (c) 2003-2007 Gauthier Quesnel <quesnel@users.sourceforge.net>
 * Copyright (c) 2003-2010 ULCO http://www.univ-littoral.fr
 * Copyright (c) 2007-2010 INRA http://www.inra.fr
 *
 * See the AUTHORS or Authors.txt file for copyright owners and contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <fstream>
#include <iostream>
#include <sstream>
#include <boost/tokenizer.hpp>
#include <boost/lexical_cast.hpp>
#include <vle/utils/Path.hpp>
#include <vle/utils/Exception.hpp>

#include <utils/System.hpp>
#include <belief/proceduralknowledge/WCSPConstraintFactory.hpp>

using vle::utils::ArgError;
using namespace std;
using namespace boost;

namespace crash {

    bool WCSPConstraintFactory::generateConstraintsFiles(
	const std::string& /*_cstFileDirName*/,
	const std::string& _cstFile,
	const std::string& _cstOption)
    {
	bool _state = false;
	ofstream  _cstFileStrm;
	_cstFileStrm.open (_cstFile.c_str(), ios::app);

        if(_cstOption =="historic") //Contraintes pourtant sur l'historique
	{
	    _state =  historicConstraints(_cstFileStrm);
	}
        else if(_cstOption =="tr") //Toutes les cultures ont une constrainte sur le délai de retour : la constante TR
	{
	    _state =  temporalCompatibilityOnSEQConstraints(_cstFileStrm);
	}
        else if(_cstOption =="kp_croppingblock") //Succession temporelle des cultures : des solutions pour les quelles l'effet précédent le KP est maximale
	{
	    _state =  successionKpCstr(_cstFileStrm);
	}
        else if(_cstOption =="crop_max_area") //L'exploitation dispose de capacités fixes de ressources.
	{
	    _state = cropMaxAllocationCstrFL(_cstFileStrm);
	}
        else if(_cstOption =="eq_managementunit") // unité de gestion gérée de façon identique
	{
	    _state = eq_managementunitCstr(_cstFileStrm);
	}
        else if(_cstOption =="rotation") // unité de gestion gérée de façon identique
	{
	    _state = ROTATIONConstraints(_cstFileStrm);
	}
        else if(_cstOption =="rotation_croppingblock") // unité de gestion gérée de façon identique
	{
	    _state = MultiROTATIONConstraints(_cstFileStrm);
	}
        else if(_cstOption =="sameSp_croppingblock") //même repartition des culture dans le temps et l'espace sur les cropping block
	{
	    _state = sameSp_croppingblock(_cstFileStrm);
	}
	else if(_cstOption =="sameTp_croppingblock") //même repartition des culture dans le temps et le temps sur les cropping block
	{
	    _state = sameTp_croppingblock(_cstFileStrm);
	}
	else if(_cstOption =="sameSp_grouping") //regroupement des allocations de cultures sur les landunits par block de cultures
	{
	    _state = sameSp_groupingCstr(_cstFileStrm);
	}
        else
	{
	    cout << "\n /!\\ WCSPConstraintFactory: Cannot find the option  " << _cstOption << endl;
	    _state =  false;
	}
	_cstFileStrm.close();
	return _state;
    }

    // HISTORIC
    bool WCSPConstraintFactory::historicConstraints
    (ofstream&  _cstFileStrm)
    {
      /*
	int nb_LU 		=LandUnits::getInstance().size();
	int nb_var 		= (nb_LU * getHistoric());

	struct VD vardomaine;

	for(int id = 0; id < nb_var; id++){
	    int luid = ((id % nb_LU) +1);
	    int sz_histo = CropSeqCandidates::getInstance().getById(luid).getSizeHist_lst();
	    if((sz_histo >=  getHistoric())){
		struct D domaine;
		struct UVC valuecost;
		int value =  (CropSeqCandidates::getInstance().getById(luid).at_hist(sz_histo - getHistoric() + (id/nb_LU))-1);///
		domaine(valuecost(value,0));
		vardomaine(id, domaine());
	    }else
		return false;
	}
	unarycost(_cstFileStrm, vardomaine(), getInfty());
      */
	return true;
    }


    //REDUCTION DE DOMAINE
    bool WCSPConstraintFactory::spatialCompatibilityConstraints(ofstream&  _cstFileStrm)
    {
	unsigned int nb_LU 		=LandUnits::getInstance().size();
	unsigned int nb_var 		= (nb_LU * getHistoric());
    	unsigned  int nb_CR 		= Crops::getInstance().size();

    	struct VD vardomaine;

    	for(unsigned  int luid = 0; luid < nb_LU; luid++){
	    std::vector<int> _croplst = CropDomains::getInstance().getById(luid+1).getCropDomain();

	    std::vector<int>::iterator  it;

	    for(unsigned  int idvar = 0; idvar < nb_var; idvar++)
	    {
		if((idvar % nb_LU)==luid){
		    struct D domaine;
		    struct UVC valuecost;
		    for(unsigned  int idC = 0; idC < nb_CR; idC++)
		    {
			it  = std::find(_croplst.begin(), _croplst.end(), idC+1);
			if(it == _croplst.end()){
			    domaine(valuecost(idC,getInfty()));
			}
		    }
		    vardomaine(idvar, domaine());
		}
	    }
    	}
    	unarycost(_cstFileStrm, vardomaine(), 0);
    	return true;
    }


    //TR sur horizon futur
    bool WCSPConstraintFactory::temporalCompatibilityOnSEQConstraints
    (ofstream&  _cstFileStrm)
    {
	unsigned int nb_LU 		= getNbLU();
	Crops::t_crop_lst crop_lst = Crops::getInstance().getCropTr(1);
	unsigned int nb_CR 		= crop_lst.size();

	for(unsigned int i = 0; i < nb_CR; i++)
	{

	    int idC = crop_lst.at(i).getId();
	    int tr = crop_lst.at(i).getRtRot();

	    for(unsigned int luid = 0; luid < nb_LU; luid++){
		struct D domaine;
		struct UVC value;
		struct VD vardomaine;

		std::vector<int> _croplst = CropDomains::getInstance().getById(luid+1).getCropDomain();
		std::vector<int>::iterator  it = std::find (_croplst.begin(), _croplst.end(), idC);

		if(it != _croplst.end())
		{
		    for(std::vector<int>::iterator it2 =_croplst.begin();
			it2 !=_croplst.end(); it2++)
			domaine(value((*it2-1), 0));
		    for(int h=0; h < getFullHorizon(); h++)
			vardomaine((luid+ h*nb_LU), domaine());
		    sregular_w_f_s(_cstFileStrm, vardomaine(), tr, idC-1, getHistoric(), getInfty());
		} else
		{
		    throw std::runtime_error("/!\\ WCSPConstraintFactory::temporalCompatibilityOnSEQConstraints");
		}
	    }
	}
	return true;
    }


    //ROTATION
    bool WCSPConstraintFactory::ROTATIONConstraints (ofstream&  _cstFileStrm)
    {
	unsigned int nb_LU 		= getNbLU();
	Crops::t_crop_lst crop_lst = Crops::getInstance().getCropTr(1);
	unsigned int nb_CR 		= crop_lst.size();

	for(unsigned int i = 0; i < nb_CR; i++)
	{
	    int idC = crop_lst.at(i).getId();
	    int tr = crop_lst.at(i).getRtRot();

	    for(unsigned int luid = 0; luid < nb_LU; luid++)
	    {
		struct D domaine;
		struct UVC value;
		struct VD vardomaine;

		std::vector<int> _croplst = CropDomains::getInstance().getById(luid+1).getCropDomain();
		std::vector<int>::iterator  it = std::find (_croplst.begin(), _croplst.end(), idC);

		if(it != _croplst.end())
		{

		    for(std::vector<int>::iterator it2 =_croplst.begin(); it2 !=_croplst.end(); it2++)
		    {
			domaine(value((*it2-1), 0));
		    }
		    for(int h = getHistoric(); h < getFullHorizon(); h++)
		    {
			vardomaine((luid+ h*nb_LU), domaine());
		    }
		    	sregular(_cstFileStrm, vardomaine(),tr, idC-1,  getInfty(), 1);
		}
	    }
	}
	return true;
    }

//Rotation CROPPINGBLOCK
    bool WCSPConstraintFactory::MultiROTATIONConstraints (ofstream&  _cstFileStrm)
    {
    	int nb_LU 		= getNbLU();
	int nb_var 		= getNbVar();
	Crops::t_crop_lst crop_lst = Crops::getInstance().getCropTr(1);
	unsigned int nb_CR 		= crop_lst.size();
    	DecisionProfile::t_multiRotationMap multiRotationMap = DecisionProfile::getInstance().getMultiRotation();

    	// boucle sur t_multiRotationMap
    	for(DecisionProfile::t_multiRotationMap::const_iterator it = multiRotationMap.begin();
    	    it !=  multiRotationMap.end();++it)
    	{
    	    std::vector<pair<int,int> > pair_lst = it->second;
	     cout<<"\t"<<it->first<<": "<< pair_lst.size() <<endl;
    	    //boucle sur les paires (id, rotLength)
    	    for(unsigned int i = 0 ; i<pair_lst.size();++i)
    	    {
    		int _id_mu=pair_lst.at(i).first;
    		int _rotLength = pair_lst.at(i).second;
    		vector<int> lu_lst;
    		if (it->first=="croppingblock")
    		{
    		    lu_lst= CroppingBlocks::getInstance().getById(_id_mu).getLuIdLst();
    		}else{
    		    throw ArgError("/!\\ WCSPConstraintFactory:: Cannot find the the management unit");
    		}

		for(unsigned int i = 0; i < nb_CR; i++)
		{
		    int idC = crop_lst.at(i).getId();
		    int tr = crop_lst.at(i).getRtRot();
		    for(unsigned int luid = 0; luid < lu_lst.size(); luid++)
		    {
			struct D domaine;
			struct UVC value;
			struct VD vardomaine;
			std::vector<int> _croplst = CropDomains::getInstance().getById( lu_lst.at(luid)).getCropDomain();
			std::vector<int>::iterator  it = std::find (_croplst.begin(), _croplst.end(), idC);
			if(it != _croplst.end())
			{
			    // definition du domaine
			    for(std::vector<int>::iterator it2 =_croplst.begin(); it2 !=_croplst.end(); it2++)
			    {
				domaine(value((*it2-1), 0));
			    }

			    for(int h = getHistoric(); h < getHistoric() + _rotLength ; h++)
			    {
                            // contrainte de cylce
				vardomaine(( lu_lst.at(luid)-1+ h*nb_LU), domaine());
                            // contrainte d'égalité
				struct LST var;
				int id = lu_lst.at(luid)-1 + h *nb_LU;
				int idscc = lu_lst.at(luid)-1 + (h+ _rotLength) *nb_LU;
				if(idscc < nb_var)
				{
				    var(id);
				    var(idscc);
				    equal(_cstFileStrm, var(), domaine(), getInfty());
				}
			    }
			    sregular(_cstFileStrm, vardomaine(),tr, idC-1,  getInfty(), 1);
			}
		    }
		}
	    }
	}
	return true;
    }


    //KP
    bool WCSPConstraintFactory::successionKpCstr(ofstream&  _cstFileStrm)
    {
    	int nb_LU 		= getNbLU();
	int nb_var 		= getNbVar();
    	int nb_KP		= CropSuccessions::getInstance().size();
    	DecisionProfile::t_kpMap kpMap = DecisionProfile::getInstance().getKpCroppingblock();

    	// boucle sur kpMap
    	for(DecisionProfile::t_kpMap::const_iterator it =  kpMap.begin();
    	    it !=  kpMap.end();++it)
    	{
    	    std::vector<pair<int,double> > pair_lst = it->second;
	     cout<<"\t"<<it->first<<": "<< pair_lst.size() <<endl;
    	    //boucle sur les paires (id, kpamx)
    	    for(unsigned int i = 0 ; i<pair_lst.size();++i)
    	    {
    		int _id_mu=pair_lst.at(i).first;
    		double _kpmax = pair_lst.at(i).second;
    		vector<int> lu_lst;
    		if (it->first=="croppingblock")
    		{
    		    lu_lst= CroppingBlocks::getInstance().getById(_id_mu).getLuIdLst();
    		}else{
    		    throw ArgError("/!\\ WCSPConstraintFactory:: Cannot find the the management unit");
    		}
    		// boucle sur les les lu du block de culture
    		for(unsigned int luid = 0; luid < lu_lst.size(); luid++)
    		{
    		    // boucle sur l'historic
    		    for(int h = getHistoric(); h < getFullHorizon()-1; h++)
    		    {
    			struct MVCS mvcs;
    			struct LST var;
    			struct MVC mvc;
    			int dec= h*nb_LU;
    			int id = lu_lst.at(luid)-1 + dec;
			if((id + nb_LU)< nb_var)
			{
			    var(id ); // lu at h
			    var(id + nb_LU) ; // lu at h+1

			    for(int idKP = 0; idKP < nb_KP; idKP++)
			    {
				int crop_id = CropSuccessions::getInstance().at(idKP).getRef_crop();
				int prec_id =  CropSuccessions::getInstance().at(idKP).getRef_prec();

				double  kp = CropSuccessions::getInstance().at(idKP).getKp();
				int val;
				if(kp <= _kpmax)
				{
				    val = 0;
				}else{
				    val =getInfty();
				}

				struct LST vallst;
				vallst(prec_id - 1);
				vallst(crop_id - 1);
				mvcs(mvc(vallst(), val) );
			    }
			    cost(_cstFileStrm, var(),  mvcs(), 0);
			}
		    }

		}
	    }
	}
	return true;
    }


//Spatial balance: Crop maximum Area
    bool WCSPConstraintFactory::cropMaxAllocationCstrFL(ofstream&  _cstFileStrm)
    {
	unsigned int nb_LU = LandUnits::getInstance().size();
	int nb_CR  = Crops::getInstance().size();

	for(int h = getHistoric(); h < getFullHorizon(); h++)
	{
	    struct LST varset;
	    struct VT vto;
	    struct UVTO uto;
	    struct TO to;
	    int dec= h*nb_LU;

	    for(unsigned int luid = 0; luid < nb_LU; luid++)
	    {
		varset((luid + dec));
	    }

	    for(int it_cr = 0; it_cr < nb_CR; it_cr++)
	    {
		int min = 0;
		int max = nb_LU/Crops::getInstance().getById(it_cr+1).getRtRot();
		vto(uto(it_cr, to(min,max)));
	    }

	    sgcc(_cstFileStrm, varset(), vto(), getInfty());
	}
	return true;
    }



// eq_managementunit
    bool WCSPConstraintFactory::eq_managementunitCstr
    (ofstream&  _cstFileStrm)
    {
	int nb_LU 		= getNbLU();
	int nb_CR 		= getNbCrops();
	DecisionProfile::t_eqManagementunitMap eqManagementUnit = DecisionProfile::getInstance().getEqManagementUnit();

	for(DecisionProfile::t_eqManagementunitMap::const_iterator it =  eqManagementUnit.begin();
	    it !=  eqManagementUnit.end();++it)
	{
	    std::vector<int> lst = it->second;
	    for(unsigned int i = 0 ; i<lst.size();++i)
	    {
		int _id_mu=lst.at(i);
		cout<<"\t"<<it->first<<": "<<_id_mu;
		// get landunit list
		vector<int> lu_lst;

		if(it->first=="plotunit")
		{
		    lu_lst= PlotUnits::getInstance().getById(_id_mu).getLuIdLst();
		}else if (it->first=="croppingblock")
		{
		    lu_lst= CroppingBlocks::getInstance().getById(_id_mu).getLuIdLst();
		}else{
		    throw ArgError("/!\\ WCSPConstraintFactory:: Cannot find the the management unit");
		}

		// apply cstr to to the list for all year
		for(int h = getHistoric(); h < getFullHorizon(); h++)
		{
		    struct LST varset;
		    struct D domaine;
		    struct UVC value;
		    int dec= h*nb_LU;

		    for(unsigned int luid = 0; luid < lu_lst.size(); luid++)
		    {
			cout<<"\t"<<luid;
			varset((lu_lst.at(luid)-1 + dec));
		    }

		    for(int it_cr = 0; it_cr < nb_CR; it_cr++)
		    {
			domaine(value(it_cr,0));
		    }
		    equal(_cstFileStrm, varset(), domaine(), getInfty());
		}
		cout<<endl;
	    }
	}
	return true;
    }

// Cropping block same spatial
    bool WCSPConstraintFactory::sameSp_croppingblock(ofstream&  _cstFileStrm)
    {
	int nb_LU 		= getNbLU();
	DecisionProfile::t_eqManagementunitMap sameSpCroppingblock = DecisionProfile::getInstance().getSameSpCroppingblock();
	for(DecisionProfile::t_eqManagementunitMap::const_iterator it =  sameSpCroppingblock.begin();
	    it !=  sameSpCroppingblock.end();++it)
	{
	    std::vector<int> lst = it->second;
	    for(unsigned int i = 0 ; i<lst.size();++i)
	    {
		int _id_mu=lst.at(i);
		cout<<"\t"<<it->first<<": "<<_id_mu<<endl;
		// get landunit list
		std::vector<int> lu_lst;
		if(it->first=="plotunit")
		{
		    lu_lst= PlotUnits::getInstance().getById(_id_mu).getLuIdLst();
		}else if (it->first=="croppingblock")
		{
		    lu_lst= CroppingBlocks::getInstance().getById(_id_mu).getLuIdLst();
		}else{
		    throw ArgError("/!\\ WCSPConstraintFactory:: Cannot find the the management unit");
		}

		// for every year in the horizon
		struct LST varset1;
		for(int hh = getHistoric(); hh < getFullHorizon()-1; hh++)
		{
		    struct LST varset1;
		    struct LST varset2;
		    int dec= hh*nb_LU;
		    int decc= (hh+1)*nb_LU;
		    for(unsigned int i = 0; i < lu_lst.size(); i++)
		     	{
			    varset1((lu_lst.at(i)-1 + dec));
			    varset2((lu_lst.at(i)-1 + decc));
			}
		    ssame(_cstFileStrm, varset1(), varset2(), getInfty());
		}
	    }
	}
	cout<<endl;
    }



// Cropping block same temporal
    bool WCSPConstraintFactory::sameTp_croppingblock(ofstream&  _cstFileStrm)
    {
	int nb_LU 		= getNbLU();
	DecisionProfile::t_eqManagementunitMap sameTpCroppingblock = DecisionProfile::getInstance().getSameTpCroppingblock();

	for(DecisionProfile::t_eqManagementunitMap::const_iterator it =  sameTpCroppingblock.begin();
	    it !=  sameTpCroppingblock.end();++it)
	{
	    std::vector<int> lst = it->second;
	    cout<<"\t"<<it->first<<": "<< lst.size() <<endl;

	    for(unsigned int i = 0 ; i<lst.size();++i)
	    {
		int _id_mu=lst.at(i);

		// get landunit list
		std::vector<int> lu_lst;

		if (it->first=="croppingblock")
		{
		    lu_lst= CroppingBlocks::getInstance().getById(_id_mu).getLuIdLst();
		}else{
		    throw ArgError("/!\\ WCSPConstraintFactory::sameSp_croppingblock Cannot find or use this management unit");
		}

		// for every lu in the mu
		struct LST varset1;
		for(unsigned int ii = 0; ii <lu_lst.size(); ii++)
		{
		    struct LST varset2;
		    if(ii==0)
		    {
			for(int h = getHistoric(); h < getFullHorizon(); h++)
			{
			    int dec= h*nb_LU;
			    varset1((lu_lst.at(ii)-1 + dec));
			}
		    }else{
			for(int hh = getHistoric(); hh < getFullHorizon(); hh++)
			{
			    int decc= hh*nb_LU;
			    varset2((lu_lst.at(ii)-1 + decc));
			}
			ssame(_cstFileStrm, varset1(), varset2(), getInfty());
		    }
		}
	    }
	}
    }

// ---------------------------------------------------------------------------------------------------
// sameSp_groupingCstr
// ---------------------------------------------------------------------------------------------------

    bool WCSPConstraintFactory::sameSp_groupingCstr(ofstream&  _cstFileStrm)
    {
	int nb_LU 		= getNbLU();
	int nb_CR 		= getNbCrops();
	DecisionProfile::t_eqManagementunitMap sameSp_gp = DecisionProfile::getInstance().getSameSpGrouping();


	for(DecisionProfile::t_eqManagementunitMap::const_iterator it =  sameSp_gp.begin();
	    it !=  sameSp_gp.end();++it)
	{
	    std::vector<int> lst = it->second;
	    cout<<it->first<<": "<< lst.size() <<endl;
	    for(unsigned int i = 0 ; i<lst.size();++i)
	    {
		int _id_mu=lst.at(i);
		std::vector<int> lu_lst;
		if (it->first=="croppingblock")
		{
		    lu_lst= CroppingBlocks::getInstance().getById(_id_mu).getLuIdLst();
		}else{
		    throw ArgError("/!\\ WCSPConstraintFactory::sameSp_grouping Cannot find or use this management unit");
		}

		// domaine
		struct D domaine;
		struct UVC value;
		for(int it_cr = 0; it_cr < nb_CR; it_cr++)
		{
		    domaine(value(it_cr,0));
		}
		// for every year in the horizon
		for(int hh = getHistoric(); hh < getFullHorizon(); hh++)
		{
		    int decc= hh*nb_LU;
		    for(unsigned int i = 0; i < lu_lst.size(); i++)
		    {
			vector<int> adj_lst=LandUnits::getInstance().getById(lu_lst.at(i)).getAdjacentLst();
			for(unsigned int ii = 0; ii < adj_lst.size(); ii++)
			{
			    struct LST varset1;
			    varset1((lu_lst.at(i)-1 + decc));
			    varset1((adj_lst.at(ii)-1 + decc));
			    equal(_cstFileStrm, varset1(), domaine(), inicost);
			}
		    }
		}
	    }
	}
    }
// ---------------------------------------------------------------------------------------------------


// //STRICT EQALITY
// bool WCSPConstraintFactory::equalityConstraints
// (ofstream&  _cstFileStrm)
// {
// 	//LandUnit 1 = landUnit 3 Si  OH (3) à la date 2
// 	int nb_LU 		= getNbLU();

// 	for(int h = getHistoric(); (h < getHistoric() + 1); h++){
// 	    struct LST var;
// 	    struct D domaine;
// 	    struct UVC value;

// 	    var((1+ h*nb_LU));
// 	    var((3+ h*nb_LU));
// 	    domaine(value(0,0)); //OH
// 	    equal(_cstFileStrm, var(), domaine(), getInfty());
// 	}
// 	return false;
// }



// //SAME
// bool WCSPConstraintFactory::sameConstraints
// (ofstream&  _cstFileStrm)
// {
// 	//LandUnit 1 = landUnit 3 Si  OH (3) à la date 2
// 	int nb_LU 		= getNbLU();
// 	struct LST varset1, varset2;
// 	for(int h = getHistoric(); h < getFullHorizon(); h++){
// 	    varset1((2+ h*nb_LU));
// 	    varset2((5+ h*nb_LU));
// 	}
// 	ssame(_cstFileStrm, varset1(), varset2(), getInfty());
// 	return false;
// }




// //Temporal balance
// bool WCSPConstraintFactory::temporalAllocationBalanceConstraints
// (ofstream&  _cstFileStrm)
// {

// 	unsigned int nb_LU 		= LandUnits::getInstance().size();
// 	for(unsigned int luid = 0; luid < nb_LU; luid++){
// 	    struct LST varset;
// 	    struct VT vto;
// 	    struct UVTO uto;
// 	    struct TO to;
// 	    for(int h = getHistoric(); h < getFullHorizon(); h++){
// 		varset((luid+ h*nb_LU));
// 	    }

// 	    vto(uto(0, to(0,2)));
// 	    sgcc(_cstFileStrm, varset(), vto(), getInfty());
// 	}
// 	return true;
// }


    void WCSPConstraintFactory::setMaxCost()
    {
	int _tempUBmax = 0;

	DecisionProfile::t_eqManagementunitMap sameSp_gp = DecisionProfile::getInstance().getSameSpGrouping();

	for(DecisionProfile::t_eqManagementunitMap::const_iterator it =  sameSp_gp.begin();
	    it !=  sameSp_gp.end();++it)
	{
	    std::vector<int> lst = it->second;
	    for(unsigned int i = 0 ; i<lst.size();++i)
	    {
		int _id_mu=lst.at(i);
		std::vector<int> lu_lst;
		if(it->first=="plotunit")
		{
		    _tempUBmax += PlotUnits::getInstance().getById(_id_mu).getNb_Adjacent();
		}else if (it->first=="croppingblock")
		{
		    _tempUBmax += CroppingBlocks::getInstance().getById(_id_mu).getNb_Adjacent();
		}else{
		    throw ArgError("/!\\ WCSPConstraintFactory::setMaxCost Cannot find the the management unit");
		}
	    }
	}

	MaxCost =_tempUBmax*inicost*(getFullHorizon() - getHistoric());
    }

    int WCSPConstraintFactory::getMaxCost()
    {
	return MaxCost;
    }

    int WCSPConstraintFactory::getMaxCost() const
    {
	return MaxCost;
    }

    int  WCSPConstraintFactory::getUB() const
    {
	int LimCost = DecisionProfile::getInstance().getLimCost();
	int ubInt = (int) MaxCost * LimCost/10;

	return ubInt;
    }

} // namespace crash
