#ifndef Itk_HPP
#define Itk_HPP

#include <belief/proceduralknowledge/CropOperation.hpp>

#include <vector>
#include <map>
#include <string>

//Crash Utils
#include <utils/CrashDBS.hpp>
#include <utils/Utils.hpp>

namespace vd=vle::devs;
namespace vv=vle::value;

namespace crash {

	class Itk {

		public:
			// variety
			typedef struct cvar{
				int id_variety;
				double density;
				std::string st_sow;
				std::string nd_sow;
				std::string st_hr;
				std::string nd_hr;
			} t_variety;
			typedef map < std::string, t_variety > t_map_variety;

			//operation
			typedef std::vector < int > t_lst_int;
			typedef std::vector < CropOperation > t_lst_op;
			typedef t_lst_op::iterator it_op;
			typedef t_lst_op::const_iterator const_it_op;

			// all rules ofrules
			typedef std::vector < std::string > t_lst_string;

			/**
			 * @brief Default constructor
			 */
			Itk();

			/**
			 * @brief Main constructor
			 */
			Itk(int _id,
					std::string _code_crop,
					std::string _type_crop,
					std::string _pre_crop,
					std::string _suc_crop,
					t_map_variety map_v,
					t_lst_string vect_ds);

			/**
			 * @brief Default destructor
			 */
			virtual ~Itk();

			/**
			 * @brief Get index crop
			 */
			int getId();
			int getId() const;

			/**
			 * @brief Get code crop
			 */
			std::string getCodeCrop();
			std::string getCodeCrop() const;

			/**
			 * @brief Get type crop
			 */
			std::string getTypeCrop();
			std::string getTypeCrop() const;

			/**
			 * @brief Get Domail soiltype
			 */
			t_lst_string getDomainSoiltype();
			t_lst_string getDomainSoiltype() const;

			/**
			 * @brief add CropOperation object
			 */
			void addCropOperation(CropOperation _operation);
			/**
			 * @brief Get Yr index of one operation based on crop type
			 */
			void setYrInd();
			/**
			 * @brief Get vector of CropOperation object
			 */
			t_lst_op & getLstOperation();
			const t_lst_op & getLstOperation() const;


			CropOperation & getOperationByType(std::string _type);

			std::string getStSow(std::string _var);
			std::string getStSow(std::string _var) const;
			std::string getNdSow(std::string _var);
			std::string getNdSow(std::string _var) const;

			std::string getStHrv(std::string _var);
			std::string getStHrv(std::string _var) const;
			std::string getNdHrv(std::string _var);
			std::string getNdHrv(std::string _var) const;

			double getDensity(std::string _var);
			double getDensity(std::string _var) const;


			/**
			 * @brief Get all unique rules of the current ITK
			 */

			CropOperation::t_map_rules getRules();
			CropOperation::t_map_rules getRules() const;

		private:

			/** @brief Index of crop */
			int id_itk;
			/** @brief Code of crop */
			std::string code_crop;
			/** @brief type of crop */
			std::string type_crop;
			/** @brief Preceding crop */
			std::string pre_crop;
			/**  @brief Succeeding crop */
			std::string suc_crop;
			/** @brief Domain soil variety */
			t_lst_string domain_soiltype;
			/** @brief vector of CropOperation object*/
			t_lst_op lst_operation;
			/** info on crop variety*/
			t_map_variety map_variety;

	};

} //namespace crash

#endif //Itk_HPP
