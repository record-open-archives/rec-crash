#include <belief/proceduralknowledge/Filter.hpp>

using namespace std;

namespace crash {

  Filter::Filter() {}

  Filter::~Filter() {}

  std::string Filter::selectSolution(const int & indexSol) {
    return CroppingPlanCandidates::getInstance().at(indexSol).getLineSolution();
  }
     
  void Filter::saveSolution(const std::string & _dirName, 
			    const std::string & _fileName,
			    const int & _nbSolution,
			    const bool & newFile) {
    std::string _wcspSeqFile(_dirName.c_str());
    _wcspSeqFile.append("/"); 
    _wcspSeqFile.append(_fileName.c_str()); 
    std::string _solFile(_wcspSeqFile.c_str());
   
    if(newFile == TRUE) {
      std::ofstream _solFilestream(vle::utils::Path::path().getPackageDataFile(_solFile).c_str(), std::ios::out);
      if(_solFilestream) {
	_solFilestream << selectSolution(_nbSolution) << endl;
	_solFilestream.close();
      } else {
	throw std::runtime_error("Filter error: Solution file cannot be generated");
      }
    } else {
      std::ofstream _solFilestream(vle::utils::Path::path().getPackageDataFile(_solFile).c_str(), std::ios::app);
      if(_solFilestream) {
	_solFilestream << selectSolution(_nbSolution) << endl;
	_solFilestream.close();
      } else {
	throw std::runtime_error("Filter error: Solution file cannot be generated");
      }
    }
  
  }
  
} //namespace crash
