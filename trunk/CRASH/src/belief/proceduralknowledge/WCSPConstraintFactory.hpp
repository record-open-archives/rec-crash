/*
 * @file record/pkgs/decision/planning/algos/
 *
 * This file is part of VLE, a framework for multi-modeling, simulation
 * and analysis of complex dynamical systems
 * http://www.vle-project.org
 *
 * Copyright (c) 2003-2007 Gauthier Quesnel <quesnel@users.sourceforge.net>
 * Copyright (c) 2003-2010 ULCO http://www.univ-littoral.fr
 * Copyright (c) 2007-2010 INRA http://www.inra.fr
 *
 * See the AUTHORS or Authors.txt file for copyright owners and contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef WCSP_HPP
#define WCSP_HPP

#include <string>
#include <ostream>
#include <vector>
#include <limits>
#include <boost/lexical_cast.hpp>
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/graph_traits.hpp>
#include <boost/graph/property_maps/constant_property_map.hpp>
#include <boost/property_map/property_map.hpp>

// Crash utils
#include <utils/Utils.hpp>
#include <utils/GenericWCSPConstraintFactory.hpp>

//Crash procedural
# include <belief/proceduralknowledge/DecisionProfile.hpp>
//# include <belief/proceduralknowledge/CropSeqCandidates.hpp>

// Crash systemKW
# include <belief/structuralknowledge/systemknowledge/farmland/LandUnits.hpp>
# include <belief/structuralknowledge/systemknowledge/farmland/CroppingBlocks.hpp>
# include <belief/structuralknowledge/systemknowledge/farmland/PlotUnits.hpp>


// Crash ExpertKW
# include <belief/structuralknowledge/expertknowledge/CropSuccessions.hpp>
# include <belief/structuralknowledge/expertknowledge/Crops.hpp>
# include <belief/structuralknowledge/expertknowledge/CropDomains.hpp>

using namespace std;
using namespace boost;

namespace crash {
    class  WCSPConstraintFactory : public GenericWCSPConstraintFactory
    {
    public:
	/**
	 * @brief Default constructor
	 */
	WCSPConstraintFactory() :
	    GenericWCSPConstraintFactory(DecisionProfile::getInstance().getNbVar(), Crops::getInstance().size() ),
	    MaxCost(0),
	    inicost(100)
	{
	    setFullHorizon(DecisionProfile::getInstance().getHorizon() + DecisionProfile::getInstance().getSizeHisto()); 
	    setHistoric(DecisionProfile::getInstance().getSizeHisto()); 
	    setWCSPDir(Utils::getInstance().getWCSPDirName()); 
	    setWCSPFile(Utils::getInstance().getWCSPFileName()); 
	    setSolutionDir(Utils::getInstance().getSolutionDirName()); 
	}
		
	virtual ~WCSPConstraintFactory(){}	
	
	bool generateConstraintsFiles(	const std::string& _cstFileDirName,  
					const std::string& _cstFile,
					const std::string& _cstOption);
					
	bool historicConstraints	(ofstream&  _cstFileStrm);
	bool spatialCompatibilityConstraints	(ofstream&  _cstFileStrm);

	/**
	 * @brief write constraints to respect crop return time along crop sequences 
	 */	
	bool temporalCompatibilityOnSEQConstraints	(ofstream&  _cstFileStrm);

	/**
	 * @brief write constraints in order to produce cyclic crop sequence solutions
	 */
	bool ROTATIONConstraints	(ofstream&  _cstFileStrm);

	/**
	 * @brief write constraints in order to produce cyclic crop sequence solutions with differnciated length between cropping block
	 */
	bool MultiROTATIONConstraints	(ofstream&  _cstFileStrm);


	/**
	 * @brief write constraints in restrict crop succession
	 */
	bool successionKpCstr(ofstream&  _cstFileStrm);
	
//	bool equalityConstraints	(ofstream&  _cstFileStrm);
//	bool sameConstraints	(ofstream&  _cstFileStrm);   


	/**
	 * @brief write constraints in order to limit the allocation of every crop to the max crop area.
	 */
	bool cropMaxAllocationCstrFL(ofstream&  _cstFileStrm);
//	bool temporalAllocationBalanceConstraints(ofstream&  _cstFileStrm);

	/**
	 * @brief write constraints to force management unit to  equally managed
	 */
	bool eq_managementunitCstr(ofstream&  _cstFileStrm);

	/**
	 * @brief write constraint to favor on  every cropping block similar spatial allocation
	 */
	bool sameSp_croppingblock(ofstream&  _cstFileStrm);

	/**
	 * @brief write constraint to favor on  every cropping block similar temporal allocation on landunit
	 */
	bool sameTp_croppingblock(ofstream&  _cstFileStrm);

	/**
	 * @brief write constraints to group as much as posssible crop on landunits
	 */
	bool sameSp_groupingCstr(ofstream&  _cstFileStrm);

	/**
	 * @brief Set & get full horizon (past + future to be considered in the decision
	 */
	void	setFullHorizon(const int _fullHorizon) { m_fullHorizon = _fullHorizon; }
	int 	getFullHorizon() { return m_fullHorizon; }
	int 	getFullHorizon() const { return m_fullHorizon; }

	void	setHistoric(const int _historic) { m_historic = _historic; }
	int 	getHistoric() { return m_historic; }
	int 	getHistoric() const { return m_historic; }

	/**
	 * @brief Set and Get  MaxCost
	 */
	void setMaxCost();
	int getMaxCost();
	int getMaxCost() const;
	int getUB() const;
	
	int getNbVar	() const
	{ return (LandUnits::getInstance().size() * getFullHorizon());}

	int getNbLU	() const
	{ return LandUnits::getInstance().size();}

	int getNbCrops	() const
	{ return Crops::getInstance().size();}
	
    private : 
	int m_fullHorizon;
	int m_historic; 
	/**
	 * @brief Maximum cost for the WCSP
	 */
	int MaxCost;
	int inicost;
    };
		
} // namespace crash

#endif
