/**
 * @file DecisionProfileuccession.hpp
 * @author The vle Development Team
 */

/*
 * VLE Environment - the multimodeling and simulation environment
 * This file is a part of the VLE environment (http://vle.univ-littoral.fr)
 * Copyright (C) 2003 - 2009 The VLE Development Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <vle/utils/Path.hpp>
#include <vle/utils/Exception.hpp>

#include <belief/proceduralknowledge/DecisionProfile.hpp>

using vle::utils::ArgError;
using namespace std;
using namespace boost;

namespace crash
{

    DecisionProfile*  	DecisionProfile::mDecisionProfile= 0;

    DecisionProfile::DecisionProfile()    {
        m_cstr_option["historic"] = false;
        m_cstr_option["tr"] = false;
        m_cstr_option["kp_croppingblock"] = false;
	m_cstr_option["rotation"] = false;
	m_cstr_option["rotation_croppingblock"] = false;
        m_cstr_option["crop_max_area"] = false;
	m_cstr_option["eq_managementunit"] = false;
	m_cstr_option["sameSp_croppingblock"] = false;
	m_cstr_option["sameTp_croppingblock"] = false;
	m_cstr_option["sameSp_grouping"] = false;
	m_cropDomain_option["soiltype"] = false;
    }

    DecisionProfile::~DecisionProfile() {};

// Constraint options
    void DecisionProfile::setCstrOption(const std::string& _option, const bool& _value)   {
        t_CstrMap::iterator it(m_cstr_option.find(_option));
        if (it == m_cstr_option.end())
	{
	    throw ArgError("DecisionProfile: This option does not implemented");
	}
        it->second = _value;
    }

    const DecisionProfile::t_CstrMap& DecisionProfile::getCstrOption() const    {
        return m_cstr_option;
    }

    void DecisionProfile::seeCstrOption() const
    {
        cout << "Constraint Options: \n" <<endl;
        for(std::map<std::string, bool>::const_iterator it = m_cstr_option.begin() ;
	    it !=  m_cstr_option.end();
	    it++)
	{
	    cout << it->first << "--> "<< it->second <<endl;
	}
    }

// horizon
    void DecisionProfile::setHorizon(const int _value)
    {
        Horizon = _value;
    }

    int DecisionProfile::getHorizon()
    {
        return Horizon;
    }

    int DecisionProfile::getHorizon() const
    {
        return Horizon;
    }

// historic
    void DecisionProfile::setSizeHisto(const int _value)
    {
        Historic = _value;
    }

    int DecisionProfile::getSizeHisto()
    {
        return Historic;
    }

    int DecisionProfile::getSizeHisto() const
    {
        return Historic;
    }

// setMULst
    DecisionProfile::t_eqManagementunitMap DecisionProfile::setMULst(const vle::value::Map& lst)
    {
	t_eqManagementunitMap temp_map;
	for (vle::value::Map::const_iterator it = lst.begin();
	     it != lst.end(); ++it)
	{
	    std::string mu  = it->first;
	    if(it->first!="activation"){
		vle::value::Set id_mu_Set((it->second)->toSet());
		std::vector<int> id_mu_lst;
		for(unsigned int itt=0; itt<id_mu_Set.size();++itt)
		{
		    int _id = id_mu_Set.getInt(itt);
		    id_mu_lst.push_back(_id);
		}
                temp_map[mu] = id_mu_lst;
	    }
	}
	return(temp_map);
    }

// Multi rotation
    void DecisionProfile::setMultiRotation(const vle::value::Map& lst)
    {
	for (vle::value::Map::const_iterator it = lst.begin();
	     it != lst.end(); ++it)
	{
	    std::string mu  = it->first;
	    if(it->first!="activation"){
		vle::value::Set id_mu_Set((it->second)->toSet());

		std::vector< std::pair<int,int> > id_multiRot_lst;
		for(unsigned int itt=0; itt<id_mu_Set.size();++itt)
		{
		    vle::value::Set mu_multiRot_Set  = id_mu_Set.get(itt)->toSet();
		    int _id =  mu_multiRot_Set.getInt(0);
		    int _rotLength  = mu_multiRot_Set.getInt(1);
		    id_multiRot_lst.push_back(make_pair(_id,_rotLength));
		}
		multiRotationMap[mu] = id_multiRot_lst;
	    }
	}
    }

    DecisionProfile::t_multiRotationMap DecisionProfile::getMultiRotation()
    {
        return multiRotationMap;
    }

    DecisionProfile::t_multiRotationMap DecisionProfile::getMultiRotation() const
    {
        return multiRotationMap;
    }

// EqManagementUnit
    void DecisionProfile::setEqManagementUnit(const vle::value::Map& lst)
    {
	eqManagementUnit=setMULst(lst);
    }

    DecisionProfile::t_eqManagementunitMap DecisionProfile::getEqManagementUnit()
    {
        return eqManagementUnit;
    }

    DecisionProfile::t_eqManagementunitMap DecisionProfile::getEqManagementUnit() const
    {
        return eqManagementUnit;
    }

// SameSpManagementUnit
    void DecisionProfile::setSameSpCroppingblock(const vle::value::Map& lst)
    {
	sameSpCroppingblock=setMULst(lst);
    }

    DecisionProfile::t_eqManagementunitMap DecisionProfile::getSameSpCroppingblock()
    {
        return sameSpCroppingblock;
    }

    DecisionProfile::t_eqManagementunitMap DecisionProfile::getSameSpCroppingblock() const
    {
        return sameSpCroppingblock;
    }

// SameTpManagementUnit
    void DecisionProfile::setSameTpCroppingblock(const vle::value::Map& lst)
    {
	sameTpCroppingblock=setMULst(lst);
    }

    DecisionProfile::t_eqManagementunitMap DecisionProfile::getSameTpCroppingblock()
    {
        return sameTpCroppingblock;
    }

    DecisionProfile::t_eqManagementunitMap DecisionProfile::getSameTpCroppingblock() const
    {
        return sameTpCroppingblock;
    }

// Grouping
    void DecisionProfile::setSameSpGrouping(const vle::value::Map& lst)
    {
	sameSpGrouping=setMULst(lst);
    }

    DecisionProfile::t_eqManagementunitMap DecisionProfile::getSameSpGrouping()
    {
        return sameSpGrouping;
    }

    DecisionProfile::t_eqManagementunitMap DecisionProfile::getSameSpGrouping() const
    {
        return sameSpGrouping;
    }



// Kp
    void DecisionProfile::setKpCroppingblock(const vle::value::Map& lst)
    {
	for (vle::value::Map::const_iterator it = lst.begin();
	     it != lst.end(); ++it)
	{
	    std::string mu  = it->first;
	    if(it->first!="activation"){
		vle::value::Set id_mu_Set((it->second)->toSet());

		std::vector< std::pair<int,double> > id_kp_lst;
		for(unsigned int itt=0; itt<id_mu_Set.size();++itt)
		{
		    vle::value::Set mu_kp_Set  = id_mu_Set.get(itt)->toSet();
		    int _id =  mu_kp_Set.getInt(0);
		    double _kp  = mu_kp_Set.getDouble(1);
		    id_kp_lst.push_back(make_pair(_id,_kp));
		}
		kpMap[mu] = id_kp_lst;
	    }
	}
    }


    DecisionProfile::t_kpMap DecisionProfile::getKpCroppingblock()
    {
	return kpMap;
    }

    DecisionProfile::t_kpMap DecisionProfile::getKpCroppingblock() const
    {
	return kpMap;
    }


// Max cost

    void DecisionProfile::setLimCost(int _cost)
    {
	LimCost = _cost;
    }

    int DecisionProfile::getLimCost()
    {
	return LimCost;
    }

    int DecisionProfile::getLimCost() const
    {
	return LimCost;
    }
// Crop Domain
    void DecisionProfile::setCropDomainOption(const std::string& _option, const bool& _value)
    {
	t_CropDomainMap::iterator it(m_cropDomain_option.find(_option));
	if (it == m_cropDomain_option.end())
	{
	    throw ArgError("DecisionProfile::setCropDomainOption : This option does not implemented");
	}
	it->second = _value;
    }

    const DecisionProfile::t_CropDomainMap & 	DecisionProfile::getCropDomainOption() const
    {
	return m_cropDomain_option;
    }

    void DecisionProfile::seeCropDomainOption() const
    {
	cout << "Crop Domain Options: \n" <<endl;
	for(std::map<std::string, bool>::const_iterator it = m_cropDomain_option.begin() ;
	    it !=  m_cropDomain_option.end();
	    it++)
	{
	    cout << it->first << "--> "<< it->second <<endl;
	}
    }

// init
    void DecisionProfile::init(const vd::InitEventList& evts)
    {
	setCstrOption( "historic",vle::value::toBoolean(evts.get("cstr_historic")));
	setCstrOption( "tr", vle::value::toBoolean(evts.get("cstr_tr")));
	setCstrOption( "rotation", vle::value::toBoolean(evts.get("cstr_rotation")));
	setCstrOption("crop_max_area", vle::value::toBoolean(evts.get("cstr_crop_max_area")));

	const vle::value::Map& lstMultiRot = evts.getMap("cstr_rotation_croppingblock");
	if (vle::value::toBoolean(lstMultiRot["activation"])){
	    setCstrOption("rotation_croppingblock", 1);
	    setMultiRotation(lstMultiRot);
	}

	const vle::value::Map& lst = evts.getMap("cstr_eq_managementunit");
	if (vle::value::toBoolean(lst["activation"])){
	    setCstrOption("eq_managementunit", 1);
	    setEqManagementUnit(lst);
	}

	const vle::value::Map& lstSp = evts.getMap("cstr_sameSp_croppingblock");
	if (vle::value::toBoolean(lstSp["activation"])){
	    setCstrOption("sameSp_croppingblock", 1);
	    setSameSpCroppingblock(lstSp);
	}

	const vle::value::Map& lstTp = evts.getMap("cstr_sameTp_croppingblock");
	if (vle::value::toBoolean(lstTp["activation"])){
	    setCstrOption("sameTp_croppingblock", 1);
	    setSameTpCroppingblock(lstTp);
	}

	const vle::value::Map& lstKp = evts.getMap("cstr_kp_croppingblock");
	if (vle::value::toBoolean(lstKp["activation"])){
	    setCstrOption( "kp_croppingblock",1);
	    setKpCroppingblock(lstKp);
	}

	const vle::value::Map& lstGrouping = evts.getMap("cstr_sameSp_grouping");
	if (vle::value::toBoolean(lstGrouping["activation"])){
	    setCstrOption( "sameSp_grouping",1);
	    setSameSpGrouping(lstGrouping);
	}

	setHorizon(vle::value::toInteger(evts.get("horizon")));
	setSizeHisto(vle::value::toInteger(evts.get("historic")));
	setCropDomainOption("soiltype", vle::value::toBoolean(evts.get("crpd_soiltype")));
	setLimCost(vle::value::toInteger(evts.get("lim_cost")));
    }

} // end namespace crash
