#ifndef ITKCROPS_HPP
#define ITKCROPS_hpp

#include <string>

#include <vector>
#include <iostream>
#include <sstream>

//Crash
#include <belief/SingletonBelief.hpp>

#include <belief/proceduralknowledge/ItkCrop.hpp>

//Crash utils
#include <utils/CrashDBS.hpp>
#include <utils/Utils.hpp>

using namespace std;
namespace vd=vle::devs;
namespace vv=vle::value;


namespace crash {

  class ItkCrops : public SingletonBelief < ItkCrops, ItkCrop > {

    /** 
     * @brief The class Crops is a vector of CropDomain object
     */
    friend class SingletonBelief < ItkCrops, ItkCrop >;

  private:

    /**
     * @brief Private constructor
     */
    ItkCrops();

    /**
     * @brief Private destructor
     */
    virtual ~ ItkCrops();

  public:

    typedef std::vector < int > t_vectInt;
    typedef std::vector < std::string > t_vectString;
    typedef std::map < std::string, t_vectString > t_mapRule;

    size_type init(const vd::InitEventList & evts);

    void setOperationRule(const std::string & nameRule, 
			  const t_vectString & namePredicate);

    void initRule(const vd::InitEventList & evts);

    string buildPlan(const std::string & namefile, ItkCrop & itk, 
		     const int & id_plot, const int & year);

    t_mapRule operationRule;

  };

} //namespace crash

#endif //ITKCROPS_HPP
