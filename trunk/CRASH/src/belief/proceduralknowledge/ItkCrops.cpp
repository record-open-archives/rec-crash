#include <belief/proceduralknowledge/ItkCrops.hpp>

namespace crash {

  ItkCrops::ItkCrops() {}

  ItkCrops::~ItkCrops() {}

  ItkCrops::size_type ItkCrops::init(const vd::InitEventList & evts) {
    CrashDBS cdb(evts);

    PGresult* db_ItkCrop = cdb.QueryDB("SELECT itkcref. *, code_crop from proceduralknowledge.itkcref, expertknowledge.crop WHERE itkcref.ref_crop = crop.id_crop");
    int nb =  PQntuples(db_ItkCrop);


    for(int i=0; i < nb; i++) {
      int id_itk = lexical_cast<int>(PQgetvalue(db_ItkCrop, i, PQfnumber(db_ItkCrop, "id_itkcref")));
      string codeItk = lexical_cast<string>(PQgetvalue(db_ItkCrop, i, PQfnumber(db_ItkCrop, "code_itk")));
      int pCrop = lexical_cast<int>(PQgetvalue(db_ItkCrop, i, PQfnumber(db_ItkCrop, "ref_preceedingcrop")));
      int sCrop = lexical_cast<int>(PQgetvalue(db_ItkCrop, i, PQfnumber(db_ItkCrop, "ref_succeedingcrop")));
      string d_variety = lexical_cast<string>(PQgetvalue(db_ItkCrop, i, PQfnumber(db_ItkCrop, "domain_variety")));
      string s_variety = lexical_cast<string>(PQgetvalue(db_ItkCrop, i, PQfnumber(db_ItkCrop, "domain_soil")));
      ItkCrops::t_vectInt vect_dv;
      ItkCrops::t_vectInt vect_ds;
      vect_dv = Utils::getInstance().getVectInt(d_variety, ",");
      vect_ds = Utils::getInstance().getVectInt(s_variety, ",");
      ItkCrop itcrop(id_itk, codeItk, pCrop, sCrop, vect_dv, vect_ds);
       
      string itk = lexical_cast<string>(id_itk);    
      string sql = "SELECT cropoperation.*, ref_itkcref FROM proceduralknowledge.cropoperation, proceduralknowledge.l_itkcref_cropoperation  WHERE cropoperation.id_cropoperation = l_itkcref_cropoperation.ref_cropoperation AND ref_itkcref = ";
      sql.append(itk);
      PGresult* db_sql = cdb.QueryDB(sql.c_str());
      int _Nb = PQntuples(db_sql);
      ItkCrop::t_vectOp temp_operation;
      for(int i=0; i < _Nb; i++) {
	int id_cpoperation = lexical_cast<int>(PQgetvalue(db_sql, i, PQfnumber(db_sql, "id_cropoperation")));
	string name_op = lexical_cast<string>(PQgetvalue(db_sql, i, PQfnumber(db_sql, "name_co")));
	string type_op = lexical_cast<string>(PQgetvalue(db_sql, i, PQfnumber(db_sql, "type_co")));
	string ref_eq = lexical_cast<string>(PQgetvalue(db_sql, i, PQfnumber(db_sql, "equipment")));
	int _duration = lexical_cast<int>(PQgetvalue(db_sql, i, PQfnumber(db_sql, "duration")));
	string dateStart = lexical_cast<string>(PQgetvalue(db_sql, i, PQfnumber(db_sql, "date_start")));
	string dateEnd = lexical_cast<string>(PQgetvalue(db_sql, i, PQfnumber(db_sql, "date_end")));

	CropOperation cp_operation(id_cpoperation, name_op, type_op, ref_eq, _duration, dateStart, dateEnd);
	//Règles et prédicats associés aux operations d'un itinéraire technique ITK
	cp_operation.initRulePredicate(evts, name_op);
	//Constraintes de précédence associées aux opérations d'un itinéraire technique ITK
	cp_operation.initPreceedingConstraint(evts, name_op);
	//Stockage des opérations d'un itinéraire technique dans un vecteur
	temp_operation.push_back(cp_operation);
      }
      itcrop.addCropOperation(temp_operation);
      add(itcrop);
    }

    return nb_elements;
  }

  string ItkCrops::buildPlan(const string & namefile, ItkCrop & itk, 
			     const int & id_plot, const int & year) {

    itk.buildItk(namefile, year, id_plot);
    std::ifstream list(namefile.c_str());
    std::stringstream buff;
    buff << list.rdbuf();

    return buff.str();
   
  }

 

 

} //namespace crash
