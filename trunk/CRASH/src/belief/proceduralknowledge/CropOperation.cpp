#include <belief/proceduralknowledge/CropOperation.hpp>


namespace crash {

    CropOperation::CropOperation(int _id, std::string _name, std::string _type, std::string _eq, std::string _st,  std::string _nd, std::string crash_ev,  const vd::InitEventList& evts) :
        id(_id),
        name(_name),
        type(_type),
        equipment(_eq),
        st(_st),
        nd(_nd),
        crash_ev(crash_ev)
    {
        initRule(evts, id);
        initParam(evts, id);
        initPrecedence(evts, id);
    }

    CropOperation::~CropOperation() {}

    int CropOperation::getId() {
        return id;
    }
    int CropOperation::getId() const {
        return id;
    }

    std::string CropOperation::getName() {
        return name;
    }
    std::string CropOperation::getName() const {
        return name;
    }

    std::string CropOperation::getType() {
        return type;
    }
    std::string CropOperation::getType() const {
        return type;
    }

    std::string CropOperation::getEquipment() {
        return equipment;
    }
    std::string CropOperation::getEquipment() const {
        return equipment;
    }


    void CropOperation::setSt(std::string _st) {
        st=_st;
    }
    std::string CropOperation::getSt() {
        return st;
    }
    std::string CropOperation::getSt() const {
        return st;
    }


    void CropOperation::setNd(std::string _nd) {
        nd=_nd;
    }
    std::string CropOperation::getNd() {
        return nd;
    }
    std::string CropOperation::getNd() const {
        return nd;
    }

    CropOperation::t_map_rules CropOperation::getRules(){
        return map_rules;
    }
    CropOperation::t_map_rules CropOperation::getRules() const{
        return map_rules;
    }

    CropOperation::t_lst_string CropOperation::getLstRules(){
        t_lst_string lst;
        for(CropOperation::const_it_rules it = map_rules.begin(); it != map_rules.end(); it++){
            std::string rl= it->first;
            lst.push_back(rl);
        }
        return lst;
    }
    CropOperation::t_lst_string CropOperation::getLstRules() const {
        t_lst_string lst;
        for(CropOperation::const_it_rules it = map_rules.begin(); it != map_rules.end(); it++){
            std::string rl= it->first;
            lst.push_back(rl);
        }
        return lst;
    }

    std::string CropOperation::getEvent() {
        return crash_ev;
    }
    std::string CropOperation::getEvent() const {
        return crash_ev;
    }

    CropOperation::t_lst_param CropOperation::getParamEv(){
        return lst_ParamEv;
    }
    CropOperation::t_lst_param CropOperation::getParamEv() const{
        return lst_ParamEv;
    }

    CropOperation::t_lst_prec CropOperation::getPrec() {
        return lst_Prec;
    }
    CropOperation::t_lst_prec CropOperation::getPrec() const {
        return lst_Prec;
    }


    void 	CropOperation::setYrIndex(pair<int,int> _ind){
        YrIndex=_ind;
    }

    std::pair<int,int> CropOperation::getYrIndex(){
	    return YrIndex;
    }
    std::pair<int,int> CropOperation::getYrIndex() const {
	    return YrIndex;
    }

    void CropOperation::initRule(const vd::InitEventList & evts, const int _id) {
	    CrashDBS cdb(evts);
	    // Rules
	    std::string sql = "SELECT DISTINCT rl.* FROM (SELECT id, cast(regexp_split_to_table(cop.lst_rule, E',') as integer) as ref_rl FROM proceduralknowledge.cropoperation cop WHERE cop.id =";
	    sql.append(lexical_cast<string>(_id));
	    sql.append(" ) as foo, proceduralknowledge.rule rl WHERE ref_rl = rl.id");
	    PGresult* db_rule = cdb.QueryDB(sql.c_str());

	    for(int j=0; j < PQntuples(db_rule); j++) {
		    int idRule = lexical_cast<int>(PQgetvalue(db_rule, j, PQfnumber(db_rule, "id")));
		    std::string nameRule = lexical_cast<string>(PQgetvalue(db_rule, j, PQfnumber(db_rule, "name")));

		    // predicate
		    std::string sql = " SELECT pr.id, pr.crash_pred FROM (SELECT rl.name as name , cast(regexp_split_to_table(rl.lst_pred, E',') as integer) as ref_pr FROM proceduralknowledge.rule rl WHERE rl.id = ";
		    sql.append(lexical_cast<string>(idRule));
		    sql.append(" ) AS fooo, proceduralknowledge.predicate pr WHERE pr.id=ref_pr ");
		    PGresult* db_pred = cdb.QueryDB(sql.c_str());
		    t_lst_pred lst_pred;
		    for(int k=0; k < PQntuples(db_pred); k++) {
			    t_pred pred;
			    pred.crash_pr = lexical_cast<string>(PQgetvalue(db_pred, k, PQfnumber(db_pred, "crash_pred")));
			    pred.id =  lexical_cast<int>(PQgetvalue(db_pred, k, PQfnumber(db_pred, "id")));
			    // param predicate
			    std::string sql = "SELECT pr.* FROM (SELECT pred.id, pred.name as name , cast(regexp_split_to_table(param, E',') as integer) as ref_param FROM proceduralknowledge.predicate pred WHERE  pred.id =";
			    sql.append(lexical_cast<string>(pred.id));
			    sql.append(") as foo, proceduralknowledge.param_predicate pr WHERE pr.id = ref_param");
			    PGresult* db_param = cdb.QueryDB(sql.c_str());
			    for (int l = 0 ; l < PQntuples(db_param); l++) {
				    t_param param;
				    param.value = lexical_cast<std::string>(PQgetvalue(db_param, l, PQfnumber(db_param, "value")));
				    param.type = lexical_cast<string>(PQgetvalue(db_param, l, PQfnumber(db_param, "type")));
				    param.name = lexical_cast<string>(PQgetvalue(db_param, l, PQfnumber(db_param, "name")));
				    pred.lst_ParamPr.push_back(param);
			    }
			    lst_pred.push_back(pred);
		    }
		    map_rules[nameRule] = lst_pred;
	    }
    }

    void CropOperation::initParam(const vd::InitEventList & evts, const int _id) {
	    CrashDBS cdb(evts);
	    std::string sql = "SELECT DISTINCT param.* FROM (SELECT ev.id as ev_id, cast(regexp_split_to_table(ev.param, E',') as integer) as ref_param FROM (SELECT cop.id, event FROM proceduralknowledge.cropoperation cop WHERE cop.id =";
	    sql.append(lexical_cast<string>(_id));
	    sql.append(") AS foo, proceduralknowledge.event ev WHERE ev.id = foo.event) as fooo, proceduralknowledge.param_event param WHERE ref_param = id;");
	    PGresult* db_oe = cdb.QueryDB(sql.c_str());

	    for(int i=0; i < PQntuples(db_oe); i++){
		    CropOperation::t_param param;
		    param.value = lexical_cast<std::string>(PQgetvalue(db_oe, i, PQfnumber(db_oe, "value")));
		    param.type = lexical_cast<string>(PQgetvalue(db_oe, i, PQfnumber(db_oe, "type")));
		    param.name = lexical_cast<string>(PQgetvalue(db_oe, i, PQfnumber(db_oe, "name")));
		    lst_ParamEv.push_back(param);
	    }


    }

    void CropOperation::initPrecedence(const vd::InitEventList & evts, const int _id) {
	    CrashDBS cdb(evts);
	    std::string sql = "SELECT prec.*, cop2.name as name2 FROM proceduralknowledge.precedence prec, proceduralknowledge.cropoperation cop, proceduralknowledge.cropoperation cop2 WHERE prec.first_cop = cop.id AND prec.second_cop = cop2.id AND cop.id =";
	    sql.append(lexical_cast<string>(_id));

	    PGresult* db_pc = cdb.QueryDB(sql.c_str());
	    int nb_pc = PQntuples(db_pc);
	    for(int i=0; i < nb_pc; i++) {
		    CropOperation::t_prec pcstr;
		    pcstr.type = lexical_cast< string >(PQgetvalue(db_pc, i, PQfnumber(db_pc, "type")));
		    pcstr.mintimelag = lexical_cast< int >(PQgetvalue(db_pc, i, PQfnumber(db_pc, "tmin")));
		    pcstr.maxtimelag = lexical_cast< int >(PQgetvalue(db_pc, i, PQfnumber(db_pc, "tmax")));
		    pcstr.second = lexical_cast< string >(PQgetvalue(db_pc, i, PQfnumber(db_pc, "name2")));
		    lst_Prec.push_back(pcstr);
	    }
    }

} //namespace crash
