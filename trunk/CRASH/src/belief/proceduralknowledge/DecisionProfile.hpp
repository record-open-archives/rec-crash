
/**
 * @file DecisionProfileuccession.hpp
 * @author The vle Development Team
 */

/*
 * VLE Environment - the multimodeling and simulation environment
 * This file is a part of the VLE environment (http://vle.univ-littoral.fr)
 * Copyright (C) 2003 - 2009 The VLE Development Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef RECORD_DecisionProfile_HPP
#define RECORD_DecisionProfile_HPP

#include <string>
#include <ostream>
#include <vector>
#include <boost/lexical_cast.hpp>

//Crash file
#include <belief/structuralknowledge/systemknowledge/farmland/LandUnits.hpp> 


using namespace std;
namespace vd = vle::devs;
namespace vv = vle::value;

namespace crash
{
    /**
     * @brief 	Decision profile is a class that store key decision variable
     */
    class  DecisionProfile
    {
    public:
	typedef std::map<std::string, bool> t_CstrMap;
	typedef std::map<std::string, bool> t_CropDomainMap;
	typedef std::map<std::string, std::vector<int> > t_eqManagementunitMap;

	typedef std::map<std::string,  std::vector<pair <int,double> > > t_kpMap;
	typedef std::map<std::string,  std::vector<pair <int,int> > > t_multiRotationMap;

	/**
	 * @brief Default constructor
	 */
	DecisionProfile();
	/**
	 * @brief Default destructor
	 */
	virtual ~DecisionProfile();
	/**
	 * @brief Return a Crop object instantiate in singleton method.
	 * @return A reference to the singleton object.
	 */
	inline static DecisionProfile& getInstance()
	{
	    if (mDecisionProfile == 0)
	    {
		mDecisionProfile = new DecisionProfile;
	    }
	    return *mDecisionProfile;
	}

	/**
	 * @brief Initialize the DecisionProfile singleton.
	 */
	inline static void kill()
	{
	    delete mDecisionProfile;
	    mDecisionProfile = 0;
	}

	/**
	 * @brief Horizon, number of year, to be considered for the decision of crop sequences
	 */
	void setHorizon(const int _value);
	int getHorizon();
	int getHorizon() const;

	/**
	 * @brief Historic to be considered as constraint (return time of crops) for the decision of crop sequences
	 */
	void setSizeHisto(const int _value);
	int getSizeHisto();
	int getSizeHisto() const;

	t_eqManagementunitMap setMULst(const vle::value::Map& lst);

	/**
	 * @brief Set and Get  map of management unitfor which the rotation length is defined
	 */
	void setMultiRotation(const vle::value::Map& lst);
	t_multiRotationMap getMultiRotation();
	t_multiRotationMap getMultiRotation() const;

	/**
	 * @brief Set and Get  map of management unit where landunit are equally treated in terms of crop allocation
	 */
	void setEqManagementUnit(const vle::value::Map& lst);
	t_eqManagementunitMap getEqManagementUnit();
	t_eqManagementunitMap getEqManagementUnit() const;

	/**
	 * @brief Set and Get  map of cropping block for which similar spatial allocation is favored
	 */
	void setSameSpCroppingblock(const vle::value::Map& lst);
	t_eqManagementunitMap getSameSpCroppingblock();
	t_eqManagementunitMap getSameSpCroppingblock() const;

	/**
	 * @brief Set and Get  map of management unit for which grouping is applied
	 */
	void setSameSpGrouping(const vle::value::Map& lst);
	t_eqManagementunitMap getSameSpGrouping();
	t_eqManagementunitMap getSameSpGrouping() const;

	/**
	 * @brief Set and Get  map of cropping block for which similar temporal allocation is favored
	 */
	void setSameTpCroppingblock(const vle::value::Map& lst);
	t_eqManagementunitMap getSameTpCroppingblock();
	t_eqManagementunitMap getSameTpCroppingblock() const;

	/**
	 * @brief Set and Get  map of cropping block for which limited kp is defined
	 */
	void setKpCroppingblock(const vle::value::Map& lst);
	t_kpMap getKpCroppingblock();
	t_kpMap getKpCroppingblock() const;

	/**
	 * @brief Set and Get  Cost limit 
	 */
	void setLimCost(int _cost);
	int getLimCost();
	int getLimCost() const;

	/**
	 * @brief Set of options that define the constraints for the WCSP
	 */
	void setCstrOption(const std::string& _option, const bool& _value);
	const t_CstrMap& getCstrOption() const;
	void seeCstrOption() const;

	/**
	 * @brief Set of options that reduce the domain of crop
	 */
	void setCropDomainOption(const std::string& _option, const bool& _value);
	const t_CropDomainMap & getCropDomainOption() const;
	void seeCropDomainOption() const;

	/**
	 * @brief Set and Get  Cost limit 
	 */
	int getNbVar(){
	    return (LandUnits::getInstance().size() * (getHorizon() + getSizeHisto()));
	}


	/**
	 * @brief Initialize the DecisionProfile Classes
	 */
	void init(const vd::InitEventList& evts);


    private:
	/**
	 * @brief Classes of constraits to be generated
	 */
	t_CstrMap m_cstr_option;

	/**
	 * @brief Horizon of solution
	 */
	int Horizon;

	/**
	 * @brief Historic to be considered
	 */
	int Historic;

	/**
	 * @brief map of kp max for every croppingblock
	 */
	t_kpMap kpMap;

	/**
	 * @brief map of for rotation length for every croppingblock
	 */
	t_multiRotationMap multiRotationMap;

	/**
	 * @brief land crop domain options
	 */
	t_CropDomainMap m_cropDomain_option;

	/**
	 * @brief map of management unit where landunit are equally treated in terms of crop allocation
	 */
	t_eqManagementunitMap eqManagementUnit;

	/**
	 * @brief map of cropping block unit for which similar spatial allocation is favored
	 */
	t_eqManagementunitMap sameSpCroppingblock;

	/**
	 * @brief map of management unit for which grouping constraint is applied
	 */
	t_eqManagementunitMap sameSpGrouping;


	/**
	 * @brief map of cropping block unit for which similar temporal allocation is favored
	 */
	t_eqManagementunitMap sameTpCroppingblock;

	/**
	 * @brief cost for the WCSP
	 */
	int LimCost;

	/**
	 * @brief The static variable DecisionProfile for singleton design pattern.
	 */
	static DecisionProfile*  	mDecisionProfile;
    };
} // end namespace crash
#endif
