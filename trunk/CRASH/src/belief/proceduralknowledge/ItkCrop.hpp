#ifndef ITKCROP_HPP
#define ITKCROP_HPP

#include <belief/proceduralknowledge/CropOperation.hpp>

#include <vector>
#include <map>
#include <string>

//Crash Utils
#include <utils/CrashDBS.hpp>
#include <utils/Utils.hpp>

namespace vd=vle::devs;
namespace vv=vle::value;

namespace crash {

  class ItkCrop {

  public:

    typedef std::vector < int > t_vectInt;
    typedef std::vector < CropOperation > t_vectOp;
    typedef std::vector < std::string > t_vectString;
    typedef std::map < std::string, t_vectString > t_mapRule;
    typedef t_vectInt::const_iterator const_iterator;
    typedef t_vectInt::size_type size_type;

    /**
     * @brief Default constructor
     */
    ItkCrop();

    /**
     * @brief Main constructor
     */
    ItkCrop(const int & _indexCrop, const std::string & _codeCrop,
	    const int & precedCrop,
	    const int & succedCrop,
	    const t_vectInt & domainVariety,
	    const t_vectInt & domainSoil);

    /**
     * @brief Default destructor
     */
    virtual ~ItkCrop();

    /**
     * @brief Set index of crop
     */
    void setIdCrop(const int & _indexCrop);

    /**
     * @brief Set code crop
     */
    void setCodeCrop(const std::string & _code);

    /**
     * @brief Set Preceeding crop
     */
    void setPrecedCrop(const int & precedCrop);

    /**
     * @brief Set Succeeding crop
     */
    void setSuccedCrop(const int & succedCrop);

    /**
     * @brief Get index crop
     */
    int getIdCrop();
    int getIdCrop() const;

    /**
     * @brief Get code crop
     */
    std::string getCodeCrop();
    std::string getCodeCrop() const;

    /**
     * @brief Get Preceeding crop
     */
    int getPrecedCrop();
    int getPrecedCrop() const;

    /**
     * @brief Get Succeeding crop
     */
    int getSuccedCrop();
    int getSuccedCrop() const;

    /**
     * @brief Get Domain crop variety
     */
    int getDomainVariety();
    int getDomainVariety() const;

    /**
     * @brief Get Domail soil
     */
    int getDomainSoil();
    int getDomainSoil() const;

    /**
     * @brief add CropOperation object
     */
    void addCropOperation(const t_vectOp & _operation);

    /**
     * @brief Get vector of CropOperation object
     */
    t_vectOp getVectOperation();
    t_vectOp getVectOperation() const;

    /**
     * @brief
     */
    void buildItk(const std::string & _charList, const int & year,
		  const int & id_plot);

    /**
     * @brief
     */
    bool concatenateRules(ofstream & _rulePredicates, const int & id_plot);

    /**
     * @brief
     */
    bool concatenateActivities(ofstream & _activities, const int & year,
			       const int & plotU);

    /**
     * @brief
     */
    bool concatenatePreceedingConstraint(ofstream & pc, const int & id_plot,
					 const int & year);

  private:

    /** @brief Index of crop */
    int IdCrop;
    /** @brief Code of crop */
    std::string codeCrop;
    /** @brief Preceding crop */
    int preceding_crop;
    /**  @brief Succeeding crop */
    int succeding_crop;
    /**  @brief Domain crop variety */
    t_vectInt domain_cropvariety;
    /** @brief Domain soil variety */
    t_vectInt domain_soil;
    /** @brief vector of CropOperation object*/
    t_vectOp cp_operation;

  };

} //namespace crash

#endif //ITKCROP_HPP
