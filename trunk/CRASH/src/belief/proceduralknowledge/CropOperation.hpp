#ifndef CROPOPERATION_HPP
#define CROPOPERATION_HPP


#include <boost/lexical_cast.hpp>

#include <iostream>
#include <string>
#include <vector>
#include <fstream>

#include <utils/CrashDBS.hpp>
#include <utils/Utils.hpp>
#include <vle/utils/Exception.hpp>
#include <utils/System.hpp>

namespace vd=vle::devs;
namespace vv=vle::value;

using vle::utils::ArgError;
using namespace std;
using namespace boost;

namespace crash {

    class CropOperation {

        public:

            typedef vector< std::string > t_lst_string ;

            // param
            typedef struct ev_param{
                std::string name;
                std::string type;
                std::string value;
            } t_param;
	    typedef vector < t_param > t_lst_param;
	    typedef map<std::string , t_lst_param> t_map_param;

            // Preceeding Contraints
            typedef	struct prec{
                std::string type;
                std::string second;
                int mintimelag;
                int maxtimelag;
            } t_prec;
            typedef vector< t_prec > t_lst_prec;

	    // Rules and predicates
	    typedef struct pred{
		    int id;
		    std::string crash_pr;
		    t_lst_param lst_ParamPr;
	    } t_pred;
	    typedef vector<t_pred> t_lst_pred;
            typedef std::map < std::string, t_lst_pred > t_map_rules;
            typedef t_map_rules::const_iterator const_it_rules;

            /**
             * @brief Main constructor
             */
            CropOperation(int _id, std::string _name, std::string _type, std::string _eq, std::string _st,  std::string _nd, std::string crash_ev , const vd::InitEventList& evts);

            /**
             * @brief Main destructor
             */
            virtual ~CropOperation();

            /**
             * @brief Get index operation
             */
            int getId();
            int getId() const;

            /**
             * @brief Get name of operation
             */
            std::string getName();
            std::string getName() const;

            /**
             * @brief Get type operation
             */
            std::string getType();
            std::string getType() const;

            /**
             * @brief Get Reference equipment
             */
            std::string getEquipment();
            std::string getEquipment() const;

            /**
             * @brief Get date start
             */
            void setSt(std::string _st);
            std::string getSt();
            std::string getSt() const;

            /**
             * @brief Get date end
             */
            void setNd(std::string _nd);
            std::string getNd();
            std::string getNd() const;

            /**
             * @brief Get list of rules			 */
            t_map_rules getRules();
            t_map_rules getRules() const;

            t_lst_string getLstRules();
            t_lst_string getLstRules() const;

            std::string getEvent();
            std::string getEvent() const;

            t_lst_param getParamEv();
            t_lst_param getParamEv() const;

            t_lst_prec getPrec();
            t_lst_prec getPrec() const;


            void setYrIndex(pair<int,int> _ind);

            /**
             * @brief Init Rules and Predicates
             */
            void initRule(const vd::InitEventList & evts, const int _id);
            /**
             * @brief Init Events
             */
            void initParam(const vd::InitEventList & evts, const int _id);
            /**
             * @brief Init PreceedingConstraint
             */
            void initPrecedence(const vd::InitEventList & evts, const int _id);


            std::pair<int,int> getYrIndex();
            std::pair<int,int> getYrIndex() const;

        private:
            /** @brief Index operation */
            int id;
            /** @brief Name operation */
            std::string name;
            /** @brief Type of operation */
            std::string type;
            /** @brief Reference equipment */
            std::string equipment;
            /** @brief date start */
            std::string st;
            /** @brief date end */
            std::string nd;
            /** @brief Year index*/
            std::pair<int,int> YrIndex;

            /** @brief name of event*/
	    std::string crash_ev;

            /** @brief map of param event*/
            t_lst_param lst_ParamEv;

            /** @brief List of Preceeding constraints*/
            t_lst_prec lst_Prec;
            /** @brief Rule and predicates of all the ITK */
            CropOperation::t_map_rules map_rules;
    };

} //namespace crash

#endif //CROPOPERATION_HPP
