/** 
 * @file SingletonBelief.hpp
 * @brief Patron(template) de classe SingletonBelief à deux parametres <T,U>
 * T = classe singleton qui va dériver du patron de classe SingletonBelief<T,U>
 * U = classe gérée par la classe singleton T
 *
 * @author D. AMENGA MBENGOUE
 *
 * @date 20 janvier 2011
 */
#ifndef SINGLETONBELIEF_HPP
#define SINGLETONBELIEF_HPP

#include <string>
#include <ostream>
#include <vector>
#include <boost/lexical_cast.hpp>
#include <iostream> 

using namespace std;

namespace crash {

    template<class T, class U> class SingletonBelief {

    protected:
	/** @brief constructeur */
	SingletonBelief() : ListObject_lst() {
	    nb_elements = size();
	}
	/** @brief destructeur */
	virtual ~SingletonBelief() {}

    public: 
	typedef typename std::vector<int> t_Id_lst;
	typedef typename std::vector<U> ListObject;
	typedef typename ListObject::iterator iterator;
	typedef typename ListObject::const_iterator const_iterator;
	typedef typename ListObject::size_type size_type;

	/** @brief création d'un objet de type T */
	inline static T & getInstance() {
	    if(_singleton == 0) {
		_singleton = new T;
	    } 
	    return * _singleton;
	}

	/** @brief destruction d'un object de type T */
	inline static void kill() {
	    delete _singleton;
	    _singleton = 0;
	}  

	/** @brief surdéfinition de l'opérateur << */
	inline friend std::ostream & operator << (std::ostream & out, const T &ob) {
	    for(typename T::size_type i=0; i != ob.size(); ++i) {
		out << endl << (ob.at(i));
	    }
	    return out;
	}  

	/** 
	 * @brief fonction qui renvoie le premier élément du vecteur
	 * ListObject_lst 
	 * @return premier élément de ListObject_lst
	 */
	iterator begin() {
	    return ListObject_lst.begin();
	}

	const_iterator begin() const {
	    return ListObject_lst.begin();
	}

	/** 
	 * @brief fonction qui renvoie le dernier élément du vecteur
	 * ListObject_lst 
	 * @return dernier élément de ListObject_lst
	 */
	iterator end() {
	    return ListObject_lst.end();
	}

	const_iterator end() const {
	    return ListObject_lst.end();
	}

	/** 
	 * @brief fonction qui renvoie la taille du vecteur ListObject 
	 * @return la taille de ListObject_lst
	 */
	size_type size() const {
	    return ListObject_lst.size();
	}

	/** 
	 * @brief fonction qui renvoie les valeurs du vecteur ListObject
	 * @param _n indice d'une valeur du vecteur 
	 * @return les valeurs de ListObject_lst
	 */
	U & at(const size_type _n) {
	    return ListObject_lst.at(_n);
	}

	const U & at(const size_type _n) const {
	    return ListObject_lst.at(_n);
	}

	/** 
	 * @brief fonction qui permet d'ajouter une nouvelle valeur par référence 
	 * après la dernière valeur du vecteur ListObject
	 * @param objet de type U 
	 * @return les valeurs de ListObject_lst
	 */
	void add(const U &  _object) {
	    ListObject_lst.push_back(_object);
	    nb_elements = size();
	}

	/** 
	 * @brief fonction de type U qui renvoie pour un indice donné, inférieur à 
	 * la taille du vecteur ListObject, la valeur correspondante du vecteur
	 * @param _index un indice du vecteur
	 * @return valeur d'un éléments du vecteur
	 */
	U getAt(const size_type & _index) {
	    if(_index < nb_elements) {
		return ListObject_lst.at(_index);
	    }
	}


	/** 
	 * @brief getById get the object of type U using the id 
	 * @param _id int id of the object of type U
	 * @return U object
	 */
	U getById(const int & _id) {
	    for(size_type i = 0; i != ListObject_lst.size(); ++i) {
	      U object = at(i);
		if(object.getId() == _id) return object;
	    }
	    //throw std::runtime_error("U:: getById");
	}

	/** @brief fonction qui recherche un objet de type U */
	bool find(const U  & _object) {
	    for(size_type i=0; i !=  ListObject_lst.size(); ++i) {
		U object = at(i);
		if(object.getId() == _object.getId()) {
		    return true;
		}
	    }
	    return false;
	}

	/** 
	 * @brief fonction qui renvoie la taille du vecteur d'objet de type U 
	 * @return nb_elements taille de ListObject_lst
	 */
	int getNbObject() {
	    return nb_elements;
	}

	int getNbObject() const {
	    return nb_elements;
	}

      /**
       * @brief Function returns a vector of the id object 
       * @return 
       */
      t_Id_lst getIdLst() {
	t_Id_lst vecId;
	for(size_type i = 0; i != ListObject_lst.size(); ++i) {
	  vecId.push_back(getAt(i).getId());
	}
	return vecId;
      }

    protected:
	/** @brief pointeur statique de type T */
	static T * _singleton;
	/** @brief identification number */
	int id;
	/** @brief nombre d'éléments du vecteur ListObject_lst (taille) */
	int nb_elements;
	/** @brief vecteur d'éléments de type U */
	ListObject ListObject_lst;

    };

    /** 
     * @brief déclaration du pointeur satique de type T 
     * à l'extérieur de la déclaration de la classe
     */
    template<typename T, typename U> T * SingletonBelief<T,U>::_singleton = 0;

} //end namespace crash

#endif //SINGLETONBELIEF_HPP
