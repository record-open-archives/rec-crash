/**
 * @file CropSuccession.hpp
 * @author The vle Development Team
 */

/*
 * VLE Environment - the multimodeling and simulation environment
 * This file is a part of the VLE environment (http://vle.univ-littoral.fr)
 * Copyright (C) 2003 - 2009 The VLE Development Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <belief/structuralknowledge/expertknowledge/CropSuccession.hpp>

namespace crash {
        	int CropSuccession::CropSuccession_counter = 0; 
		
			CropSuccession::CropSuccession():
				id(++CropSuccession_counter), 
				ref_crop(0), 
				ref_preceeding(0),
				kp(0)
			{};
			
			CropSuccession::CropSuccession(const int& _ref_crop,
			                const int& _ref_preceeding,
			                const double& _kp):
				id(++CropSuccession_counter), 
				ref_crop(_ref_crop), 
				ref_preceeding(_ref_preceeding),
				kp(_kp)
			{};
			
			CropSuccession::CropSuccession(const int& _id,
			                const int& _ref_crop,
			                const int& _ref_preceeding,
			                const double& _kp):
			    id(_id), 
				ref_crop(_ref_crop), 
				ref_preceeding(_ref_preceeding),
				kp(_kp)
			{++CropSuccession_counter; }
			
			CropSuccession::CropSuccession(const CropSuccession& _CropSuccession):
				id(_CropSuccession.getId()), 
				ref_crop(_CropSuccession.getRef_crop()), 
				ref_preceeding(_CropSuccession.getRef_prec()),
				kp(_CropSuccession.getKp())
			{++CropSuccession_counter;}
				
	
} // namespace crash

