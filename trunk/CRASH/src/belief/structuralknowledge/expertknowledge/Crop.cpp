/**
 * @file
 * @author RECORD Team,
 *   http://record.toulouse.inra.fr ,
 *   Applied Mathematics and Informatics Research Division &
 *   Environment and Agronomy Research Division,
 *   French National Institute for Agricultural Research
 * @version d: $
 */

/*
 * Copyright (C) 2009 - INRA
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */


#include <belief/structuralknowledge/expertknowledge/Crop.hpp>

namespace crash
{

  int Crop::getId() const {
    return id;
  }

  int Crop::getId() {
    return id;
  }

  int Crop::getIdDB() const {
    return idDB;
  }

  int Crop::getIdDB() {
    return idDB;
  }

  string Crop::getCode() const {
    return code;
  }

  string Crop::getType() const {
    return type;
  }

  int Crop::getRtRot() const {
    return d_rt_rot;
  }


  void Crop::setTmnSow(double _tmn){
	  d_tmn_sow = _tmn;
  }
  double Crop::getTmnSow() {
    return d_tmn_sow;
  }
  double Crop::getTmnSow() const {
    return d_tmn_sow;
  }


  void Crop::setH2oHrv(double _h2orec) {
    d_h2o_hrv = _h2orec;
  }
  double Crop::getH2oHrv() {
    return d_h2o_hrv;
  }
  double Crop::getH2oHrv() const {
    return  d_h2o_hrv;
  }

  void Crop::setInnFer(double _inn) {
    d_inn_fer = _inn;
  }

  double Crop::getInnFer() {
    return d_inn_fer;
  }

  double Crop::getInnFer() const {
    return d_inn_fer;
  }

  void Crop::setSwfacIrr(double _swfac) {
    d_swfac_irr = _swfac;
  }

  double Crop::getSwfacIrr() {
    return d_swfac_irr;
  }

  double Crop::getSwfacIrr() const {
	  return d_swfac_irr;
  }

  void Crop::addCropVariety(std::string code_variety, int id_variety, std::string st_sow, std::string nd_sow, std::string st_hr, std::string nd_hr, double density_sow){
	  t_variety var;
	  var.st_sow = st_sow;
	  var.nd_sow = nd_sow;
	  var.st_hr = st_hr;
	  var.nd_hr = nd_hr;
	  var.id_variety=id_variety;
	  var.density_sow = density_sow;
	  map_variety.insert(pair<std::string, t_variety >(code_variety, var));
  }

  Crop::t_variety Crop::getVariety(std::string _code_var){
	  const_it_variety var = map_variety.find(_code_var);
		if(var!=map_variety.end()){
		  return var->second;
	  }
	  throw std::runtime_error("ITK:: getOperationByType, operation not found");
  }

  Crop::t_variety Crop::getVariety(std::string _code_var)const {
	  t_variety v = map_variety.find(_code_var)->second;
	  return v;
  }
} // namespace crash

