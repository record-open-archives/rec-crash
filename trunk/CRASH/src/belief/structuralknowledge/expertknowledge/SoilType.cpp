/**
 * @file
 * @author RECORD Team,
 *   http://record.toulouse.inra.fr ,
 *   Applied Mathematics and Informatics Research Division &
 *   Environment and Agronomy Research Division,
 *   French National Institute for Agricultural Research
 * @version d: $
 */

/*
 * Copyright (C) 2009 - INRA
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */


#include <belief/structuralknowledge/expertknowledge/SoilType.hpp>

namespace crash {
	SoilType::SoilType():
		Id(0),
		IdDB(0),
		CodeSoil("unknown soiltype"),
		Clay(0),
		Silt(0),
		Sand(0),
		d_hucc_sow(0)
	{}

	SoilType::SoilType(int id, int iddb, string _code, double clay, double silt, double sand, double d_hucc):
		Id(id),
		IdDB(iddb),
		CodeSoil(_code),
		Clay(clay),
		Silt(silt),
		Sand(sand),
		d_hucc_sow(d_hucc)
	{}

	SoilType::~SoilType() {}

	int SoilType::getId() const
	{
		return Id;
	}

	int SoilType::getIdDB()	const
	{
		return IdDB;
	}

	string SoilType::getCode() const
	{
		return CodeSoil;
	}

	double SoilType::getClay() const
	{
		return Clay;
	}

	double SoilType::getSand() const
	{
		return Sand;
	}

	double SoilType::getSilt() const
	{
		return Silt;
	}

	double SoilType::getDhuccSow() const
	{
		return d_hucc_sow;
	}

} // namespace crash

