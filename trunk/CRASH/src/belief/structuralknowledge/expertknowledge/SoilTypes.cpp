#include <belief/structuralknowledge/expertknowledge/SoilTypes.hpp>
#include <vle/utils/Path.hpp>
#include <vle/utils/Exception.hpp>

namespace crash {


    SoilTypes::SoilTypes() {}

    SoilTypes::~SoilTypes() {}



    SoilType SoilTypes::getByCode(std::string & _code) {
	for (size_type i = 0; i != size(); ++i) {
	    SoilType c = at(i);
	    if(c.getCode() == _code) {
		return c;
	    }
	}
	throw std::runtime_error("SoilTypes:: getByCode");
    }



    SoilTypes::size_type SoilTypes::init(const vd::InitEventList & evts) {
	std::string sql="SELECT * from expertknowledge.soiltype";
	CrashDBS cdb(evts);
	PGresult* db_SoilType= cdb.QueryDB(sql.c_str());
	int nb_SoilType =  PQntuples(db_SoilType);
	for (int i=0; i<nb_SoilType; i++) {
	    int idDB = lexical_cast<int>(PQgetvalue(db_SoilType, i, PQfnumber(db_SoilType, "id_soiltype")));
	    string name = lexical_cast<string>(PQgetvalue(db_SoilType, i, PQfnumber(db_SoilType, "name_soiltype")));
	    double mClay = lexical_cast<double>(PQgetvalue(db_SoilType, i, PQfnumber(db_SoilType, "clay")));
	    double mSilt = lexical_cast<double>(PQgetvalue(db_SoilType, i, PQfnumber(db_SoilType, "silt")));
	    double mSand = lexical_cast<double>(PQgetvalue(db_SoilType, i, PQfnumber(db_SoilType, "sand")));
	    double mSWACforSowing = lexical_cast<double>(PQgetvalue(db_SoilType, i, PQfnumber(db_SoilType, "swacforsowing")));
	    SoilType st(i+1, idDB, name, mClay, mSilt, mSand, mSWACforSowing);
	    add(st);
	}
	return nb_SoilType;
    }
}
