#include <belief/structuralknowledge/expertknowledge/CropDomains.hpp>

using namespace std;

namespace crash {
    // Constructor
    CropDomains::CropDomains()
    {};    
    

    // Destructor
    CropDomains::~CropDomains()
    {};

    void CropDomains::setDefaultDomain()
    {

	int nb_lu = LandUnits::getInstance().size();

	t_crop_lst croplst = Crops::getInstance().getIdLst();

	for(int i=0; i<nb_lu; i++)
	    {
		int id_lu = LandUnits::getInstance().at(i).getId();
		CropDomain cd(id_lu); 
		cd.setCropDomain(croplst);
		add(cd);
	    }
    }


    // initialization
    void CropDomains::init()
    {
	setDefaultDomain();
	//ont récupère les options pour réduire les domaines
	//	DecisionProfile::t_CropDomainMap Otps =  DecisionProfile::getInstance().getCropDomainOption();
    }

  
} //end namespace crash


