#include <belief/structuralknowledge/expertknowledge/Crops.hpp>

using namespace std;

namespace crash {
    // Constructor
    Crops::Crops()
    {};
    // Destructor
    Crops::~Crops()
    {};

    Crop & Crops::getByCode(const std::string& _code)
    {
	for (size_type i = 0; i != size(); ++i) {
	    Crop c = at(i);
	    if(c.getCode() == _code) {
		return c;
	    }
	}
	throw std::runtime_error("Crops:: getByCode");
    }

    Crops::t_crop_lst Crops::getCropTr(const int _value)
    {
	t_crop_lst lst;
	for (size_type i = 0; i != size(); ++i) {
	    Crop c = at(i);
	    if(c.getRtRot() > _value ) {
		lst.push_back(c);
	    }
	}
	return lst;
    }


    // initialization
    Crops::size_type	Crops::init(const vd::InitEventList & evts)
    {
	CrashDBS cdb(evts);
	cdb.DeleteDB("vle.cropid");

	PGresult* db_Crop= cdb.QueryDB("SELECT * from expertknowledge.crop");
	int nb =  PQntuples(db_Crop);

	for (int i=0; i < nb; i++) {
	    int idDB = lexical_cast<int>(PQgetvalue(db_Crop, i, PQfnumber(db_Crop, "id_crop")));
	    string name = lexical_cast<string>(PQgetvalue(db_Crop, i, PQfnumber(db_Crop, "name_crop")));
	    string code = lexical_cast<string>(PQgetvalue(db_Crop, i, PQfnumber(db_Crop, "code_crop")));
	    string type = lexical_cast<string>(PQgetvalue(db_Crop, i, PQfnumber(db_Crop, "type_crop")));
	    double d_rt = lexical_cast<double>(PQgetvalue(db_Crop, i, PQfnumber(db_Crop, "d_rt")));
	    double d_tminsow = lexical_cast<double>(PQgetvalue(db_Crop, i, PQfnumber(db_Crop, "d_tminsow")));
	    double d_h2orec = lexical_cast<double>(PQgetvalue(db_Crop, i, PQfnumber(db_Crop, "d_h2orec")));

	    double d_inn = 0.8;
	    double d_swfac = 0.6;

	    Crop c(i+1, idDB, code, type, d_rt, d_tminsow, d_h2orec, d_inn, d_swfac);
	    add(c);
	    char queryINSERTCS[250];
	    sprintf(queryINSERTCS, "INSERT INTO vle.cropid VALUES (%d,%d)", idDB,i+1);
	    cdb.InsertDB(queryINSERTCS);
	}
        return nb_elements;
    }

} //end namespace crash


