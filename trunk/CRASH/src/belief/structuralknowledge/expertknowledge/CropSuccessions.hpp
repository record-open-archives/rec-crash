#ifndef CROPSUCCESSIONS_HPP
#define CROPSUCCESSIONS_HPP

#include <string>
#include <ostream>
#include <vector>
#include <boost/lexical_cast.hpp>

#include <utils/CrashDBS.hpp>
//Crash
#include <belief/SingletonBelief.hpp>
//Expert knowledge
#include <belief/structuralknowledge/expertknowledge/CropSuccession.hpp>

using namespace std;
namespace vd=vle::devs;
namespace vv=vle::value;

namespace crash {

  /**
   * @brief The class CropSuccessions is a vector of CropSuccession object
   */
  class CropSuccessions : public SingletonBelief<CropSuccessions,CropSuccession>
  {

    friend class SingletonBelief<CropSuccessions, CropSuccession>;

  private:
    /**
     * @brief Default constructor
     */
    CropSuccessions();

    /**
     * @brief Default destructor
     */
    virtual ~CropSuccessions();

  public:

    /**
     * @brief Initialization of CropSuccession object
     */
    size_type init(const vd::InitEventList & evts);
    CropSuccession getByCropPrec(const int & _ref_crop, const int & _ref_prec );

  };

}

#endif //CROPSUCCESSIONS_HPP
