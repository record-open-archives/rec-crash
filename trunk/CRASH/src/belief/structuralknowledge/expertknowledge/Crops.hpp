/** @file Market.hpp 
 * @brief classe Market (classe singleton) qui dérive de la classe
 * SingletonBelief<Market, CropPrice>
 * Cette classe permet de stocker dans un vecteur des éléments de type CropPrice
 * qui correspondent aux prix des differents types de cultures 
 * d'une exploitation
 *
 * @author D. AMENGA MBENGOUE
 * @date 20 janvier 2011
 * @version 1
 */


#ifndef CROPS_HPP
#define CROPS_HPP

#include <string>
#include <ostream>
#include <vector>
#include <boost/lexical_cast.hpp>

#include <vle/extension/Decision.hpp>
#include <vle/utils.hpp>
#include <vle/devs.hpp>

//Crash
#include <belief/SingletonBelief.hpp>

// Crash procedural knowledge
#include <belief/proceduralknowledge/DecisionProfile.hpp>

// Crash expert knowledge
#include <belief/structuralknowledge/expertknowledge/Crop.hpp>

// Crash system knowledge
#include <belief/structuralknowledge/systemknowledge/farmland/LandUnits.hpp>
//Crash utils
#include <utils/CrashDBS.hpp>

using namespace std;
namespace vd=vle::devs;
namespace vv=vle::value;

namespace crash {

    class Crops : public SingletonBelief <Crops, Crop> {

	/** 
	 * @brief The class Crops is a vector of CropDomain object
	 */
	friend class SingletonBelief <Crops, Crop>;

    private:
	/** 
	 * @brief Default constructor 
	 */
	Crops();	
	
	/** 
	 * @brief Default destructor 
	 */
	virtual ~Crops();

    public:
	typedef vector<Crop> t_crop_lst;

	/**
	 * @brief Get crop using code
	 */
	Crop & getByCode(const std::string& _code);

	/**
	 * @brief Get crop using code
	 */
	t_crop_lst getCropTr(const int _value);

	/**
	 * @brief Initialize the object Crop
	 */
      size_type init(const vd::InitEventList & evts);

    private:

    };


} //end namespace crash


#endif //CROPDOMAINS_HPP
