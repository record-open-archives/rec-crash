/**
 * @file
 * @author RECORD Team,
 *   http://record.toulouse.inra.fr ,
 *   Applied Mathematics and Informatics Research Division &
 *   Environment and Agronomy Research Division,
 *   French National Institute for Agricultural Research
 * @version d: $
 */

/*
 * Copyright (C) 2009 - INRA
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#ifndef Crop_H
#define Crop_H

#include <vle/extension/Decision.hpp>
#include <vle/utils/Tools.hpp>
#include <vle/utils/DateTime.hpp>
#include <sstream>
#include <numeric>
#include <iterator>
#include <boost/algorithm/string.hpp>


using namespace std;
using boost::lexical_cast;
using boost::bad_lexical_cast;
namespace ve = vle::extension::decision;
namespace vd = vle::devs;
namespace vu = vle::utils;
namespace vv = vle::value;

namespace crash
{
    /**
     * @brief Crop: This class is part of the CrashAgent Belief. It contains all attributes
     * that the CrashAgent knows  about the Crop. Crop is part of the expertknowledge
     */
	class Crop
	{
		public:

			typedef struct cvar{
				int id_variety;
				std::string st_sow;
				std::string nd_sow;
				std::string st_hr;
				std::string nd_hr;
				double density_sow;
			} t_variety;
			typedef map < std::string, t_variety > t_map_variety;
			typedef t_map_variety::const_iterator const_it_variety;

			/**@brief Main default constructor */
			Crop():
				id(0),
				idDB(0),
				code("ZZ"),
				type("ZZ"),
				d_rt_rot(0),
				d_tmn_sow(0),
				d_h2o_hrv(0),
				d_inn_fer(0),
				d_swfac_irr(0)
		{}

			/** @brief Main constructor used at model initialization */
			Crop(int id, int idDB, string code, string type, double RRT,  double TfS, double _h2orec, double _inn, double _swfac):
				id(id),
				idDB(idDB),
				code(code),
				type(type),
				d_rt_rot(RRT),
				d_tmn_sow(TfS),
				d_h2o_hrv(_h2orec),
				d_inn_fer(_inn),
				d_swfac_irr(_swfac)
		{}


			/** @brief Default destructor */
			~Crop() {}

			/** @brief Get id of the crop */
			int getId() const;
			int getId();

			/** @brief Get id of the crop */
			int getIdDB() const;
			int getIdDB();

			/** @brief Get code of the crop */
			string getCode() const;

			/** @brief Get type of the crop (Winter or Summer) */
			string getType() const;

			/** @brief Get Th_RecoReturnTime */
			int getRtRot() const;


			/** @brief set d_tmn_swo */
			void setTmnSow(double _tmn);
			/** @brief Get Th_TminforSowing */
			double getTmnSow()	const;
			double getTmnSow();

			/** @brief Set the level water content of reserves */
			void setH2oHrv(double _h2orec);
			/** @brief Get the level water content of reserves */
			double getH2oHrv();
			double getH2oHrv() const;

			/** @brief Set the level of nitrogen nutrition index */
			void setInnFer(double _inn);
			/** @brief Get the level of nitrogen nutrition index */
			double getInnFer();
			double getInnFer() const;

			/** @brief */
			void setSwfacIrr(double _swfac);
			/** @brief */
			double getSwfacIrr();
			double getSwfacIrr() const;

			/** @brief add crop variety */
			void addCropVariety(std::string code_variety, int id_variety, std::string st_sow, std::string nd_sow, std::string st_hr, std::string nd_hr, double density_sow);

			t_variety getVariety(std::string _code_var);
			t_variety getVariety(std::string _code_var)const;
			/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
			inline friend std::ostream& operator<<(std::ostream& out, const Crop& c) {
				out << "\tid: " << c.getId() << "\t idDB: " << c.getIdDB() << endl;
				out << "\t\tCode: " << c.getCode() << endl;
				out << "\t\tType: " << c.getType() << endl;
				out << "\t\tH2O for harvesting: " << c.getH2oHrv();
				out << "\t\tINN for fertilization: " << c.getInnFer();
				return out;
			}

		protected:
			//  typedefs
			//  variables
			//  functions

		private:
			//  typedefs
			//  variables
			/** @brief Id */
			int id;
			/** @brief Id from database */
			int idDB;
			/** @brief Code Crop */
			string code;
			/** @brief Code Crop */
			string type;
			/** @brief Recommended return time */
			int d_rt_rot;
			/** @brief Minimum temperature for sowing */
			double d_tmn_sow;
			/** @brief The level of water content of reserves */
			double d_h2o_hrv;
			/** @brief The level of nitrogen nutrition index */
			double d_inn_fer;
			/** @brief the level of index of stomatal water stress */
			double d_swfac_irr;
			/** @brief map of crop variety */
			t_map_variety map_variety;
	};

} // namespace crash

#endif //Crop_H

