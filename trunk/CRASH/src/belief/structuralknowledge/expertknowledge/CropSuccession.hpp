/**
 * @file CropSuccession.hpp
 * @author The vle Development Team
 */

/*
 * VLE Environment - the multimodeling and simulation environment
 * This file is a part of the VLE environment (http://vle.univ-littoral.fr)
 * Copyright (C) 2003 - 2009 The VLE Development Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef RECORD_CropSuccession_HPP
#define RECORD_CropSuccession_HPP

#include <string>
#include <ostream>
#include <vector>
#include <boost/lexical_cast.hpp>

using namespace std;
using namespace boost;

namespace crash {
	/**
	* @brief 	Define a CropSuccession object to describe all parameter that 
	* 			characterize a crop succession
	*/
    class  CropSuccession
    {
		public:					
			/**
			* @brief Default constructor
			*/
			CropSuccession();
			
			/**
			* @brief Constructor 
			* @param _id_db
			* @param _ref_crop
			* @param _ref_preceeding
			* @param _kp
			*/
			CropSuccession( const int& _ref_crop,
			                const int& _ref_preceeding,
			                const double& _kp);
			
            /**
			* @brief Constructor 
			* @param _id
			* @param _id_db
			* @param _ref_crop
			* @param _ref_preceeding
			* @param _kp
			*/
			CropSuccession(const int& _id,
			                const int& _ref_crop,
			                const int& _ref_preceeding,
			                const double& _kp);
			
			/**
			* @brief Copy constructor
			* @param _CropSuccession 	a CropSuccession
			*/
			CropSuccession(const CropSuccession& _CropSuccession);
			
			virtual ~CropSuccession()
			{}
			
			inline int getId() { 	return id;  }
			inline int getId() const {return id;  }
			inline void setId(const int& _id) { id = _id;  }		

			inline int getRef_crop() { 	return ref_crop;  }
			inline int getRef_crop() const {return ref_crop;  }
			inline void setRef_crop(const int& _ref_crop) { ref_crop = _ref_crop;  }	

			inline int getRef_prec() { 	return ref_preceeding;  }
			inline int getRef_prec() const {return ref_preceeding;  }
			inline void setRef_prec(const int& _ref_preceeding) { ref_preceeding = _ref_preceeding;  }	
			
			inline double getKp() { 	return kp;  }
			inline double getKp() const {return kp;  }
			inline void setKp(const double& _kp) { kp = _kp;  }
			
			
			
			/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */							
			inline friend std::ostream& operator<<(std::ostream& out, const CropSuccession& cs)
			{
				out <<"\tid: " << cs.getId()	<< endl;
				out <<"\t\tref_crop: "<< cs.getRef_crop() << endl;
				out <<"\t\tref_preceeding: "<< cs.getRef_prec() <<endl;
				out <<"\t\tKp: "<< cs.getKp();
				return out;
			}
			
		private:
			int       		id; 			/* Identifier of a CropSuccession 	*/
			int             ref_crop;       /* Reference to a crop */
			int             ref_preceeding; /* Reference to a preceeding crop */
			double          kp;             /* kp see Leteinturier 2006*/    
			static int		CropSuccession_counter ; 
    };
	
} // end namespace crash

#endif

