#include <vle/utils/Path.hpp>
#include <vle/utils/Exception.hpp>

#include <belief/structuralknowledge/expertknowledge/CropSuccessions.hpp>

namespace crash {

  CropSuccessions::CropSuccessions() {}

  CropSuccessions::~CropSuccessions() {}


CropSuccession CropSuccessions::getByCropPrec(const int & _ref_crop, 
					      const int & _ref_prec) {
  for (size_type i = 0; i != ListObject_lst.size(); ++i) {
    CropSuccession cs = at(i);
        if(cs.getRef_crop() == _ref_crop and 
	   cs.getRef_prec() == _ref_prec) {
	  return cs;
        }
  }
    throw std::runtime_error("Crops:: getByCropPrec");
}
 


  CropSuccessions::size_type	CropSuccessions::init(const vd::InitEventList & evts) {
    CrashDBS cdb(evts);
    PGresult* db_CropSuccession= cdb.QueryDB("SELECT * from expertknowledge.cropsuccession");
    int nb =  PQntuples(db_CropSuccession);
    
    for (int i=0; i < nb; i++) {        
        int ref_crop = lexical_cast<int>(PQgetvalue(db_CropSuccession, i, PQfnumber(db_CropSuccession, "ref_crop")));
        int ref_preceeding = lexical_cast<int>(PQgetvalue(db_CropSuccession, i, PQfnumber(db_CropSuccession, "ref_preceeding")));
        double kp = lexical_cast<double>(PQgetvalue(db_CropSuccession, i, PQfnumber(db_CropSuccession, "kp")));
        CropSuccession cs(i+1, ref_crop, ref_preceeding, kp);
        add(cs);
    }
        return nb_elements;
}


} //namespace crash
