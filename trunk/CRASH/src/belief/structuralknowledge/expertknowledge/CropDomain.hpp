/**
 * @file
 * @author RECORD Team,
 *   http://record.toulouse.inra.fr ,
 *   Applied Mathematics and Informatics Research Division &
 *   Environment and Agronomy Research Division,
 *   French National Institute for Agricultural Research
 * @version d: $
 */

/*
 * Copyright (C) 2009 - INRA
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#ifndef CROPDOMAIN_H
#define CROPDOMAIN_H

#include <vle/extension/Decision.hpp>
#include <vle/utils/Tools.hpp>
#include <vle/utils/DateTime.hpp>
#include <sstream>
#include <numeric>
#include <iterator>
#include <boost/algorithm/string.hpp>

//Crash
# include <belief/structuralknowledge/expertknowledge/Crops.hpp>


using namespace std;
using boost::lexical_cast;
using boost::bad_lexical_cast;
namespace ve = vle::extension::decision;
namespace vd = vle::devs;
namespace vu = vle::utils;
namespace vv = vle::value;

namespace crash
{
    /**
     * @brief CropDomain: This class is part of the expertknowledge of the agent and represent the domain of land where crop could be grown
     */
    class CropDomain
    {
    public:
	typedef vector<int> t_crop_lst;

	/**
	 * @brief Main default constructor 
	 */
	CropDomain();

	/** 
	 * @brief Constructor 
	 */
	CropDomain(int _idlu);


	/** 
	 * @brief Default destructor 
	 */
	virtual ~CropDomain();

	/** 
	 * @brief Get id of the CropDomain
	 */
	int getId();
	int getId()	const;

	/** 
	 * @brief set the list of crops that are candidates to grow on the land unit 
	 */
	void setCropDomain(t_crop_lst _crop_lst);
	
	/** 
	 * @brief Get the list of crops that are candidates to grow on the land unit 
	 */
	const t_crop_lst & getCropDomain() const;

	int at_cropDom(int _n);
	int at_cropDom(int _n) const;

	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
	inline friend std::ostream& operator<<(std::ostream& out, const CropDomain& cd)
	{        
	    
	    out <<"CropDomain: \t"<< cd.getId() <<endl; 
	    for(int i = 0; i!=cd.cropDom_lst.size(); i++)
		{
		    out<<"\t"<<Crops::getInstance().getById(cd.at_cropDom(i)).getCode();
		}
	    out<<endl;
	    return out;
	}

    protected:

    private:
	/** 
	 * @brief Id of the cropDomain object: correspond the landunit ID.
	 */
	int id;
	/** 
	 * @brief Crop list
	 */
	t_crop_lst cropDom_lst;
	
       
    };

} // namespace crash

#endif //CROPDOMAIN_P

