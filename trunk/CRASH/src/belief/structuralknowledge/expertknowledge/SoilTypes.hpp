#ifndef TESTSOL_HPP
#define TESTSOL_HPP

#include <belief/SingletonBelief.hpp>
#include <string>
#include <ostream>
#include <vector>
#include <boost/lexical_cast.hpp>

//Crash
#include <belief/SingletonBelief.hpp>
#include <utils/CrashDBS.hpp>

//Crash Expert knowledge
#include <belief/structuralknowledge/expertknowledge/SoilType.hpp>

using namespace std;
using namespace boost;
namespace vd=vle::devs;
namespace vv=vle::value;

namespace crash {

  /**
   * @brief The class SoilTypes is a vector of SoilType object
   */
  class SoilTypes : public SingletonBelief<SoilTypes, SoilType> {

    friend class SingletonBelief<SoilTypes, SoilType>;

  private:

    /**
     * @brief Default constructor
     */
    SoilTypes();
    /**
     * @brief Default destructor
     */
    virtual ~SoilTypes();

  public:

    /**
     * @brief  Initialization of SoilType object from database
     */
    size_type init(const vd::InitEventList & evts);

    /**
     * @brief This function GET the name of Soil Type
     */
    SoilType getByCode(std::string & _code);

  };
}
#endif //TESTSOL_HPP
