/**
 * @file
 * @author RECORD Team,
 *   http://record.toulouse.inra.fr ,
 *   Applied Mathematics and Informatics Research Division &
 *   Environment and Agronomy Research Division,
 *   French National Institute for Agricultural Research
 * @version d: $
 */

/*
 * Copyright (C) 2009 - INRA
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#ifndef SoilType_H
#define SoilType_H

#include <vle/extension/Decision.hpp>
#include <vle/utils/Tools.hpp>
#include <vle/utils/DateTime.hpp>
#include <sstream>
#include <numeric>
#include <iterator>
#include <boost/algorithm/string.hpp>


using namespace std;
using boost::lexical_cast;
using boost::bad_lexical_cast;
namespace ve = vle::extension::decision;
namespace vd = vle::devs;
namespace vu = vle::utils;
namespace vv = vle::value;

namespace crash {
/**
 * @brief SoilType: This class is part of the CrashAgent Belief. It contains all attributes
 * that the CrashAgent knows  about the SoilType. SoilType is part of the expertknowledge
 */
class SoilType
{
public:
//  typedefs
//  variables

//  functions
	/**@brief Main default constructor */
	SoilType();

	/** @brief Main constructor used at model initialization */
	SoilType(int id, int iddb, string _code, double clay, double silt, double sand, double d_hucc);


	/** @brief Default destructor */
	virtual ~SoilType();

	/** @brief Get id of the soil type */
    int getId()	const;

	/** @brief Get id from the database of the soil type */
    int getIdDB()	const;

	/** @brief Get name of the soil type */
    string getCode()	const;

	/** @brief Get clay content of the soil type */
    double getClay() const;

	/** @brief Get sand content of the soil type */
    double getSand() const;

	/** @brief Get silt content of the soil type */
    double getSilt() const;

	/** @brief Get Th_SAWCforSowing */
    double getDhuccSow() const;

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
			inline friend std::ostream& operator<<(std::ostream& out, const SoilType& st)
			{
				out <<"\t"<< st.getCode() << "\tid: " << st.getId() << "\tidDB: " << st.getIdDB() << endl;
		    	out <<"\t\tClay: "<<st.getClay()<< "\tSand: "<<st.getSand() << "\tSilt: "<<st.getSilt() << endl;
		    	out <<"\t\t% of Hucc for Sowing: "<< st.getDhuccSow();
				return out;
			}



protected:
//  typedefs
//  variables
//  functions

private:
//  typedefs
//  variables
    /** @brief Id */
    int Id;
    /** @brief IdDB */
    int IdDB;
    /** @brief Code SoilType*/
    string CodeSoil;
    /** @brief Clay content (%)*/
    double Clay;
    /** @brief silt content (%)*/
    double Silt;
    /** @brief Sand content (%)*/
    double Sand;
    /** @brief Threshold of soil water content above which sowing cannot occur without too high risk of damaging the soil (see Leenhardt 2002) (% of SWAC). Th_SAWCforSowing is a decision variable*/
    double d_hucc_sow;
//  functions



};

} // namespace crash

#endif //SoilType_H

