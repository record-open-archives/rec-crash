/** @file Market.hpp 
 * @brief classe Market (classe singleton) qui dérive de la classe
 * SingletonBelief<Market, CropPrice>
 * Cette classe permet de stocker dans un vecteur des éléments de type CropPrice
 * qui correspondent aux prix des differents types de cultures 
 * d'une exploitation
 *
 * @author D. AMENGA MBENGOUE
 * @date 20 janvier 2011
 * @version 1
 */


#ifndef CROPDOMAINS_HPP
#define CROPDOMAINS_HPP

#include <string>
#include <ostream>
#include <vector>
#include <boost/lexical_cast.hpp>

#include <vle/extension/Decision.hpp>
#include <vle/utils.hpp>
#include <vle/devs.hpp>

//Crash
#include <belief/SingletonBelief.hpp>

// Crash procedural knowledge
#include <belief/proceduralknowledge/DecisionProfile.hpp>

// Crash expert knowledge
#include <belief/structuralknowledge/expertknowledge/Crops.hpp>
#include <belief/structuralknowledge/expertknowledge/CropDomain.hpp>

// Crash system knowledge
#include <belief/structuralknowledge/systemknowledge/farmland/LandUnits.hpp>

using namespace std;

namespace crash {

    class CropDomains : public SingletonBelief <CropDomains, CropDomain> {

	/** @brief The class CropDomains is a vector of CropDomain object
	 */
	friend class SingletonBelief <CropDomains, CropDomain>;

    private:
	/** 
	 * @brief Default constructor 
	 */
	CropDomains();
	

	/** 
	 * @brief Default destructor 
	 */
	virtual ~CropDomains();

    public:
	typedef vector<int> t_crop_lst;

	/**
	 * @brief Set the dfault crop domain: all crops are allowed to be grown on the landunit
	 */	
	void setDefaultDomain();

	/**
	 * @brief Initialize the object CropDomain
	 */
	void init();

    private:

    };


} //end namespace crash


#endif //CROPDOMAINS_HPP
