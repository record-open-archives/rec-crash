#include <belief/structuralknowledge/systemknowledge/offfarm/CropPrice.hpp>

namespace crash {


  void CropPrice::setPrice(const vv::Value & Price) {
    rotate(priceList.rbegin(), priceList.rbegin() + 1, priceList.rend());
    priceList[0]=Price.toDouble().value(); 
  }

  CropPrice::t_price_lst CropPrice::getPrice() const {
    return priceList;
  }
	
  string CropPrice::getCodeCrop() const {
    return code_crop;
  }

  int CropPrice::getId() const {
    return id;
  }	
	
 
} // namespace crash 

 
