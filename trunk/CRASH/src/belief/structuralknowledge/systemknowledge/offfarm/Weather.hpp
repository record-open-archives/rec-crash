/**
 * @file
 * @author RECORD Team,
 *   http://record.toulouse.inra.fr ,
 *   Applied Mathematics and Informatics Research Division &
 *   Environment and Agronomy Research Division,
 *   French National Institute for Agricultural Research
 * @version d: $
 */

/*
 * Copyright (C) 2009 - INRA
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */


#ifndef WEATHER_H
#define WEATHER_H


#include <iostream>
#include <fstream>
#include <string>

#include <vle/extension/Decision.hpp>
#include <vle/utils.hpp>
#include <vle/devs.hpp>

using namespace std;

namespace ve = vle::extension::decision;
namespace vd = vle::devs;
namespace vv = vle::value;
namespace vz = vle::vpz;
namespace vu = vle::utils;

namespace crash {
	/**
	 * @brief Weather: This class is the CrashAgent knowledge about the weather
	 */
	class Weather
	{
		public:
			typedef vector<double> t_lstDouble;
			typedef t_lstDouble::iterator it_lstDouble;

			/** @brief Main constructor */
			Weather();

			/** @brief Default destructor */
			virtual ~Weather()
			{}

			/** @brief Set the rain (double)*/
			void setLst_rain(const vv::Value& _ran);

			/** @brief Set the temperature (double)*/
			void setLst_tn(const vv::Value& _tmp);
			vector<double> getLstTn(int _lastValue);
			vector<double> getLstTn(int _lastValue) const;

			/** @brief Set the max temperature (double)*/
			void setLst_tx(const vv::Value& _tmp);
			vector<double> getLstTx(int _lastValue);
			vector<double> getLstTx(int _lastValue) const;

			/** @brief get the moy temperature over the last past day*/
			vector<double> getLstTmoy(int _lastValue);
			vector<double> getLstTmoy(int _lastValue) const;

			/** @brief Predicate on the current rainfall */
			bool p_rain(int _day, double _limrain) const;

		private:
			/** @brief  memory of the last 30 days of rainfall*/
			t_lstDouble lst_rain;
			/** @brief  list of the last 30 of Tmin temperature*/
			t_lstDouble lst_tn;

			/** @brief  list of the last 30 of Tmax temperature*/
			vector<double> lst_tx;

			/** @brief Predicate on minimum daily temperature memory */
			bool pastTn(int _day, double _mxTmp) const;

			/** @brief Predicate on minimum daily temperature memory */
			bool pastTx(int _day, double _mxTmp) const;

	};

} // namespace crash
#endif //WEATHER_H
