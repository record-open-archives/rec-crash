/**
 * @file
 * @author RECORD Team,
 *   http://record.toulouse.inra.fr ,
 *   Applied Mathematics and Informatics Research Division &
 *   Environment and Agronomy Research Division,
 *   French National Institute for Agricultural Research
 * @version d: $
 */

/*
 * Copyright (C) 2009 - INRA
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#include <belief/structuralknowledge/systemknowledge/offfarm/Weather.hpp>

namespace crash {

	Weather::Weather():
		lst_rain(30, 0.),
		lst_tn(30, 0.),
		lst_tx(30, 0.)
	{}


	void Weather::setLst_rain(const vv::Value& _ran)
	{
		double value = _ran.toDouble().value();
		Weather::it_lstDouble it =lst_rain.begin();
		lst_rain.erase(it + 29);
		lst_rain.insert (it,value);
		//std::cout<<"\tset rain: "<<lst_rain.at(1) <<" -- " << value <<std::endl;

	}

	void Weather::setLst_tn(const vv::Value& _tmp)
	{
		rotate(lst_tn.rbegin(), lst_tn.rbegin() + 1, lst_tn.rend());
		lst_tn[0]=_tmp.toDouble().value();
	}

	vector<double> Weather::getLstTn(int _lastValue){
		vector<double> lst;
		for(int i=0; i<_lastValue;i++){
			lst.push_back(lst_tn.at(i));
		}
		return lst_tn;
	}
	vector<double> Weather::getLstTn(int _lastValue)const{
		vector<double> lst;
		for(int i=0; i<_lastValue;i++){
			lst.push_back(lst_tn.at(i));
		}
		return lst_tn;
	}


	void Weather::setLst_tx(const vv::Value& _tmp)
	{
		rotate(lst_tx.rbegin(), lst_tx.rbegin() + 1, lst_tx.rend());
		lst_tx[0]=_tmp.toDouble().value();
	}
	vector<double> Weather::getLstTx(int _lastValue){
		vector<double> lst;
		for(int i=0; i<_lastValue;i++){
			lst.push_back(lst_tx.at(i));
		}
		return lst;
	}
	vector<double> Weather::getLstTx(int _lastValue)const{
		vector<double> lst;
		for(int i=0; i<_lastValue;i++){
			lst.push_back(lst_tx.at(i));
		}
		return lst;
	}

	vector<double> Weather::getLstTmoy(int _lastValue){
		vector<double> lst;
		for(int i=0; i<_lastValue;i++){
			lst.push_back( (lst_tx.at(i) + lst_tn.at(i))/2 );
		}

		return lst;
	}

	vector<double> Weather::getLstTmoy(int _lastValue)const{
		vector<double> lst;
		for(int i=0; i<_lastValue;i++){
			lst.push_back((lst_tx.at(i) + lst_tn.at(i))/2);
		}
		return lst;
	}

	bool Weather::p_rain(int _day, double _limrain) const {
		double sum = 0;
		
		for(int i = 0; i < _day + 1 ; i++){
			sum += lst_rain.at(i);
		}
		if ( sum <= _limrain){
			return 1;
		}else{
			return 0;
		}
	}


	bool Weather::pastTn(int _day, double _mxTmp) const
	{
		return accumulate(lst_tn.begin(), lst_tn.begin() + _day, 0) < _mxTmp;
	}

	bool Weather::pastTx(int _day, double _mxTmp) const
	{
		return accumulate(lst_tx.begin(), lst_tx.begin() + _day, 0.) < _mxTmp;
	}

} // namespace crash

