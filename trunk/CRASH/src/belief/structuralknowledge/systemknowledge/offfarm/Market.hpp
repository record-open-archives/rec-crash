/** @file Market.hpp 
 * @brief Market: this class is derived class of template class
 * SingletonBelief<Market, CropPrice>
 * it can manage objects of type CropPrice (price of cultures)
 *
 * @author D. AMENGA MBENGOUE
 * @date 20 janvier 2011
 * @version 1
 */

#ifndef MARKET_HPP
#define MARKET_HPP

#include <string>
#include <ostream>
#include <vector>
#include <boost/lexical_cast.hpp>

#include <vle/extension/Decision.hpp>
#include <vle/utils.hpp>
#include <vle/devs.hpp>

#include <belief/SingletonBelief.hpp>
#include <belief/structuralknowledge/systemknowledge/offfarm/CropPrice.hpp>
#include <belief/structuralknowledge/expertknowledge/Crops.hpp>

using namespace std;

namespace crash {

  class Market : public SingletonBelief <Market, CropPrice> {

    /** @brief fiend template class */
    friend class SingletonBelief <Market, CropPrice>;

 private:
    /** @brief constructor */
    Market() : nb_element(0) {}
    /** @brief destructor */
    virtual ~Market() {}

  public:
    /** @brief construction of the vector ListObject_lst from the database*/
    int init();
    /** @brief give CropPrice object corresponds of identity _id*/
    CropPrice getById(int _id);

  private:
    int nb_element;

  };


} //end namespace crash


#endif //MARKET_HPP
