/**
 * @file CropPrice.hpp
 * @author 
 * @version 1.1
 * @date 18 janvier 2011
 */


#ifndef CROPPRICE_H
#define CROPPRICE_H


#include <iostream>
#include <fstream>
#include <string>

#include <vle/extension/Decision.hpp>
#include <vle/utils.hpp>
#include <vle/devs.hpp> 

using namespace std;  

namespace ve = vle::extension::decision;
namespace vd = vle::devs;
namespace vv = vle::value;
namespace vz = vle::vpz;
namespace vu = vle::utils;
 
namespace crash {
/**
 * @brief Weather: This class is the CrashAgent knowledge about 
 * the prices cultures
 */
  class CropPrice {

  public:
    typedef vector<double>  t_price_lst;
    typedef t_price_lst::const_iterator it;
    typedef t_price_lst::size_type size_type;

    /** @brief Main constructor 
     * @param _code the name of culture (string)
     * @param _id identity of culture (int)
     */
 CropPrice(string _code, int _id) : 
      code_crop(_code),
      id(_id),
      priceList(12, 0.)   
    {}
     
    /** @brief Default destructor */
    virtual ~CropPrice() {}
	
    /** @brief Set the Price (double)
     * @param Price the price of the culture
     */		
    void setPrice(const vv::Value & Price);
    
    /** @brief get the Price (double)
     * @return the Price of culture */		
    CropPrice::t_price_lst getPrice() const;	

    /** @brief get the Name of the culture (string)
     * @return the code of crop (name) 
     */
    string getCodeCrop() const;

    /** @brief get the identity of crop
     * @return identity of crop
     */
    int getId() const;
   
    /** @brief operator << */
    inline friend ostream & operator << (ostream & out, const CropPrice & cp) {
      out << "\t Name  : " << cp.getCodeCrop() << endl;
      out << "\t Id    : " << cp.getId() << endl;
      out <<  "\t Price : ";
      for(int i=0; i < cp.getPrice().size(); i++) 
	{
	out << "\t" << cp.getPrice().at(i);
        }
      out << endl;
      return out;
 
  }
   
 
  private:  

    //variables

    /** @brief  the code of culture */
    string code_crop;
    /** @brief the indice of culture */
    int id;
   /** @brief  memory of the last twelve months price*/
    t_price_lst priceList;
  }; 
 
} // namespace crash 

#endif //CROPPRICE_H
