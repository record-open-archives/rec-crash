#include <belief/structuralknowledge/systemknowledge/offfarm/Market.hpp>

using namespace std;

namespace crash {

  CropPrice Market::getById(int _id) {
    for(size_type i = 0; i != ListObject_lst.size(); ++i) {
      CropPrice cp(at(i));
      if(cp.getId() == _id) return cp;
    }
    throw std::runtime_error("Market:: getById");
  }
 
  int Market::init() {
    size_type nb_crop = Crops::getInstance().size();
    for(size_type i = 0; i != nb_crop; i++) {
      string code_crop = Crops::getInstance().getAt(i).getCode();
      int idCrop = Crops::getInstance().getAt(i).getId();
      CropPrice cp(code_crop, idCrop);	
      add(cp);
       }
    return nb_crop;
  }

  
} //end namespace crash


