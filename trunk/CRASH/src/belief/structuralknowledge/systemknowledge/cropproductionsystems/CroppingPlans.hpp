/** @file Market.hpp 
 * @brief classe Market (classe singleton) qui dérive de la classe
 * SingletonBelief<Market, CropPrice>
 * Cette classe permet de stocker dans un vecteur des éléments de type CropPrice
 * qui correspondent aux prix des differents types de cultures 
 * d'une exploitation
 *
 * @author D. AMENGA MBENGOUE
 * @date 20 janvier 2011
 * @version 1
 */


#ifndef CROPPINGPLANS_HPP
#define CROPPINGPLANS_HPP

#include <string>
#include <ostream>
#include <vector>
#include <boost/lexical_cast.hpp>

#include <vle/extension/Decision.hpp>
#include <vle/utils.hpp>
#include <vle/devs.hpp>

#include <belief/SingletonBelief.hpp>
#include <belief/structuralknowledge/systemknowledge/cropproductionsystems/CroppingPlan.hpp>
using namespace std;

namespace crash {

  class CroppingPlans : public SingletonBelief <CroppingPlans, CroppingPlan> {

    /** @brief la classe SingletonBelief<CroppingPlans, CropPrice> est déclarée
     * comme classe amie, lui permettant ainsi à elle et à elle seule l'accès
     * au constructeur et destructeur de la classe CroppingPlans
     */
    friend class SingletonBelief <CroppingPlans, CroppingPlan>;
  
  public:
    CroppingPlans(){}

    virtual ~CroppingPlans() {}

   
    /*
     * @brief Get Cropping Plan using season
     */ 
    CroppingPlan getBySeason(string _season);
    CroppingPlan getBySeason(string _season) const;

    
    /*
     * @brief Initialize CroppingPlan Object at initialization 
     */  
    int init();

  private:

  };


} //end namespace crash


#endif //CROPPINGPLANS_HPP
