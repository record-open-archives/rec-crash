/**
 * @version d: $
 */

/*
 * Copyright (C) 2009 - INRA
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#ifndef CPS_H
#define CPS_H

#include <string> 
#include <math.h>
#include <iostream>
#include <map> 
#include <sstream>
#include <numeric>
#include <iterator>
#include <boost/algorithm/string.hpp>

// VLE
#include <vle/extension/Decision.hpp>
#include <vle/utils.hpp>
#include <vle/utils/Tools.hpp>
#include <vle/utils/DateTime.hpp>
#include <vle/devs.hpp>

//Crash file
#include <utils/CrashDB.hpp>
#include <belief/structuralknowledge/systemknowledge/cropproductionsystems/CroppingPlans.hpp>


using namespace std;
using boost::lexical_cast;
using boost::bad_lexical_cast;
namespace ve = vle::extension::decision;
namespace vd = vle::devs;
namespace vu = vle::utils;
namespace vv = vle::value;
 
namespace crash {
/**
 * @brief CPS: This class aggregates all Classes related to the expert kowldege of the Agent
 */
class CPS
{
public:
 /**
    * @brief Main constructor used at initialization: Landunit objects are created from the crashdb
    * @param _Id_LandUnit, _Ref_SoilType 
    */
    CPS();
     
	/** @brief Default destructor  */
	virtual ~CPS();

protected:
//  typedefs
//  variables
//  functions
 
private:  
//  typedefs
//  variables  
};

} // namespace crash 
#endif //CPS_H
