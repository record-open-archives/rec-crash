
#include <belief/structuralknowledge/systemknowledge/cropproductionsystems/CroppingPlans.hpp>

using namespace std;

namespace crash {


  CroppingPlan  CroppingPlans::getBySeason(string _season)
  {
    for(int  i = 0; i != ListObject_lst.size(); ++i) {
      CroppingPlan cp = at(i);
      if(cp.getSeason() == _season) return cp;
    }
    throw std::runtime_error("CroppingPlans:: getBySeason");
  }

  CroppingPlan  CroppingPlans::getBySeason(string _season) const
  {
    for(int i = 0; i != ListObject_lst.size(); ++i) {
      CroppingPlan cp = at(i);
      if(cp.getSeason() == _season) return cp;
    }
    throw std::runtime_error("CroppingPlans:: getBySeason");
  }


int  CroppingPlans::init()
  {
    
    /// Récupérer l'année max est min dans la tavle cropping plan et créer le nombre d'object cp adéquate. e.g. max 2005 min 2000 , on créer 5 cp dans une  boucle

    // ensuite on remplis les object cp 

    return nb_elements;
  } 

} //end namespace crash


