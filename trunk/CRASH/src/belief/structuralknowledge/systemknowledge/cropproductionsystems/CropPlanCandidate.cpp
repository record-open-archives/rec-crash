#include <belief/structuralknowledge/systemknowledge/cropproductionsystems/CropPlanCandidate.hpp>


namespace crash {

  CropPlanCandidate::CropPlanCandidate() {}
 
  CropPlanCandidate::~CropPlanCandidate() {}

  CropPlanCandidate::iterator CropPlanCandidate::begin() {
    return mCropByUnit.begin();
  }

  CropPlanCandidate::const_iterator CropPlanCandidate::begin() const {
    return mCropByUnit.begin();
  }

  CropPlanCandidate::iterator CropPlanCandidate::end() {
    return mCropByUnit.end();
  }

  CropPlanCandidate::const_iterator CropPlanCandidate::end() const {
    return mCropByUnit.end();
  }
  
  CropPlanCandidate::size_type CropPlanCandidate::size() const {
    return mCropByUnit.size();
  }

  std::string CropPlanCandidate::getCropByLunit(const int & lUnit) {
    std::string _crop;
    t_cropByUnitMap::iterator it = mCropByUnit.find(lUnit);
    if(it == end()) {
      throw std::runtime_error("CropPlanCandidate:: getCropByLunit");
    } else {
      _crop = it->second;
    }
    return _crop;
  }


  void CropPlanCandidate::add(const int & lUnit, const std::string & _cp) {
    mCropByUnit.insert(std::make_pair < int, std::string > (lUnit, _cp));
  }

  CropPlanCandidate::t_cropAcreageMap CropPlanCandidate::CropAcreage() {
    t_cropAcreageMap temp;
    for(t_cropByUnitMap::const_iterator it = mCropByUnit.begin();
	it != mCropByUnit.end(); ++it) {

      double area= LandUnits::getInstance().getById(it->first).getArea();
      std::string _crop = it->second;

      if(temp.find(_crop)->second) {
	temp[_crop]= area + temp.find(_crop)->second;
      } else {
	temp[_crop] = area;
      }
    }
    return temp;
  }

  CropPlanCandidate::t_IdPlotLuAdjMap CropPlanCandidate::getIdPlotAdjLu() {
    t_IdPlotLuAdjMap _idPltUnitMap;
    int nb_plot = 1;
    int max_plot = 0;
    for(t_cropByUnitMap::const_iterator it = mCropByUnit.begin();
	it != mCropByUnit.end(); ++it) {
      if(_idPltUnitMap.find(it->first)->second){
	nb_plot = _idPltUnitMap[it->first];
      } else {
	nb_plot = max_plot + 1;
	max_plot=nb_plot;
	_idPltUnitMap[it->first]=nb_plot;
      }
      t_IdLu _luAdjacent = LandUnits::getInstance().at(it->first).getAdjacentLst();
      for(t_IdLu::size_type im = 0; im < _luAdjacent.size(); ++im) {
	if(getCropByLunit(_luAdjacent.at(im)) == it->second 
	   and ! _idPltUnitMap.find(_luAdjacent.at(im))->second) {
	  _idPltUnitMap[_luAdjacent.at(im)] = nb_plot;
	}
      }
    }
    return _idPltUnitMap;
  }

} //namespace crash
