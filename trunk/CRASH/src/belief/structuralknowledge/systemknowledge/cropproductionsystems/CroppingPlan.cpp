/**
 * @file
 * @author RECORD Team,
 *   http://record.toulouse.inra.fr ,
 *   Applied Mathematics and Informatics Research Division &
 *   Environment and Agronomy Research Division,
 *   French National Institute for Agricultural Research
 * @version d: $
 */

/*
 * Copyright (C) 2009 - INRA
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */



#include <belief/structuralknowledge/systemknowledge/cropproductionsystems/CroppingPlan.hpp>

namespace crash
{
  // Constructor
  CroppingPlan::CroppingPlan() {};

  // Destructor
  CroppingPlan::~CroppingPlan() {};

  //Functions

  void CroppingPlan::setId(int _id)
  {
    Id = _id;
  }

  int CroppingPlan::getId()
  {
    return Id;
  }

  int CroppingPlan::getId() const
  {
    return Id;
  }

  string CroppingPlan::getSeason()
  {
    return Season;
  }

  string CroppingPlan::getSeason() const
  {
    return Season;
  }

  void CroppingPlan::setType(std::string _type)
  {
    Type = _type;
  }

  string CroppingPlan::getType()
  {
    return Type;
  }


  string CroppingPlan::getType() const
  {
    return Type;
  }

  CroppingPlan::t_croppingPlan CroppingPlan::getCropPlanMap()
  {
    return mCropPlanMap;
  }
  CroppingPlan::t_croppingPlan CroppingPlan::getCropPlanMap()const
  {
    return mCropPlanMap;
  }


  void CroppingPlan::init()
  {

  }




} // namespace crash

