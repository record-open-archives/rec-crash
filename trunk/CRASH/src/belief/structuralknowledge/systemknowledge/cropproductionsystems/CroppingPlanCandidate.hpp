/**
 * @author Dragan AMENGA MBENGOUE
 * @date 11 avril 2011
 * @version 1.2
 *
 */
#ifndef CROPPINGPLANCANDIDATE_HPP
#define CROPPINGPLANCANDIDATE_HPP

#include <belief/structuralknowledge/systemknowledge/cropproductionsystems/CropPlanCandidate.hpp>
#include <belief/structuralknowledge/expertknowledge/CropSuccessions.hpp>
#include <belief/structuralknowledge/expertknowledge/Crops.hpp>

#include <vector>
#include <iostream>
#include <map>
#include <string>

//VLE
#include <vle/extension/Decision.hpp>
#include <vle/utils/Tools.hpp>
#include <vle/utils/DateTime.hpp>
#include <vle/utils/Exception.hpp>
#include <vle/utils/Path.hpp>

namespace ve = vle::extension::decision;
namespace vd = vle::devs;
namespace vu = vle::utils;
namespace vv = vle::value;
using vle::utils::ArgError;


using namespace std;

namespace crash {

  class CroppingPlanCandidate {

  public:
    
    typedef std::map < int, CropPlanCandidate > t_croppingPlanMap;
    typedef std::map < int, std::string > t_cropPlanMap;
    typedef std::vector < int > t_listInt;
    typedef t_listInt::size_type size_lstInt;
    typedef std::vector < t_listInt > t_stockSol;
    typedef t_stockSol::size_type size_stockSol;
    typedef std::vector < std::string > t_cropSeq;
    typedef std::vector < t_cropSeq > t_seqList;
    typedef t_croppingPlanMap::const_iterator const_iterator;
    typedef t_croppingPlanMap::iterator iterator;
    typedef t_croppingPlanMap::size_type size_type;

    /**
     * @brief Main constructor
     */
    CroppingPlanCandidate();

    /**
     * @brief Default destructor
     */
    virtual ~CroppingPlanCandidate();
    
    /**
     * @brief This function gives the first element of the vector
     */
    iterator begin();

    /**
     * @brief This function gives the first elements of the vector
     */
    const_iterator begin() const;

    /**
     * @brief This function gives the last element of the vector
     */
    iterator end();

    /**
     * @brief This function gives the last element of the vector
     */
    const_iterator end() const;

    /**
     * @brief This function gives the size of the vector
     */
    size_type size() const;

    /**
     * @brief This function add  the new CropPlanCandidate object by year
     * @param _year year of cropping plan candidate
     * @param _cpl CropPlanCandidate object
     */
    void add(const int & _year, const CropPlanCandidate & _cpl);

    /**
     * @brief This function gives a crop by year and by land unit
     * @param _year year of cropping plan candidate
     * @param _lunit land unit
     * @return crop (string)
     */
    std::string getCropOfList(const int & _year, const int & _lunit);


    /**
     * @brief This function gives a crop succession vector of solution
     * @param lstLu vector of crop (std::vector < int >)
     * @param lstYear vector of year (std::vector < int >)
     * @param t_seqList vector of vector of crop succession
     */
    t_seqList getCrpByYearLu(const t_listInt & lstLu, const t_listInt & lstYear);

    /**
     * @brief This function gives Kp of crop succession
     * @param lstLu vector of crop (std::vector < int >)
     * @param lstYear vector of year (std::vector < int >)
     * @return kp of cropsuccession (double)
     */
    double getKpMeanList(const t_listInt & lstLu, const t_listInt & lstYear);

    /**
     * @brief
     */
    void addSolstr(const std::string & _lineSol);

    /**
     * @brief
     */
    std::string getLineSolution();

  private:

    /** @brief cropping plan candidate by year */
    t_croppingPlanMap mCroppingPlanMap;
    /** @brief Year of Cropping Plan */
    int mYear;
    /** @brief */
    std::string lineSolution;

  };

} //namespace crash

#endif //CROPPINGPLANCANDIDATE_HPP
