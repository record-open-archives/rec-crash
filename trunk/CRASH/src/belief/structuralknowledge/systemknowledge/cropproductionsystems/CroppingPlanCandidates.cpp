#include <belief/structuralknowledge/systemknowledge/cropproductionsystems/CroppingPlanCandidates.hpp>

namespace crash {

  CroppingPlanCandidates::CroppingPlanCandidates() {}

  CroppingPlanCandidates::~CroppingPlanCandidates() {}

  std::string CroppingPlanCandidates::getFilePath(const std::string & _fileDir,
   						  const std::string & _fileName)
   {
     std::string _path(vle::utils::Path::path().getPackageDataDir().c_str());
	
     _path.append("/");
     _path.append(_fileDir.c_str());
     _path.append("/");
     _path.append(_fileName.c_str());
     _path.append(".sol");
     return _path;
   }

  

  void CroppingPlanCandidates::init(const std::string & _fileDir,
				    const std::string & _fileName) {

    std::string _file_Name = getFilePath(_fileDir,_fileName);
    ifstream _SolFile(_file_Name.c_str(), ios::in);
    int nbLU = LandUnits::getInstance().size();
    int crop_index;
    
    CropPlanCandidate cs;
 
   
    if(_SolFile) {
      int l = 0;
      std::string line; 
      while(getline(_SolFile, line)) {
  	if(line.empty()) break;
	l++;
	CroppingPlanCandidate ccp;
	ccp.addSolstr(line);

  	boost::char_separator<char> sep(" ");
  	tokenizer tokens_crop(line, sep);
	int index = 0;
	int year = 0;
  	for(tokenizer::iterator tok_iter = tokens_crop.begin(); tok_iter != tokens_crop.end(); ++tok_iter) {
  	   crop_index = boost::lexical_cast< int >( *tok_iter);
 	   string codeCrop = Crops::getInstance().getById(crop_index + 1).getCode();
	   if(index%nbLU == 0){
	     if(year>0){
	       ccp.add(year, cs);
	     }
	     CropPlanCandidate cs;
	     year=year+1;
	   }

	   int luid = LandUnits::getInstance().at(index-(nbLU*(year-1))).getId();
	   cs.add(luid, codeCrop);
	   index++;  
	 }
	add(ccp);
      }
      std::cout << " Number of solutions: " << l << endl;
      std::cout << " Number of croppingPlanCandidate: " << size() << endl;
    }	
  }  

} //namespace crash
