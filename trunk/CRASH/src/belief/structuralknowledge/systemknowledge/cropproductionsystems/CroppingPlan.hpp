/**
 * @file
 * @author RECORD Team,
 *   http://record.toulouse.inra.fr ,
 *   Applied Mathematics and Informatics Research Division &
 *   Environment and Agronomy Research Division,
 *   French National Institute for Agricultural Research
 * @version d: $
 */

/*
 * Copyright (C) 2009 - INRA
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#ifndef CROPPINGPLAN_H
#define CROPPINGPLAN_H

#include <string> 
#include <math.h>
#include <iostream>
#include <map> 
#include <sstream>
#include <numeric>
#include <iterator>
#include <boost/algorithm/string.hpp>

// VLE
#include <vle/extension/Decision.hpp>
#include <vle/utils/Tools.hpp>
#include <vle/utils/DateTime.hpp>

//Crash file
#include <utils/CrashDB.hpp>

using namespace std;
using boost::lexical_cast;
using boost::bad_lexical_cast;
namespace ve = vle::extension::decision;
namespace vd = vle::devs;
namespace vu = vle::utils;
namespace vv = vle::value;
 
namespace crash {
class CrashAgent; 
/**
 * @brief 
 */
class CroppingPlan
{
public:
//  typedefs
  typedef map<int, vector<string> > t_croppingPlan; 
//  variables
    
//  functions
    /** @brief Main constructor */
    CroppingPlan();
     
	/** @brief Default destructor  */
	virtual ~CroppingPlan();  


 
    /** @brief Set Cropping Plan Id*/
    void setId(int _id);

    /** @brief get Cropping Plan Id*/
    int getId();
    int getId() const;    

    /** @brief get Cropping Plan Season*/
    string getSeason();
    string getSeason() const;

    /** @brief get Cropping Plan type*/
    void setType(std::string _type);

    /** @brief get Cropping Plan type*/
    string getType();
    string getType() const;
    
    /** @brief getCropPlanMap */
    t_croppingPlan getCropPlanMap();
    t_croppingPlan getCropPlanMap()const;
 
    /** @brief This function create CropPlan objects from the CrashDb*/
    void init();

    /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
			// inline friend std::ostream& operator<<(std::ostream& out, const CroppingPlan& cp)
			// {
			//     cout << "CroppingPlan: "<< cp.getId() << " -- type: "<< cp.getType()<< endl;
			// 	for (t_croppingPlan::const_iterator iter = cp.getCropPlanMap().begin(); iter != cp.getCropPlanMap().end(); iter++) {
			// 		out  << endl << "ref landunit: " << (iter->first) << "\n";// <<iter->second;
			// 	}
			// 	return out;
			// }

protected:
//  typedefs
//  variables
//  functions
 
private:  
//  typedefs
//  variables    
    /** @brief ID*/
    int Id;
    /** @brief Cropping plan type (current or future or planB)*/
    std::string Type;      
    /** @brief Cropping plan season*/
    std::string Season;  
   /** @brief  Map of all the Annual CropPlan objects*/
    t_croppingPlan mCropPlanMap;   
//  functions  
    

        
   

};
 
} // namespace crash 
#endif //CROPPINGPLAN_H
