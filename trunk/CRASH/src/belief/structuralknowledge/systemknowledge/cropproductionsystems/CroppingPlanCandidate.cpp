#include <belief/structuralknowledge/systemknowledge/cropproductionsystems/CroppingPlanCandidate.hpp>


namespace crash {

  
  CroppingPlanCandidate::CroppingPlanCandidate() {}
  
  CroppingPlanCandidate::~CroppingPlanCandidate() {}

  CroppingPlanCandidate::iterator CroppingPlanCandidate::begin() {
    return mCroppingPlanMap.begin();
  }

  CroppingPlanCandidate::const_iterator CroppingPlanCandidate::begin() const {
    return mCroppingPlanMap.begin();
  }

  CroppingPlanCandidate::iterator CroppingPlanCandidate::end() {
    return mCroppingPlanMap.end();
  }

  CroppingPlanCandidate::const_iterator CroppingPlanCandidate::end() const {
    return mCroppingPlanMap.end();
  }

  CroppingPlanCandidate::size_type CroppingPlanCandidate::size() const {
    return mCroppingPlanMap.size();
  }

  void CroppingPlanCandidate::add(const int & _year, 
				     const CropPlanCandidate & _cpl) {
    mCroppingPlanMap[_year] = _cpl;

  }

 
  std::string CroppingPlanCandidate::getCropOfList(const int & _year, const int & _lunit) {
    std::string _codeCrop;
    CroppingPlanCandidate::const_iterator it = mCroppingPlanMap.find(_year);
    if(it == end()) {
      throw std::runtime_error("CroppingPlanCandidate:: getCropOfList");
    } else {
      CropPlanCandidate _cpl = it->second;
      _codeCrop = _cpl.getCropByLunit(_lunit);
    }
    return _codeCrop;
  }

  CroppingPlanCandidate::t_seqList CroppingPlanCandidate::getCrpByYearLu(const t_listInt & lstLu, 
									 const t_listInt & lstYear) {
    t_seqList seqList;
    CroppingPlanCandidate::const_iterator it;
    for(t_cropSeq::size_type i = 0; i != lstLu.size(); ++i) {
      t_cropSeq cropSeqVect;
      for(t_cropSeq::size_type j = 0; j != lstYear.size(); ++j) {
	it = mCroppingPlanMap.find(lstYear.at(j));
	if(it == end()) {
	  throw std::runtime_error("CroppingPlanCandidate:: getCrpByYearLu");
	} else {
	  CropPlanCandidate cpl = it->second;
	  std::string codeCrop = cpl.getCropByLunit(lstLu.at(i));
	  cropSeqVect.push_back(codeCrop);
	}
      }
      seqList.push_back(cropSeqVect);
    }
    return seqList;
  }

  double CroppingPlanCandidate::getKpMeanList(const t_listInt & lstLu, const t_listInt & lstYear) {
    t_seqList seqList = getCrpByYearLu(lstLu, lstYear);
    int k = 0;
    double som = 0;
    for(t_seqList::size_type u = 0; u < seqList.size(); u++) {
      for(t_cropSeq::size_type v = 0; v < seqList[u].size()-1; v++) {
	std::string ref_prec = seqList[u][v];
	std::string ref_crop = seqList[u][v+1];
	int it_prec = Crops::getInstance().getByCode(ref_prec).getId();
	int it_crop = Crops::getInstance().getByCode(ref_crop).getId();
	double _kp = CropSuccessions::getInstance().getByCropPrec(it_crop, it_prec).getKp();
	som = som + _kp;
	k++;
      }
    }
    return som/k;
  }
 
  void CroppingPlanCandidate::addSolstr(const std::string & _lineSol) {
    lineSolution = _lineSol;
  }

  std::string CroppingPlanCandidate::getLineSolution() {
    return lineSolution;
  }

} //namespace crash
