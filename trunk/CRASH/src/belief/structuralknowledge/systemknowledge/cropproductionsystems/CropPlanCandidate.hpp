/**
 * @author Dragan AMENGA MBEGOUE
 * @date 11 avril 2011
 * @version 1.3
 */


#ifndef CROPPLANCANDIDATE_HPP
#define CROPPLANCANDIDATE_HPP

#include <map>
#include <iostream>
#include <string>
#include <iterator>
#include <belief/structuralknowledge/systemknowledge/farmland/LandUnits.hpp>
#include <belief/structuralknowledge/systemknowledge/farmland/LandUnit.hpp>

using namespace std;

namespace crash {

  class CropPlanCandidate {

    public:

    /**
     * @brief This map represente LandU nit and Crop
     */
    typedef std::map < int, std::string >  t_cropByUnitMap;
    /**
     * @brief This map represente Crop and Acreage of the crop
     */
    typedef std::map < std::string, double > t_cropAcreageMap;
    /**
     * @brief Vector of LandUnit
     */
    typedef std::vector < int > t_IdLu;
    /**
     * @brief This map represente Plo tUnit and Land Unit
     */
    typedef std::map < int, int > t_IdPlotLuAdjMap;
    /**
     * @brief const iterator of the map t_cropByUnitMap
     */
    typedef t_cropByUnitMap::const_iterator const_iterator;
    /**
     * @brief iterator of the map t_cropByUnitMap
     */
    typedef t_cropByUnitMap::iterator iterator;
    /**
     * @brief size_type of the map t_cropByUnitMap
     */
    typedef t_cropByUnitMap::size_type size_type;


    /**
     * @brief Main constructor
     */
    CropPlanCandidate();

    /**
     * @brief Destructor
     */
    virtual ~CropPlanCandidate();

    /**
     * @brief This function gives the begin of the map
     */
    iterator begin();

    /**
     * @brief This function gives the const beging of the map
     */
    const_iterator begin() const;

    /**
     * @brief This function gives the end of the map
     */
    iterator end();

    /**
     * @brief This function gives the const end of the map
     */
    const_iterator end() const;

    /**
     * @brief This function gives the size of the map
     */
    size_type size() const;

    /**
     * @brief This function gives the crop type
     * @param lUnit Land unit
     * @return a crop of the land unit (string)
     */
    std::string getCropByLunit(const int & lUnit);

    /**
     * @brief This function add crop and land unit in the map
     * @param lUnit the land unit
     * @param _cp the crop
     */
    void add(const int & lUnit, const std::string & _cp);

    /**
     * @brief Crop acreage calculation for the object CropPlanCandidate
     * @return map < string, double > (crop, acreage of crop)
     */
    t_cropAcreageMap CropAcreage();

    /**
     * @brief This function gives a map of Plot Unit and Land Unit
     */
    t_IdPlotLuAdjMap getIdPlotAdjLu();

  private:

    /** @brief Map contains land unit and type crop */
    t_cropByUnitMap mCropByUnit;

  };

} //namespace crash

#endif //_CROPPLANCANDIDATE_HP
