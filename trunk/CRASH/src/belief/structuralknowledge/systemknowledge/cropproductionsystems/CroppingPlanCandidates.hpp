#ifndef CROPPINGPLAN_CANDIDATES_HPP
#define CROPPINGPLAN_CANDIDATES_HPP

#include <belief/structuralknowledge/systemknowledge/cropproductionsystems/CroppingPlanCandidate.hpp>
#include <belief/structuralknowledge/systemknowledge/cropproductionsystems/CropPlanCandidate.hpp>
#include <belief/structuralknowledge/systemknowledge/farmland/LandUnits.hpp>

#include <belief/SingletonBelief.hpp>
#include <belief/structuralknowledge/expertknowledge/Crops.hpp>
#include <utils/CrashDBS.hpp>

#include <vector>
#include <iostream>
#include <fstream>
#include <string>

//VLE
#include <vle/extension/Decision.hpp>
#include <vle/utils/Tools.hpp>
#include <vle/utils/DateTime.hpp>
#include <vle/utils/Exception.hpp>
#include <vle/utils/Path.hpp>
//Boost
#include <boost/tokenizer.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/lexical_cast.hpp>

namespace ve = vle::extension::decision;
namespace vd = vle::devs;
namespace vu = vle::utils;
namespace vv = vle::value;
using boost::lexical_cast;
using boost::bad_lexical_cast;

using namespace std;


namespace crash {

  /**
   * @brief The class CroppingPlanCandidates is a vector of CroppingPlanCandidate object
   */
  class CroppingPlanCandidates : public SingletonBelief<CroppingPlanCandidates, CroppingPlanCandidate> {

	friend class SingletonBelief <CroppingPlanCandidates, CroppingPlanCandidate>;

  private:

    /**
     * @brief This Constructor is private
     */
    CroppingPlanCandidates();

    /**
     * @brief This Destructor is private
     */
    virtual ~CroppingPlanCandidates();

  public:

    typedef boost::tokenizer<boost::char_separator<char> > tokenizer;

    /**
     * @brief This function gives the file name of solution
     */
    std::string getFilePath(const std::string & _fileDir,
     			    const std::string & _fileName);

    /**
     * @brief This function add the CroppingPlancandidate object in
     * vector
     */
    void init(const std::string & _fileDir,
    	      const std::string & _fileName);
  };

} //namespace crash

#endif //CROPPINGPLAN_CANDIDATES_HPP
