/**
 * @file
 * @author RECORD Team,
 *   http://record.toulouse.inra.fr ,
 *   Applied Mathematics and Informatics Research Division &
 *   Environment and Agronomy Research Division,
 *   French National Institute for Agricultural Research
 * @version d: $
 */

/*
 * Copyright (C) 2009 - INRA
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#ifndef FARMLAND_H
#define FARMLAND_H

#include <string>
#include <math.h>
#include <iostream>
#include <map>
#include <sstream>
#include <numeric>
#include <iterator>

//Geos Library
#include <geos.h>

#include <vle/extension/Decision.hpp>
#include <vle/utils/Tools.hpp>
#include <vle/utils/DateTime.hpp>
#include <boost/algorithm/string.hpp>

//Crash file
#include <belief/structuralknowledge/systemknowledge/farmland/ManagementUnit.hpp>
#include <belief/structuralknowledge/systemknowledge/farmland/CroppingBlocks.hpp>
#include <belief/structuralknowledge/systemknowledge/farmland/PlotUnits.hpp>

#include <belief/structuralknowledge/systemknowledge/farmland/LandUnits.hpp>

#include <belief/structuralknowledge/systemknowledge/waterresource/WaterResourceUnits.hpp>

#include <belief/structuralknowledge/expertknowledge/SoilType.hpp>


using namespace geos;
using namespace std;
using boost::lexical_cast;
using boost::bad_lexical_cast;
namespace ve = vle::extension::decision;
namespace vd = vle::devs;
namespace vu = vle::utils;
namespace vv = vle::value;

namespace crash
{
    /**
     * @brief FarmLand: This class aggregate all Classes related to the spatial units (e.g. Landunits)
     * Agent knowledge about the farm
     */
    class FarmLand
    {
        public:
            //  typedefs
            //  variables
            //  functions
            /**
            * @brief Main constructor used at initialization: Landunit objects are created from the crashdb
            */
      FarmLand(const vd::InitEventList & evts);

            /** @brief Default destructor  */
            virtual ~FarmLand();

     /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
		inline friend std::ostream& operator<<(std::ostream& out, const FarmLand& /*fl*/)
		{
            out <<"\n----------------------------------------------" << endl;
            out <<"\tCrashAgent belief: Check the Farmland" << endl;
            out <<"----------------------------------------------\n" << endl;
            out << "LandUnits: " <<endl;
            out << LandUnits::getInstance();
            out << "\nPlotUnits: " <<endl;
            out << PlotUnits::getInstance();
	    out << "\nWaterResourceUnits : " << endl;
	    out << WaterResourceUnits::getInstance();
            out << "\n---------------------------------------------\n"<< endl;
			return out;
		}

        protected:
            //  typedefs
            //  variables
            //  functions

        private:
            //  typedefs
            //  variables
            //  functions
    };

} // namespace crash
#endif //FARMLAND_H
