/**
 * @file
 * @author RECORD Team,
 *   http://record.toulouse.inra.fr ,
 *   Applied Mathematics and Informatics Research Division &
 *   Environment and Agronomy Research Division,
 *   French National Institute for Agricultural Research
 * @version d: $
 */

/*
 * Copyright (C) 2009 - INRA
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */



#include <iostream>
#include <fstream>
#include <string>
#include <iterator>
#include <boost/algorithm/string.hpp>

#include <belief/structuralknowledge/systemknowledge/farmland/Soil.hpp>



namespace crash {

	void    Soil::setAvgHur(const vv::Value& HUR)
	{
		AvgHur = HUR.toDouble().value();
	}
	double Soil::getAvgHur(){
		return AvgHur;
	}
	double Soil::getAvgHur() const {
		return AvgHur;
	}

	void    Soil::setAvgHur30(const vv::Value& HUR)
	{
		AvgHur30 = HUR.toDouble().value();
	}
	double Soil::getAvgHur30(){
		return AvgHur30;
	}
	double Soil::getAvgHur30() const {
		return AvgHur30;
	}


	void    Soil::setAvgHucc(const vv::Value& HUCC)
	{
		AvgHucc = HUCC.toDouble().value();
	}
	double Soil::getAvgHucc(){
		return AvgHucc;
	}
	double Soil::getAvgHucc() const {
		return AvgHucc;
	}

	void    Soil::setAvgHucc30(const vv::Value& HUCC)
	{
		AvgHucc30 = HUCC.toDouble().value();
	}
	double Soil::getAvgHucc30(){
		return AvgHucc30;
	}
	double Soil::getAvgHucc30() const {
		return AvgHucc30;
	}


	void    Soil::setSAWC(const vv::Value& _SAWC){
		SAWC = _SAWC.toDouble().value();
	}
	double Soil::getSAWC(){
		return SAWC;
	}
	double Soil::getSAWC() const {
		return SAWC;
	}



	void    Soil::setMaxSAWC(const vv::Value& _MaxSAWC)
	{
		MaxSAWC = _MaxSAWC.toDouble().value();
	}
	double Soil::getMaxSAWC(){
		return MaxSAWC;
	}
	double Soil::getMaxSAWC() const{
		return MaxSAWC;
	}

	void    Soil::setRsurru(const vv::Value& _Rsurru)
	{
		Rsurru = _Rsurru.toDouble().value();
	}
	double Soil::getRsurru(){
		return Rsurru;
	}
	double Soil::getRsurru() const{
		return Rsurru;
	}


	void    Soil::setWaterDepletion(const vv::Value& _wd)
	{
		WaterDepletion = _wd.toDouble().value();
	}
	double Soil::getWaterDepletion(){
		return WaterDepletion;
	}
	double Soil::getWaterDepletion() const{
		return WaterDepletion;
	}


	string Soil::getCodeSoilType()
	{
		return CodeSoilType;
	}

} // namespace crash

