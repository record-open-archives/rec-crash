#include <belief/structuralknowledge/systemknowledge/farmland/DoneOperation.hpp>


namespace crash {

  void DoneOperation::setAirg(const vv::Value & _airg) {
    mAirg = _airg.toDouble().value();
  }

  double DoneOperation::getAirg() {
    return mAirg;
  }

  double DoneOperation::getAirg() const {
    return mAirg;
  }


} //namespace crash
