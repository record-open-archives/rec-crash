#ifndef CROPPINGBLOCKS_HPP
#define CROPPINGBLOCKS_HPP



#include <string>
#include <ostream>
#include <vector>
#include <boost/lexical_cast.hpp>

#include <vle/extension/Decision.hpp>
#include <vle/utils.hpp>
#include <vle/devs.hpp>
//Crash
#include <belief/SingletonBelief.hpp>
//System knowledge
#include <belief/structuralknowledge/systemknowledge/farmland/CroppingBlock.hpp>
#include <belief/structuralknowledge/systemknowledge/farmland/LandUnits.hpp>

using namespace std;

namespace crash {

  /**
   * @brief The class CroppingBlocks is a vector of CroppingBlock object
   */
  class CroppingBlocks:public SingletonBelief<CroppingBlocks, CroppingBlock> {

    friend class SingletonBelief<CroppingBlocks, CroppingBlock>;

  private:
    /**
     * @brief Default constructor
     */
    CroppingBlocks();
    /**
     * @brief Default destructor
     */
    virtual ~CroppingBlocks();

  public:
    /**
     * @brief initialieation of CroppingBlock object from database
     */
    size_type init();
    /**
     * @brief This function add a Land Unit in each Cropping Block
     */
    void addLuToCroppingBlock(const CroppingBlock& _cb, LandUnit* _lupt);

  };

}
#endif //CROPPINGBLOCKS_HPP
