/**
 * @file
 * @author RECORD Team,
 *   http://record.toulouse.inra.fr ,
 *   Applied Mathematics and Informatics Research Division &
 *   Environment and Agronomy Research Division,
 *   French National Institute for Agricultural Research
 * @version d: $
 */

/*
 * Copyright (C) 2009 - INRA
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */


#ifndef Soil_H
#define Soil_H

#include <iostream>
#include <fstream>
#include <string>

#include <vle/utils.hpp>
#include <vle/devs.hpp>

using namespace std;

namespace vd = vle::devs;
namespace vv = vle::value;
namespace vz = vle::vpz;
namespace vu = vle::utils;

namespace crash {
	/**
	 * @brief Soil: This class is part of the CrashAgent Belief. It contains all attributes
	 * that the CrashAgent knows  about the soil for each Soil
	 */
	class Soil
	{
		public:
			//  typedefs

			//  variables
			//  functions
			/** @brief Default constructor */
			Soil() :
				CodeSoilType("unknown soil"),
				AvgHur(0),
				AvgHur30(0),
				AvgHucc(0),
				AvgHucc30(0),
				SAWC(0),
				MaxSAWC(0),
				Rsurru(0),
				WaterDepletion(0)
		{}

			/** @brief Default constructor */
			Soil(string codeType) :
				CodeSoilType(codeType),
				AvgHur(0),
				AvgHur30(0),
				AvgHucc(0),
				AvgHucc30(0),
				SAWC(0),
				MaxSAWC(0),
				Rsurru(0),
				WaterDepletion(0)
		{}

			/** @brief Default destructor */
			virtual ~Soil()
			{}

			/** @brief Set the HUR --> TRUE/FALSE*/
			void    setAvgHur(const vv::Value& HUR);
			double getAvgHur();
			double getAvgHur() const;

			void    setAvgHur30(const vv::Value& HUR);
			double getAvgHur30();
			double getAvgHur30() const;

			void    setAvgHucc(const vv::Value& HUCC);
			double getAvgHucc();
			double getAvgHucc() const;

			void    setAvgHucc30(const vv::Value& HUCC);
			double getAvgHucc30();
			double getAvgHucc30() const;

			/** @brief Set the Soil Available Water Content (mm) */
			void    setSAWC(const vv::Value& _SAWC);
			/** @brief get the Soil Available Water Content (mm) */
			double  getSAWC() ;
			double  getSAWC() const;

			/** @brief Set the maximum Soil Available Water Content (mm)*/
			void    setMaxSAWC(const vv::Value& _MaxSAWC);
			/** @brief get the maximu Soil Available Water Content (mm) */
			double  getMaxSAWC() ;
			double  getMaxSAWC() const;

			/** @brief Set the soil water status as a proportion of readily available water */
			void    setRsurru(const vv::Value& _Rsurru);
			/** @brief get the maximu Soil Available Water Content (mm) */
			double  getRsurru() ;
			double  getRsurru() const;

			void   setWaterDepletion(const vv::Value& _Wd);
			double getWaterDepletion();
			double getWaterDepletion() const ;

			/** @brief get the soiltype code*/
			string getCodeSoilType();
		private:

			/** @brief SoilType Code*/
			string CodeSoilType;
			/** @brief soil water content */
			double AvgHur;
			double AvgHur30;

			/** @brief field capacity */
			double AvgHucc;
			double AvgHucc30;

			/** @brief max available water content (mm) --> fr:Reserve Utile */
			double SAWC;
			/** @brief readily soil available water content (% SAWC) */
			double MaxSAWC;
			/** @brief Set the soil water status as a proportion of readily available water */
			double Rsurru;

			double WaterDepletion;
	};


} // namespace crash

#endif //Soil_H

