/**
 * @file
 * @author RECORD Team,
 *   http://record.toulouse.inra.fr ,
 *   Applied Mathematics and Informatics Research Division &
 *   Environment and Agronomy Research Division,
 *   French National Institute for Agricultural Research
 * @version d: $
 */

/*
 * Copyright (C) 2009 - INRA
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */



#include <iostream>
#include <fstream>
#include <string>
#include <iterator>
#include <boost/algorithm/string.hpp>

#include <belief/structuralknowledge/systemknowledge/farmland/LandCover.hpp>



namespace crash {

	double LandCover::getLai() {
		return mLai;
	}

	void LandCover::setLai(const vv::Value&  Lai) {
		mLai = Lai.toDouble().value();
	}

	void LandCover::setMafruit(const vv::Value& Mafruit) {
		mMafruit = Mafruit.toDouble().value();
	}

	double LandCover::getMafruit() {
		return mMafruit;
	}

	void LandCover::setHarvestedOrganeStage(const vv::Value& HarvestedOrganeStage){
		mHarvestedOrganeStage = HarvestedOrganeStage.toDouble().value();
	}
	double LandCover::getHarvestedOrganeStage() {
		return mHarvestedOrganeStage;
	}
	double LandCover::getHarvestedOrganeStage() const {
		return mHarvestedOrganeStage;
	}

	void LandCover::setVegetativeStage(const vv::Value& VegetativeStage){
		mVegetativeStage = VegetativeStage.toDouble().value();
	}

	double LandCover::getVegetativeStage() {
		return mVegetativeStage;
	}
	double LandCover::getVegetativeStage() const {
		return mVegetativeStage;
	}

	void LandCover::setNbLeaf(const vv::Value& _nbl){
		nbleaf = _nbl.toInteger().value();
	}

	int LandCover::getNbLeaf() {
		return nbleaf;
	}
	int LandCover::getNbLeaf() const {
		return nbleaf;
	}

	void LandCover::setH2oHrv(const vv::Value& H2Ohrv) {
		h2o_hrv = H2Ohrv.toDouble().value();
	//cout<<"\t" <<H2Ohrv.toDouble().value();
	}

	double LandCover::getH2oHrv() {
		return h2o_hrv;
	}

	double LandCover::getH2oHrv() const {
		return h2o_hrv;
	}

	string LandCover::getCodeCrop() {
		return codeCrop;
	}

	void LandCover::setINN(const vv::Value& INN) {
		mINN = INN.toDouble().value();
	}

	double LandCover::getINN() {
		return mINN;
	}

	double LandCover::getINN() const {
		return mINN;
	}

	void LandCover::setSwfac(const vv::Value & Swfac) {
		mSwfac = Swfac.toDouble().value();
	}

	double LandCover::getSwfac() {
		return mSwfac;
	}

	double LandCover::getSwfac() const {
		return mSwfac;
	}

} // namespace crash

