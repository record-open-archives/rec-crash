/**
 * @file
 * @author RECORD Team,
 *   http://record.toulouse.inra.fr ,
 *   Applied Mathematics and Informatics Research Division &
 *   Environment and Agronomy Research Division,
 *   French National Institute for Agricultural Research
 * @version d: $
 */

/*
 * Copyright (C) 2009 - INRA
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */


#ifndef LandCover_H
#define LandCover_H

#include <iostream>
#include <fstream>
#include <string>
#include <geos.h>

#include <vle/utils.hpp>
#include <vle/devs.hpp>

//#include <belief/structuralknowledge/systemknowledge/LandUnit.hpp>

using namespace geos;

using namespace std;

namespace vd = vle::devs;
namespace vv = vle::value;
namespace vz = vle::vpz;
namespace vu = vle::utils;

namespace crash {
	/**
	 * @brief LandCover: This class is part of the CrashAgent Belief. It contains all attributes
	 * that the CrashAgent knows  about the Land Cover
	 */
	class LandCover
	{
		public:
			//  typedefs

			//  variables
			//  functions
			/** @brief Default constructor */
			LandCover() :
				codeCrop("unknown crop"),
				mMafruit(0),
				mLai(0),
				mVegetativeStage(0),
				nbleaf(0),
				h2o_hrv(0),
				mINN(0),
				mSwfac(0)
		{}

			LandCover(std::string _code_crop) :
				codeCrop(_code_crop),
				mMafruit(0),
				mLai(0),
				mVegetativeStage(0),
				nbleaf(0),
				h2o_hrv(0),
				mINN(0),
				mSwfac(0)
		{}

			/** @brief Default destructor */
			virtual ~LandCover()
			{}

			/** @brief Get the LandCover Lai (double)*/
			double getLai();

			/** @brief Set the LandCover Lai (double)*/
			void setLai(const vv::Value& Lai);

			/** @brief Get Mafruit --> dry matter of harvested organs (t/ha) */
			double getMafruit();

			/**
			 * @brief Set the Mafruit
			 * --> dry matter of harvested organs (t/ha).
			 * Set also all variables that depend on
			 * Mafruit: MafruitQuant
			 * --> dry matter of harvested organs (t).
			 */
			void setMafruit(const vv::Value& Mafruit);

			/** @brief Set the Harvested Organe Stage  */
			void setHarvestedOrganeStage(const vv::Value& HarvestOrganeStage);
			/** @brief Get the Harvested Organe Stage */
			double getHarvestedOrganeStage();
			double getHarvestedOrganeStage() const;

			/** @brief Set the VegetativeStage --> Vegetative crop stages */
			void setVegetativeStage(const vv::Value& VegetativeStage);
			/** @brief Get the VegetativeStage --> Vegetative crop stages */
			double getVegetativeStage();
			double getVegetativeStage() const;

			/** @brief Set the VegetativeStage --> Vegetative crop stages */
			void setNbLeaf(const vv::Value& _nbl);
			/** @brief Get the VegetativeStage --> Vegetative crop stages */
			int getNbLeaf();
			int getNbLeaf() const;


			/** @brief Get the H2Orec (double) */
			double getH2oHrv();

			/** @brief Get the const H2Orec (double) */
			double getH2oHrv() const;

			/** @brief Set the H2Orec */
			void setH2oHrv(const vv::Value & H2Ohrv);

			/** @brief */
			string getCodeCrop();

			/** @brief Set the INN LandCover */
			void setINN(const vv::Value & INN);

			/** @brief Get The INN (double) */
			double getINN();

			/** @brief Get The const INN (double) */
			double getINN() const;

			/** @brief Set the Swfac LandCover */
			void setSwfac(const vv::Value & Swfac);

			/** @brief Get the Swfac (double) */
			double getSwfac();

			/** @brief Get the const Swfac (double) */
			double getSwfac() const;


		protected:
			//  typedefs
			//  variables
			//  functions

		private:
			//  typedefs
			//  variables

			/** @brief */
			string codeCrop;
			/** @brief Dry matter of harvested organs (t/ha)*/
			double mMafruit;
			/** @brief Leaf Area Index*/
			double mLai;
			/** @brief Crop vegetative stage*/
			double mVegetativeStage;
			/** @brief Crop nb of leaf*/
			int nbleaf;
			/** @brief Crop reproductive stage*/
			double mHarvestedOrganeStage;
			/** @brief Water content of reserves */
			double h2o_hrv;
			/** @brief Nitrogen nutrition index */
			double mINN;
			/** @brief index of stomatal water stress */
			double mSwfac;
			//  functions
	};


} // namespace crash

#endif //LandCover_H

