#include <vle/utils/Path.hpp>
#include <vle/utils/Exception.hpp>   

#include <belief/structuralknowledge/systemknowledge/farmland/CroppingBlocks.hpp>

using vle::utils::ArgError;
using namespace std;
using namespace boost;

namespace crash {


  CroppingBlocks::CroppingBlocks() {}

  CroppingBlocks::~CroppingBlocks() {}


   void CroppingBlocks::addLuToCroppingBlock(const CroppingBlock& _cb, 
					     LandUnit* _lupt) {       
     for (size_type i = 0; i != ListObject_lst.size(); ++i) {
       if(ListObject_lst.at(i).getId()==_cb.getId()) ListObject_lst.at(i).addLU(_lupt);
     }
   }

  CroppingBlocks::size_type	CroppingBlocks::init() {
    for (size_type itLU = 0; itLU !=  LandUnits::getInstance().size(); ++itLU) {
      LandUnit* LUpt = &(LandUnits::getInstance().getAtref(itLU));
      int RefCB = LUpt->getRefCB();
      CroppingBlock cb(RefCB, LUpt);     
      if(size()==0) {
	ListObject_lst.push_back(cb);
	nb_elements = size();
      } else {
	bool flag = find(cb); 
	if(flag==true) {       
	  addLuToCroppingBlock(cb, LUpt);
	} else {            
	  ListObject_lst.push_back(cb);
	  nb_elements = size();
	}
      }
    }
    return nb_elements;
  }

} //crash

