/**
 * @file
 * @author RECORD Team,
 *   http://record.toulouse.inra.fr ,
 *   Applied Mathematics and Informatics Research Division &
 *   Environment and Agronomy Research Division,
 *   French National Institute for Agricultural Research
 * @version d: $
 */

/*
 * Copyright (C) 2009 - INRA
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#ifndef MANAGEMENTUNIT_HPP
#define MANAGEMENTUNIT_HPP


#include <vle/extension/Decision.hpp>
#include <vle/utils/Tools.hpp>
#include <vle/utils/DateTime.hpp>
#include <sstream>
#include <numeric>
#include <iterator>
#include <boost/algorithm/string.hpp>


//Crash file
#include <belief/structuralknowledge/systemknowledge/farmland/LandUnit.hpp>


using namespace std;
namespace ve = vle::extension::decision;


namespace crash
{
    /**
     * @brief ManagementUnit: This class is part of the CrashAgent Belief. 
     * The Management unit is an aggregation of LandUnit
     */
    class ManagementUnit
    {
    public:
	//  typedefs
	typedef std::vector<LandUnit*> t_LuLst;
	typedef std::vector<int> t_LuIdLst;
	typedef t_LuLst::size_type size_type;
	//  variables
	//  functions

	/**
	 * @brief Main constructor used at model initialization 
	 * and when the farmer 
	 */
	ManagementUnit(int id_MU, string name_MU, t_LuLst  LU_lst);

	ManagementUnit(int id_MU, string name_MU, LandUnit*  LUpt);


	/**  
	 * @brief Default destructor  
	 */
	virtual ~ManagementUnit();

	/**
	 * @brief Get ManagementUnit name:e.g.  LU1
	 */
      virtual int getId();
      virtual int getId() const;

	/** 
	 * @brief Set &LandUnit to the ManagementUnit Object
	 */
	virtual void addLU(LandUnit*  LUpt);

	/** 
	 * @brief Get &LandUnit of the ManagementUnit Object*
	 */
	virtual t_LuLst getLuLst() const;

	/** 
	 * @brief Get vector of LandUnit id of the ManagementUnit Object*
	 */
	virtual t_LuIdLst getLuIdLst() const;

	/** 
	 * @brief Get area of the ManagementUnit Object
	 */
	double getArea();
	double getArea() const;
	
	int getNb_Adjacent() const;

    private:
	/** @brief  Id ManagementUnit */
	int Id;
	/** @brief  Name ManagementUnit */
	string Name;
	/** @brief  Name ManagementUnit */
	double Area;
	/** @brief  Map of all the LandUnit object of the ManagementUnit */
	t_LuLst mLU_lst;
    };

} // namespace crash

#endif //MANAGEMENTUNIT_H
