/**
 * @file
 * @author RECORD Team,
 *   http://record.toulouse.inra.fr ,
 *   Applied Mathematics and Informatics Research Division &
 *   Environment and Agronomy Research Division,
 *   French National Institute for Agricultural Research
 * @version d: $
 */

/*
 * Copyright (C) 2009 - INRA
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */


#include <belief/structuralknowledge/systemknowledge/farmland/LandUnit.hpp>


namespace crash
{
    LandUnit::LandUnit() :
	soil("unknown soil"),
	landcover(),
	operation(),
        id(0),
        name("LU0"),
	ref_CroppingBlock(0),
	ref_waterresource(0),
        Polygon(),
        Adjacent_lst(),
        nb_Adjacent(0)
    {};

    LandUnit::LandUnit(int IdLU,  int RefCB, int RefWS, geom::Geometry* PolygonLU, string code_SoilType):
	soil(code_SoilType),
	landcover(),
	operation(),
	id(IdLU),
        name( str(boost::format("%1%_%2%") % "LU_" % id)),
	ref_CroppingBlock(RefCB),
	ref_waterresource(RefWS),
        Polygon(PolygonLU),
        Adjacent_lst(),
        nb_Adjacent(0)
    {};


    int LandUnit::getId() {
	return id;
    }

    int LandUnit::getId() const {
	return id;
    }

    string LandUnit::getName() {
	return name;
    }

    string LandUnit::getName() const {
	return name;
    }

    double LandUnit::getArea() {
	return (Polygon->getArea()/10000);
    }

    double LandUnit::getArea() const {
	return (Polygon->getArea()/10000);
    }

    int LandUnit::getRefCB() {
	return ref_CroppingBlock;
    }

    int LandUnit::getRefCB() const {
	return ref_CroppingBlock;
    }

    int LandUnit::getRefWS() {
	return ref_waterresource;
    }

    int LandUnit::getRefWS() const {
	return ref_waterresource;
    }

    geom::Geometry* LandUnit::getPolygon() {
	return Polygon;
    }

    geom::Geometry* LandUnit::getPolygon() const {
	return Polygon;
    }
    LandCover& LandUnit::getpLandCover(){
	return landcover;
    }

    const LandCover& LandUnit::getpLandCover() const {
	return landcover;
    }

    Soil& LandUnit::getpSoil(){
	return soil;
    }

    const Soil& LandUnit::getpSoil() const {
	return soil;
    }

  DoneOperation & LandUnit::getpDoneOperation() {
    return operation;
  }

  const DoneOperation & LandUnit::getpDoneOperation() const {
    return operation;
  }

    void LandUnit::addAdjacent(int _id) {
	Adjacent_lst.push_back(_id);
	nb_Adjacent = Adjacent_lst.size();
    }
 

    LandUnit::t_Adjacent_lst LandUnit::getAdjacentLst() const {
	return Adjacent_lst;
    }


    int LandUnit::getNb_AdjacentLu() {
	return Adjacent_lst.size();
    }

    int LandUnit::getNb_AdjacentLu() const {
	return  Adjacent_lst.size();
    }

} // namespace crash

