/**
 * @file
 * @author RECORD Team,
 *   http://record.toulouse.inra.fr ,
 *   Applied Mathematics and Informatics Research Division &
 *   Environment and Agronomy Research Division,
 *   French National Institute for Agricultural Research
 * @version d: $
 */

/*
 * Copyright (C) 2009 - INRA
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#include <belief/structuralknowledge/systemknowledge/farmland/ManagementUnit.hpp>


namespace crash
{

    ManagementUnit::ManagementUnit(int id_MU, string name_MU, t_LuLst LU_lst):
        Id (id_MU),
        Name(name_MU),
	mLU_lst(LU_lst)
    {};

    ManagementUnit::ManagementUnit(int id_MU, string name_MU, LandUnit*  LUpt):
        Id (id_MU),
        Name(name_MU)
    {
        addLU(LUpt);
    };

    ManagementUnit::~ManagementUnit() {};


    int ManagementUnit::getId()
    {
        return Id;
    }

    int ManagementUnit::getId()	const
    {
        return Id;
    }

    void ManagementUnit::addLU(LandUnit*  _LUpt)
    {
      //add *LU to the map in the ManagementUnit objects
      mLU_lst.push_back(_LUpt);
    }

    ManagementUnit::t_LuLst ManagementUnit::getLuLst() const
    {
        return mLU_lst;
    }

    ManagementUnit::t_LuIdLst ManagementUnit::getLuIdLst() const
    {
	t_LuIdLst lst;
	for (size_type i = 0; i !=  mLU_lst.size(); ++i)
        {        
	    lst.push_back(mLU_lst.at(i)->getId());
        }
        return lst;
    }

    double ManagementUnit::getArea()
    {
        double area_MU = 0.0;
        for (size_type i = 0; i !=  mLU_lst.size(); ++i)
        {        
            area_MU = area_MU + (mLU_lst.at(i))->getArea();
        }
        return area_MU;
    }

    double ManagementUnit::getArea() const
    {
        double area_MU = 0.0;
        for (size_type i = 0; i !=  mLU_lst.size(); ++i)
        {        
            area_MU = area_MU + (mLU_lst.at(i))->getArea();
        }
	
        return area_MU;
    }

    int ManagementUnit::getNb_Adjacent() const
    {
        int adj = 0;
        for (size_type i = 0; i !=  mLU_lst.size(); ++i)
        {        
            adj = adj + (mLU_lst.at(i))->getNb_AdjacentLu();
        }
	
        return adj;
    }
} // namespace crash


