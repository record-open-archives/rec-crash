/**
 * @file
 * @author RECORD Team,
 *   http://record.toulouse.inra.fr ,
 *   Applied Mathematics and Informatics Research Division &
 *   Environment and Agronomy Research Division,
 *   French National Institute for Agricultural Research
 * @version d: $
 */

/*
 * Copyright (C) 2009 - INRA
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#ifndef PLOTUNIT_H
#define PLOTUNIT_H

//#include <vle/extension/Decision.hpp>
#include <vle/utils/Tools.hpp>
#include <vle/utils/DateTime.hpp>
#include <sstream>
#include <numeric>
#include <iterator>
#include <boost/algorithm/string.hpp>


//Crash file
#include <belief/structuralknowledge/systemknowledge/farmland/ManagementUnit.hpp>

#include <belief/structuralknowledge/expertknowledge/SoilTypes.hpp>
#include <belief/structuralknowledge/expertknowledge/Crops.hpp>
using namespace std;
using boost::lexical_cast;
using boost::bad_lexical_cast;
namespace ve = vle::extension::decision;
namespace vd = vle::devs;
namespace vu = vle::utils;
namespace vv = vle::value;   
 
namespace crash { 

  /**
   * @brief PlotUnit: This class is part of the CrashAgent Belief. It contains all attributes
   * that the CrashAgent knows  about the PlotUnit. A PlotUnit is management unit.
   */
  class PlotUnit : public ManagementUnit {

  public:
    /**
     * @brief Main constructor used at model initialization
     * @param _Id_PlotUnit
     */
      PlotUnit(int RefPU, vector<LandUnit*> LU_lst): 
      ManagementUnit(RefPU, str(boost::format("%1%_%2%") % "PU_" % RefPU), LU_lst),
      code_Crop("unknown Crop")
    {}
		
    /**
     * @brief Default destructor 
     */
    virtual ~PlotUnit() 
    {} 

    /** @brief Predicate: BearingCapacity. The bearing capacity is FALSE if one LandUnit SAWC if < th_SAWC   */
    bool Pred_BearingCapacity() const;

    /**
     * @brief Predicate of the variable H2Orec
     * This predicate is TRUE if one LandUnit averageH2Orec < d_InnForFertilization
     */
    bool Pred_H2Orec() const;

    /**
     * @brief Predicate of the varaible INN
     * This predicate is TRUE if one LandUnit averageINN < d_H2OrecforHarvesting
     */
    bool Pred_INN() const;

    /**
     * @brief Predicate of the variavle Swfac
     */
    bool Pred_Swfac() const;

    /**
     * @brief Predicate of the variable Airg
     */
    bool Pred_Airg() const;


    bool isAlwaysFalse() const;

    /**
     * @brief This function SET The name of Crop
     * @param _nameCrop the name of Crop
     */
    void setCropCode(string _codeCrop);

    /**
     * @brief This function GET the name of Crop
     */
    string getCropCode();

    /**
     * @brief Thise function Get the const name of Crop
     */
    string getCropCode() const;

	
  void semis(const std::string& name, const ve::Activity& activity, 
	     vd::ExternalEventList& output) const;

  void TestDecision(const std::string& name, const ve::Activity& activity, 
		    vd::ExternalEventList& output) const;

           
  inline friend std::ostream& operator<<(std::ostream& out, 
					 const PlotUnit& pu) {
    out <<"\tPlotUnit: "<< pu.getId() <<endl;        
                
    t_LuLst Lulst = pu.getLuLst();

    for (size_type i = 0; i !=  Lulst.size(); ++i) {         
	out <<"\t\tLandUnit: "<< (Lulst.at(i))->getId()<<"\tArea: "<< (Lulst.at(i))->getArea() <<endl;
    } 
    out <<"\t\t\t\tTotal Area:" << pu.getArea()<<endl;
    return out;
  }
  private: 
    //Variables
    string code_Crop;
};
 
} // namespace crash 

#endif //PLOTUNIT_H

