/**
 * @file
 * @author RECORD Team,
 *   http://record.toulouse.inra.fr ,
 *   Applied Mathematics and Informatics Research Division &
 *   Environment and Agronomy Research Division,
 *   French National Institute for Agricultural Research
 * @version d: $
 */

/*
 * Copyright (C) 2009 - INRA
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */


#include <belief/structuralknowledge/systemknowledge/farmland/CapIslet.hpp>
 
namespace crash { 

//    bool CapIslet::Pred_BearingCapacity() const
//    {
//    
////    doit le connaitre double th_PercentageSAWC  get th_
//        int PredicateOutput=0;
//        
//        t_landunitsPtMap LUptMap = getLandUnitsPtMap();
//        for (t_landunitsPtMap::const_iterator iter = mLUsMap.begin(); iter != mLUsMap.end(); iter++)
//        {            
//            double SAWC_PU = (iter->second)->getSAWC_LandUnit();
//            double MaxSAWC_PU = (iter->second)->getMaxSAWC_LandUnit();
//            double PercentageSAWC = SAWC_PU/MaxSAWC_PU;
//            
//            if(PercentageSAWC>th_PercentageSAWC)
//            {
//                PredicateOutput=PredicateOutput+1;            
//            }  
//        }
//        return  PredicateOutput==0;
//    }

 
} // namespace crash 

