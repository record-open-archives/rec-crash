#ifndef DONEOPERATION_HPP
#define DONEOPERATION_HPP

#include <iostream>
#include <fstream>
#include <string>

#include <vle/utils.hpp>
#include <vle/devs.hpp>

using namespace std;

namespace vd = vle::devs;
namespace vv = vle::value;
namespace vz = vle::vpz;
namespace vu = vle::utils;

namespace crash {


  class DoneOperation {

  public:

    /**
     * @brief Default constructor
     */
    DoneOperation() :  mAirg(0) {}

    /**
     * @brief Default destructor
     */
    virtual ~DoneOperation() {}

    /**
     * @brief
     */
    void setAirg(const vv::Value & _airg);

    /**
     * @brief
     */
    double getAirg();

    /**
     * @brief
     */
    double getAirg() const;

  private:

    /** @brief */
    double mAirg;

  };

} //namespace crash

#endif // DONEOPERATION_HPP
