/**
 * @file LandUnit.hpp
 * @author The vle Development Team
 */

/*
 * VLE Environment - the multimodeling and simulation environment
 * This file is a part of the VLE environment (http://vle.univ-littoral.fr)
 * Copyright (C) 2003 - 2009 The VLE Development Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef RECORD_LandUnits_HPP
#define RECORD_LandUnits_HPP

#include <string>
#include <ostream>
#include <vector>
#include <boost/lexical_cast.hpp>


//Crash
#include <belief/SingletonBelief.hpp>
//Crash utils
#include <utils/CrashDBS.hpp>


// Crash system knowledge
#include <belief/structuralknowledge/systemknowledge/farmland/LandUnit.hpp>


using namespace boost;
using namespace std;
namespace vd=vle::devs;
namespace vv=vle::value;

namespace crash {

    class LandUnits : public SingletonBelief <LandUnits, LandUnit> {

	/** @brief The class LandUnits is a vector of LandUnitDomain object
	 */
	friend class SingletonBelief <LandUnits, LandUnit>;

    private:
	/** 
	 * @brief Default constructor 
	 */
	LandUnits();	
	
	/** 
	 * @brief Default destructor 
	 */
	virtual ~LandUnits();

	/**
	 * @brief number of border between landunits
	 */ 
	int nb_adj;


    public:
	/**
	 * @brief 
	 */ 
	LandUnit& getAtref(size_type& _index);
	/**
	 * @brief 
	 */ 
	LandUnit& getRefById(int _id);
	/**
	 * @brief set nb_adj + set every the LandUnit's adjacent
	 */           
	void setAdjacent(int _id);     
	int getAdjacent();
	int getAdjacent() const;

	/**
	 * @brief Initialize the object LandUnit
	 */
      size_type	init(const vd::InitEventList & evts);

    };

} // end namespace crash

#endif

