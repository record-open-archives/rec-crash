/**
 * @file
 * @author RECORD Team,
 *   http://record.toulouse.inra.fr ,
 *   Applied Mathematics and Informatics Research Division &
 *   Environment and Agronomy Research Division,
 *   French National Institute for Agricultural Research
 * @version d: $
 */

/*
 * Copyright (C) 2009 - INRA
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#ifndef CROPPINGBLOCK_H
#define CROPPINGBLOCK_H

#include <vle/extension/Decision.hpp>
#include <vle/utils/Tools.hpp>
#include <vle/utils/DateTime.hpp>
#include <sstream>
#include <numeric>
#include <iterator>
#include <boost/algorithm/string.hpp>


//Crash file
#include <belief/structuralknowledge/systemknowledge/farmland/ManagementUnit.hpp>

using namespace std;
using boost::lexical_cast;
using boost::bad_lexical_cast;
namespace ve = vle::extension::decision;
namespace vd = vle::devs;
namespace vu = vle::utils;
namespace vv = vle::value;   
 
namespace crash { 
/**
 * @brief CroppingBlock: This class is part of the CrashAgent Belief. It contains all attributes
 * that the CrashAgent knows  about the CroppingBlock. A CroppingBlock is management unit.
 */
    class CroppingBlock : public ManagementUnit
    {
    public:
	/**
	 * @brief Main constructor used at model initialization
	 * @param _Id_CroppingBlock
	 */
	CroppingBlock(int RefCB, LandUnit*  LUpt): 
	    ManagementUnit(RefCB, str(boost::format("%1%_%2%") % "CB_" % RefCB), LUpt)
	{}
		
	/**
	 * @brief Default destructor 
	 */
	virtual ~CroppingBlock()
	{} 

		
	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
	inline friend std::ostream& operator<<(std::ostream& out, const CroppingBlock& cb)
	{
	    out <<"\tCroppingBlock: "<< cb.getId() <<endl;                       
	    t_LuLst Lulst = cb.getLuLst();
	    for (size_type i = 0; i !=  Lulst.size(); ++i)
	    {          
		out <<"\t\tLandUnit: "<< (Lulst.at(i))->getId()<<"\tArea: "<< (Lulst.at(i))->getArea() <<endl; 
	    } 
	    out <<"\t\t\t\tTotal Area:" << cb.getArea();
	    return out;
	}
    private:  
    };
 
} // namespace crash 

#endif //CROPPINGBLOCK_H

