#ifndef PLOTUNITS_HPP
#define PLOTUNITS_HPP

#include <string>
#include <vector>
#include <ostream>
#include <boost/lexical_cast.hpp>

//Crash
#include <belief/SingletonBelief.hpp>
//Crash utils
#include <utils/CrashDBS.hpp>
#include <utils/Utils.hpp>

//Crash System knowledge
#include <belief/structuralknowledge/systemknowledge/farmland/PlotUnit.hpp>
#include <belief/structuralknowledge/systemknowledge/farmland/LandUnits.hpp>


namespace crash {

  /**
   * @brief The class PlotUnits is a vector of PlotUnit object
   */
  class PlotUnits : public SingletonBelief<PlotUnits, PlotUnit> {

    friend class SingletonBelief<PlotUnits, PlotUnit>;

  public:

    /**
     * @brief Default constructor
     */
    PlotUnits();
    /**
     * @brief Default destructor
     */
    virtual ~PlotUnits();

  public:

    /**
     * @brief Initialization of PlotUnit object from database
     */
    size_type init(const vd::InitEventList & evts);
    /**
     * @brief This function add a Land Unit in each Plot unit
     */
    void addLuToPlot(const PlotUnit & _pu, LandUnit * _lupt);

  };

}
#endif //PLOTUNITS_HPP
