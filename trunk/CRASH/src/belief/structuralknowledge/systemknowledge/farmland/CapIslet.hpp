/**
 * @file
 * @author RECORD Team,
 *   http://record.toulouse.inra.fr ,
 *   Applied Mathematics and Informatics Research Division &
 *   Environment and Agronomy Research Division,
 *   French National Institute for Agricultural Research
 * @version d: $
 */

/*
 * Copyright (C) 2009 - INRA
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#ifndef CapIslet_H
#define CapIslet_H

#include <vle/extension/Decision.hpp>
#include <vle/utils/Tools.hpp>
#include <vle/utils/DateTime.hpp>
#include <sstream>
#include <numeric>
#include <iterator>
#include <boost/algorithm/string.hpp>


#include <belief/structuralknowledge/systemknowledge/farmland/ManagementUnit.hpp>

using namespace std;
using boost::lexical_cast;
using boost::bad_lexical_cast;
namespace ve = vle::extension::decision;
namespace vd = vle::devs;
namespace vu = vle::utils;
namespace vv = vle::value;   
 
namespace crash { 
/**
 * @brief CapIslet: This class is part of the CrashAgent Belief. It contains all attributes
 * that the CrashAgent knows  about the CapIslet. A CapIslet is management unit.
 */
class CapIslet : public ManagementUnit
{
public:
//  typedefs
//  variables  
//  functions
	/**
	* @brief Main constructor used at model initialization
	* @param _Id_CapIslet
	*/
	CapIslet(int RefCI, LandUnit* LUpt, const ve::Agent& a): 
	    ManagementUnit(RefCI, str(boost::format("%1%_%2%") % "CI_" % RefCI), LUpt, a )
	{}
	
	
	/**
    * @brief Default destructor 
    */
	virtual ~CapIslet()
	{} 

	/** @brief Predicate: BearingCapacity. The bearing capacity is FALSE if one LandUnit SAWC if < th_SAWC   */
//    bool Pred_BearingCapacity(double th_SAWC) const;
	


	
 
 
protected:
//  typedefs
//  variables
//  functions
 
private:  
//  typedefs
//  variables
//  functions 
 


};
 
} // namespace crash 

#endif //CapIslet_H

