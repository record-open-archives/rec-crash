#include <belief/structuralknowledge/systemknowledge/farmland/LandUnits.hpp>

using vle::utils::ArgError;
using namespace std;
using namespace boost;

namespace crash
{
    // Constructor
    LandUnits::LandUnits():
	nb_adj(0)
    {};
    // Destructor
    LandUnits::~LandUnits()
    {};
   
    LandUnit& LandUnits::getAtref(LandUnits::size_type& _index)
    {
        if (_index < nb_elements)
        {
            return 	 ListObject_lst.at(_index) ;
        }
        else
        {
            throw std::runtime_error("LandUnits:: getAt");
        }
    }
    
    LandUnit& LandUnits::getRefById(int _id)
    {
        
        for (size_type i = 0; i != ListObject_lst.size(); ++i)
        {
            LandUnit& lu = ListObject_lst.at(i);
            if(lu.getId() == _id)
            {
                return lu;
            }
        }
        throw std::runtime_error("LandUnits:: getRefById");
    }

    void LandUnits::setAdjacent(int _id)
    {
        LandUnit& lu1 = getRefById(_id);       
        for (size_type i = 0; i != ListObject_lst.size(); ++i)
        {
            LandUnit& lu2 = ListObject_lst.at(i);
            if(lu2.getId() != _id and lu1.getPolygon()->intersects(lu2.getPolygon())) // si il est voisin
            {
                lu1.addAdjacent(lu2.getId());
		nb_adj =nb_adj + 1;
            }
        }
    }

    int LandUnits::getAdjacent()
    {
	return nb_adj;
    }

    int LandUnits::getAdjacent() const
    {
	return nb_adj;
    }    

    LandUnits::size_type	LandUnits::init(const vd::InitEventList & evts)
    {
	CrashDBS cdb(evts);
	std::string sql = "(SELECT id_landunit, ref_cb, ref_wr, code_soiltype, astext(the_geom) as the_geom FROM systemknowledge.farmland);";
	PGresult* db_lu= cdb.QueryDB(sql.c_str());
	int nb_elements =  PQntuples(db_lu);
	
	if(nb_elements<1){
	    throw ArgError("/!\\ LandUnits.cpp:: Cannot find anuy LandUnit in the table systemknowledge.farmland");
	}

        for (int i=0; i<nb_elements; i++){
            int id_lu = lexical_cast<int>(PQgetvalue(db_lu, i, PQfnumber(db_lu, "id_landunit")));          
	    int ref_cb = lexical_cast<int>(PQgetvalue(db_lu, i, PQfnumber(db_lu, "ref_cb")));
	    int ref_wr = lexical_cast<int>(PQgetvalue(db_lu, i, PQfnumber(db_lu, "ref_wr")));
            string code_st = lexical_cast<string>(PQgetvalue(db_lu, i, PQfnumber(db_lu, "code_soiltype")));
            string geom = lexical_cast<string>(PQgetvalue(db_lu, i, PQfnumber(db_lu, "the_geom")));
            WKTReader wktr;
            geom::Geometry* Poly = wktr.read(geom);
            LandUnit lu(id_lu, ref_cb, ref_wr, Poly, code_st);
            add(lu);
	    cout<<"lu :"<<id_lu<<endl;
        }

        // set Neighbour list
        for (int i=0; i<nb_elements; i++){
            setAdjacent(at(i).getId());
        }        
        return nb_elements;
    }
} // end namespace crash



