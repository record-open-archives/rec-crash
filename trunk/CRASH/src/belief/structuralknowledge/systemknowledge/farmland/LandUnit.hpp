/**
 * @file
 * @author RECORD Team,
 *   http://record.toulouse.inra.fr ,
 *   Applied Mathematics and Informatics Research Division &
 *   Environment and Agronomy Research Division,
 *   French National Institute for Agricultural Research
 * @version d: $
 */

/*
 * Copyright (C) 2009 - INRA
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */


#ifndef LANDUNIT_H
#define LANDUNIT_H

#include <iostream>
#include <fstream>
#include <string>
#include <geos.h>
#include <iterator>
#include <boost/algorithm/string.hpp>

#include <vle/utils.hpp>
#include <vle/devs.hpp>

#include <belief/structuralknowledge/systemknowledge/farmland/LandCover.hpp>
#include <belief/structuralknowledge/systemknowledge/farmland/Soil.hpp>
#include <belief/structuralknowledge/systemknowledge/farmland/DoneOperation.hpp>


using namespace geos;
using namespace std;
using boost::lexical_cast;

namespace vd = vle::devs;
namespace vv = vle::value;
namespace vz = vle::vpz;
namespace vu = vle::utils;

namespace crash
{
    /**
     * @brief LandUnit: This class is part of the CrashAgent Belief. It contains all attributes
     * that the CrashAgent knows  about the Land Unit
     */
    class LandUnit
    {
    public:
	//  typedefs
	typedef std::vector<int> t_Adjacent_lst;
	//  variables

	//  functions
	/** @brief Default constructor */
	LandUnit();

	/** @brief Main constructor used at model initialization */
	LandUnit(int IdLU,int RefCB,  int RefWS, geom::Geometry* PolygonLU, string name_SoilType);

	/** @brief Default destructor */
	virtual ~LandUnit()
	{}

	/** @brief Get the landunit id (int)*/
	int getId();
	int getId()	const;

	/** @brief Get the landunit name:e.g.  LU1*/
	string getName();
	string getName()	const;

	/**@brief Get the landunit area (double) --> (m²)*/
	double getArea();
	double getArea()	const ;

	/** @brief Get the landunit ref_croppingblock (int) */
	int getRefCB();
	int getRefCB()	const;    

	/** @brief Get the landunit ref_waterresource */
	int getRefWS();
	int getRefWS() const;
        
	/** @brief Get the Polygon */
	geom::Geometry* getPolygon();
	geom::Geometry* getPolygon()	const;

	/**@brief Get the object Landcover*/
	LandCover & getpLandCover();
	const LandCover& getpLandCover() const;

	/**
	 * @brief Get the object Soil
	 */
	Soil & getpSoil();
	const Soil& getpSoil() const;

	/**
	 * @brief Get the object DoneOperation
	 */
	DoneOperation & getpDoneOperation();
	const DoneOperation & getpDoneOperation() const;

	/** 
	 * @brief Get list of id of adjacent landunits
	 */
	virtual t_Adjacent_lst getAdjacentLst() const;

	/** 
	 * @brief add LandUnit* to neighbour list 
	 */            
	void addAdjacent(int _id);

	/** 
	 * @brief get Nb of adjacent lu
	 */ 
	int getNb_AdjacentLu();
	int getNb_AdjacentLu() const;
            
	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
	inline friend std::ostream& operator<<(std::ostream& out, const LandUnit& lu)
	{
	    out <<"\t" << "LandUnit: " << lu.getId() <<  endl;
	    out <<"\t\t\t Area: \t\t"<< lu.getArea() << " ha" << endl;
	    out <<"\t\t\t Cropping Block: \t\tn°"<<lu.getRefCB() << endl;
	    out <<"\t\t\t Water Resource: \t\tn°"<<lu.getRefWS() << endl;
	    //out <<"\t\t\t" << " SoilType: \t" << lu.getpSoil().getCodeSoilType();
	    out <<"\t\t\t" << " nb_Adjacent: \t" << lu.getAdjacentLst().size()<<endl;
	    return out;
	}

    private:

	/**
	 * @brief LandCover Object 
	 */
	LandCover landcover;

	/** 
	 * @brief Soil Object 
	 */  
	Soil  soil;     

      /**
       * @brief
       */
      DoneOperation operation;

	/**
	 * @brief Id 
	 */
	int id;
	/**
	 * @brief Name (e.g. LU_1) 
	 */
	string name;

	/** 
	 * @brief CroppingBlock reference
	 */
	double ref_CroppingBlock;
	/**
	 * @brief waterresource reference
	 */
	int ref_waterresource;
	/** 
	 * @brief Spatial polygon build from wkt_string
	 */
	geom::Geometry* Polygon;
	/** 
	 * @brief  Map of all the LandUnit object of the ManagementUnit
	 */
	t_Adjacent_lst Adjacent_lst;
	/** 
	 * @brief  Number of Neighbour
	 */
	int nb_Adjacent;

    };
} // namespace crash

#endif //LANDUNIT_H

