/**
 * @file
 * @author RECORD Team,
 *   http://record.toulouse.inra.fr ,
 *   Applied Mathematics and Informatics Research Division &
 *   Environment and Agronomy Research Division,
 *   French National Institute for Agricultural Research
 * @version d: $
 */

/*
 * Copyright (C) 2009 - INRA
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#include <belief/structuralknowledge/systemknowledge/farmland/PlotUnit.hpp>

namespace crash
{

    bool PlotUnit::Pred_BearingCapacity() const
    {
        //int PredicateOutput=0;
        //t_LuLst Lulst = getLuLst();
	//for (size_type i = 0; i !=  Lulst.size(); ++i)
        //{
            //string name_ST = (Lulst.at(i))->getpSoil().getCodeSoilType();
            //const double d_SAWCforSowing  = SoilTypes::getInstance().getByCode(name_ST).getSAWCforSowing();
            //double SAWC = (Lulst.at(i))->getpSoil().getSAWC();
            //double MaxSAWC = (Lulst.at(i))->getpSoil().getMaxSAWC();
            //double PercentageSAWC = SAWC/MaxSAWC;
            //if(PercentageSAWC>d_SAWCforSowing) PredicateOutput=PredicateOutput+1;
        //}

        //if(PredicateOutput>0)
        //{
	  ////cout << "*** Pred_BearingCapacity is FALSE ***" << endl;
            //return 0;
        //}
        //else
        //{
	  ////cout << "*** Pred_BearingCapacity is TRUE ***" << endl;
            //return 1;
        //}
    }


  bool PlotUnit:: Pred_H2Orec() const {
         //double Sum_H2Orec = 0;
        //t_LuLst Lulst = getLuLst();
	//string _codeCrop = "BH";// getCropCode();
	//const double d_H2OrecforHarvesting  = Crops::getInstance().getByCode(_codeCrop).getH2orecHrv();
	//for (size_type i = 0; i !=  Lulst.size(); ++i) {
	  //double H2Orec = (Lulst.at(i))->getpLandCover().getH2Orec();
	  //Sum_H2Orec += H2Orec;
	  //cout << "Sum_H2Orec :" << Sum_H2Orec << endl;
	//}

	//double averageH2Orec = Sum_H2Orec/Lulst.size();
        //if(averageH2Orec < d_H2OrecforHarvesting )
	//{
            //return 1;
        //} else {
	 return 0;
        //}
  }



  bool PlotUnit::Pred_INN() const {
    //double Sum_INN = 0;
    //t_LuLst Lulst = getLuLst();
    //string _nameCrop = "BH";//getCropCode();
     //const double d_InnForFertilization = Crops::getInstance().getByCode(_nameCrop).getInnFer();
    //for(size_type i = 0; i != Lulst.size(); ++i) {
      //double INN = (Lulst.at(i))->getpLandCover().getINN();
      //Sum_INN += INN;
    //}
    //double averageINN = Sum_INN/Lulst.size();
    //if(averageINN < d_InnForFertilization) {
       //return 1;
    //} else {
      return 0;
    //}
  }



  bool PlotUnit::Pred_Swfac() const {
     //double Sum_Swfac;
     //t_LuLst Lulst = getLuLst();
     //string _codeCrop = "BH";//getCropCode();
     //const double d_SwfacForIrrigation = Crops::getInstance().getByCode(_codeCrop).getSwfacIrr();

     //cout << "d_SwfacForIrrigation: " << d_SwfacForIrrigation << endl;
     //for(size_type i=0; i != Lulst.size(); ++i) {
       //double Swfac = (Lulst.at(i))->getpLandCover().getSwfac();
       //Sum_Swfac += Swfac;
     //}
     //double averageSwfac = Sum_Swfac/Lulst.size();
     //if(averageSwfac < d_SwfacForIrrigation) {
       //return 1;
     //} else {
      return 0;
     //}
  }

  bool PlotUnit::isAlwaysFalse() const {
    return 1;
  }


  void PlotUnit::setCropCode(string _codeCrop) {
    code_Crop = _codeCrop;
  }


  string PlotUnit::getCropCode() {
    return  code_Crop;
  }

  string PlotUnit::getCropCode() const {
    return code_Crop;
  }

    void PlotUnit::semis(const std::string& name, const ve::Activity& activity,
			 vd::ExternalEventList& output) const
    {
        if (activity.isInStartedState()) {
            vd::ExternalEvent* evt = new vd::ExternalEvent("Sowing_" + lexical_cast<string>(getId()));
            evt->putAttribute("name", new vv::String(name));
            evt->putAttribute("activity", new vv::String(name));
            evt->putAttribute("densite", new vv::Double(10.));
            output.addEvent(evt);
	}
    }


 void PlotUnit::TestDecision(const std::string& name, const ve::Activity& activity,
            vd::ExternalEventList& output) const
    {
        if (activity.isInStartedState()) {
            vd::ExternalEvent* evt = new vd::ExternalEvent("Sowing_" + lexical_cast<string>(getId()));
            evt->putAttribute("name", new vv::String(name));
            evt->putAttribute("activity", new vv::String(name));
            output.addEvent(evt);
         }
    }



} // namespace crash

