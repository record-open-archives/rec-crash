#include <vle/utils/Path.hpp>
#include <vle/utils/Exception.hpp>

#include <belief/structuralknowledge/systemknowledge/farmland/PlotUnits.hpp>

using vle::utils::ArgError;
using namespace std;
using namespace boost;

namespace crash {


    PlotUnits::PlotUnits() {}

    PlotUnits::~PlotUnits() {}

    void PlotUnits::addLuToPlot(const PlotUnit& _pu, LandUnit* _lupt) {       
	for (size_type i = 0; i != ListObject_lst.size(); ++i) {
	    if(ListObject_lst.at(i).getId()==_pu.getId()) ListObject_lst.at(i).addLU(_lupt);
	}
    }



    PlotUnits::size_type	PlotUnits::init(const vd::InitEventList & evts) {
	CrashDBS cdb(evts);
	std::string sql = "SELECT id_plotunit, concatenate(cast(id_landunit as varchar)) as lst_lu FROM systemknowledge.plotunit, systemknowledge.landunit WHERE intersects(plotunit.the_geom, landunit.the_geom) GROUP BY id_plotunit";
	PGresult* db_pu= cdb.QueryDB(sql.c_str());
        int nb_elements =  PQntuples(db_pu);

	if(nb_elements<1){
	    throw ArgError("/!\\ PlotUnits.cpp:: Cannot find anuy PlotUnit in the table systemknowledge.plotunit");
	}

        for (int i=0; i<nb_elements; i++){
            int id_pu = lexical_cast<int>(PQgetvalue(db_pu, i, PQfnumber(db_pu, "id_plotunit")));
          
	    vector<int> lu_lst = Utils::getInstance().getVectInt(lexical_cast<string>(PQgetvalue(db_pu, i, PQfnumber(db_pu, "lst_lu"))), ",");
	    vector<LandUnit*> LU_lst;

	    for(size_type j=0; j!=lu_lst.size() ; j++){
		LandUnit* LUpt = &(LandUnits::getInstance().getRefById(lu_lst.at(j)));
		LU_lst.push_back(LUpt);
	    }

	    PlotUnit pu(id_pu, LU_lst);
	    ListObject_lst.push_back(pu);
        }
	return nb_elements;
    }
}
