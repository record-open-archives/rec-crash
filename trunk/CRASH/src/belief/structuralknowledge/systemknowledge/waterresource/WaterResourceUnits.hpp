/**
 * @author Dragan AMENGA MBENGOUE
 *
 */
#ifndef WATERRESOURCEUNITS_HPP
#define WATERRESOURCEUNITS_HPP

#include <string>
#include <ostream>
#include <vector>
#include <boost/lexical_cast.hpp>

#include <vle/extension/Decision.hpp>
#include <vle/utils.hpp>
#include <vle/devs.hpp>
//Crash
#include <belief/SingletonBelief.hpp>
//System knowledge
#include <belief/structuralknowledge/systemknowledge/waterresource/WaterResourceUnit.hpp>
#include <belief/structuralknowledge/systemknowledge/farmland/LandUnits.hpp>
//
#include <utils/CrashDBS.hpp>
using namespace std;
namespace vd=vle::devs;
namespace vv=vle::value;

namespace crash {

  /**
   * @brief The class WaterResourceUnits is a vector of WaterResource object
   */
  class WaterResourceUnits : public SingletonBelief<WaterResourceUnits,
						    WaterResourceUnit> {

    friend class SingletonBelief<WaterResourceUnits, WaterResourceUnit>;

  private:
    /**
     * @brief Default constructor
     */
    WaterResourceUnits();
    /**
     * @brief Default destructor
     */
    virtual ~WaterResourceUnits();

  public:
    size_type init(const vd::InitEventList & evts);
    void addLUToWaterResourceUnit(const WaterResourceUnit & wR, LandUnit* _lupt);

  };

}

#endif //WATERRESOURCEUNITS_HPP
