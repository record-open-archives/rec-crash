/**
 * @author Dragan AMENGA MBENGOUE
 */
#ifndef WATERRESOURCEUNIT_HPP
#define WATERRESOURCEUNIT_HPP

#include <vle/utils/Tools.hpp>
#include <vle/utils/DateTime.hpp>
#include <sstream>
#include <numeric>
#include <iterator>
#include <boost/algorithm/string.hpp>

#include<belief/structuralknowledge/systemknowledge/farmland/ManagementUnit.hpp>

using namespace std;
using boost::lexical_cast;
using boost::bad_lexical_cast;
namespace ve = vle::extension::decision;
namespace vd = vle::devs;
namespace vu = vle::utils;
namespace vv = vle::value; 
  
namespace crash {

  class WaterResourceUnit : public ManagementUnit {

  public:

    /**
     * @brief The main constructor
     */
    WaterResourceUnit(int RefPU, LandUnit*  LUpt,
		      double _quantity, double _flowRate):
      ManagementUnit(RefPU, str(boost::format("%1%_%2%") % "PU_" % RefPU), LUpt),
      quantity(_quantity),
      flowRate(_flowRate)
    {}


    /**
     * @brief Default destructor
     */
    virtual ~WaterResourceUnit() {}



    inline friend std::ostream& operator<<(std::ostream & out, 
					   const WaterResourceUnit & wtRU)
    {
      out << "\t WaterResourceUnit: " << wtRU.getId() << endl; 
      out << "\t\t\tQuantity: " << wtRU.getQuantity() << endl;  
      out << "\t\t\tFlow rate:  " << wtRU.getFlowRate() << endl;                 
      t_LuLst Lulst = wtRU.getLuLst();
      for (size_type i = 0; i !=  Lulst.size(); ++i) {          
	out << "\t\t\tLandUnit: "<< (Lulst.at(i))->getId()<<"\tArea: "
	    << (Lulst.at(i))->getArea() <<endl; 
      } 
      out << "\t\t\tTotal Area:" << wtRU.getArea();

      return out;
    }

    /**
     * @brief This function set quantity of water
     */
    void setQuantity(const double & qT);
    /**
     * @brief This function set flow water
     */
    void setFlowRate(const double & fR);
    /**
     * @brief This function get the quantity of water
     */
    double getQuantity();
    /**
     * @brief This function get const quantity of water
     */
    double getQuantity() const;
    /**
     * @brief This function get flow rate
     */
    double getFlowRate();
    /**
     * @brief This function get const flow rate
     */
    double getFlowRate() const;

  private:
    /** @brief Quantity of water */
    double quantity;
    /** @brief Flow Rate */
    double flowRate;

  };

} //namespace crash


#endif //WATERRESOURCEUNIT_HPP
