#include <belief/structuralknowledge/systemknowledge/waterresource/WaterResourceUnit.hpp>


namespace crash {

  
  void WaterResourceUnit::setQuantity(const double & qT) {
    quantity = qT;
  }

  void WaterResourceUnit::setFlowRate(const double & fR) {
    flowRate = fR;
  }

  double WaterResourceUnit::getFlowRate() {
    return flowRate;
  }

  double WaterResourceUnit::getFlowRate() const {
    return flowRate;
  }

  double WaterResourceUnit::getQuantity() {
    return quantity;
  }

  double WaterResourceUnit::getQuantity() const {
    return quantity;
  }

} //namespace crash


