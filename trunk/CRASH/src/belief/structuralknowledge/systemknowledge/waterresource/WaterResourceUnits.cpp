#include <vle/utils/Path.hpp>
#include <vle/utils/Exception.hpp>   

#include <belief/structuralknowledge/systemknowledge/waterresource/WaterResourceUnits.hpp>

using vle::utils::ArgError;
using namespace std;
using namespace boost;

namespace crash {


  WaterResourceUnits::WaterResourceUnits() {}

  WaterResourceUnits::~WaterResourceUnits() {}

  void WaterResourceUnits::addLUToWaterResourceUnit(const WaterResourceUnit& wR,
						    LandUnit* _lupt) {
    for(size_type i=0; i != ListObject_lst.size(); ++i) {
      if(ListObject_lst.at(i).getId() == wR.getId()) ListObject_lst.at(i).addLU(_lupt);
    }
  }

  WaterResourceUnits::size_type	WaterResourceUnits::init(const vd::InitEventList & evts) {
    CrashDBS cdb(evts);
    PGresult* db_waterResource = cdb.QueryDB("SELECT * from systemknowledge.waterresource");
    int nb_resource = PQntuples(db_waterResource);
    for (size_type itLU = 0; itLU !=  LandUnits::getInstance().size(); ++itLU) {
	LandUnit* LUpt = &(LandUnits::getInstance().getAtref(itLU));
	int RefWS = LUpt->getRefWS();
	int _qt;
	int _debit;
	bool flagWR = false;
	for(int i=0; i < nb_resource; i++) {
	  int id_wr =  lexical_cast<int>(PQgetvalue(db_waterResource, i, PQfnumber(db_waterResource, "id_waterresource")));
	  if(id_wr == RefWS){
	    _qt = lexical_cast<int>(PQgetvalue(db_waterResource, i, PQfnumber(db_waterResource, "ref_quantity")));
	    _debit = lexical_cast<int>(PQgetvalue(db_waterResource, i, PQfnumber(db_waterResource, "ref_flowrate")));
	    flagWR = true;
	  }
	}

	if (flagWR == false ){
	    throw std::runtime_error(" WaterResourceUnits:init() Reference waterResource not exist");
	  }

	  WaterResourceUnit wR(RefWS, LUpt, _qt, _debit);          
	  if(size()==0) {
	    ListObject_lst.push_back(wR);
	    nb_elements = size();
	  } else {
	    bool flag = find(wR); 
	    if(flag==true) {      
	      addLUToWaterResourceUnit(wR, LUpt);
	    } else {            
	      ListObject_lst.push_back(wR);
	      nb_elements = size();
	    }
	  }
    }
    return nb_elements;
  }

} //namespace crash
