/**
 * @file
 * @author RECORD Team,
 *   http://record.toulouse.inra.fr ,
 *   Applied Mathematics and Informatics Research Division &
 *   Environment and Agronomy Research Division,
 *   French National Institute for Agricultural Research
 * @version $Id: $
 */

/*
 * Copyright (C) 2010 - INRA
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#include <CrashAgent.hpp>

namespace crash
{
       CrashAgent::CrashAgent(const vd::DynamicsInit& mdl, const vd::InitEventList& evts):
		ve::Agent(mdl, evts),
		mExpertKnowledge(evts),
		mFarmLand(evts),
		mCPS(),
		mWeather()
	{
		Utils::getInstance().init(evts);
		CropDomains::getInstance().init();
		bool WCSPTool = vle::value::toBoolean(evts.get("wcsp"));
		bool SimulationTool = vle::value::toBoolean(evts.get("simulation"));
		bool Filtering = vle::value::toBoolean(evts.get("filtering"));
		bool newFile = vle::value::toBoolean(evts.get("newfile"));
		int numSolution = vle::value::toInteger(evts.get("solution"));

		/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
		 * WCSP tools
		 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
		if(WCSPTool == TRUE)
		{
			cout<<"\n----------------------------------------------"<< endl;
			cout<<"\tCrashAgent: WCSP" << endl;
			cout<<"----------------------------------------------\n"<< endl;
			DecisionProfile::getInstance().init(evts);
			WCSPConstraintFactory mWCSPFactory;
			string _file = mWCSPFactory.createConstraintFile(mWCSPFactory.getWCSPDir(), mWCSPFactory.getWCSPFile());
			DecisionProfile::t_CstrMap::const_iterator itb = DecisionProfile::getInstance().getCstrOption().begin();
			DecisionProfile::t_CstrMap::const_iterator ite = DecisionProfile::getInstance().getCstrOption().end();

			for(; itb != ite; itb++) {
				if((itb->second)==true) {
					cout <<" options: " <<itb->first << endl;
					mWCSPFactory.generateConstraintsFiles(mWCSPFactory.getWCSPDir(), _file ,itb->first);
				}
			}
			std::string toto;
			cout<<"SAVE CONSTRAINTS: "<<endl;
			mWCSPFactory.setMaxCost();
			cout<<"\t ub: "<<mWCSPFactory.getUB()<<endl;
			char ub[16];
			sprintf (ub, "%d",  mWCSPFactory.getUB());

			mWCSPFactory.saveConstraintFile(_file, "Strategic_Decision", mWCSPFactory.getNbVar(),  mWCSPFactory.getNbCrops());
			System::getInstance().solve( mWCSPFactory.getWCSPDir(),  mWCSPFactory.getWCSPFile(),  "all" ,ub);
			System::getInstance().setTransformFilePath();
			System::getInstance().transform( mWCSPFactory.getWCSPDir(),  mWCSPFactory.getWCSPFile());
		}

		/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
		 * Filtering tools
		 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
		if(Filtering == TRUE)
		{
			cout<<"\n----------------------------------------------"<< endl;
			cout << "Filtering Tools" << endl;
			CroppingPlanCandidates::getInstance().init(Utils::getInstance().getWCSPDirName(), Utils::getInstance().getWCSPFileName());

			Filter ft;
			ft.selectSolution(numSolution);
			ft.saveSolution(Utils::getInstance().getWCSPDirName(), Utils::getInstance().getWCSPSeqFile(),
					numSolution, newFile);

			cout<<"\n----------------------------------------------"<< endl;
		}


		/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
		 * Simulation tools
		 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
		if(SimulationTool == TRUE){
			// Connection between port and fact
			for (LandUnits::size_type lu =0; lu != LandUnits::getInstance().size(); lu++)
			{
				//LandCover
				int mId = LandUnits::getInstance().at(lu).getId();
				cout << "mId :" << LandUnits::getInstance().at(lu).getId() << endl;
				addFact("Lai_" + lexical_cast<string>(mId),
						boost::bind(&LandCover::setLai, (&(&LandUnits::getInstance().at(lu))->getpLandCover()), _1));
				addFact("Mafruit_" + lexical_cast<string>(mId),
						boost::bind(&LandCover::setMafruit, (&(&LandUnits::getInstance().at(lu))->getpLandCover()), _1));
				addFact("VegetativeStage_" + lexical_cast<string>(mId),
						boost::bind(&LandCover::setVegetativeStage, (&(&LandUnits::getInstance().at(lu))->getpLandCover()), _1));
				addFact("H2Orec_" + lexical_cast<string>(mId),
						boost::bind(&LandCover::setH2oHrv, (&(&LandUnits::getInstance().at(lu))->getpLandCover()), _1));
				addFact("INN_" + lexical_cast<string>(mId),
						boost::bind(&LandCover::setINN, (&(&LandUnits::getInstance().at(lu))->getpLandCover()), _1));
				addFact("Swfac_" + lexical_cast<string>(mId),
						boost::bind(&LandCover::setSwfac, (&(&LandUnits::getInstance().at(lu))->getpLandCover()), _1));
				//Soil
				addFact("AvgHur_" + lexical_cast<string>(mId),
						boost::bind(&Soil::setAvgHur, (&(&LandUnits::getInstance().at(lu))->getpSoil()), _1));
				addFact("SAWC_" + lexical_cast<string>(mId),
						boost::bind(&Soil::setSAWC, (&(&LandUnits::getInstance().at(lu))->getpSoil()), _1));
				addFact("MaxSAWC_" + lexical_cast<string>(mId),
						boost::bind(&Soil::setMaxSAWC, (&(&LandUnits::getInstance().at(lu))->getpSoil()), _1));
				//DoneOperation
				addFact("Airg_" + lexical_cast<string>(mId),
						boost::bind(&DoneOperation::setAirg, (&LandUnits::getInstance().at(lu))->getpDoneOperation(), _1));
			}

			//Connection between port and fact (Market)
			Market::getInstance().init();
			for(Market::size_type j=0; j != Market::getInstance().size(); j++) {
				std::string _codeCrop = Market::getInstance().at(j).getCodeCrop();
				addFact(_codeCrop,
						boost::bind(&CropPrice::setPrice, (&Market::getInstance().at(j)), _1));
			}


			//Weather
			addFact("Rain",
					boost::bind(&Weather::setLst_rain, &mWeather, _1));
			addFact("Tmin",
					boost::bind(&Weather::setLst_tn, &mWeather, _1));

		}
	}
	CrashAgent::~CrashAgent() {};

}  // namespace crash
DECLARE_DYNAMICS(crash::CrashAgent);

