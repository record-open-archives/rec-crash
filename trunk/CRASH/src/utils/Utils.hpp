/**
 * @file DecisionProfileuccession.hpp
 * @author The vle Development Team
 */

/*
 * VLE Environment - the multimodeling and simulation environment
 * This file is a part of the VLE environment (http://vle.univ-littoral.fr)
 * Copyright (C) 2003 - 2009 The VLE Development Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef UTILS_HPP
#define UTILS_HPP

#include <string>
#include <ostream>
#include <vector>
#include <boost/lexical_cast.hpp>

//vle
#include <vle/utils.hpp>
#include <vle/utils/Tools.hpp>
#include <vle/devs.hpp>

#include <boost/tokenizer.hpp>

using namespace boost;
namespace vd = vle::devs;
namespace vv = vle::value;
namespace vu = vle::utils;
using namespace std;

namespace crash {

  /**
   * @brief 	Decision profile is a class that store key decision variable
   */
  class  Utils {

  public:
    typedef std::map<std::string, bool> t_CstrMap;
    typedef boost::tokenizer<boost::char_separator<char> > tokenizer;
    typedef std::vector < int > t_vectInt;
    typedef std::vector < std::string > t_vectString;

    /**
     * @brief Default constructor
     */
    Utils();

    /**
     * @brief Default destructor
     */
    virtual ~Utils();

    /**
     * @brief Return a Crop object instantiate in singleton method.
     * @return A reference to the singleton object.
     */
    inline static Utils& getInstance() {
      if (mUtils == 0) {
	mUtils = new Utils;
      }
      return *mUtils;
    }

    /**
     * @brief Initialize the Utils singleton.
     */
    inline static void kill() {
      delete mUtils;
      mUtils = 0;
    }

    void init(const vd::InitEventList& evts);

    int getPrimeNumberAt(const int _at);
    int getPrimeNumberAt(const int _at) const;
    const std::set<int>& getPrimeNumbers() const;

    string getWCSPDirName() const;
    string getWCSPFileName() const;
    string getSolutionDirName() const;
    string getWCSPSeqFile() const;
    string getFileItk() const;

    /**
     * @brief This function retrieves the integers in a string and stock in a vector
     * @param line a string
     * @param _sep a separator in a string
     * @return t_vectInt vector of integers
     */
    t_vectInt getVectInt(const std::string & line, const std::string & _sep);

    /**
     * @brief This function retrieves the character in a string and stock in a vector
     * @param line a string
     * @param _sep a separator in a string
     * @return t_vectString vector of characters
     */
    t_vectString getVectString(const std::string & line, const std::string & _sep);

    /**
     * @brief
     */
    int getDateJulianDayNumber(const std::string & dateString, const int & year);

    int toInt(std::string _str);
    double toDouble(std::string _str);
  private:
    /**
     * @brief Set of prime number
     */
    std::set<int> m_prime_number;

    /**
     * @brief Dir Name for WCSP file
     */
    string m_WCSPDirName;

    /**
     * @brief File Name for WCSP file
     */
    string m_WCSPFileName;

    /**
     * @brief
     */
    string m_WCSPSeqFile;

    /**
     * @brief
     */
    string m_fileItk;

    /**
     * @brief
     */
    t_vectInt m_VectInt;

    /**
     * @brief
     */
    t_vectString m_VectString;



    /**
     * @brief The static variable Utils for singleton design pattern.
     */
    static Utils* mUtils;
    };

} // end namespace crash
#endif
