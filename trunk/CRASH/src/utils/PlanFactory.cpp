#include <utils/PlanFactory.hpp>

using vle::utils::ArgError;

namespace crash {

	PlanFactory::PlanFactory() {}

	PlanFactory::~PlanFactory(){}

	void PlanFactory::duplicateOperation( const std::string _name, const std::string _newname){
		// operation
		if (map_activities.find(_name) != map_activities.end()){
			t_opr opr = map_activities.find(_name)->second;
			map_activities.insert(pair <std::string , t_opr>( _newname ,opr ));
		}else{
			throw std::runtime_error("PlanFactory::duplicateOperation   operation not found");
		}
		// duplicate events param
		for( const_it_paramItk it = map_paramEvent.begin(); it != map_paramEvent.end(); it++){
			if((it->first).first == _name){
				pair < std::string, std::string > key_map = it->first;
				key_map.first = _newname;
				std::string value = it->second;
				map_paramEvent.insert(std::pair<pair<std::string, std::string>, std::string >(key_map, value));
			}
		}

		// duplicate corresponding precedence Constraints
		for(const_it_prec it= map_precedences.begin(); it!=map_precedences.end(); it++){
			if((it->second).first == _name ){
				t_id_prec prec_name;
				prec_name.first = _newname;
				prec_name.second = (it->first).second;
				t_prec prec = it->second;
				prec.first = _newname;
				map_precedences.insert(pair< t_id_prec, t_prec> (prec_name,prec));
			}else if((it->second).second == _name  ){
				t_id_prec prec_name;
				prec_name.first = _newname;
				prec_name.second = (it->first).second;
				t_prec prec = it->second;
				prec.first = prec.second;
				prec.second = _newname;
				map_precedences.insert(pair< t_id_prec, t_prec> (prec_name,prec));
			}
		}
	}

	void PlanFactory::buildItkRules(const Itk _itk, const std::string _cd){
		CropOperation::t_map_rules itk_rule = _itk.getRules();
		for(CropOperation::const_it_rules it= itk_rule.begin(); it!=itk_rule.end(); it++){  // loop on rules
			std::string rl = it->first;
			rl = writeName(rl, _cd);
			t_lst_string lst_pr;
			CropOperation::t_lst_pred lst_pred = it->second;
			for(unsigned int i = 0; i<lst_pred.size();i++){ // loop on predicate
				std::string pred = lexical_cast<string>(lst_pred.at(i).id);
				pred.append("_");
				pred.append(lst_pred.at(i).crash_pr);
				lst_pr.push_back(writeName(pred, _cd));

				// predicate param
				CropOperation::t_lst_param lst_param = lst_pred.at(i).lst_ParamPr;
				pair< std::string, std::string > pr_key;
				pr_key.first = writeName(pred, _cd); // name_pred
				pr_key.second = lst_pred.at(i).crash_pr; // name_fun
				map <std::string , std::string > map_param;
				for (unsigned int j = 0; j < lst_param.size(); j++) {  // loop on param-predicate
						map_param.insert(pair< std::string, std::string> ( lst_param.at(j).name , lst_param.at(j).value ));
				}
				map_paramPred.insert(std::pair<pair<std::string, std::string>, map < std::string, std::string> >(pr_key, map_param));
			}
			map_rules.insert( std::pair<string, t_lst_string >(rl,lst_pr));
		}
	}


	int toInt(std::string _str){
		std::istringstream stm;
		stm.str(_str);
		int x;
		stm>>x;
		return x;
	}


	void PlanFactory::buildItkPrecedences(Itk _itk, const std::string _cd){
		// operation
		Itk::t_lst_op itk_operations = _itk.getLstOperation();

		for(unsigned int i= 0 ; i<itk_operations.size(); i++){
			//Name operation
			std::string op = itk_operations.at(i).getName();
			op = writeName(op, _cd);
			t_opr opr;
			//Preceeding Constraints
			CropOperation::t_lst_prec op_prec = itk_operations.at(i).getPrec();
			for(unsigned int j=0; j < op_prec.size(); j++){
				t_id_prec prec_name;
				prec_name.first = op;
				prec_name.second = j ;
				t_prec prec;
				prec.type =  op_prec.at(j).type;
				prec.first = op;
				prec.second = writeName(op_prec.at(j).second,_cd);
				prec.mintimelag = op_prec.at(j).mintimelag;
				prec.maxtimelag = op_prec.at(j).maxtimelag;

				if ( map_activities.find(prec.first) != map_activities.end() && map_activities.find(prec.second) != map_activities.end()) {
					map_precedences.insert(pair< t_id_prec, t_prec> (prec_name,prec));
				}
			}
		}
	}

	void PlanFactory::buildItkOperations(Itk _itk, const std::string _var, const std::string _cd, int _yr){
		Itk::t_lst_op itk_operations = _itk.getLstOperation();
		// update sowing and harvesting operation based on the crop variety specificity
		for(unsigned int i= 0 ; i<itk_operations.size(); i++){
			std::string st_date;
			std::string nd_date;
			if(itk_operations.at(i).getType()=="sowingVar"){
				st_date = _itk.getStSow(_var);
				_itk.getOperationByType("sowingVar").setSt(st_date);
				nd_date = _itk.getNdSow(_var);
				_itk.getOperationByType("sowingVar").setNd(nd_date);
			}
			if(itk_operations.at(i).getType()=="harvestingVar"){
				st_date = _itk.getStHrv(_var);
				_itk.getOperationByType("harvestingVar").setSt(st_date);
				nd_date = _itk.getNdHrv(_var);
				_itk.getOperationByType("harvestingVar").setNd(nd_date);
				_itk.setYrInd();
			}
			if(itk_operations.at(i).getType()=="harvesting"){
				_itk.setYrInd();
			}
		}
		// get operation to be stored into the map_activities
			itk_operations = _itk.getLstOperation();
		for(unsigned int i= 0 ; i<itk_operations.size(); i++){

			//Name operation
			std::string op = itk_operations.at(i).getName();
			op = writeName(op,_cd);
			t_opr opr;

			//rules
			t_lst_string op_rules = itk_operations.at(i).getLstRules();
			for(unsigned int j=0; j < op_rules.size(); j++){
				std::string rl = writeName(op_rules.at(j), _cd);
				opr.rules.push_back(rl);
			}

			// events param
			opr.event = writeName(itk_operations.at(i).getEvent(),_cd);
			CropOperation::t_lst_param op_param = itk_operations.at(i).getParamEv();
			for(unsigned j=0 ; j<op_param.size(); j++){
				pair< std::string, std::string > op_ev;
				op_ev.first = op; // name op
				op_ev.second = op_param.at(j).name ; //writeName(op_param.at(j).name, _cd); // name param
				std::string value =    op_param.at(j).value;

				// Sowing density is set from crop var
				if(op_param.at(j).name=="density"  && op_param.at(j).type=="sow" && value=="NA") {
					value = _itk.getDensity(_var);
				}

				map_paramEvent.insert(std::pair<pair<std::string, std::string>, std::string >(op_ev, value));
			}

			//date
			int _yySt = itk_operations.at(i).getYrIndex().first;
			opr.start = Utils::getInstance().getDateJulianDayNumber(itk_operations.at(i).getSt(), _yr + _yySt );
			int _yyNd = itk_operations.at(i).getYrIndex().second;
			opr.finish = Utils::getInstance().getDateJulianDayNumber(itk_operations.at(i).getNd(), _yr + _yyNd);
			map_activities.insert( std::pair<string, t_opr >(op, opr));
			cout<<"op "<< op <<" _yySt "<< _yySt << " _yyNd "<<_yyNd<<endl;
		}
	}


	std::string PlanFactory::writeName(std::string _name,  const std::string  _cd){
		_name.append(".");
		_name.append(lexical_cast<string>(_cd));
		return _name;
	}

	std::string PlanFactory::writeChain(const t_lst_string _lst) {
		std::string _flx;
		for(unsigned int j=0; j < _lst.size(); j++) {
			if(j >= 1) {
				_flx.append(" , ");
			}
			_flx.append("\"");
			_flx.append(_lst.at(j).c_str());
			_flx.append("\"");
		}
		return _flx;
	}

	std::string PlanFactory::writeActivities(){
		std::string _flx = "\n## Activities\n";
		_flx.append("activities {\n");
		for(const_it_op it= map_activities.begin(); it!=map_activities.end(); it++){
			_flx.append(writeActivity(it->first));
		}
		_flx.append("}");
		return _flx;
	}

	std::string PlanFactory::writeOneActivity(const std::string _name){
		std::string _flx = "\n## Activities\n";
		_flx.append("activities {\n");

		if (map_activities.find(_name) != map_activities.end()){
			_flx.append(writeActivity( map_activities.find(_name)->first));
		}else{
			throw std::runtime_error("PlanFactory::duplicateOperation   operation not found");
		}
		_flx.append("}");
		return _flx;
	}

	std::string PlanFactory::writeActivity(const std::string _activity) {
		t_opr opr = map_activities.find(_activity)->second;
		std::string _flx= "\tactivity { \n";
		_flx.append("\t\tid = \"");
		_flx.append(_activity);
		_flx.append("\";\n");
		if(opr.rules.size()>0){
			_flx.append("\t\trules = ");
			_flx.append(writeChain(opr.rules));
			_flx.append(";\n");
		}
		_flx.append("\t\ttemporal {\n");
		_flx.append("\t\t\tstart = ");
		_flx.append(lexical_cast<string>(opr.start));
		_flx.append(";\n");
		_flx.append("\t\t\tfinish = ");
		_flx.append(lexical_cast<string>(opr.finish));
		_flx.append(";\n");
		_flx.append("\t\t} \n");
		//_flx.append("\t\t\t\t ack = ");
		//_flx.append(";\n");
		_flx.append("\t\toutput = \"");
		_flx.append(opr.event);
		_flx.append("\";\n");
		_flx.append("\t}\n");
		return _flx;
	}

	std::string PlanFactory::writeRules(){
		std::string _flxrules = "## Rules\n";
		_flxrules.append("rules {\n");

		for(const_it_rules it= map_rules.begin(); it!=map_rules.end(); it++){
			_flxrules.append(writeRule(it->first));
		}
		_flxrules.append("}");
		return _flxrules;
	}
	std::string PlanFactory::writeRule(const std::string _rule) {
		std::string _flxrule= "\trule { \n";
		_flxrule.append("\t\tid = \"");
		_flxrule.append(_rule);
		_flxrule.append("\";\n");
		_flxrule.append("\t\tpredicates = ");
		t_lst_string _predicate = map_rules.find(_rule)->second;
		_flxrule.append(writeChain(_predicate));
		_flxrule.append(";\n");
		_flxrule.append("\t}\n");
		return _flxrule;
	}


	std::string PlanFactory::writePrecedences(){
		std::string _flx = "\n## Precedences\n";
		_flx.append("precedences {\n");

		for(const_it_prec it= map_precedences.begin(); it!=map_precedences.end(); it++){
			_flx.append(writePrecedence(it->first));
		}
		_flx.append("}");
		return _flx;
	}


	std::string PlanFactory::writeOnePrecedence(const std::string _name){
		std::string _flx = "\n## Precedences\n";
		_flx.append("precedences {\n");

		for(const_it_prec it= map_precedences.begin(); it!=map_precedences.end(); it++){
			if((it->second).first == _name || (it->second).second == _name){
				_flx.append(writePrecedence(it->first));
			}
		}
		_flx.append("}");
		return _flx;

	}



	std::string PlanFactory::writePrecedence(const t_id_prec _prec) {
		t_prec _precedence = map_precedences.find(_prec)->second;
		std::string _flx= "\tprecedence {\n";
		_flx.append("\t\ttype = ");
		_flx.append(_precedence.type);
		_flx.append(";\n");
		_flx.append("\t\tfirst = \"");
		_flx.append(_precedence.first);
		_flx.append("\";\n");
		_flx.append("\t\tsecond = \"");
		_flx.append(_precedence.second);
		_flx.append("\";\n");
		_flx.append("\t\tmintimelag = ");
		_flx.append(lexical_cast<string>(_precedence.mintimelag));
		_flx.append(";\n");
		_flx.append("\t\tmaxtimelag = ");
		_flx.append(lexical_cast<string>(_precedence.maxtimelag));
		_flx.append(";\n");
		_flx.append("\t}\n");
		return _flx;
	}

	PlanFactory::t_map_paramItk PlanFactory::getMapParamEvent(){
		return map_paramEvent;
	}
	PlanFactory::t_map_paramItk PlanFactory::getMapParamEvent() const{
		return map_paramEvent;
	}

	PlanFactory::t_map_paramPred PlanFactory::getMapParamPred(){
		return map_paramPred;
	}
	PlanFactory::t_map_paramPred PlanFactory::getMapParamPred() const{
		return map_paramPred;
	}


	std::string PlanFactory::getEventValue(std::string _name_op, std::string _name_ev) {
		pair < std::string, std::string > key_map;
		key_map.first = _name_op;
		key_map.second = _name_ev;
		const_it_paramItk it = map_paramEvent.find(key_map);
		if(it!=map_paramEvent.end()){
			return it->second;
		}
		return 0;
	}

	PlanFactory::t_lst_string PlanFactory::getLstRuleByActName(const std::string _activity){
		t_opr opr = map_activities.find(_activity)->second;
		return opr.rules;
	}

} //namespace crash
