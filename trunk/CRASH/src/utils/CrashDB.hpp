/**
 * @file CrashDB.hpp
 * @author Jerome Dury
 */

/*
 * VLE Environment - the multimodeling and simulation environment
 * This file is a part of the VLE environment (http://vle.univ-littoral.fr)
 * Copyright (C) 2003 - 2009 The VLE Development Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef  CRASHDB_H
#define CRASHDB_H

#include <ostream>
#include <vle/utils.hpp>
#include <vle/utils/Tools.hpp>
#include <vle/devs.hpp>
#include <boost/lexical_cast.hpp>

#include "postgresql/libpq-fe.h"
using namespace std;
namespace vd = vle::devs;
namespace vv = vle::value;


namespace crash {
/**
 * @brief CrashDB: Crash data base connexion class
 */
class CrashDB
{
public:
    /** @brief Default constructor  */
    CrashDB();
    
    /** @brief Main constructor  */
    CrashDB(const vd::InitEventList& evts);
    
    /** @brief Default destructor  */
    virtual ~CrashDB();
    
     /**
    * @brief Return a CrashDB object instantiate in singleton method.
    * @return A reference to the singleton object.
    */
    inline static CrashDB* getInstance() {
        if (mCrashDB == 0) {
            mCrashDB = new CrashDB;
        }

        return mCrashDB;
    }

    /**
    * @brief Initialize the CropSuccessions singleton.
    */
    inline static void kill() {
        delete mCrashDB;
        mCrashDB = 0;
    }

    /**
    * @brief    Database connexion
    */
    void InitDb();
    
    /**
    * @brief  Database connexion closure
    */
    void EndDb(int code);
    
    /**
    * @brief Check query result
    */
    void CheckQueryResult(ExecStatusType stat);
    
    /**
    * @brief Get query results
    */
    PGresult* QueryDB(const char* _sql);

   /**
    * @brief Insert value in the database 
    */
   void InsertDB(const char* _sql);    

   /**
    * @brief Insert value in the database 
    */
    void DeleteDB(const string _table); 

   /**
    * @brief set connexion information from the vpz
    */
    void init(const vd::InitEventList& evts);
    
    /**
    * @brief get connexion information
    */
    std::string getConnexion_string() const;
    
    /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
    inline friend std::ostream& operator<<(std::ostream& out, const CrashDB& cdb) {
            out  << endl << cdb.getConnexion_string();
        return out;
    }

private:
    PGconn* connexion;
    PGresult* result;
    const char* connexion_string;
    const char* query;
    static CrashDB*  mCrashDB; 	/* The static variable CrashDB for singleton design pattern. */
    
};

} // namespace crash

#endif //CRASHDB_H

