/*
 * @file record/pkgs/decision/planning/algos/GenericWCSPConstraintFactory.cpp
 *
 * This file is part of VLE, a framework for multi-modeling, simulation
 * and analysis of complex dynamical systems
 * http://www.vle-project.org
 *
 * Copyright (c) 2003-2007 Gauthier Quesnel <quesnel@users.sourceforge.net>
 * Copyright (c) 2003-2010 ULCO http://www.univ-littoral.fr
 * Copyright (c) 2007-2010 INRA http://www.inra.fr
 *
 * See the AUTHORS or Authors.txt file for copyright owners and contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <fstream>
#include <iostream>
#include <sstream>
#include <boost/tokenizer.hpp>
#include <boost/lexical_cast.hpp>
#include <vle/utils/Path.hpp>
#include <utils/GenericWCSPConstraintFactory.hpp>
#include <vle/utils/Exception.hpp>
#include <utils/System.hpp>


using vle::utils::ArgError;
using namespace std;
using namespace boost;

namespace crash {
	
bool GenericWCSPConstraintFactory::fileHeader(ofstream& _cstFileStrm,
				const std::string& _pbName, 
				unsigned int _nbvar,
				unsigned int _domainSize)
{
	int maxSize = _domainSize;
	std::stringstream ss;
	ss.str(std::string("")); 
	VariableDomaine::iterator it(m_additionnalvar.begin());
	while (it != m_additionnalvar.end()) {
		int dsize = (it->second).at(0).first; 
		if(maxSize <  dsize)
			maxSize = dsize;
			
		ss << dsize << " "; 
		it++;
	}
	
	try
	{ 
		_cstFileStrm << _pbName << " "
			 << _nbvar  + m_additionnalvar.size() << " "
			 << maxSize   << " "
			 << m_cstr  << " " //Nb constraint
			 << getInfty()  << " " //Nb constraint
			<< endl;
		for(unsigned int i=0; i< _nbvar; i++)
			_cstFileStrm << _domainSize  << " ";
		
		_cstFileStrm << ss.str() <<  endl;
	}
	catch(ofstream::failure e )
	{
		throw ArgError("Unable to write the header of the constraint!");
	}
	return true;
}

bool GenericWCSPConstraintFactory::unarycost	(ofstream&  _cstFileStrm,
					VariableDomaine _vd,
					unsigned int _default)
{
	struct UVCValue val;
	struct UVCCost cos;
	
	VariableDomaine::iterator it(_vd.begin());
	while (it != _vd.end()) {
		if((it->second).size() > 0){
			_cstFileStrm 	<< "1 "  << it->first
							<< " " << _default << " "
							<< (it->second).size() << endl;
			for (Domaine::size_type i=0; i< (it->second).size(); i++)
			{
				_cstFileStrm	<< val((it->second).at(i))
						<< " " 	<<	cos((it->second).at(i))  << endl;
			}
			m_cstr++;
		}
		it++;
	}
	if(m_viewer.show(ConstraintsViewer::SH_UNARYCOST))
		show_unarycost(_vd);
		
	return true;
}

bool GenericWCSPConstraintFactory::cost	(ofstream&  _cstFileStrm,
					Vlist _var,
					MultiValueCosts _mvc,
					unsigned int _default)
{
	struct MVCValue mvcv;
	struct MVCCost mvcc; 
	
	if(_var.size()>0){
		MultiValueCosts::iterator itmvc (_mvc.begin());
		while (itmvc != _mvc.end()) {
			if(	mvcv(*itmvc).size() != _var.size())
				return false; 
			itmvc++;
		}
		
		_cstFileStrm << _var.size() ;
		Vlist::iterator it (_var.begin());
		while (it != _var.end()) {
			_cstFileStrm << " " <<  (*it); 
			it++;
		}
		_cstFileStrm << " " << _default << " " << _mvc.size() << std::endl;

		itmvc = _mvc.begin();
		while (itmvc != _mvc.end()) {
			Vlist::iterator itvc (mvcv(*itmvc).begin());
			for(unsigned int i = 0; i< mvcv(*itmvc).size(); i++){
				_cstFileStrm <<  mvcv(*itmvc).at(i) << " " ;
			}
			
			_cstFileStrm << mvcc(*itmvc) << std::endl;
			itmvc++;
		}
		m_cstr++;
		if(m_viewer.show(ConstraintsViewer::SH_COST))
			show_cost(_var,_mvc);
		return true;
	}else
		return false;
		
}

bool GenericWCSPConstraintFactory::globalarithmetic	(
					ofstream&  _cstFileStrm,
					Vlist _var,
					unsigned int _UB,
					CompOperator _fOP,
					unsigned int _default)
{
	struct D domain;
	struct UVC dsize;
	std::stringstream varlist;
	std::stringstream domainlist; 
	Vlist::iterator it (_var.begin());

	while (it != _var.end()) {
		if((*it)< m_nbvariable)
			domain(dsize(m_maxDsize, 0));
		else
		{
			std::map<unsigned int, Domaine>::iterator
					itd = m_additionnalvar.find(*it);
			if(itd != m_additionnalvar.end() )
			{
				unsigned int ds =	((itd->second).at(0)).first;
				domain(dsize(ds, 0));
				domainlist << ds << " ";
			}else
				return false; 
		}
		varlist << (*it) << " ";
		it++;
	}

	std::string exten_cstr = filter(_var, domain(), _UB, _fOP, true);
	int nbCstr = std::count(exten_cstr.begin(), exten_cstr.end(), '\n');
	_cstFileStrm << _var.size() << " " << varlist.str();
	_cstFileStrm << " " << _default << " " << nbCstr << std::endl;
	_cstFileStrm << exten_cstr << std::endl;

	if(m_viewer.show(ConstraintsViewer::SH_ARITHMETIC))
		show_globalarithmetic(_var, _UB); 
	m_cstr++;
	return true;

}

					
bool GenericWCSPConstraintFactory::pseudocumulative	(
				ofstream&  _cstFileStrm,
				Vlist _var,
				MultiValueCosts _mvc,
				unsigned int _default)
{
	
	struct MVCValue mvcv;
	struct MVCCost mvcc;
	
	struct D domain;
	struct UVC dsize;

	std::set<unsigned int> _costdomain; 
	int _maxcost = -1* INFTY;
	unsigned int _intermvar ;
	m_tmpvar.clear();
	
	if(_var.size()>0){
		MultiValueCosts::iterator itmvc (_mvc.begin());
		while (itmvc != _mvc.end())
		{
			if(	mvcv(*itmvc).size() != _var.size())
				return false;
			//Get the max cost value that will be used as domain size
			if(_maxcost < mvcc(*itmvc))
				_maxcost = mvcc(*itmvc);
				
			_costdomain.insert(mvcc(*itmvc)); 
			itmvc++;
		}

		//Add intermediary cost variable
		_intermvar = m_nbvariable + m_additionnalvar.size();
		domain(dsize(_maxcost+1,0)); 
		m_additionnalvar.insert(std::make_pair < unsigned int, Domaine>
				(_intermvar, domain()));
		m_tmpvar.push_back(_intermvar);
		_cstFileStrm << _var.size() + m_tmpvar.size();
		Vlist::iterator it (_var.begin());
		while (it != _var.end()) {
			_cstFileStrm << " " <<  (*it); 
			it++;
		}
		_cstFileStrm << " " << _intermvar ;
		_cstFileStrm << " " << _default << " " << _mvc.size() << std::endl;

		itmvc = _mvc.begin();
		while (itmvc != _mvc.end()) {
			Vlist::iterator itvc (mvcv(*itmvc).begin());
			for(unsigned int i = 0; i< mvcv(*itmvc).size(); i++){
				_cstFileStrm <<  mvcv(*itmvc).at(i) << " " ;
			}
			_cstFileStrm << mvcc(*itmvc) << " " ;
			_cstFileStrm << 0 << std::endl;
			itmvc++;
		}
		m_cstr++;

		{
			//Reduction of the cost domain
			struct VD _vd;
			struct D  _cdomain;
			struct UVC _u;

			set<unsigned int >::iterator _itcdomain;
			
			for (_itcdomain=_costdomain.begin();
					_itcdomain!=_costdomain.end(); _itcdomain++)
				_cdomain(_u(*_itcdomain, 0));
				
			_vd(_intermvar, _cdomain());
			m_viewer.addNode(_intermvar, ConstraintsViewer::CSTR_TMP_VARIABLE);
			unarycost(_cstFileStrm, _vd(), INFTY);
		}
		if(m_viewer.show(ConstraintsViewer::SH_PS_CUMULATIVE))
			show_pseudocumulative(_var, _mvc);
			
		return true;
	}else
		return false;
}

bool GenericWCSPConstraintFactory::sregular	(ofstream&  _cstFileStrm,
					VariableDomaine _vd,
					unsigned int _nbState,
					unsigned int _value,
					unsigned int _default,
					unsigned int _option)
{
	if(_vd.size() > 0 ){
		_cstFileStrm << _vd.size() ;
		VariableDomaine::iterator it(_vd.begin());
		
		while (it != _vd.end()) {
			_cstFileStrm << " " <<  it->first; 
			it++;
		}
		_cstFileStrm << " -1 sregular var " << _default << endl;
		if(_option){
			_cstFileStrm << _nbState << endl
					<< "1 0" << endl
					<< _nbState ;
			for(unsigned int i=0; i<_nbState; i++)
					_cstFileStrm << " " << i  ;
			_cstFileStrm  << endl;
			sequenceREGULARConstraintGraph(_nbState, _value,
					(_vd.begin())->second);
		}else
		{
			_cstFileStrm << 2*_nbState << endl
					<< "2 0 " << _nbState << endl
				    << "2 0 " << _nbState << endl; 
			cyclicSREGULARConstraintGraph(_nbState, _value,
					(_vd.begin())->second);
		}
		_cstFileStrm  << SREGULAR2String() << endl;
		if(m_viewer.show(ConstraintsViewer::SH_SREGULAR))
			show_sregular(_vd, _value);
		m_cstr++;
	}
	return true;
}

bool GenericWCSPConstraintFactory::sregular_w_f_s(
					ofstream&  _cstFileStrm,
					VariableDomaine _vd,
					unsigned int _nbState,
					unsigned int _value,
					unsigned int _historic, 
					unsigned int _default,
					unsigned int _option)
{
	if(_vd.size() > 0 )
	{
		_cstFileStrm << _vd.size() ;
		VariableDomaine::iterator it(_vd.begin());
		
		while (it != _vd.end()) {
			_cstFileStrm << " " <<  it->first; 
			it++;
		}
		_cstFileStrm << " -1 sregular var " << _default << endl;
		if(_option){
			_cstFileStrm << _nbState + _historic << endl
					<< "1 0" << endl
					<< _nbState;
			for(unsigned int i=0; i<_nbState; i++)
					_cstFileStrm << " " << i+ _historic  ;
			_cstFileStrm  << endl;
			sequenceSREGULARWithFixedState(_nbState, _value, _historic,
					(_vd.begin())->second);
		}else
		{
			_cstFileStrm << 2*_nbState << endl
					<< "2 0 " << _nbState << endl
				    << "2 0 " << _nbState << endl; 
			cyclicSREGULARConstraintGraph(_nbState, _value,
					(_vd.begin())->second);
		}
		if(m_viewer.show(ConstraintsViewer::SH_SREGULAR_WFS))
			show_sregular(_vd, _value, true);
		
		_cstFileStrm  << SREGULAR2String() << endl;			 
		m_cstr++;
	}
	return true;
}


bool GenericWCSPConstraintFactory::equal(ofstream&  _cstFileStrm,
					Vlist _var,
					Domaine _val,
					unsigned int _default)
{
	struct UVCValue uvcv;
	struct UVCCost uvcc;
	
	Vlist::iterator it(_var.begin());
	_cstFileStrm << _var.size() ;
	while (it != _var.end()) {
		_cstFileStrm << " " <<  (*it); 
		it++;
	}
	_cstFileStrm << " " << _default << " "
				 << _val.size() << endl;
				 
	for (Domaine::size_type i=0; i< _val.size(); i++)
	{
		it = _var.begin();
		while (it != _var.end()) {
			_cstFileStrm <<  uvcv(_val.at(i)) << " " ; 
			it++;
		}
		_cstFileStrm	<<	uvcc(_val.at(i))  << endl;
	}
	m_cstr++;
	if(m_viewer.show(ConstraintsViewer::SH_EQUAL))
		show_equal(_var, _val.size() ); 
	return true;
}

bool GenericWCSPConstraintFactory::ssame(ofstream&  _cstFileStrm,
					Vlist _varset1,
					Vlist _varset2,
					unsigned int _default)
{
	if(_varset1.size() != _varset2.size())
		return false;
		
	Vlist::iterator it(_varset1.begin());
	_cstFileStrm << (_varset1.size() +  _varset2.size()) ;
	while (it != _varset1.end()) {
		_cstFileStrm << " " <<  (*it); 
		it++;
	}
	it = _varset2.begin();
	while (it != _varset2.end()) {
		_cstFileStrm << " " <<  (*it); 
		it++;
	}
	_cstFileStrm << " -1 ssame " << _default << endl;
	_cstFileStrm <<  _varset1.size() << " " << _varset2.size() << endl;
	it = _varset1.begin();
	while (it != _varset1.end()) {
		_cstFileStrm <<  (*it) << " " ; 
		it++;
	}
	
	_cstFileStrm << endl;
	it = _varset2.begin();
	while (it != _varset2.end()) {
		_cstFileStrm <<  (*it) <<  " " ; 
		it++;
	}
	_cstFileStrm << endl;

	m_cstr++;
	if(m_viewer.show(ConstraintsViewer::SH_SSAME))
		show_ssame(_varset1, _varset2);
	return true;
}


bool GenericWCSPConstraintFactory::sgcc(ofstream&  _cstFileStrm,
					Vlist _var,
					ValuesTolerance _t,
					unsigned int _default)
{
	struct TOMin tmin;
	struct TOMax tmax;
	struct UVTOValue tval;
	struct UVTOInterv tint;
	
	_cstFileStrm << (_var.size()) ;
	for(unsigned int i = 0; i < _var.size();  i++ )
	{
		_cstFileStrm << " " <<  _var.at(i); 
	}
	_cstFileStrm << " -1 sgcc var " << _default << endl;
	_cstFileStrm << _t.size() << endl;
	
	ValuesTolerance::iterator it(_t.begin());
	while (it != _t.end()) {
		_cstFileStrm 	<< tval(*it) << " " <<  tmin(tint(*it))
						<< " " <<  tmax(tint(*it)) << endl; 
		it++;
	}
	if(m_viewer.show(ConstraintsViewer::SH_SGCC))
		show_sgcc(_var, _t.size());
	m_cstr++;
	return true;
}
//-------------------------------------------------------------------
//OTHER
void GenericWCSPConstraintFactory::sequenceREGULARConstraintGraph
		(const unsigned int _state, const unsigned int _cropID,
		 const Domaine& _croplst)
{
	struct UVCValue uvcal; 
	m_graph = graph_t(_state);
	if(_state > 1){
	Domaine::const_iterator it;
	vertex_iterator vi, vi_end, next, first, last;
	tie(vi, vi_end) = vertices(m_graph);
	first = vi; 
	for (next = vi; vi != vi_end; vi = next) {
		++next;
		if(*vi == 0)
		for(it=_croplst.begin(); it !=_croplst.end(); it++)
			if(uvcal(*it) != (_cropID))
			add_edge(*vi, *vi, (uvcal(*it)), m_graph);
			else
			add_edge(*vi, *next, (uvcal(*it)), m_graph);
		else if((*vi > 0) &&  (next != vi_end) )
		for(it=_croplst.begin(); it !=_croplst.end(); it++)
			if(uvcal(*it) != (_cropID))
			add_edge(*vi, *next, (uvcal(*it)), m_graph);
				
	}
	last = --vi;
	for(it=_croplst.begin(); it !=_croplst.end(); it++)
			if(uvcal(*it) != (_cropID))
				add_edge(*last, *first, (uvcal(*it)), m_graph);
	}
}

void GenericWCSPConstraintFactory::cyclicSREGULARConstraintGraph
		(const unsigned int _state, const unsigned int _cropID,
		 const Domaine& _croplst)
{
	struct UVCValue uvcal; 
	m_graph = graph_t(_state*2);
	if(_state > 1){
	vertex_iterator vi, vi_end, next, first, last;
	Domaine::const_iterator it;
	tie(vi, vi_end) = vertices(m_graph);
	for (next = vi; vi != vi_end; vi = next) {
		++next;
		if(*vi == 0)
		for(it=_croplst.begin(); it !=_croplst.end(); it++)
			if(uvcal(*it) != (_cropID))
			add_edge(*vi, *vi, (uvcal(*it)), m_graph);
			else
			add_edge(*vi, *next, (uvcal(*it)), m_graph);
		else if((*vi > 0) && (*vi < (unsigned int) _state))
		for(it=_croplst.begin(); it !=_croplst.end(); it++)
			if(uvcal(*it) != (_cropID))
			add_edge(*vi, *next, (uvcal(*it)), m_graph);
			
	}
	for(it=_croplst.begin(); it !=_croplst.end(); it++)
			if(uvcal(*it) != (_cropID))
			add_edge(0, (_state-1), (uvcal(*it)), m_graph);

	

	tie(vi, vi_end) = vertices(m_graph);
	for (next = vi; vi != vi_end; vi = next) {
		++next;
		if(*vi == ((unsigned int)_state))
		for(it=_croplst.begin(); it !=_croplst.end(); it++)
			if(uvcal(*it) != (_cropID)){
			add_edge(*vi, *vi, (uvcal(*it)), m_graph);
			add_edge(*vi, *next, (uvcal(*it)), m_graph);
			 }	
		else if((*vi > ((unsigned int)_state)))
		for(it=_croplst.begin(); it !=_croplst.end(); it++)
			if(uvcal(*it) != (_cropID))
			add_edge(*vi, *next, (uvcal(*it)), m_graph);
			
	}
	for(it=_croplst.begin(); it !=_croplst.end(); it++)
			if(uvcal(*it) == (_cropID))
			add_edge(((_state*2)-1), (_state), (uvcal(*it)), m_graph);				
	}
}


//----------- OTHER
void GenericWCSPConstraintFactory::sequenceSREGULARWithFixedState
		(const unsigned int _state, const unsigned int _cropID,
		 const unsigned int _historic,
		 const Domaine& _croplst)
{
	struct UVCValue uvcal; 
	m_graph = graph_t(_state+_historic);
	if(_state > 1)
	{
		Domaine::const_iterator it;
		vertex_iterator vi, vi_end, next, first, last;
		tie(vi, vi_end) = vertices(m_graph);
		for (next = vi; vi != vi_end; vi = next) {
			++next;
			if(*vi < _historic)
			{	
				for(it=_croplst.begin(); it !=_croplst.end(); it++){
					if(*next!=_historic)
						add_edge(*vi, *next, (uvcal(*it)), m_graph);
					else if (*next==_historic)
					{
						if (uvcal(*it) != _cropID)
							add_edge(*vi, *next, (uvcal(*it)), m_graph);
					}
					
					if (uvcal(*it) == _cropID)
						add_edge(*vi, _historic+1, (uvcal(*it)), m_graph);
					
				}
			}
		}
		
		tie(vi, vi_end) = vertices(m_graph);
		first = vi;
		for (next = vi; vi != vi_end; vi = next) {
			++next;
			if(*vi >= _historic){
				if(*vi == _historic)
				{
					for(it=_croplst.begin(); it !=_croplst.end(); it++)
						if(uvcal(*it) != (_cropID))
							add_edge(*vi, *vi, (uvcal(*it)), m_graph);
						else
							add_edge(*vi, *next, (uvcal(*it)), m_graph);
				}
				else if((*vi > _historic) &&  (next != vi_end) )
				{
					for(it=_croplst.begin(); it !=_croplst.end(); it++)
						if(uvcal(*it) != (_cropID))
						add_edge(*vi, *next, (uvcal(*it)), m_graph);
				}
			}
		}
		last = --vi;
		for(it=_croplst.begin(); it !=_croplst.end(); it++)
				if(uvcal(*it) != (_cropID))
					add_edge(*last, _historic, (uvcal(*it)), m_graph);
	}
}

std::string GenericWCSPConstraintFactory::SREGULAR2String()
{
	std::string s(""); 
	if(num_edges(m_graph) > 0){
	boost::property_map<graph_t, edge_weight_t>::type    
		label = get(edge_weight, m_graph);
	edge_iterator first, last;
	s.append(boost::lexical_cast< std::string >(num_edges(m_graph)));
	s.append("\n"); 
	for(tie(first, last) = edges(m_graph); first != last; first++)
	{
		s.append(boost::lexical_cast< std::string >(source(*first, m_graph)));
		s.append(" ");
		s.append(boost::lexical_cast< std::string >(get(get(edge_weight, m_graph), *first)));
		s.append(" ");
		s.append(boost::lexical_cast< std::string >(target(*first, m_graph)));
		s.append("\n");
	}	
	}
	return s;     
}
//-------------------------------------------------------------------

void GenericWCSPConstraintFactory::buildCompleteGraph
		(unsigned int _nbnode,
		 unsigned int _nbedge,
		 vertex_descriptor _root,
		 bool _init)
{
	static vertex_descriptor trg;
	static std::stringstream s;
	static std::vector<int> values;
	static unsigned int _nbN = _nbnode;
	if(_init){
		m_graph = graph_t();
		_init = false;
		s 	<< "";
	}
	
	if(_nbnode > 1)
	{
		
		vertex_descriptor vd = _root; 
		if(_root == 0){
			vd = add_vertex(m_graph);
			trg = add_vertex(m_graph);
		}

		for(unsigned int value =0; value < _nbedge; value++)
		{
			vertex_descriptor vd2 = add_vertex(m_graph);
			add_edge(vd, vd2, value, m_graph);
			values.push_back(value);
			buildCompleteGraph(_nbnode-1, _nbedge, vd2, false);
			values.pop_back();
		}
		
	}else{
		std::stringstream tmp;
		std::vector<int>::reverse_iterator rit;
		for ( rit=values.rbegin() ; rit < (values.rbegin()+_nbN-1); ++rit )
				tmp << *rit << " " ;

		for(unsigned int value =0; value < _nbedge; value++){
			add_edge(_root, trg, value, m_graph);
			s << tmp.str() << value << std::endl; 
		}
		
	}
}

/*
std::string GenericWCSPConstraintFactory::filter
		(unsigned int _UB, CompOperator _fOP)
{
	property_map<graph_t, edge_weight_t>::type weightmap
			= get(edge_weight, m_graph);
	std::vector<vertex_descriptor> p(num_vertices(m_graph));
	std::vector<int> d(num_vertices(m_graph));
	vertex_descriptor s = vertex(0, m_graph);

	dijkstra_shortest_paths_no_color_map(m_graph, s,
			predecessor_map(&p[0]).distance_map(&d[0]));
	std::cout << "distances and parents:" << std::endl;
	vertex_iterator vi, vend;
	
	for (boost::tie(vi, vend) = vertices(m_graph); vi != vend; ++vi) {
	 std::cout << "distance( node " << *vi << ") = " << d[*vi] << ", ";
	 std::cout << "parent( node " << *vi << ") = node " << p[*vi] << std::endl;
	}
	std::cout << std::endl;
	
	std::ofstream dot_file(vle::utils::Path::path().getPackageOutputFile("dijkstra-eg.dot").c_str(), ios::out); 
	//std::ofstream dot_file("figs/dijkstra-eg.dot");

  dot_file << "digraph D {\n"
    << "  rankdir=LR\n"
    << "  size=\"40,40\"\n"
    << "  ratio=\"fill\"\n"
    << "  edge[style=\"bold\"]\n" << "  node[shape=\"circle\"]\n";

	edge_iterator ei, ei_end;
	for (boost::tie(ei, ei_end) = edges(m_graph); ei != ei_end; ++ei) {
		edge_descriptor e = *ei;
		vertex_descriptor
		  u = source(e, m_graph), v = target(e, m_graph);
		dot_file << u << " -> " <<v
		  << "[label=\"" << get(weightmap, e) << "\"";
		if (p[v] == u)
		  dot_file << ", color=\"blue\"";
		else
		  dot_file << ", color=\"red\"";
		dot_file << "]";
	}
	dot_file << "}";

}*/
std::string GenericWCSPConstraintFactory::filter
		(Vlist _var,
		 Domaine _domain,
		 unsigned int _UB,
		 CompOperator _fOP,
		 bool _init)
{
	static std::stringstream s;
	static std::vector<int> values;
	static unsigned int _nbN = _var.size();
	if(_init){
		_init = false;
		s.str(std::string(""));
	}
	
	if(_var.size() > 1)
	{
		unsigned int _nbedge = _domain.at(0).first; 
		for(unsigned int value =0; value < _nbedge; value++)
		{
			values.push_back(value);
			if(value == 0){
				_var.erase (_var.begin());
				_domain.erase (_domain.begin());
			}
			filter(_var, _domain, _UB, _fOP, false);
			values.pop_back();
		}
	}else{
		
		std::stringstream tmp;
		int cummul = 0; 
		std::vector<int>::reverse_iterator rit;
		for ( rit=values.rbegin(); rit < (values.rbegin()+_nbN -1); ++rit ){
				tmp << *rit << " " ;
				cummul+= *rit;
		}
		unsigned int _nbedge = _domain.at(0).first; 
		for(unsigned int value =0; value < _nbedge; value++){
			switch(_fOP)
			{
				case EQ_CSTR :
						if(	cummul + value == _UB)
							s << tmp.str() << value << " 0" << std::endl; 
					break;
				case GEQ_CSTR :
						if(	cummul + value >= _UB)
							s << tmp.str() << value << " 0" <<  std::endl; 
					break; 
				case LEQ_CSTR :
						if(	cummul + value <= _UB)
							s << tmp.str() << value << " 0" << std::endl; 
					break; 
				case G_CSTR :
						if(	cummul + value > _UB)
							s << tmp.str() << value << " 0" << std::endl; 
					break; 
				case L_CSTR :
						if(	cummul + value < _UB)
							s << tmp.str() << value << " 0" << std::endl; 
					break;
				default :
					break;
			}
		}
	 }

	return s.str(); 
}
		 
std::string GenericWCSPConstraintFactory::filter
		(unsigned int _nbnode,
		 unsigned int _nbedge,
		 unsigned int _UB,
		 CompOperator _fOP,
		 bool _init)
{
	
	static std::stringstream s;
	static std::vector<int> values;
	static unsigned int _nbN = _nbnode;
	if(_init){
		_init = false;
		s 	<< "";
	}
	if(_nbnode > 1)
	{
		for(unsigned int value =0; value < _nbedge; value++)
		{
			values.push_back(value);
			filter(_nbnode-1, _nbedge, _UB, _fOP, false);
			values.pop_back();
		}
	}else{
		std::stringstream tmp;
		int cummul = 0; 
		std::vector<int>::reverse_iterator rit;
		for ( rit=values.rbegin() ; rit < (values.rbegin()+_nbN-1); ++rit ){
				tmp << *rit << " " ;
				cummul+= *rit;
		}
		for(unsigned int value =0; value < _nbedge; value++){
			switch(_fOP)
			{
				case EQ_CSTR :
						if(	cummul + value == _UB)
							s << tmp.str() << value << " 0" << std::endl; 
					break;
				case GEQ_CSTR :
						if(	cummul + value >= _UB)
							s << tmp.str() << value << " 0" <<  std::endl; 
					break; 
				case LEQ_CSTR :
						if(	cummul + value <= _UB)
							s << tmp.str() << value << " 0" << std::endl; 
					break; 
				case G_CSTR :
						if(	cummul + value > _UB)
							s << tmp.str() << value << " 0" << std::endl; 
					break; 
				case L_CSTR :
						if(	cummul + value < _UB)
							s << tmp.str() << value << " 0" << std::endl; 
					break;
				default :
					break;
			}
		}
		
	}
	return s.str(); 
}

//-------------------------------------------------------------------
std::string GenericWCSPConstraintFactory::createConstraintFile
		(	const std::string& _cstFileDirName,
			const std::string& _cstFileName)
{
	if(createConstraintFileDir(_cstFileDirName))
	{
		std::string _fdname(_cstFileDirName); 
		_fdname.append("/"); 
		_fdname.append(_cstFileName);
		ofstream  _stfile(vle::utils::Path::path().
				getPackageDataFile(_fdname).c_str(), ios::out);
		if(_stfile){
		_stfile.close();
		return vle::utils::Path::path().getPackageDataFile(_fdname);
		}	
		else
		throw ArgError("Cannot create the constraint file !");	 
	}else
		throw ArgError("Cannot create the constraint file !");	 
}

bool GenericWCSPConstraintFactory::createConstraintFileDir
		    (const std::string& _cstFileDirName)
{
	if(not System::getInstance().existFile(
		vle::utils::Path::path().getPackageDataDir(), _cstFileDirName))
	{
		System::getInstance().addDirectory(
			vle::utils::Path::path().getPackageDataDir(),
			_cstFileDirName);	
		return System::getInstance().existFile(vle::utils::Path::path().
				getPackageDataDir(), _cstFileDirName); 				
	}else
		return true; 	    
		
}

void  GenericWCSPConstraintFactory::saveConstraintFile (
					const std::string& _cstFile,
					const std::string& _pbName, 
					unsigned int _nbvar,
					unsigned int _domainSize)
{
				
	std::ifstream 	file(_cstFile.c_str());
	std::stringstream 	buf;
	buf << file.rdbuf();
	std::string buffer(buf.str());
	file.close();
	ofstream  _cstFileStrm;
	_cstFileStrm.open (_cstFile.c_str(), ios::out);
	fileHeader(_cstFileStrm, _pbName,  _nbvar, _domainSize );
	_cstFileStrm << buffer;
	_cstFileStrm.close(); 
}   

//-------------------------------------------------------------------
void GenericWCSPConstraintFactory::show_unarycost(const VariableDomaine& vd)
{
	VariableDomaine::const_iterator it(vd.begin());
	while (it != vd.end()) {
		int dsize = (it->second).size();
		ConstraintsViewer::vertex_descriptor vdesc =
			m_viewer.getVariableDescriptor(it->first);
		ConstraintsViewer::vertex_descriptor cstrdesc =
			m_viewer.addNode(m_cstr, ConstraintsViewer::CSTR_UNARYCOST);
			
		m_viewer.pushBackEdge(vdesc, cstrdesc, dsize);
		m_viewer.pushBackEdge(cstrdesc, vdesc, dsize);
		it++;
	}
}


void GenericWCSPConstraintFactory::show_cost(
			const Vlist& _var, const MultiValueCosts& _mvc)
{
	int dsize = _mvc.size(); 
	ConstraintsViewer::vertex_descriptor cstrdesc
			=	m_viewer.addNode(m_cstr, ConstraintsViewer::CSTR_COST);
	Vlist::const_iterator it (_var.begin());
	while (it != _var.end()) {
		ConstraintsViewer::vertex_descriptor vdesc =
			m_viewer.getVariableDescriptor(*it);
		m_viewer.pushBackEdge(cstrdesc, vdesc, dsize);
		it++;
	}
}

void GenericWCSPConstraintFactory::show_globalarithmetic(
			const Vlist& _var, unsigned int _UB)
{
	ConstraintsViewer::vertex_descriptor cstrdesc
			=	m_viewer.addNode(m_cstr, ConstraintsViewer::CSTR_ARITHMETIC);
	Vlist::const_iterator it (_var.begin());
	while (it != _var.end()) {
		ConstraintsViewer::vertex_descriptor vdesc =
			m_viewer.getVariableDescriptor(*it);
		m_viewer.pushBackEdge(cstrdesc, vdesc, _UB);
		it++;
	}
}

void GenericWCSPConstraintFactory::show_pseudocumulative(
			const Vlist& _var, const MultiValueCosts& _mvc)
{
	int dsize = _mvc.size();
	ConstraintsViewer::vertex_descriptor cstrdesc
			=	m_viewer.addNode(m_cstr, ConstraintsViewer::CSTR_PS_CUMULATIVE);
			
	Vlist::const_iterator it (_var.begin());
	while (it != _var.end()) {
		ConstraintsViewer::vertex_descriptor vdesc =
			m_viewer.getVariableDescriptor(*it);
		m_viewer.pushBackEdge(cstrdesc, vdesc, dsize);
		it++;
	}
	it = m_tmpvar.begin();
	while (it != m_tmpvar.end()) {
		ConstraintsViewer::vertex_descriptor vdesc =
			m_viewer.getVariableDescriptor(*it);
		m_viewer.pushBackEdge(cstrdesc, vdesc, dsize);
		it++;
	}
}

void GenericWCSPConstraintFactory::show_sregular(
			const VariableDomaine& vd, unsigned int _value, bool _histo)
{
	VariableDomaine::const_iterator it(vd.begin());
	while (it != vd.end()) {
		ConstraintsViewer::vertex_descriptor vdesc =
			m_viewer.getVariableDescriptor(it->first);
		ConstraintsViewer::vertex_descriptor cstrdesc =
			m_viewer.addNode(m_cstr,
			((_histo) ?	ConstraintsViewer::CSTR_SREGULAR_WFS :
						ConstraintsViewer::CSTR_SREGULAR ));
			
		m_viewer.pushBackEdge(vdesc, cstrdesc, _value);
		m_viewer.pushBackEdge(cstrdesc, vdesc, _value);
		it++;
	}
}

void GenericWCSPConstraintFactory::show_equal(
			const Vlist& _var, unsigned int _nbcstr)
{
	ConstraintsViewer::vertex_descriptor cstrdesc
			=	m_viewer.addNode(m_cstr, ConstraintsViewer::CSTR_EQUAL);
	Vlist::const_iterator it (_var.begin());
	while (it != _var.end()) {
		ConstraintsViewer::vertex_descriptor vdesc =
			m_viewer.getVariableDescriptor(*it);
		m_viewer.pushBackEdge(cstrdesc, vdesc, _nbcstr);
		it++;
	}
}

void GenericWCSPConstraintFactory::show_ssame(
			const Vlist& _var1, const Vlist& _var2)
{
	ConstraintsViewer::vertex_descriptor cstrdesc
			=	m_viewer.addNode(m_cstr, ConstraintsViewer::CSTR_SSAME);
	ConstraintsViewer::vertex_descriptor cstrdesc1
			=	m_viewer.addNode(m_cstr, ConstraintsViewer::CSTR_SSAME);
	ConstraintsViewer::vertex_descriptor cstrdesc2
			=	m_viewer.addNode(m_cstr, ConstraintsViewer::CSTR_SSAME);
	int dsize1 = _var1.size();
	int dsize2 = _var2.size();
	
	Vlist::const_iterator it (_var1.begin());
	while (it != _var1.end()) {
		ConstraintsViewer::vertex_descriptor vdesc =
			m_viewer.getVariableDescriptor(*it);
		m_viewer.pushBackEdge(cstrdesc1, vdesc, dsize1);
		it++;
	}

	it = _var2.begin();
	while (it != _var2.end()) {
		ConstraintsViewer::vertex_descriptor vdesc =
			m_viewer.getVariableDescriptor(*it);
		m_viewer.pushBackEdge(cstrdesc2, vdesc, dsize2);
		it++;
	}

	m_viewer.pushBackEdge(cstrdesc1, cstrdesc, dsize1);
	m_viewer.pushBackEdge(cstrdesc2, cstrdesc, dsize2);
}

void GenericWCSPConstraintFactory::show_sgcc(
			const Vlist& _var, unsigned int _nbcstr)
{
	ConstraintsViewer::vertex_descriptor cstrdesc
			=	m_viewer.addNode(m_cstr, ConstraintsViewer::CSTR_SGCC);
	Vlist::const_iterator it (_var.begin());
	while (it != _var.end()) {
		ConstraintsViewer::vertex_descriptor vdesc =
			m_viewer.getVariableDescriptor(*it);
		m_viewer.pushBackEdge(cstrdesc, vdesc, _nbcstr);
		it++;
	}
}

} // namespace crash
