/**
 * @file System.hpp
 * @author The VLE Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * VLE Environment - the multimodeling and simulation environment
 * This file is a part of the VLE environment
 * http://www.vle-project.org
 *
 * Copyright (C) 2007-2010 INRA http://www.inra.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef RECORD_SYSTEM_HPP
#define RECORD_SYSTEM_HPP

#include <glibmm/spawn.h>
#include <glibmm/thread.h>
#include <string>
#include <list>

using namespace std;

namespace crash {

    /**
     * @brief A class to manage System command via the shell
     */
    class  System
    {
    public:
		void solve(		const std::string& _wcspDirName,  
						const std::string& _cstFileName,
						const std::string& _optionNBSolution,
						const std::string& _optionMaxCost);
		
		void transform(	
						const std::string& _dirName,  
						const std::string& _cstFileName);
												
        /**
         * @brief Return true if the current process is finished.
         * @return True if finish, false otherwise.
         */
        inline bool isFinish() const { return m_stop; }

        /**
         * @brief Return true if the latest execution of a process returns true
         * of false.
         *
         * @return True if latest process success, false otherwise.
         */
        bool isSuccess() const { return m_success; }

        

        void getOutput(std::string& str) const;
        void getError(std::string& str) const;
		void cleanOutput();
		void cleanError();
		
        /**
         * @brief Change the current directory to the output directory. 
         */
     //   void changeToOutputDirectory();

        void addFile(const std::string& path, const std::string& name);
        void addDirectory(const std::string& path, const std::string& name);
		
		void removeFile(const std::string& pathFile);
        
        void copyFile(const std::string& sourceFile, std::string& targetName);

        bool existFile(const std::string& path, const std::string& name);
		bool existDir(const std::string& path, const std::string& name);

		
		/****************************TOULBAR2***************************/
		 
	
		inline std::string getTransformFilePath() 
			{return m_transform_file;  }
		inline std::string getTransformFilePath() const 
			{return m_transform_file;  }
		
		inline void setTransformFilePath() 
			{
				std::string _pkgdir(vle::utils::Path::path().getPackageDir().c_str()); 
				_pkgdir.append("/scripts/transform");
				m_transform_file =_pkgdir;
			}
		
	
		/**************************************************************/

        /**
         * @brief Return a instance of System using the singleton system.
         * @return A reference to the singleton object.
         */
        inline static System& getInstance()
        { if (m_system == 0) m_system = new System; return *m_system; }

        /**
         * @brief Initialise the System singleton.
         */
        inline static void init()
        { getInstance(); }

        /**
         * @brief Delete the System singleton.
         */
        inline static void kill()
        { delete m_system; m_system = 0; }

    private:
        /**
         * @brief Hide constructor.
         */
        System()
            : m_stop(true), m_success(false), m_out(0), m_err(0), m_wait(0),
            m_pid(0)
        {}

        static System* m_system; ///< singleton attribute.
        bool m_stop; ///< true if process is stopped.
        bool m_success; ///< true if process is successfull.
        Glib::Thread* m_out; ///< standard output stream reader thread.
        Glib::Thread* m_err; ///< standard error stream reader thread.
        Glib::Thread* m_wait; ///< wait process thread.
        mutable Glib::Mutex m_mutex; ///< mutex to access to strerr and strout.
        Glib::Pid m_pid; ///< pid of the current process.
        std::string m_strout; ///< standard output string.
        std::string m_strerr; ///< standard error string.
		
		std::string m_transform_file; ///< SHELL script to transform toulbar solution into right format

        /**
         * @brief Start the process taken from the list of argument in the
         * working directory. Launch the process asynchronously an start two
         * threads to read output and error standard stream and move data into
         * stream.
         * @param workingDir The directory to start the process.
         * @param lst The command line argument.
         */
        void process(const std::string& workingDir,
                     const std::list < std::string >& lst);

        /**
         * @brief A threaded method to read the stream and fill the output.
         * @param stream The stream (ie. a file) to read error.
         * @param output A string filled by the stream.
         */
		void readStandardOutputStream(int stream);
       

        /**
         * @brief A threaded method to read the stream and fill the output.
         * @param stream The stream (ie. a file) to read error.
         * @param output A string filled by the stream.
         */
		void readStandardErrorStream(int stream);
       

        void waitProcess();

        void appendOutput(const char* str);
        void appendError(const char* str);
        void appendOutput(const std::string& str);
        void appendError(const std::string& str);
    };

} // namespace record decision

#endif
