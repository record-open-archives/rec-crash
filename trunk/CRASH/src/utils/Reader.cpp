/** @brief This class is a Finite State Automaton (FSA) that reads a data
 * file that contains dates irregular, which has the advantage of data values
 * at the right time step.
 **/

#include <vle/extension/fsa/Statechart.hpp>


#include <string>
#include <ostream>
#include <vector>

#include <iomanip>
#include <iostream>
#include <vle/extension/DifferenceEquation.hpp>
#include <fstream>

#include <boost/algorithm/string.hpp>
#include <vle/utils/Path.hpp>
#include <boost/version.hpp>
#include <boost/cstdint.hpp>
#include <vle/utils.hpp>
#include <vle/devs.hpp>


#if BOOST_VERSION < 103600
#include <boost/spirit/utility/chset.hpp>
#include <boost/spirit/core.hpp>
#include <boost/spirit/utility/confix.hpp>
#include <boost/spirit/utility/lists.hpp>
#include <boost/spirit/utility/escape_char.hpp>

using namespace boost::spirit;

#else
#include <boost/spirit/include/classic_chset.hpp>
#include <boost/spirit/include/classic_core.hpp>
#include <boost/spirit/include/classic_confix.hpp>
#include <boost/spirit/include/classic_lists.hpp>
#include <boost/spirit/include/classic_escape_char.hpp>

using namespace BOOST_SPIRIT_CLASSIC_NS;
using namespace std;
#endif
namespace vd = vle::devs;
namespace vf = vle::extension::fsa;
namespace vv = vle::value;
namespace ve = vle::extension;
namespace vu = vle::utils;


namespace filereader {

class Reader : public vf::Statechart
{
    enum state_t { Initial, Wait, Final};

public:
    Reader(
       const vd::DynamicsInit& init,
       const vd::InitEventList& evts)
      : vf::Statechart(init, evts)
    {
      filePath = vle::value::toString(evts.get("market_file"));
      mFilePath = vle::utils::Path::path().getPackageDataFile(filePath);
      mFile.open(getFilePath().c_str());
      mBegin = 0;
      mColumnSeparator = " \t\n ";
      mDateFormat = " ";
      mDayColumn = -1;
      mMonthColumn = -1;
      mDayColumn = -1;


      if (not mFile) {
	throw vle::utils::ModellingError(
	    vle::fmt(_("[%1%] Reader - cannot"	\
		       " open file : %2%"))
	    % getModelName() % mFilePath);
      }


      std::vector < std::string > fields;
      parseParameters(evts);
      jumpLines(mBegin - 2);
      parseHeader(fields);
        for (std::map < std::string,
                 std::string >::const_iterator it = mMapping.begin();
             it != mMapping.end(); ++it) {
	  parseColumnHeader(fields, it->first, it->second);
        }

        states(this) << Initial << Wait << Final;

        transition(this, Initial, Wait) << action(&Reader::Recup) << send(&Reader::out);

      transition(this, Wait, Final) << guard(&Reader::eoFile);


        transition(this, Wait, Wait) << when(&Reader::ReturnTime) << send(&Reader::out);

      state(this, Wait) << inAction(&Reader::Increment);

      initialState(Initial);

      timeStep(1);
    }

    virtual ~Reader()
    {}

private:

  //Fonctions of StateChart

  /** 
   * @brief (Inititial state) Retrieve the values of variables at the 
   * start date of simulation
   *
   * @param time of the current system date
   */
  void Recup(const vd::Time& time) {
    readLine(time);

    if (timefile>time) {
      parseLine(previousLine);
    }

  for(std::map <std::string, int>::const_iterator
	it = mColumns.begin(); it != mColumns.end(); ++it) {
      setStockage(it->first, boost::lexical_cast<double>(
                      mColumnValues.at(it->second)));
  }

}

  /** @brief Finale state of the Statechart
   * guard(boolean fonction)
   * @return end of the file
   **/
  bool eoFile(const vd::Time& /*julianDay*/) {

        return mFile.eof();
  }

  /** @brief Entering state Wait, this function allows you to move to
   * the next line of file by retrieving the variable values
   * @param time of the current system date
   **/
  void Increment(const vd::Time& time) {

    if(time == timefile) {
      readLine(time);
    }  else {
        parseLine(line);
    }

    for(std::map <std::string, int>::const_iterator
	  it = mColumns.begin(); it != mColumns.end(); ++it) {
        setStockage(it->first, boost::lexical_cast<double>(
							   mColumnValues.at(it->second)));
    }

  }

  /** @brief The transition to the next line is crossed at the date
  ** timefile(date of the next libne)
  * @return timefile (date of the next line)
  **/
  vd::Time ReturnTime(const vd::Time& /*time*/) {

    return  timefile;
  }


  /** @brief Output of the values */
void out(const vd::Time& time,
         vd::ExternalEventList& output) const
{
 for(std::map < std::string, double>::const_iterator it = mStockValue.begin();
     it != mStockValue.end(); ++it) {
   output << (ve::DifferenceEquation::Var(it->first) = it->second);
 }
}
    
  //Fonctions d'analyse et de lecture de fichier

  /** @brief Parse the parameters of experimental conditions*/
 void parseParameters(const vle::devs::InitEventList& events) {
    if (events.exist("begin")) {
      mBegin = vle::value::toInteger(events.get("begin"));
    }
    if (events.exist("columns")) {
      const vle::value::Map& mapping = events.getMap("columns");
      const vle::value::MapValue& lst = mapping.value();
            for (vle::value::MapValue::const_iterator it = lst.begin();
                 it != lst.end(); ++it) {
	      std::string column = it->first;
	      std::string varName = vle::value::toString(it->second);
	      mMapping[column] = varName;
            }
    }
    if (events.exist("column_separator")) {
            mColumnSeparator = vle::value::toString(
						    events.get("column_separator"));
    }
    if (events.exist("date_format")) {
            mDateFormat = vle::value::toString(
					       events.get("date_format"));
    }
    if (events.exist("year_column")) {
      mYearColumn = vle::value::toInteger(events.get("year_column"));
    }
    if (events.exist("month_column")) {
            mMonthColumn = vle::value::toInteger(
						 events.get("month_column"));
    }
    if (events.exist("day_column")) {
      mDayColumn = vle::value::toInteger(events.get("day_column"));
    }
 }

  /** @brief This fonction returns column separator*/
  std::string columnSeparator() const {
    return mColumnSeparator;
  }

  /** @brief This fonction returns the date fomat*/
  std::string dateFormat() const {
    return mDateFormat;
  }

  /** @brief This fonction returns the position of the year column 
   * in the file datas */
  int yearColumn() const {
    return mYearColumn;
  }

  /** @brief This focntion returns the position of the month column
   * in the file datas*/
  int monthColumn() const {
    return mMonthColumn;
  }

  /** @brief This fonction returns the position of the day comulmn
   * int the dile datas */
  int dayColumn() const {
    return mDayColumn;
  }

  /** @brief This fonction returns the File*/
  std::ifstream & getFile() {
    return mFile;
  }

  /** @brief This fonction returns the File Path*/
  const std::string & getFilePath() const {
    return mFilePath;
  }


  bool isFirstLine(const vle::devs::Time& time) {
   
        std::string strtimefile = mColumnValues[yearColumn()] + "-" + mColumnValues[monthColumn()] + "-" + mColumnValues[dayColumn()];
        timefile = vu::DateTime::toJulianDayNumber(strtimefile);

        return (time <= timefile);
  }

  /** @brief Allows newline*/
  void jumpLines(unsigned int n) {
    std::string line;
    for (boost::uint32_t i = 0; i < n; ++i) {
      getline(getFile(), line);
    }
  }

  /** @brief Parse line of the file and load the values of variables 
   * line after ligne 
   * @param line of file */
 bool parseLine(std::string& line) {
    mColumnValues.clear();
    boost::trim(line);

    rule <> list_csv, list_csv_item;

    list_csv_item =
        !(
	    confix_p('\"', *c_escape_ch_p, '\"')
	    | longest_d[real_p | int_p]
	    | +alnum_p
	    );

    list_csv = list_p(
	list_csv_item[push_back_a(mColumnValues)],
	+chset_p(columnSeparator().c_str()));

    parse_info <> result = parse (line.c_str(), list_csv);

    if (not result.hit) {
	mColumnValues.clear();
    }
    return result.hit;
  }

  /** @brief Skips to the next line of the file datas */
  void nextLine(const vle::devs::Time& time) {
    do {
      previousLine = line;
      getline(mFile, line);
      if (not line.empty()) {
	parseLine(line);
      }
    } while(not isFirstLine(time) and not mFile.eof() and mFile.good());
  }

  /** @brief this fonction read line of the file and load
   * the data structure as below
   * ---------------------------------------------------------
   * day |month|year |BH  |CH  |MA |   <-variables name
   * ---------------------------------------------------------
   * 12  | 4   | 1993|12.3|14.5| 17|   <- values of variables
   * ---------------------------------------------------------
   */
  void readLine(const vle::devs::Time& time) {
    nextLine(time);
    if (not mColumnValues.empty()) {
	for (std::map < std::string, int >::const_iterator
		 it = mColumns.begin(); it != mColumns.end(); ++it) {
	  setVariable(it->first,
		      boost::lexical_cast<double>(mColumnValues.at(it->second)));
	}
    }
  }

  /** @brief This function add a column
   * @param name of the column
   * @param index of the column
   **/
  void addColumn(const std::string & name, int index) {
    mColumns[name] = index;
  }

  /** @brief This function set a variables and values of the
   * variables
   * @param name of the variable
   * @param value of the variable
   **/
  void setVariable(const std::string & name, double value) {
    mVariable[name] = value;
  }

  /** @brief Same role as the function setVariable */
  void setStockage(const std::string & _name, double _value) {
  mStockValue[_name] = _value;
  }

  /** @brief This function add a column
   * @param colName name of column
   * @param varName name of variable
   **/
  void addColumn(const std::string & colName, const std::string & varName) {
    mColumnT[colName] = varName;
  }

  /** @brief This function parse the header column of the file
   * @param fields
   * @param colName
   * @param name
   **/
void parseColumnHeader(const std::vector < std::string >& fields,
			       const std::string& colName,
			       const std::string& name)
{
    std::vector < std::string >::const_iterator it =
	find(fields.begin(), fields.end(), colName);

    if (it != fields.end()) {
	addColumn(colName, name);
	addColumn(name, (int)distance(fields.begin(), it));
    } else {
	throw vle::utils::FileError(
	    boost::format(_("[%1%] Reader: column %2% missing"))
	    % getModelName() % colName);
    }
}


  /** @brief Parse Header of the file
   * @param fields
   **/
void parseHeader(std::vector < std::string >& fields)
{
    std::string header;

    getline(getFile(), header);
    parseLine(fields, header);
}

  /** @brief
   * @param fields
   * @param line
   */
void parseLine(std::vector < std::string >& fields,
		       std::string& line)
{
    boost::trim(line);

    rule <> list_csv, list_csv_item;

    list_csv_item =
        !(
	    confix_p('\"', *c_escape_ch_p, '\"')
 	    | +(alnum_p | ch_p('_') | ch_p('-'))
	    | longest_d[real_p | int_p]
	    );

    list_csv =
	list_p(
	    list_csv_item[push_back_a(fields)],
	    +chset_p(columnSeparator().c_str()));

    parse_info <> result = parse (line.c_str(), list_csv);

    for (std::vector < std::string >::iterator it = fields.begin();
	 it != fields.end(); ++it) {
	if ((*it)[0] == '\"' and (*it)[it->size() - 1] == '\"') {
	    *it = it->substr(1, it->size() - 2);
	}
    }
}


  //Typedefs
  typedef std::map < std::string, int > columns_t;
  typedef std::vector < std::string > columnValues_t;

  //Variables
  int mBegin;
  int mYearColumn;
  int mMonthColumn;
  int mDayColumn;
  std::string line;
  std::string previousLine;
  std::string mColumnSeparator;
  std::string mDateFormat;
  std::ifstream mFile;
  std::string mFilePath;
  std::string filePath;
  std::map <std::string, std::string> mMapping;
  std::map<std::string, std::string> mColumnT;
  std::map < std::string, double > mVariable, mStockValue;
  vd::Time mNextTime;
  double timefile;
  columns_t mColumns;
  columnValues_t mColumnValues;
 
}; //end class Reader


} // namespace filereader


DECLARE_DYNAMICS(filereader::Reader)
