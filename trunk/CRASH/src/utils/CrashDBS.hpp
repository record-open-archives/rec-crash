#ifndef CRASHDBS_HPP
#define CRASHDBS_HPP

#include <ostream>
#include <vle/utils.hpp>
#include <vle/utils/Tools.hpp>
#include <vle/devs.hpp>
#include <boost/lexical_cast.hpp>

#include "postgresql/libpq-fe.h"
using namespace std;
namespace vd = vle::devs;
namespace vv = vle::value;

namespace crash {


  class CrashDBS {

  public:

    /**
     * @brief Default constructor
     */
    CrashDBS();

    /**
     * @brief Main constructor
     */
    CrashDBS(const vd::InitEventList & evts);

    /**
     * @brief Default constructor
     */
    virtual ~CrashDBS();

    /**
     * @brief Database connexion
     */
    void InitDb();

    /**
     * @brief Database connexion closure
     */
    void EndDb(int code);

    /**
     * @brief Check query result
     */
    void CheckQueryResult(ExecStatusType stat);

    /**
     * @brief Get query results
     */
    PGresult * QueryDB(const char * _sql);

    /**
     * @brief Insert value in the database
     */
    void InsertDB(const char * _sql);

    /**
     * @brief
     */
    void DeleteDB(const string _table);

    /**
     * @brief Set connexion information from the vpz
     */
    void init(const vd::InitEventList & evts);

    /**
     * @brief Get connexion information
     */
    std::string getConnexion_string() const;

    /**
     * @brief
     */
    inline friend std::ostream & operator << (std::ostream & out,
					      const CrashDBS & cdb) {
      out << endl << cdb.getConnexion_string();
      return out;
    }

  private:
    PGconn * connexion;
    PGresult * result;
    const char * connexion_string;
    const char * query;

  };

} //namespace crash

#endif //CRASHDBS_HPP
