#include <utils/CrashDBS.hpp>

using vle::utils::ArgError;
using namespace std;

namespace vd = vle::devs;
namespace vv = vle::value;

namespace crash {

	CrashDBS::CrashDBS() : connexion(),
	result(),
	connexion_string(),
	query()
	{}

	CrashDBS::CrashDBS(const vd::InitEventList & evts) :
		connexion(),
		result(),
		connexion_string(vv::toString(evts.get("CrashDbConnexion")).c_str()),
		query()
	{}

	CrashDBS::~CrashDBS() {}

	void CrashDBS::InitDb() {
		connexion = PQconnectdb(connexion_string);
		if(connexion == NULL) {
			cout << "\nCrashDB: L'objet connexion n'a pas pu être cée" << endl;
			exit(1);
		}
		if(PQstatus(connexion) != CONNECTION_OK) {
			cout << "\nCrashDB: La connexion a la base n'a pu être établie" << endl;
			PQfinish(connexion);
			exit(1);
		}
	}

	void CrashDBS::EndDb(int code) {
		PQfinish(connexion);
		if(code > 0) {
			exit(code);
		}
	}

	void CrashDBS::CheckQueryResult(ExecStatusType stat) {
		if(result == NULL) {
			EndDb(1);
		}
		if(PQresultStatus(result) != stat) {
			cout << "\nCrashDB: La requete n'a pas donnée le résultat prévu" << endl;
			cerr << PQerrorMessage(connexion) << endl;
			cout << "\nCrashDB: On arrête tout" << endl;
			PQclear(result);
			EndDb(1);
		}
	}

	PGresult * CrashDBS::QueryDB(const char * _sql) {
		//Get database connection
		InitDb();
		//Query database
		//Get dbLandUnits
		result = PQexec(connexion, _sql);
		CheckQueryResult(PGRES_TUPLES_OK);
		//Close database connection
		EndDb(0);
		return result;
	}

	void CrashDBS::InsertDB(const char * _sql) {
		//Get database connection
		InitDb();
		//Query database
		//Get dbLandUnits
		PQexec(connexion, _sql);
		//Close database connection
		EndDb(0);
	}

	void CrashDBS::DeleteDB(const string _table) {
		//Get database connection
		InitDb();
		char queryDELETE[250];
		sprintf(queryDELETE, "DELETE FROM %s", _table.c_str());
		//Get dbLandUnits
		PQexec(connexion, queryDELETE);
		//Close database connection
		EndDb(0);
	}

	void CrashDBS::init(const vd::InitEventList & evts) {
		connexion_string = vv::toString(evts.get("CrashDbConnexion")).c_str();
	}

	std::string CrashDBS::getConnexion_string() const {
		return connexion_string;
	}

} //namespace crash
