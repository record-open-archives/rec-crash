/**
 * @file CrashDB.hpp
 * @author Jerome Dury
 */

/*
 * VLE Environment - the multimodeling and simulation environment
 * This file is a part of the VLE environment (http://vle.univ-littoral.fr)
 * Copyright (C) 2003 - 2009 The VLE Development Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "CrashAgent.hpp"
#include <utils/CrashDB.hpp>

using vle::utils::ArgError;
using namespace std;

namespace vd = vle::devs;
namespace vv = vle::value;

namespace crash
{
    CrashDB*  	CrashDB::mCrashDB= 0;

    CrashDB::CrashDB() :
	connexion(),
	result(),
	connexion_string(),
	query()
    {};

    CrashDB::CrashDB(const vd::InitEventList& evts) :
	connexion(),
	result(),
	connexion_string(vv::toString(evts.get("CrashDbConnexion")).c_str()),
	query()
    {};

    CrashDB::~CrashDB(){};

    void CrashDB::InitDb()
    {
	connexion = PQconnectdb(connexion_string);

	if (connexion == NULL) {
	    cout << "\nCrashDB: L'objet connexion n'a pas pu etre cree" << endl;
	    exit(1);
	}
	if (PQstatus(connexion) != CONNECTION_OK) {
	    cout << "\nCrashDB: La connexion a la base n'a pu etre etablie" << endl;
	    PQfinish(connexion);
	    exit(1);
	}
	//    cout << "\nCrashDB: Connexion etablie" << endl;
    }


    void CrashDB::EndDb(int code)
    {
	PQfinish(connexion);
	//    cout << "\nCrashDB: Connexion fermee" << endl;
	if (code>0) {
	    exit(code);
	}
    }


    void CrashDB::CheckQueryResult(ExecStatusType stat)
    {
	if (result == NULL) {
	    EndDb(1);
	}
	if (PQresultStatus(result) != stat) {
	    cout << "\nCrashDB: La requete n'a pas donne le resultat prevu" << endl;
	    cerr << PQerrorMessage(connexion) << endl;
	    cout << "\nCrashDB: On arrete tout" << endl;
	    PQclear(result);
	    EndDb(1);
	}
    }


    PGresult* CrashDB::QueryDB(const char* _sql)
    {
	//Get database connection
	InitDb();
	//Query database
	//  Get dbLandUnits
	result = PQexec(connexion, _sql);
	CheckQueryResult(PGRES_TUPLES_OK);
	//  Close database connection
	EndDb(0);
	return result;
    }

    void CrashDB::InsertDB(const char* _sql)
    {
	//Get database connection
	InitDb();
	//Query database
	//  Get dbLandUnits
	PQexec(connexion, _sql);
	//  Close database connection
	EndDb(0);
    }

    void CrashDB::DeleteDB(const string _table)
    {
	//Get database connection
	InitDb();

	char queryDELETE[250];
	sprintf(queryDELETE,"DELETE FROM %s",_table.c_str()) ;
	//  Get dbLandUnits
	PQexec(connexion, queryDELETE);
	//  Close database connection
	EndDb(0);
    }

    void CrashDB::init(const vd::InitEventList& evts)
    {
	connexion_string = vv::toString(evts.get("CrashDbConnexion")).c_str();
    }

    std::string CrashDB::getConnexion_string() const
    {
	return connexion_string;
    }


} // end namespace crash

