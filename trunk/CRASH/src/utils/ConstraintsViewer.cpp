/*
 * @file record/pkgs/decision/utils/ConstraintsViewer.cpp
 *
 * This file is part of VLE, a framework for multi-modeling, simulation
 * and analysis of complex dynamical systems
 * http://www.vle-project.org
 *
 * Copyright (c) 2003-2007 Gauthier Quesnel <quesnel@users.sourceforge.net>
 * Copyright (c) 2003-2010 ULCO http://www.univ-littoral.fr
 * Copyright (c) 2007-2010 INRA http://www.inra.fr
 *
 * See the AUTHORS or Authors.txt file for copyright owners and contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <iostream>
#include <utils/ConstraintsViewer.hpp>

namespace crash {
	


ConstraintsViewer::edge_descriptor	ConstraintsViewer::pushBackEdge(
							const ConstraintsViewer::vertex_descriptor &v1,
							const ConstraintsViewer::vertex_descriptor &v2,
							const int edge_desc ) 
{
	std::pair<edge_descriptor, bool> e; 
	e = add_edge(v1,v2, edge_desc, m_graph);
	return e.first; 
}

ConstraintsViewer::vertex_descriptor ConstraintsViewer::getVariableDescriptor(
								const unsigned int id) const
{
    ConstraintsViewer::vertex_descriptor vd = -1;
    std::pair<vertex_iterator, vertex_iterator> pv_it = vertices(m_graph);

    while (pv_it.first != pv_it.second) {
        vd = *pv_it.first;
        if(m_graph[vd].id == id &&
			(m_graph[vd].type == CSTR_VARIABLE
				|| m_graph[vd].type == CSTR_TMP_VARIABLE))
            return vd;

        pv_it.first++;
    }
	
    return -1;
}

ConstraintsViewer::vertex_descriptor ConstraintsViewer::getCstrDescriptor(
								const unsigned int id) const
{
    ConstraintsViewer::vertex_descriptor vd = -1;
    std::pair<vertex_iterator, vertex_iterator> pv_it = vertices(m_graph);

    while (pv_it.first != pv_it.second) {
        vd = *pv_it.first;
        if(m_graph[vd].id == id &&
			!((m_graph[vd].type == CSTR_VARIABLE
				|| m_graph[vd].type == CSTR_TMP_VARIABLE)))
            return vd;

        pv_it.first++;
    }
	
    return -1;
}

void ConstraintsViewer::addOption(const Option _option)
{
	m_option = (Option) (m_option | _option); 
}

bool ConstraintsViewer::show(const Option _option) const
{
    if ((m_option & _option) == _option)
		return true;
	else
		return false; 
}

void ConstraintsViewer::toDotFile (const std::string& _fileName) const 
{
	std::ofstream dot_file(
				vle::utils::Path::path().getPackageOutputFile(
						_fileName).c_str(), ios::out);

	boost::property_map<graph_t, edge_weight_t>::const_type    
			value = get(edge_weight, graph());

	dot_file << std::endl;
	dot_file << "digraph D {\n"
	   << "  rankdir=LR\n"
	   << "  ratio=\"fill\"\n"
	   << "  edge[style=\"bold\"]\n";
	std::pair<vertex_iterator, vertex_iterator> pv_it;
	for (pv_it = vertices(graph());	pv_it.first != pv_it.second;
			++pv_it.first)
	{
		unsigned int node = *(pv_it.first);
		int type = m_graph[*(pv_it.first)].type;
		std::string c(m_graph[*(pv_it.first)].color); 
		dot_file << "\""<< node << "\" ";
		
		if(!((type == CSTR_TMP_VARIABLE) || (type == CSTR_VARIABLE)))
			dot_file << "[fillcolor=\"" << c
					<< "\" , shape=\"diamond\",  style=\"filled\" , color=\"black\"] ";
		else
			dot_file << "[fillcolor=\"" << c
					<< "\" , shape=\"circle\", style=\"filled\" , color=\"black\"] ";
		dot_file << std::endl;
	}
	edge_iterator ei, ei_end;
	 {
	   for (tie(ei, ei_end) = edges(m_graph) ; ei != ei_end; ++ei)
	   {
			 edge_descriptor e = *ei;
			 vertex_descriptor u = source(e, m_graph);
			 vertex_descriptor v = target(e, m_graph);
			 
			 dot_file 	<< "\""<< u << "\" ";
			 dot_file 	<< " -> \"" << v << "\" "
						<< "[label=\"" << value[e]  << "\""
						<< ", color=\"grey\"]";
			 dot_file << std::endl;
	   }
	 }
	 dot_file << "label = \"\\n\\nVariables and constraints graph"
			<< " \";\n fontsize=20\n;";

	 dot_file << "}";
} 

} // namespace crash


