#ifndef PLANFACT_HPP
#define PLANFACT_HPP

#include <ostream>
#include <vle/utils.hpp>
#include <vle/utils/Tools.hpp>
#include <vle/devs.hpp>
#include <boost/lexical_cast.hpp>

// Crash Procedural knowledge
#include <belief/proceduralknowledge/Itk.hpp>
#include <belief/proceduralknowledge/CropOperation.hpp>
#include <belief/structuralknowledge/expertknowledge/Crop.hpp>
#define INFTY 999999;


namespace crash {

	class PlanFactory {

		public:

			typedef std::vector< std::string > t_lst_string;

			// rules
			typedef map< string, t_lst_string > t_map_rules;
			typedef t_map_rules::iterator it_rules;
			typedef t_map_rules::const_iterator const_it_rules;

			// operations with events
			typedef	struct opr{
				t_lst_string rules;
				int start;
				int finish;
				std::string event;
			} t_opr;
			typedef map< string, t_opr > t_map_op;
			typedef t_map_op::iterator it_op;
			typedef t_map_op::const_iterator const_it_op;

			// precedence
			typedef	struct prec{
				std::string type;
				std::string first;
				std::string second;
				int mintimelag;
				int maxtimelag;
			} t_prec;
			typedef pair <std::string, int > t_id_prec;
			typedef map< t_id_prec , t_prec > t_map_prec;
			typedef t_map_prec::const_iterator const_it_prec;

			// All operation and Events with param
			typedef map< pair<std::string, std::string>, std::string> t_map_paramItk;
			typedef t_map_paramItk::const_iterator const_it_paramItk;

			typedef map< pair<std::string, std::string> , map < std::string, std::string > > t_map_paramPred;
			typedef t_map_paramPred::const_iterator const_it_paramPred;


			/**
			 * @brief Default constructor
			 */
			PlanFactory();

			/**
			 * @brief Default constructor
			 */
			virtual ~PlanFactory();

			/**
			 * @brief build rules from itk and plot unit
			 */
			void buildItkRules(const  Itk _itk, const std::string _cd);
			/**
			 * @brief build operation from itk, id plot unit and year.
			 */
			void buildItkOperations(Itk _itk, const std::string _var, const std::string _cd, int _yr);

			/**
			 * @brief build precedence.
			 */
			void buildItkPrecedences(Itk _itk, const std::string _cd);

			/**
			 * @brief build precedence.
			 */
			void duplicateOperation(const std::string _name, const std::string _newname);


			/**
			 * @brief Write name with _pu
			 */
			std::string writeName(std::string _name, const std::string _cd);
			std::string writeChain(const t_lst_string _lst);

			std::string writeRules();
			std::string writeRule(const std::string _rule);
			std::string writeActivities();
			std::string writeOneActivity(const std::string _name);
			std::string writeActivity(const std::string _activity);
			std::string writePrecedences();
			std::string writeOnePrecedence( const std::string _name);
			std::string writePrecedence(const t_id_prec _prec);

			t_map_paramItk getMapParamEvent();
			t_map_paramItk getMapParamEvent() const;

			t_map_paramPred getMapParamPred();
			t_map_paramPred getMapParamPred() const;

			std::string getEventValue(std::string _name_op, std::string _name_ev);
			t_lst_string getLstRuleByActName(const std::string _activity);

		private:

			/**
			 * @brief list of rules
			 */
			t_map_rules map_rules;

			/**
			 * @brief list of Activities (operations)
			 */
			t_map_op map_activities;

			/**
			 * @brief list of precedence
			 */
			t_map_prec map_precedences;

			/**
			 * @brief list of operation, events with param
			 */
			t_map_paramItk map_paramEvent;
			t_map_paramPred map_paramPred;



	};

} //namespace crash

#endif //PLAN_HPP
