/*
 * @file record/pkgs/decision/planning/algos/GenericWCSPConstraintFactory.hpp
 *
 * This file is part of VLE, a framework for multi-modeling, simulation
 * and analysis of complex dynamical systems
 * http://www.vle-project.org
 *
 * Copyright (c) 2003-2007 Gauthier Quesnel <quesnel@users.sourceforge.net>
 * Copyright (c) 2003-2010 ULCO http://www.univ-littoral.fr
 * Copyright (c) 2007-2010 INRA http://www.inra.fr
 *
 * See the AUTHORS or Authors.txt file for copyright owners and contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef RECORD_PKGS_DECISION_GENERIC_WCSP_CONSTRAINT_FACTORY_HPP
#define RECORD_PKGS_DECISION_GENERIC_WCSP_CONSTRAINT_FACTORY_HPP

#define INFTY 999999

#include <string>
#include <ostream>

#include <vector>
#include <set>
#include <limits>
#include <boost/lexical_cast.hpp>
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/graph_traits.hpp>
#include <boost/graph/property_maps/constant_property_map.hpp>
#include <boost/property_map/property_map.hpp>
#include <boost/graph/adjacency_list.hpp>

#include <boost/graph/dijkstra_shortest_paths_no_color_map.hpp>


#include <utils/ConstraintsViewer.hpp>


using namespace std;
using namespace boost;

namespace crash {
typedef boost::property<boost::edge_weight_t, int> EdgeTRProperty;


class  GenericWCSPConstraintFactory
{
public:
	enum CompOperator {EQ_CSTR, GEQ_CSTR, LEQ_CSTR, G_CSTR, L_CSTR};

	typedef std::pair<unsigned int, int> UnaryValueCost;
	typedef std::vector<UnaryValueCost> Domaine;
	typedef std::map<unsigned int, Domaine> VariableDomaine;

	typedef std::vector<unsigned int> Vlist;
	typedef std::pair<Vlist, int> MultiValueCost;
	typedef std::vector<MultiValueCost> MultiValueCosts;

	typedef std::pair<int, int> Tolerance;
	typedef std::pair<int, Tolerance> UnaryValueTolerance;
	typedef std::vector<UnaryValueTolerance> ValuesTolerance;
	/**
	* @brief Default constructor
	*/
	GenericWCSPConstraintFactory(unsigned int _nbvar,
			unsigned int _maxDomainSize) :
			m_cstr(0),
			m_nbvariable(_nbvar),
			m_maxDsize(_maxDomainSize),
			m_viewer(_nbvar)
	{}
		
	virtual ~GenericWCSPConstraintFactory(){}
	
	/**
	 * Write the WCSP file header
	 * @param std::string _pbName the probleme name
	 * @param int _nbvar the number of variable
	 * @param int _domainSize the maximum domain size
	 */
	bool fileHeader(ofstream& _cstFileStrm,
					const std::string& _pbName, 
					unsigned int _nbvar,
					unsigned int _domainSize);

	/**
	 * UniArityConstraint : WCSP file format
	 * A simple cost function. Arity = 1
	 * E.g. to express cost({1,2}) in {0 1 2 3 4 5 6 7}
	 * default  cost = 12222
	 *  
	 * 1 12222 2  // Variable 1 
	 * 0 0
	 * 5 1
	 * 2 12222 3  // Variable 1 
	 * 0 2
	 * 2 5
	 * 7 0
	 * 
	 * @param  ofstream _cstFileStrm : an ofstream to write the result
	 * @param  VariableDomaine _var : the list of variables and associated
	 * 				values and cost
	 * @param  unsigned int _default : The default cost_per_violation
	 * @return true if evrything is ok
	 */
	
	bool unarycost	(ofstream&  _cstFileStrm, VariableDomaine _vd,
					unsigned int _default = INFTY);
	/**
	 * Cost : WCSP file format
	 * A simple cost function
	 * E.g. to express cost({1,2,3}) in {0 1 2 3 4 5 6 7}
	 * if value = 0 5 3  => cost = 0  
	 * if value = 1 3 3  => cost = 2
	 * else  cost = 12222
	 *
	 * 1 2 3 12222 2
	 * 0 5 3 0
	 * 1 3 3 2
	 * 
	 * @param  ofstream _cstFileStrm : an ofstream to write the result
	 * @param  Vlist _var : the list of variables
	 * @param  MultiValueCosts _mvc : a set of MultiValueCost
	 * @param  unsigned int _default : The default cost_per_violation
	 * @return true if evrything is ok
	 */
	bool cost	(ofstream&  _cstFileStrm,
					Vlist _var,
					MultiValueCosts _mvc,
					unsigned int _default = INFTY);

	/**
	 * Cost : WCSP file format
	 * A simple cost function
	 * E.g. to express cost({1,2,3}) in {0 1 2 3 4 5 6 7}
	 * if value = 0 5 3  => cost = 0  
	 * if value = 1 3 3  => cost = 2
	 * else  cost = 12222
	 *
	 * 1 2 3 12222 2
	 * 0 5 3 0
	 * 1 3 3 2
	 * 
	 * @param  ofstream _cstFileStrm : an ofstream to write the result
	 * @param  Vlist _var : the list of variables
	 * @param  MultiValueCosts _mvc : a set of MultiValueCost
	 * @param  unsigned int _default : The default cost_per_violation
	 * @return true if evrything is ok
	 */
	bool globalarithmetic	(ofstream&  _cstFileStrm,
					Vlist _var,
					unsigned int _UB,
					CompOperator _fOP,
					unsigned int _default = INFTY);
					
	/**
	 * Pseudocumulative : WCSP file format
	 * This method add a set of cost variables with can be got by
	 * calling the method getTmpVarList(). Users must combine this method
	 * with globalarithmetic() to complete the global constraint.
	 * 
	 * @param  ofstream _cstFileStrm : an ofstream to write the result
	 * @param  Vlist _var : the list of variables
	 * @param  MultiValueCosts _mvc : a set of MultiValueCost
	 * @param  unsigned int _default : The default cost_per_violation
	 * @return true if evrything is ok
	 */
	bool pseudocumulative	(ofstream&  _cstFileStrm,
					Vlist _var,
					MultiValueCosts _mvc,
					unsigned int _default = INFTY);
					
	/**
	 * Equal : WCSP file format
	 * A simple cost function
	 * E.g. to express equal({1,2,3}) if in {1,2}
	 * if value = 1 => cost = 5 :
	 * if value = 2 => cost = 12 :
	 * else  cost = 12222
	 *
	 * 1 2 3 12222 2
	 * 1 1 1 5
	 * 2 2 2 12
	 * 
	 * @param  ofstream _cstFileStrm : an ofstream to write the result
	 * @param  Vlist _var : the list of variables
	 * @param  Domaine _val : a set of Unary value cost
	 * @param  unsigned int _default : The default cost_per_violation
	 * @return true if evrything is ok
	 */
	bool equal	(ofstream&  _cstFileStrm,
					Vlist _var,
					Domaine _val,
					unsigned int _default = INFTY);
	/**
	 * Same : WCSP file format
	 * ssame cost_per_violation list_size1 list_size2, followed for
	 * two lists of variable indexes (must be of equal size).
	 * E.g. to express soft_same({x1,x2,x3,x4}, {x5,x6,x7,x8}):
	 * test 8 4 1 999999
	 * 4 4 4 4 4 4 4 4
	 * 8 0 1 2 3 4 5 6 7 -1 ssame 10
	 * 4 4
	 * 0 1 2 3
	 * 4 5 6 7
	 * 
	 * @param  ofstream _cstFileStrm : an ofstream to write the result
	 * @param  Vlist _varset1 : the list of first set of variables
	 * @param  Vlist _varset2 : the list of second set of variables
	 * @param  unsigned int _default : The default cost_per_violation
	 * @return true if evrything is ok
	 */
	
	bool ssame(ofstream&  _cstFileStrm,
					Vlist _varset1,
					Vlist _varset2,
					unsigned int _default = INFTY);

	/**
	 * Global Cardinality : WCSP file format
	 * sgcc (var | dec) cost_per_violation nb_values,
	 * followed for each value by: value lower_bound upper_bound.
	 * E.g. to express soft_gcc({x1,x2,x3,x4}) with each value
	 * v from 1 to 4 only appear at most v+1 and at least v-1
	 * times by variable-based violation measure:
	 * 4 1 2 3 4 -1 sgcc var 1
	 * 4
	 * 1 0 2
	 * 2 1 3
	 * 3 2 4
	 * 4 3 5
	 * 
	 * @param  ofstream _cstFileStrm : an ofstream to write the result
	 * @param  Vlist _var : the list of variable 
	 * @param  ValuesTolerance _t : vector of variable and associated
	 * 					Tolerance values
	 * @param  unsigned int _default : The default cost_per_violation
	 * @return true if evrything is ok
	 */ 
	bool sgcc(ofstream&  _cstFileStrm,
					Vlist _var,
					ValuesTolerance _t,
					unsigned int _default = INFTY);
	

	/**
	 * Regular: WCSP file format
	 * sregular (var | edit) cost_per_violation, followed by
	 * DFA represented as follows:
	 * nb_state : the number of states represented by numbers starting
	 * 		from 0
	 * nb_start : number of initial states, followed by a list
	 * 		of starting states
	 * nb_end : number of final states, followed by a list of ending states
	 * nb_transition : number of transitions followed by a list of
	 * 		transitions represented as start_state symbol end_state
	 * E.g. to express soft_regular({x1,x2,x3}) with DFA (3*)+(4*)
	 * 		and variable-based violation measure:
	 * 3 1 2 3 -1 sregular var 10
	 * 2	;; Only two states: 0 and 1
	 * 1 0  ;; Initial state: 1
	 * 2 0 1    ;;  Final state: 0, 1
	 * 3	;; Only three transitions
	 * 0 3 0    ;; encounter 3 at state 0, go to state 0
	 * 0 4 1    ;; encounter 4 at state 0, go to state 1
	 * 1 4 1    ;; encounter 4 at state 1, go to state 1
	 * @param  ofstream _cstFileStrm : an ofstream to write the result
	 * @param  VariableDomaine _vd : map of variables and related domains.
	 * 			This map must /!\ These variable must have the same domain
	 * @param  unsigned int _nbState : the number of state in the DFA. (eg:
	 * 				the return time)
	 * @param  unsigned int _value : a specific value of variables in the DFA
	 * @param  unsigned int _default : The default cost_per_violation
	 * @return unsigned int _option : an option to specify sequence or
	 * 			cyclique word | 0 => a cyclique word | 1 => a sequence 
	 * @return true if evrything is ok
	 */ 

	bool sregular	(ofstream&  _cstFileStrm,
					VariableDomaine _vd,
					unsigned int _nbState,
					unsigned int _value,
					unsigned int _default=INFTY,
					unsigned int _option=1); //0 =Cycle 

	/**
	 * Regular: WCSP file format
	 * sregular (var | edit) cost_per_violation, followed by
	 * DFA represented as follows:
	 * nb_state : the number of states represented by numbers starting
	 * 		from 0
	 * nb_start : number of initial states, followed by a list
	 * 		of starting states
	 * nb_end : number of final states, followed by a list of ending states
	 * nb_transition : number of transitions followed by a list of
	 * 		transitions represented as start_state symbol end_state
	 * E.g. to express soft_regular({x1,x2,x3}) with DFA (3*)+(4*)
	 * 		and variable-based violation measure:
	 * 3 1 2 3 -1 sregular var 10
	 * 2	;; Only two states: 0 and 1
	 * 1 0  ;; Initial state: 1
	 * 2 0 1    ;;  Final state: 0, 1
	 * 3	;; Only three transitions
	 * 0 3 0    ;; encounter 3 at state 0, go to state 0
	 * 0 4 1    ;; encounter 4 at state 0, go to state 1
	 * 1 4 1    ;; encounter 4 at state 1, go to state 1
	 *
	 * SREGULAR with  fixed state
	 * @param  ofstream _cstFileStrm : an ofstream to write the resultsregular_w_f_s
	 * @param  VariableDomaine _vd : map of variables and related domains.
	 * 			This map must /!\ These variable must have the same domain
	 * @param  unsigned int _nbState : the number of state in the DFA. (eg:
	 * 				the return time)
	 * @param  unsigned int _value : a specific value of variables in the DFA
	 * @param  unsigned int _historic : size of the historic (fixed states)
	 * @param  unsigned int _default : The default cost_per_violation
	 * @return unsigned int _option : an option to specify sequence or
	 * 			cyclique word | 0 => a cyclique word | 1 => a sequence 
	 * @return true if evrything is ok
	 */ 

	bool 	sregular_w_f_s(ofstream&  _cstFileStrm,
					VariableDomaine _vd,
					unsigned int _nbState,
					unsigned int _value,
					unsigned int _historic,
					unsigned int _default=INFTY,
					unsigned int _option=1); //0 =Cycle
	/**
	 * Defined value of INFTY
	 * @return int INFTY
	 */ 
	int 	getInfty() const { return INFTY ; }
	
	/**
	 * Return the number of generated constraint
	 * @return int INFTY
	 */ 
	int 	getNbCstr() const { return m_cstr;  }
	
	/**
	 * Set the WCSP dir  name
	 * @param std::string _dir the dir name
	 */ 
	void	setWCSPDir(const std::string& _dir)
			{ m_wcsp_dir = _dir; }
	/**
	 * Return the WCSP dirname
	 * @return std::string _dir
	 */ 
	std::string 	getWCSPDir() { return m_wcsp_dir; }
	/**
	 * Return the WCSP dirname
	 * @return std::string _dir
	 */ 
	std::string 	getWCSPDir() const { return m_wcsp_dir; }

	void	setWCSPFile(const std::string& _file){ m_wcsp_file = _file; }
	std::string 	getWCSPFile() { return m_wcsp_file; }
	std::string 	getWCSPFile() const { return m_wcsp_file; }



	/**
	 * Set the solution dir  name
	 * @param std::string _dir the dir name
	 */ 
	void	setSolutionDir(const std::string& _dir)
			{ m_solution_dir = _dir; }
	/**
	 * Return the solution dirname
	 * @return std::string _dir
	 */ 
	std::string 	getSolutionDir() { return m_solution_dir; }
	/**
	 * Return the solution dirname
	 * @return std::string _dir
	 */ 
	std::string 	getSolutionDir() const { return m_solution_dir; }

	/**
	 * Create the constraint dir
	 * @param std::string _dir the dir name
	 * @return true if evrything is ok
	 */ 
	bool createConstraintFileDir  (	const std::string& _cstFileDirName);
	/**
	 * Create the constraint file and return its name
	 * @param std::string _cstFileDirName the dir name
	 * @param std::string _cstFileName the file name
	 * @return std::string  the file name
	 */
	std::string createConstraintFile (const std::string& _cstFileDirName,
					const std::string& _cstFileName);
					
	/**
	 * Write the WCSP file header and save it on the disk
	 * @param std::string _cstFile the generated constraint file
	 * @param std::string _pbName the probleme name
	 * @param int _nbvar the number of variable
	 * @param int _domainSize the maximum domain size
	 */
	void saveConstraintFile (const std::string& _cstFile,
					const std::string& _pbName, 
					unsigned int _nbvar,
					unsigned int _domainSize);

	//Typedef for boost 
	typedef boost::adjacency_list<boost::vecS, boost::vecS,
		  boost::directedS, no_property, EdgeTRProperty> graph_t;				  
	typedef boost::graph_traits<graph_t>::vertex_descriptor  vertex_descriptor;
	typedef boost::graph_traits<graph_t>::vertex_iterator 	 vertex_iterator;
	typedef boost::graph_traits<graph_t>::edge_descriptor 	 edge_descriptor;
	typedef boost::graph_traits<graph_t>::edge_iterator 	 edge_iterator;
	typedef boost::graph_traits<graph_t>::out_edge_iterator  out_edge_iterator;
	typedef boost::graph_traits<graph_t>::in_edge_iterator 	 in_edge_iterator;

	/**
	 * Build a graph that containt sregular constraint by using sequence
	 * 		option
	 * @param unsigned int _state the number of state in the DFA. (ex:
	 * 				the return time)
	 * @param unsigned int _cropID a specific value of variables in the DFA
	 * @param Domain _croplst : a vector of UnaryValueCost that contain
	 * 			the domain. Only values are used. All data related to
	 * 			cost are purely ignore by this method
	 */
	void sequenceREGULARConstraintGraph (
						const unsigned int _state,
						const unsigned int _cropID,
						const Domaine& _croplst);
	/**
	 * Build a graph that containt sregular constraint by using cycle
	 * 		option
	 * @param unsigned int _state the number of state in the DFA. (eg:
	 * 				the return time)
	 * @param unsigned int _cropID a specific value of variables in the DFA
	 * @param Domain _croplst : a vector of UnaryValueCost that contain
	 * 			the domain. Only values are used. All data related to
	 * 			cost are purely ignore by this method
	 */
	void cyclicSREGULARConstraintGraph (
						const unsigned int _state,
						const unsigned int _cropID,
						const Domaine& _croplst);

	/**
	 * Build a graph that containt sregular constraint by using sequence
	 * 		option
	 * @param unsigned int _state the number of state in the DFA. (ex:
	 * 				the return time)
	 * @param unsigned int _cropID a specific value of variables in the DFA
	 * @param unsigned int _historic : size of the historic
	 * @param Domain _croplst : a vector of UnaryValueCost that contain
	 * 			the domain. Only values are used. All data related to
	 * 			cost are purely ignore by this method
	 */
	void sequenceSREGULARWithFixedState (
						const unsigned int _state,
						const unsigned int _cropID,
						const unsigned int _historic,
						const Domaine& _croplst);
						
	/**
	 * Generate regular constraint from the containt graph
	 * @return return a string of constraint in WCSP format
	 */
	std::string SREGULAR2String();

	/**
	 * Return the temporary	variable list
	 * @return Vlist&
	 */ 
	const Vlist&	getTmpVarList() const{ return m_tmpvar; }

	/**
	 * Return a ConstraintsViewer
	 * @return ConstraintsViewer&
	 */ 
    const ConstraintsViewer&	constraints() const{ return m_viewer; }

	/**
	 * Return a const 	ConstraintsViewer
	 * @return ConstraintsViewer&
	 */ 
    ConstraintsViewer&	constraints() { return m_viewer; }
	
private : 
	std::string	m_wcsp_dir ; 
        std::string	m_wcsp_file ; 
	std::string	m_solution_dir ;
	int		m_cstr ;
	unsigned int	m_nbvariable ;
	unsigned int	m_maxDsize ;
	VariableDomaine	m_additionnalvar ;
	Vlist	m_tmpvar; 
	graph_t	m_graph;

	ConstraintsViewer m_viewer; 


	/**
	 * Build a complet graph 
	 * @param unsigned int _nbnode number of node
	 * @param unsigned int _nbedge maximum number of edge
	 */
	void buildCompleteGraph(unsigned int _nbnode,
			unsigned int _nbedge,
			vertex_descriptor _root,
			bool _init);
			
	/**
	 * Build a complet graph
	 * @param unsigned int _nbnode number of node
	 * @param unsigned int _nbedge maximum number of edge
	 * @param unsigned int _UB upper bound
	 * @param CompOperator _fOP comparaison constraint
	 * @param bool _init initialization flag
	 */
	std::string filter(
		 unsigned int _nbnode,
		 unsigned int _nbedge,
		 unsigned int _UB,
		 CompOperator _fOP,
		 bool _init);
		 
	/**
	 * filter using arithmetic constraint
	 * @param Vlist _var variable list
	 * @param Domaine _domain variable domain
	 * @param unsigned int _UB upper bound
	 * @param CompOperator _fOP comparaison constraint
	 * @param bool _init initialization flag
	 * @return std::string extention definition of constraint
	 */
	std::string filter(
		 Vlist _var,
		 Domaine _domain,
		 unsigned int _UB,
		 CompOperator _fOP,
		 bool _init);

	void show_unarycost(const VariableDomaine& vd);
	void show_cost(const Vlist& _var, const MultiValueCosts& _mvc);
	void show_globalarithmetic(const Vlist& _var, unsigned int _UB);
	void show_pseudocumulative(const Vlist& _var, const MultiValueCosts& _mvc); 
	void show_sregular(const VariableDomaine& vd, unsigned int _value,
						bool _histo = false);
	void show_equal(const Vlist& _var, unsigned int _nbcstr);
	void show_ssame(const Vlist& _var1, const Vlist& _var2);
	void show_sgcc(const Vlist& _var, unsigned int _nbcstr);
};

typedef GenericWCSPConstraintFactory GWCF;

/**
 * Functor to build UnaryValueCost
 */ 
struct UVC {
	GWCF::UnaryValueCost operator()(unsigned int _value, int  _cost=0) 
    {
		return std::pair<unsigned int, int>(_value, _cost);
	}
};

/**
 * Functor to return Value from UnaryValueCost
 */ 
struct UVCValue {
	unsigned int operator()(const GWCF::UnaryValueCost& vc) 
    {
		return vc.first;
	}
};

/**
 * Functor to return Cost from UnaryValueCost
 */ 
struct UVCCost {
	int operator()(const GWCF::UnaryValueCost& vc) 
    {
		return vc.second;
	}
};

/**
 * Functor to build Domaine
 */
struct D {
	D() : d() {}
	
	void operator()(const GWCF::UnaryValueCost vc)
		{ d.push_back(vc); 	}
		
	GWCF::Domaine& operator()()
		{	return d; 	}
	GWCF::Domaine d;
 };

/**
 * Functor to build VariableDomaine
 */
struct VD {
	VD() : v() {}
	
	void operator()(const unsigned int _var, GWCF::Domaine  _d)
		{
			v.insert(std::make_pair < unsigned int, GWCF::Domaine>(_var, _d));
		}
	GWCF::VariableDomaine& operator()()
		{	return v; 	}
		
	GWCF::VariableDomaine v;
 };


/**
 * Functor to build Variable or Value list Vlist
 */
struct LST {
	LST() : l() {}
	
	void operator()(const unsigned int vl)
		{ l.push_back(vl); 	}
		
	GWCF::Vlist operator()()
		{	return l; 	}
	GWCF::Vlist l;
 };

/**
 * Functor to build MultiValueCost
 */ 
struct MVC {
	GWCF::MultiValueCost operator()(GWCF::Vlist _value, int  _cost=0) 
    {
		return std::pair<GWCF::Vlist, int>(_value, _cost);
	}
};

/**
 * Functor to return Value from MultiValueCost
 */ 
struct MVCValue {
	GWCF::Vlist operator()(const GWCF::MultiValueCost& mvc) 
    {
		return mvc.first;
	}
};

/**
 * Functor to return Cost from MultiValueCost
 */ 
struct MVCCost {
	int operator()(const GWCF::MultiValueCost& mvc) 
    {
		return mvc.second;
	}
};

/**
 * Functor to build a vector of MultiValueCost
 */ 
struct MVCS {
	MVCS() : d() {}
	
	void operator()(const GWCF::MultiValueCost vc)
		{ d.push_back(vc); 	}
		
	GWCF::MultiValueCosts& operator()()
		{	return d; 	}
	GWCF::MultiValueCosts d;
 };

/**
 * Functor to build a Tolerance data
 */
struct TO {
	GWCF::Tolerance operator()(int _min, int  _max) 
    {
		return std::pair<int, int>(_min, _max);
	}
};

/**
 * Functor to return the min value from a Tolerance
 */
struct TOMin {
	int operator()(const GWCF::Tolerance& to) 
    {
		return to.first;
	}
};

/**
 * Functor to return the max value from a Tolerance
 */
struct TOMax {
	int operator()(const GWCF::Tolerance& to) 
    {
		return to.second;
	}
};

/**
 * Functor to build an UnaryValueTolerance
 */
struct UVTO {
	GWCF::UnaryValueTolerance operator()(int _value, GWCF::Tolerance  _to) 
    {
		return std::pair<int, GWCF::Tolerance>(_value,  _to);
	}
};

/**
 * Functor to return value from an  UnaryValueTolerance
 */
struct UVTOValue {
	int operator()(const GWCF::UnaryValueTolerance& to) 
    {
		return to.first;
	}
};
/**
 * Functor to return the Tolerance from an UnaryValueTolerance
 */

struct UVTOInterv {
	GWCF::Tolerance operator()(const GWCF::UnaryValueTolerance& to) 
    {
		return to.second;
	}
};

/**
 * Functor to build a vector of  UnaryValueTolerance
 */
struct VT {
	VT() : t() {}
	
	void operator()(const GWCF::UnaryValueTolerance vc)
		{ t.push_back(vc); 	}
		
	GWCF::ValuesTolerance& operator()()
		{	return t; 	}
	GWCF::ValuesTolerance t;
 }; 
} // namespace crash

#endif
