/*
 * @file record/pkgs/decision/utils/ConstraintsViewer.hpp
 *
 * This file is part of VLE, a framework for multi-modeling, simulation
 * and analysis of complex dynamical systems
 * http://www.vle-project.org
 *
 * Copyright (c) 2003-2007 Gauthier Quesnel <quesnel@users.sourceforge.net>
 * Copyright (c) 2003-2010 ULCO http://www.univ-littoral.fr
 * Copyright (c) 2007-2010 INRA http://www.inra.fr
 *
 * See the AUTHORS or Authors.txt file for copyright owners and contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef RECORD_PKGS_DECISION_CONSTRAINTS_VIEWER_HPP
#define RECORD_PKGS_DECISION_CONSTRAINTS_VIEWER_HPP


#include <boost/property_map/property_map.hpp>
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/graph_traits.hpp>

#include <string>
#include <iostream>
#include <fstream>
#include <algorithm>
#include <ostream>
#include <vector>
#include <vle/utils/Exception.hpp>
#include <vle/utils/Path.hpp>


using namespace std;
using namespace boost;


namespace crash {
	
typedef boost::property<boost::edge_weight_t, int> VVProperty;
/**
 * The strucutre for vertex properties
 */
struct CstrNodeProperty {
	unsigned int  id;
	int  type;
	std::string name;
	std::string color;
};

class ConstraintsViewer 
{
public:
	enum NodeType {CSTR_VARIABLE, CSTR_TMP_VARIABLE, CSTR_UNARYCOST,
			CSTR_COST, CSTR_ARITHMETIC, CSTR_PS_CUMULATIVE,
			CSTR_EQUAL, CSTR_SSAME, CSTR_SGCC,	CSTR_SREGULAR,
			CSTR_SREGULAR_WFS, CSTR_TEMPORAL, CSTR_RESOURCE };

	/** Defines option to generate graph constraint
     * @code
     * record::pkgs::decision::utils::ConstraintsViewer v = START | FINISH;
     * @endcode
     */
    enum Option {
        SH_VAR 			= 1 << 0,  	/**< option for variable list */
        SH_TMP_VAR 		= 1 << 1, 	/**< option for additionnal var list */
        SH_UNARYCOST 	= 1 << 2,  	/**< option for unary cost constraint*/
        SH_COST 	= 1 << 3,  		/**< option for cost constraint*/
        SH_ARITHMETIC 	= 1 << 4,   /**< option for arithmetic constraint*/
        SH_PS_CUMULATIVE 	= 1 << 5,    /**< option for cumulative constraint*/
		SH_EQUAL 	= 1 << 6,  	/**< option for equal constraint. */
        SH_SSAME 	= 1 << 7, 	/**< option for ssame constraint*/
        SH_SGCC 	= 1 << 8,   /**< option for sgcc cost constraint*/
        SH_SREGULAR = 1 << 9,   /**< option for sregular constraint*/
        SH_SREGULAR_WFS = 1 << 10,   /**< option for sregular_wfs constraint*/
        SH_TEMPORAL 	= 1 << 11,    /**< option for temporal constraint*/
		SH_RESOURCE 	= 1 << 12,    /**< option for resource constraint*/
		SH_ALL_CSTR 	= 1 << 13    /**< option for all constraints*/
    };


	typedef adjacency_list<boost::vecS, boost::vecS,
		boost::bidirectionalS , CstrNodeProperty, VVProperty> graph_t;
		
	typedef graph_traits<graph_t>::vertex_descriptor vertex_descriptor;
	typedef graph_traits<graph_t>::vertex_iterator   vertex_iterator;
	typedef graph_traits<graph_t>::edge_descriptor 	 edge_descriptor;
	typedef graph_traits<graph_t>::edge_iterator 	 edge_iterator;
	
	typedef graph_traits<graph_t>::out_edge_iterator out_edge_iterator;
	typedef graph_traits<graph_t>::in_edge_iterator  in_edge_iterator;

	
	ConstraintsViewer() : m_graph(), m_color(), 
		m_option ((ConstraintsViewer::Option)(
			ConstraintsViewer::SH_VAR
			| ConstraintsViewer::SH_TMP_VAR))
	{
		initColors();
	}
	ConstraintsViewer(const unsigned int _nbnode) :
		m_color() , 
		m_option((ConstraintsViewer::Option)(
			ConstraintsViewer::SH_VAR
			| ConstraintsViewer::SH_TMP_VAR))
	{
		m_graph = graph_t();
		initColors();
		
		for (unsigned int i=0; i< _nbnode; i++)
			addNode(i, CSTR_VARIABLE);
	}
	
	virtual ~ConstraintsViewer() {}

	void initColors()
	{
		m_color.push_back("white"); //CSTR_VARIABLE
		m_color.push_back("lightgrey"); //CSTR_TMP_VARIABLE
		m_color.push_back("orange"); //CSTR_UNARYCOST
		m_color.push_back("red"); //CSTR_COST
		m_color.push_back("violet"); //CSTR_ARITHMETIC
		m_color.push_back("moccasin"); //CSTR_PS_CUMULATIVE
		m_color.push_back("yellowgreen"); //CSTR_EQUAL
		m_color.push_back("powderblue"); //CSTR_SSAME
		m_color.push_back("olivedrab1"); //CSTR_SGCC
		m_color.push_back("royalblue"); //CSTR_SREGULAR 
		m_color.push_back("turquoise"); //CSTR_SREGULAR_WFS
		m_color.push_back("darkseagreen"); //CSTR_TEMPORAL
		m_color.push_back("gold"); //CSTR_RESOURCE
	}
	
	void removeAllEdges()
	{
		std::pair<edge_iterator, edge_iterator> p = edges(m_graph);
		for (edge_iterator it = p.first; it != p.second; ++it)
			remove_edge(*it,m_graph);
	}

	void removeAllNode()
	{
		std::pair<vertex_iterator, vertex_iterator> pv_it;
		for (pv_it = vertices(m_graph);	pv_it.first != pv_it.second;
				++pv_it.first)
			boost::remove_vertex(*pv_it.first ,m_graph);
	}

	/**
	 * @brief Add a new node to the graph
	 * */
	inline vertex_descriptor addNode(const int _id, NodeType _type)
	{
		if(_type < CSTR_VARIABLE || _type > CSTR_RESOURCE)
			throw vle::utils::ArgError("CstrsViewer: Unknown node type");
		
		vertex_descriptor vd = add_vertex(m_graph);
		m_graph[vd].id 		= _id;
		m_graph[vd].type 	= _type;
		m_graph[vd].color 	= m_color.at((unsigned int) _type);
		return  vd; 
	}
	

	/**
	 * @brief Remove a node from the graph
	 * */
	inline void removeNode(const vertex_descriptor &vd)
	{
		clear_vertex(vd,m_graph);
		remove_vertex(vd,m_graph);
	}

	edge_descriptor pushBackEdge(	const vertex_descriptor &v1,
						const vertex_descriptor &v2,
						const int edge_desc ) ;

	inline  edge_descriptor	getEdgeDescriptor (
										vertex_descriptor src,
										vertex_descriptor dst)
	{
		return edge(src, dst, m_graph).first; 
	}
	inline edge_descriptor	getEdgeDescriptor (const vertex_descriptor src, 
									const vertex_descriptor dst) const
	{
		return edge(src, dst, m_graph).first; 
	}
	
	vertex_descriptor getCstrDescriptor(const unsigned int id) const;
	vertex_descriptor getVariableDescriptor(const unsigned int id) const; 
								
	const  graph_t& graph()	const {return m_graph; }
	graph_t& graph()	 {return m_graph; }

	Option addAll ()
	{
		return m_option =  (Option)(ConstraintsViewer::SH_VAR
				|ConstraintsViewer::SH_TMP_VAR
				|ConstraintsViewer::SH_UNARYCOST
				|ConstraintsViewer::SH_COST
				|ConstraintsViewer::SH_ARITHMETIC
				|ConstraintsViewer::SH_PS_CUMULATIVE
				|ConstraintsViewer::SH_EQUAL
				|ConstraintsViewer::SH_SSAME
				|ConstraintsViewer::SH_SGCC
				|ConstraintsViewer::SH_SREGULAR
				|ConstraintsViewer::SH_SREGULAR_WFS
				|ConstraintsViewer::SH_RESOURCE
				|ConstraintsViewer::SH_ALL_CSTR	);
	}
	void addOption(const Option _option);
	
	void resetOption()
	{
		m_option =  (Option)(ConstraintsViewer::SH_VAR
				|ConstraintsViewer::SH_TMP_VAR);
	}
	
	bool show(const Option _option) const;

	void toDotFile (const std::string& _fileName)  const ; 
private:
	graph_t 		m_graph;
	std::vector<std::string> m_color;
	Option m_option;
};

} // namespace crash
#endif
