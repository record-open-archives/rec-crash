/**
 * @file System.cpp
 * @author The VLE Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * VLE Environment - the multimodeling and simulation environment
 * This file is a part of the VLE environment
 * http://www.vle-project.org
 *
 * Copyright (C) 2007-2010 INRA http://www.inra.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <vle/utils/Path.hpp>
#include <vle/utils/Exception.hpp>
#include <boost/filesystem.hpp>
#include <boost/version.hpp>
#include <glibmm/timer.h>
#include <glibmm/stringutils.h>
#include <glibmm/miscutils.h>
#include <fstream>
#include <ostream>
#include <iostream>
#include <cstring>
#include <glib/gstdio.h>


#include <sys/wait.h>
#include <unistd.h>
#include <errno.h>

#include <utils/System.hpp>

namespace fs = boost::filesystem;

using namespace boost;
using namespace vle;

namespace crash {

System* System::m_system = 0;




void System::solve(	const std::string& _dirName,  
						const std::string& _cstFileName,
						const std::string& _optionNBSolution,
						const std::string& _optionMaxCost)
	{
 cout<<" SOLVE: "<<endl;

				/*option : -all -first */
			std::string _wcspFile(_dirName.c_str()); 
			_wcspFile.append("/"); 
			_wcspFile.append(_cstFileName.c_str()); 

			std::string _solFile(_wcspFile.c_str()); 
			_solFile.append(".sol");//file.txt
			
			std::string _dir(vle::utils::Path::path().getPackageDataDir().c_str()); 
			_dir.append("/"); 
			_dir.append(_dirName.c_str()); //cstr
			
			std::string _soldir(_dirName.c_str()); 
			_soldir.append("/"); 
			_soldir.append(_solFile); 
			
			std::list < std::string > argv;
			
			argv.push_back(vle::utils::Path::path().getPackageLibFile("toulbar2"));
			argv.push_back(_cstFileName);

			//Max Cost
			
			if((_optionNBSolution == "ALL") || (_optionNBSolution == "all")){
				std::string _cost("-ub=");
				_cost.append(_optionMaxCost.c_str());
				argv.push_back(_cost);
				argv.push_back("-e");
				argv.push_back("-a");

			}
			argv.push_back("-s");
			
			try {
				 /*std::cout 	<<"_dirName.............. : " << _dirName << std::endl
							<<std::endl
							<<"_wcspFile................. : " << _wcspFile << std::endl
							<<"_solFile.................. : " << _solFile << std::endl
							<< std::endl
							<<"_dir...................... : " << _dir << std::endl
							<<"_soldir................... : " << _soldir << std::endl
							<<"getToulbarBinaryFilePath.............. : " << getToulbarBinaryFilePath() << std::endl ;
				*/
				
				process(_dir, argv);
				
					std::ofstream _solFilestream(
						vle::utils::Path::path().getPackageDataFile(_solFile).c_str(), std::ios::out); 
					if(_solFilestream)
					{ 
						_solFilestream << m_strout; 
						//std::cout 		<<" m_strerr.............. : " << m_strerr << std::endl ;
						//std::cout 		<<" m_strout.............. : " << m_strout << std::endl ; 
						cleanOutput(); 
						cleanError(); 
						_solFilestream.close(); 
					}
					
			} catch(const Glib::SpawnError& e) {
				throw vle::utils::InternalError(fmt(
						_("System error: Solution file cannot be generated  %1%")) % e.what());
			}
	}
		
void System::transform(	
						const std::string& _dirName,  
						const std::string& _cstFileName)
	{
			std::string _wcspFile(_dirName.c_str()); 
			_wcspFile.append("/"); 
			_wcspFile.append(_cstFileName.c_str()); 
			std::string _soldir(_wcspFile.c_str()); 
			_soldir.append(".sol");//file.sol
			std::string _solfile(_cstFileName.c_str()); 
			_solfile.append(".sol");//file.txt
			std::string _dir(vle::utils::Path::path().getPackageDataDir().c_str()); 
			_dir.append("/"); 
			_dir.append(_dirName.c_str()); //cstr
			std::string _soldir2(_dir.c_str()); 
			_soldir2.append("/"); 
			_soldir2.append(_solfile); 
			
			std::list < std::string > argv;
			argv.push_back(getTransformFilePath());
			argv.push_back(_soldir2);
				
			try {
				process(_dir, argv);
					std::ofstream _solFilestream(
						vle::utils::Path::path().getPackageDataFile(_soldir).c_str(), std::ios::out); 
					if(_solFilestream)
					{    
						_solFilestream << m_strout; 
						//std::cout 		<<" m_strerr.............. : " << m_strerr << std::endl ;
						//std::cout 		<<" m_strout.............. : " << m_strout << std::endl ; 
						cleanOutput(); 
						cleanError(); 
						_solFilestream.close(); 
					}
					
			} catch(const Glib::SpawnError& e) {
				throw vle::utils::InternalError(fmt(
						_("System error: Solution file cannot be generated  %1%")) % e.what());
			}
	}

void System::waitProcess()
{
    m_success = false;
    int status;
    int result;

    result = waitpid(m_pid, &status, 0);
    if (result != -1) {
        if (WIFEXITED(status)) {
            if (WEXITSTATUS(status) > 0) {
                appendError((fmt(_("Error '%1%' in subprocess")) %
                             WEXITSTATUS(status)).str());
            } else {
                m_success = true;
            }
        }
    }

    m_out->join();  /* we wait the end of data readed from thread */
    m_err->join();
    Glib::spawn_close_pid(m_pid); /* we close process (win32 requirement) */

	
    m_out = 0;
    m_err = 0;
    m_stop = true;
}


void System::addFile(const std::string& path, const std::string& name)
{
    if (not fs::exists(utils::Path::buildFilename(path, name))) {
		std::ofstream file(utils::Path::buildFilename(path, name).c_str());
    }
}

void System::addDirectory(const std::string& path, const std::string& name)
{
    if (not fs::exists(utils::Path::buildDirname(path, name))) {
        fs::create_directory(utils::Path::buildDirname(path, name));
    }
}



void System::cleanOutput()
{
    Glib::Mutex::Lock lock(m_mutex);
    m_strout.clear();
}

void System::cleanError()
{
    Glib::Mutex::Lock lock(m_mutex);
    m_strerr.clear();
}


bool System::existDir(const std::string& path, const std::string& name)
{
    return fs::exists(utils::Path::buildDirname(path, name)) ; 
}

bool System::existFile(const std::string& path, const std::string& name)
{
    return fs::exists(utils::Path::buildFilename(path, name)) ; 
}

void System::removeFile(const std::string& pathFile)
{
    std::string path = utils::Path::buildFilename(
        vle::utils::Path::path().getPackageDir(), pathFile);

    fs::remove_all(path);
    fs::remove(path);
}



void System::copyFile(const std::string& sourceFile, std::string& targetFile)
{
    fs::copy_file(sourceFile, targetFile);
}



/*   manage thread   */
void System::process(const std::string& workingDir,
                      const std::list < std::string >& lst)
{
	
    if (m_out != 0 or m_err != 0) {
        throw utils::InternalError(_("System error: wait an unknow process"));
    }

    m_stop = false;

    try {
		int out, err;
        Glib::spawn_async_with_pipes(workingDir,
                                     lst,
									 Glib::SPAWN_SEARCH_PATH |
                                     Glib::SPAWN_DO_NOT_REAP_CHILD,
                                     sigc::slot < void >(),
                                     &m_pid,
                                     0, &out, &err);
	
		m_out = Glib::Thread::create(
            sigc::bind(sigc::mem_fun(*this, &System::readStandardOutputStream),
                       out), true);
				
        m_err = Glib::Thread::create(
            sigc::bind(sigc::mem_fun(*this, &System::readStandardErrorStream),
                       err), true);
					   
        m_wait = Glib::Thread::create(
            sigc::mem_fun(*this, &System::waitProcess), false);
	
		while(!m_stop) {
			Glib::usleep(10000);
		}

    } catch(const std::exception& e) {
        appendError((fmt(_("Std exception: '%1%'")) % e.what()).str());
	throw utils::InternalError(e.what());
    } catch(const Glib::Exception& e) {
        appendError((fmt(_("Glib exception: '%1%'")) % e.what()).str());
	throw utils::InternalError(e.what());
    }
}



void System::readStandardOutputStream(int stream)
{
    const size_t bufferSize = 64;
    char* buffer = new char[bufferSize];
    int result;

    do {
        std::memset(buffer, 0, bufferSize);
        result = ::read(stream, buffer, bufferSize - 1);
        if (result == -1) {
            appendOutput(_("Error reading stream"));
            if (m_stop) {
                result = 0;
            }
        } else if (result == 0) {
            appendOutput("\n");
        } else {
            appendOutput(buffer);
        }
    } while (result);
    delete[] buffer;
	
    ::close(stream);
}


void System::readStandardErrorStream(int stream)
{
    const size_t bufferSize = 64;
    char* buffer = new char[bufferSize];
    int result;

    do {
        std::memset(buffer, 0, bufferSize);
        result = ::read(stream, buffer, bufferSize - 1);
		
        if (result == -1) {
            appendError(_("Error reading stream"));
            if (m_stop) {
                result = 0;
            }
        } else if (result == 0) {
            appendError("\n");
        } else {
            appendError(buffer);
        }
    } while (result);

    delete[] buffer;
    ::close(stream);
}


void System::getOutput(std::string& str) const
{
    Glib::Mutex::Lock lock(m_mutex);
    str.append(m_strout);
}

void System::getError(std::string& str) const
{
    Glib::Mutex::Lock lock(m_mutex);
    
	str.append(m_strerr);
}

void System::appendOutput(const char* str)
{
    Glib::Mutex::Lock lock(m_mutex);

    m_strout.append(str);
}

void System::appendError(const char* str)
{
    Glib::Mutex::Lock lock(m_mutex);
    m_strerr.append(str);
}

void System::appendOutput(const std::string& str)
{
    Glib::Mutex::Lock lock(m_mutex);

    m_strout.append(str);
}

void System::appendError(const std::string& str)
{
    Glib::Mutex::Lock lock(m_mutex); 
    m_strerr.append(str);
}

} // namespace record decision
