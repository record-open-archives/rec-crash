/**
 * @file
 * @author RECORD Team,
 *   http://record.toulouse.inra.fr ,
 *   Applied Mathematics and Informatics Research Division &
 *   Environment and Agronomy Research Division,
 *   French National Institute for Agricultural Research
 * @version $Id: $
 */

/*
 * Copyright (C) 2010 - INRA
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#include <littleCrash/LittleAgent.hpp>
namespace crash
{
	LittleAgent::LittleAgent(const vd::DynamicsInit& mdl, const vd::InitEventList& evts):
		ve::Agent(mdl, evts),
		mWeather(),
		flag(false)
	{
		/*********************      LittleAgent expert & system knowledge        ********************/
		initCrop(evts);
		initSoilType(evts);
		initLandUnit(evts);
		initITK(evts);
                
	
		/*********************      add facts	 ********************/
		/* Crop*/
		addFact("H2Orec",
				boost::bind(&LandCover::setH2oHrv,  &(mLandUnit.getpLandCover()), _1));
		addFact("HarvestedOrganeStage",
				boost::bind(&LandCover::setHarvestedOrganeStage,  &(mLandUnit.getpLandCover()), _1));
		addFact("NbLeaf",
				boost::bind(&LandCover::setNbLeaf,  &(mLandUnit.getpLandCover()), _1));
		addFact("Swfac",
				boost::bind(&LandCover::setSwfac,  &(mLandUnit.getpLandCover()), _1));
		addFact("VegetativeStage",
				boost::bind(&LandCover::setVegetativeStage,  &(mLandUnit.getpLandCover()), _1));
		/* Soil */
		addFact("AvgHur30",
				boost::bind(&Soil::setAvgHur30,  &(mLandUnit.getpSoil()), _1));
		addFact("AvgHucc30",
				boost::bind(&Soil::setAvgHucc30,  &(mLandUnit.getpSoil()), _1));
		addFact("AvgHur",
				boost::bind(&Soil::setAvgHur,  &(mLandUnit.getpSoil()), _1));
		addFact("AvgHucc",
				boost::bind(&Soil::setAvgHucc,  &(mLandUnit.getpSoil()), _1));
		addFact("WaterDepletion",
				boost::bind(&Soil::setWaterDepletion,  &(mLandUnit.getpSoil()), _1));

		/* weather */
		addFact("Rain",
				boost::bind(&Weather::setLst_rain, &mWeather,_1));
		addFact("Tmax",
				boost::bind(&Weather::setLst_tx, &mWeather,_1));
		addFact("Tmin",
				boost::bind(&Weather::setLst_tn, &mWeather,_1));

		/* Crop Operation */
		

	}// end constructor

	vd::Time LittleAgent::init(const vd::Time& time){

		id = 1; // id pu
		int _yr = vu::DateTime::year(time);
		std::string code = setCode( mItk.getId(), id, _yr);
		pf.buildItkRules(mItk, code);
		pf.buildItkOperations(mItk, mVariety, code, _yr );

		/*********************      add output functions	 ********************/

		outputFunctions().add("sow." + code,
				boost::bind(&LittleAgent::outSow, this,_1,_2,_3));
		outputFunctions().add("hrv." + code,
				boost::bind(&LittleAgent::outHrv, this,_1,_2,_3));
		outputFunctions().add("frtN." + code,
				boost::bind(&LittleAgent::outFrtN, this,_1,_2,_3));
		outputFunctions().add("sow_frtN." + code,
				boost::bind(&LittleAgent::outSowFrtN, this,_1,_2,_3));
		outputFunctions().add("irr." + code,
				boost::bind(&LittleAgent::outIrr, this,_1,_2,_3));
		outputFunctions().add("newIrr." + code,
				boost::bind(&LittleAgent::outNewIrr, this,_1,_2,_3));

		/*********************      add predicates	 ********************/

		PlanFactory::t_map_paramPred map_paramPred =  pf.getMapParamPred();
		for(PlanFactory::const_it_paramPred it = map_paramPred.begin() ;
				it !=  map_paramPred.end();
				it++){

			std::string name_pred = it->first.first;
			std::string fun_pred = it->first.second;
			map< std::string , std::string > map_param = it->second;

			/* Soil */
			if(fun_pred=="p_bearingCapacity_sol"){
				double th = Utils::getInstance().toDouble(map_param.find("tolerance")->second);
				predicates().add(name_pred, boost::bind(&LittleAgent::p_bearingCapacitySol, this, th));
			}
			if(fun_pred=="p_waterDepletion_sol"){
				double wd = Utils::getInstance().toDouble(map_param.find("waterDepletion")->second);
				predicates().add(name_pred, boost::bind(&LittleAgent::p_waterDepletionSol, this, wd));
			}


			/* Crop*/
			if(fun_pred=="p_h2o_hrv"){
				double h2ohrv = Utils::getInstance().toDouble(map_param.find("h2oHrv")->second);
				predicates().add( name_pred, boost::bind(&LittleAgent::p_h2oHrv, this, h2ohrv));
			}
			if(fun_pred=="p_harvestedOrganeStage_crp"){
				std::string op = map_param.find("operator")->second;
				int stg = Utils::getInstance().toInt(map_param.find("harvestedOrganeStage")->second);
				predicates().add(name_pred,  boost::bind(&LittleAgent::p_harvestedOrganeStageCrp, this,stg, op));
			}

			if(fun_pred=="p_vegetativeStage_crp"){
				std::string op = map_param.find("operator")->second;
				int stg = Utils::getInstance().toInt(map_param.find("vegetativeStage")->second);
				predicates().add(name_pred,  boost::bind(&LittleAgent::p_vegetativeStageCrp, this,stg, op));
			}

			if(fun_pred=="p_nbleaf_crp"){
				int nb = Utils::getInstance().toInt(map_param.find("nbLeaf")->second);
				predicates().add(name_pred,  boost::bind(&LittleAgent::p_nbLeafCrp, this,nb));
			}
			if(fun_pred=="p_swfac_crp"){
				double nb = Utils::getInstance().toDouble(map_param.find("swfac")->second);
				predicates().add(name_pred,  boost::bind(&LittleAgent::p_swfacCrp, this,nb));
			}

			/* weather */
			if(fun_pred=="p_rain_wth"){
				int nDay = Utils::getInstance().toInt(map_param.find("nDay")->second);
				double LimRain = Utils::getInstance().toDouble(map_param.find("limRain")->second);
				predicates().add(name_pred, boost::bind(&Weather::p_rain, &mWeather, nDay, LimRain));
			}
			if(fun_pred=="p_tmn_sow"){
				predicates().add(name_pred, boost::bind(&LittleAgent::p_tmnSow, this));
			}

			/* Farmer activity */
			if(fun_pred=="p_at_date"){
				int date = 0;
				std::string dm;
				if (map_param.find("date")!= map_param.end()){
					dm = map_param.find("date")->second;
					date = Utils::getInstance().getDateJulianDayNumber(dm, _yr);
				}

				if (map_param.find("date+1")!= map_param.end()){
					dm = map_param.find("date+1")->second;
					date = Utils::getInstance().getDateJulianDayNumber(dm, _yr+1);
				}

				predicates().add(name_pred, boost::bind(&LittleAgent::p_atDate, this,date));
			}
		}


		/*********************      Build plan	 ********************/
		pf.buildItkPrecedences(mItk, code);

		std::string rl = pf.writeRules();
		std::string op = pf.writeActivities();
		std::string pr = pf.writePrecedences();

		std::string plan = "";
		plan.append(rl);
		plan.append(op);
		plan.append(pr);
		cout<<plan<<endl;
		/*********************     run plan	 ********************/
		KnowledgeBase::plan().fill(plan);

		return ve::Agent::init(time);
	}

	LittleAgent::~LittleAgent() {};



	/*********************     predicate     ********************/

	bool LittleAgent::p_bearingCapacitySol(double _th) const {
		double hucc =  mLandUnit.getpSoil().getAvgHucc30();
		double hur =  mLandUnit.getpSoil().getAvgHur30();
		
		double d  = mSoilType.getDhuccSow();
		if(hur > (d + _th) * hucc){
			return 0;
		} else {
			return 1;
		}
	}


	bool LittleAgent::p_waterDepletionSol(double _wd) const {
		double waterdepletion = mLandUnit.getpSoil().getWaterDepletion();
		double wd = _wd;
		if( wd > waterdepletion){
			return 0;
		} else {
			return 1;
		}
	}

	bool LittleAgent::p_harvestedOrganeStageCrp(int _stg, std::string _op) const {
		int stg =  (int) mLandUnit.getpLandCover().getHarvestedOrganeStage();
if(_op==">"){
			if(_stg > stg){
				return 0;
			} else {
				return 1;
			}
		}
	if(_op=="<"){
			if(_stg < stg){
				return 0;
			} else {
				return 1;
			}
		}
	if(_op==">="){
			if(_stg >= stg){
				return 0;
			} else {
				return 1;
			}
		}

	if(_op=="=="){
			if(stg == _stg){
				return 0;
			} else {
				return 1;
			}
		}
	}

	bool LittleAgent::p_vegetativeStageCrp(int _stg, std::string _op) const {
		int stg = (int) mLandUnit.getpLandCover().getVegetativeStage();
	
	if(_op==">"){
			if(_stg > stg){
				return 0;
			} else {
				return 1;
			}
		}
	if(_op=="<"){
			if(_stg < stg){
				return 0;
			} else {
				return 1;
			}
		}
	if(_op==">="){
			if(_stg >= stg){
				return 0;
			} else {
				return 1;
			}
		}

	if(_op=="=="){
			if(stg == _stg){
				return 0;
			} else {
				return 1;
			}
		}
}

	bool LittleAgent::p_nbLeafCrp(int _nb) const {
		int nb = (int) mLandUnit.getpLandCover().getNbLeaf();
		if(nb >= _nb){
			return 0;
		} else {
			return 1;
		}
	}

	bool LittleAgent::p_swfacCrp(double _nb) const {
		double nb = mLandUnit.getpLandCover().getSwfac();
		if(nb >= _nb){
			return 0;
		} else {
			return 1;
		}
	}

	bool LittleAgent::p_tmnSow() const {
		vector<double> tmoy=  mWeather.getLstTmoy(5);
		double d_tmn  = mCrop.getTmnSow();
		double min = d_tmn;
		for(unsigned int i=0; i<tmoy.size();i++){
			if (tmoy.at(i)<min) min = tmoy.at(i);
		}
		if(min < d_tmn){
			return 0;
		} else {
			return 1;
		}
	}

	bool LittleAgent::p_h2oHrv(double _h20_hrv) const {
		double d_H2OrecHrv;
		if( _h20_hrv == 0) {
			d_H2OrecHrv  = mCrop.getH2oHrv();
		}else{
			d_H2OrecHrv = _h20_hrv;
		}
		double H2Orec = mLandUnit.getpLandCover().getH2oHrv();
		cout<<H2Orec <<" -- "<<d_H2OrecHrv<<endl;
		if(H2Orec > d_H2OrecHrv ){
			return 0;
		} else {
			return 1;
		}
	}

	bool LittleAgent::p_atDate(int date) const {
		if(currentTime() < date ){
			return 0;
		} else {
			return 1;
		}
	}
	/*********************     out function     ********************/

	// SOWING
	void  LittleAgent::outSow(const std::string& name, const ve::Activity& activity, vd::ExternalEventList& out){
		vv::Set lulst;
		vector<int> lst_lu = getLstLu();
		for(unsigned int i=0; i<lst_lu.size();i++){
			lulst.add(vv::Integer(lst_lu.at(i)));
		}
		double density =Utils::getInstance().toDouble(pf.getEventValue(name, "density"));

		if (activity.isInStartedState()){
			vd::ExternalEvent* evt = new vd::ExternalEvent("actSow");
			evt->putAttribute("name", new vv::String(name));
			evt->putAttribute("activity", new vv::String(name));
			evt->putAttribute("densite", new vv::Double(density));
			evt->putAttribute("lstlu", new vv::Set(lulst));
			out.addEvent(evt);
			if (flag==false){sendAck(name, out);}
		}
	}

	void  LittleAgent::outSowFrtN(const std::string& name, const ve::Activity& activity, vd::ExternalEventList& out){
		if (activity.isInStartedState()){
			flag = true;
			outSow(name, activity, out);
			outFrtN(name, activity, out);
			sendAck(name, out);
		}
		flag= false;
	}

	// HARVESTING
	void  LittleAgent::outHrv(const std::string& name, const ve::Activity& activity, vd::ExternalEventList& out){
		vv::Set lulst;
		vector<int> lst_lu = getLstLu();
		for(unsigned int i=0; i<lst_lu.size();i++){
			lulst.add(vv::Integer(lst_lu.at(i)));
		}
		if (activity.isInStartedState()){
			vd::ExternalEvent* evt = new vd::ExternalEvent("actHrv");
			evt->putAttribute("name", new vv::String(name));
			evt->putAttribute("activity", new vv::String(name));
			evt->putAttribute("lstlu", new vv::Set(lulst));
			out.addEvent(evt);
			if (flag==false){sendAck(name, out);}
		}
	}

	// FERTILIZING
	void  LittleAgent::outFrtN(const std::string& name, const ve::Activity& activity, vd::ExternalEventList& out){
		vv::Set lulst;
		vector<int> lst_lu = getLstLu();
		for(unsigned int i=0; i<lst_lu.size();i++){
			lulst.add(vv::Integer(lst_lu.at(i)));
		}
		double fertN = Utils::getInstance().toDouble(pf.getEventValue(name, "N"));

		if (activity.isInStartedState()){
			vd::ExternalEvent* evt = new vd::ExternalEvent("actFrtN");
			evt->putAttribute("name", new vv::String(name));
			evt->putAttribute("activity", new vv::String(name));
			evt->putAttribute("dose", new vv::Double(fertN));
			evt->putAttribute("lstlu", new vv::Set(lulst));
			out.addEvent(evt);
			if (flag==false){sendAck(name, out);}
		}
	}

	// IRRIGATING
	void  LittleAgent::outIrr(const std::string& name, const ve::Activity& activity, vd::ExternalEventList& out){
		vv::Set lulst;
		vector<int> lst_lu = getLstLu();
		for(unsigned int i=0; i<lst_lu.size();i++){
			lulst.add(vv::Integer(lst_lu.at(i)));
		}
		double irr = Utils::getInstance().toDouble(pf.getEventValue(name, "h2o"));

		if (activity.isInStartedState()){
			vd::ExternalEvent* evt = new vd::ExternalEvent("actIrr");
			evt->putAttribute("name", new vv::String(name));
			evt->putAttribute("activity", new vv::String(name));
			evt->putAttribute("dose", new vv::Double(irr));
			evt->putAttribute("lstlu", new vv::Set(lulst));
			out.addEvent(evt);
			if(nbIrr < 2) newIrrName = name;
			nbIrr +=1;
			if (flag==false){sendAck(name, out);}
		}
	}

	void  LittleAgent::outNewIrr(const std::string& name, const ve::Activity& activity, vd::ExternalEventList& out){
		// run activity
		outIrr(name, activity, out);

		if (activity.isInStartedState()){
			std::string _newname = newIrrName;
			_newname.append(".");
			_newname.append(lexical_cast<string>(nbIrr));
			pf.duplicateOperation(name, _newname );
			std::string flx;
			flx.append(pf.writeOneActivity(_newname));
			flx.append(pf.writeOnePrecedence(_newname));
			KnowledgeBase::plan().fill(flx);
		}
	}



	/*********************     utils decision     ********************/
	void LittleAgent::sendAck(std::string _nm, vd::ExternalEventList& out) {
		vd::ExternalEvent* order = new vd::ExternalEvent("ack");
		order->putAttribute("name", new vv::String(_nm));  // accusé de reception
		order->putAttribute("value", new vv::String("done"));
		out.addEvent(order);
	}

	void LittleAgent::removeActivity(const std::string& name)
	{
		if (activities().exist(name)) {
			const ve::Activity& a = activities().get(name)->second;
			if (!a.isInDoneState()) {
				std::cout << " removeActivity,setActivityFailed : " << name <<std::endl;
				setActivityFailed(name, currentTime());
			}
		}
	}

	/*********************     param function     ********************/

	/* Density of the vairiety*/

	vector<int> LittleAgent::getLstLu(){
		vector<int> lst;
		lst.push_back(1);
		return lst;
	}


	std::string LittleAgent::getCode(std::string name){
		vector<std::string> vec = Utils::getInstance().getVectString(name, ".");
		std::string code;
		code.append(".");
		code.append(vec.at(vec.size()-3));
		code.append(".");
		code.append(vec.at(vec.size()-2));
		code.append(".");
		code.append(vec.at(vec.size()-1));
		return code;
	}

	std::string LittleAgent::setCode(int _id_itk, int _id_pu, int _yr){
		std::string code;
		code.append(lexical_cast<string>(_id_itk));
		code.append(".");
		code.append(lexical_cast<string>(_id_pu));
		code.append(".");
		code.append(lexical_cast<string>(_yr));
		return code;
	}

	/*********************     init     ********************/


	void LittleAgent::initLandUnit(const vd::InitEventList & evts){
		std::string code_soil = vv::toString(evts.get("code_soiltype"));
		std::string  The_geom = "POLYGON((1 0,1 100,101 100,101 0,1 0))"; // 1 ha
		WKTReader wktr;
		geom::Geometry* mPolygon = wktr.read(The_geom);
		LandUnit lu(1,1,1, mPolygon, code_soil);
		mLandUnit = lu;
	}

	void LittleAgent::initCrop(const vd::InitEventList& evts){
		CrashDBS cdb(evts);
		std::string sql="SELECT * from expertknowledge.crop WHERE code='";
		sql.append(vv::toString(evts.get("code_crop")));
		sql.append("'");
		PGresult* db_Crop= cdb.QueryDB(sql.c_str());
		if (PQntuples(db_Crop)==0) {
			throw std::runtime_error("LittleAgent:: Crop not found in the DB");
		}
		int i=0;
		int idDB = lexical_cast<int>(PQgetvalue(db_Crop, i, PQfnumber(db_Crop, "id")));
		string name = lexical_cast<string>(PQgetvalue(db_Crop, i, PQfnumber(db_Crop, "name")));
		string code = lexical_cast<string>(PQgetvalue(db_Crop, i, PQfnumber(db_Crop, "code")));
		string type = lexical_cast<string>(PQgetvalue(db_Crop, i, PQfnumber(db_Crop, "type")));
		double d_rt = lexical_cast<double>(PQgetvalue(db_Crop, i, PQfnumber(db_Crop, "rt_rot")));
		double d_tminsow = lexical_cast<double>(PQgetvalue(db_Crop, i, PQfnumber(db_Crop, "tmin_sow")));
		double d_h2orec = lexical_cast<double>(PQgetvalue(db_Crop, i, PQfnumber(db_Crop, "h2o_hrv")));
		double d_swfac = lexical_cast<double>(PQgetvalue(db_Crop, i, PQfnumber(db_Crop, "swfac_irr")));
		double d_inn = lexical_cast<double>(PQgetvalue(db_Crop, i, PQfnumber(db_Crop, "inn_frt")));
		
		Crop c(i+1, idDB, code, type, d_rt, d_tminsow, d_h2orec, d_inn, d_swfac);

		// Crop variety
		sql = "SELECT var.* FROM expertknowledge.cropvariety var, expertknowledge.crop crp WHERE var.code_crop = crp.code AND crp.code='";
		sql.append(vv::toString(evts.get("code_crop")));
		sql.append("' AND var.code ='");
		sql.append(vv::toString(evts.get("code_variety")));
		sql.append("'");
		PGresult* db_var= cdb.QueryDB(sql.c_str());
		if (PQntuples(db_var)==0) {
			throw std::runtime_error("LittleAgent:: Variety not found in the DB");
		}
		int id_var = lexical_cast<int>(PQgetvalue(db_Crop, i, PQfnumber(db_var, "id")));
		std::string code_var = lexical_cast<string>(PQgetvalue(db_var, i, PQfnumber(db_var, "code")));
		std::string st_sow = lexical_cast<string>(PQgetvalue(db_var, i, PQfnumber(db_var, "st_sow")));
		std::string nd_sow = lexical_cast<string>(PQgetvalue(db_var, i, PQfnumber(db_var, "nd_sow")));
		std::string st_hr = lexical_cast<string>(PQgetvalue(db_var, i, PQfnumber(db_var, "st_hrv")));
		std::string nd_hr = lexical_cast<string>(PQgetvalue(db_var, i, PQfnumber(db_var, "nd_hrv")));
		double density = lexical_cast<double>(PQgetvalue(db_var, i, PQfnumber(db_var, "density")));

		c.addCropVariety(code_var, id_var,st_sow, nd_sow, st_hr, nd_hr, density);
		mCrop =  c;


		mVariety=code_var;
		mDensity = density;
	}

	void LittleAgent::initSoilType(const vd::InitEventList& evts){
		CrashDBS cdb(evts);
		std::string sql="SELECT * from expertknowledge.soiltype WHERE code_soiltype='";
		sql.append(vv::toString(evts.get("code_soiltype")));
		sql.append("'");
		PGresult* dbType= cdb.QueryDB(sql.c_str());
		int i=0;
		if (PQntuples(dbType)==0) {
			throw std::runtime_error("LittleAgent:: SoilType not found in the DB");
		}
		int idDB = lexical_cast<int>(PQgetvalue(dbType, i, PQfnumber(dbType, "id_soiltype")));
		string name = lexical_cast<string>(PQgetvalue(dbType, i, PQfnumber(dbType, "name_soiltype")));
		double clay = lexical_cast<double>(PQgetvalue(dbType, i, PQfnumber(dbType, "clay")));
		double silt = lexical_cast<double>(PQgetvalue(dbType, i, PQfnumber(dbType, "silt")));
		double sand = lexical_cast<double>(PQgetvalue(dbType, i, PQfnumber(dbType, "sand")));
		double huccsow = lexical_cast<double>(PQgetvalue(dbType, i, PQfnumber(dbType, "d_hucc_sow")));
		SoilType st(i+1, idDB, name, clay, silt, sand, huccsow);
		mSoilType =  st;
	}

	void LittleAgent::initITK(const vd::InitEventList & evts) {
		// ITK
		CrashDBS cdb(evts);
		std::string sql = "SELECT itk. *, crop.type as type_crop from proceduralknowledge.itk, expertknowledge.crop WHERE itk.code_crop = crop.code AND itk.code ='";
		sql.append(vv::toString(evts.get("code_itk")));
		sql.append("'");
		PGresult* db_itk = cdb.QueryDB(sql.c_str());

		if (PQntuples(db_itk)==0) {
			throw std::runtime_error("LittleAgent:: ITK not found in the DB");
		}
		int i=0;
		int id_itk = lexical_cast<int>(PQgetvalue(db_itk, i, PQfnumber(db_itk, "id")));
		std::string code_crop = lexical_cast<string>(PQgetvalue(db_itk, i, PQfnumber(db_itk, "code_crop")));
		std::string type_crop = lexical_cast<string>(PQgetvalue(db_itk, i, PQfnumber(db_itk, "type_crop")));
		std::string pre = lexical_cast<string>(PQgetvalue(db_itk, i, PQfnumber(db_itk, "code_prec")));
		std::string suc = lexical_cast<string>(PQgetvalue(db_itk, i, PQfnumber(db_itk, "code_suc")));
		std::string d_variety = lexical_cast<string>(PQgetvalue(db_itk, i, PQfnumber(db_itk, "domain_variety")));
		std::string s_variety = lexical_cast<string>(PQgetvalue(db_itk, i, PQfnumber(db_itk, "domain_soiltype")));
		vector<string> vect_ds = Utils::getInstance().getVectString(s_variety, ",");

		// variety
		sql = "SELECT foo.id_itk, var.*FROM (SELECT i.id as id_itk, i.code_crop, cast(regexp_split_to_table(i.domain_variety, E',') as varchar) as code_var FROM proceduralknowledge.itk i WHERE i.id =";
		sql.append(lexical_cast<string>(id_itk));
		sql.append(") AS foo, expertknowledge.cropvariety var, expertknowledge.crop cp WHERE foo.code_var = var.code");
		PGresult* db_itk_var = cdb.QueryDB(sql.c_str());

		Itk::t_map_variety map_var;
		for(int j=0; j < PQntuples(db_itk_var) ; j++){
			Itk::t_variety var;
			var.id_variety = lexical_cast<int>(PQgetvalue(db_itk_var, i, PQfnumber(db_itk_var, "id")));
			var.density = lexical_cast<double>(PQgetvalue(db_itk_var, i, PQfnumber(db_itk_var, "density")));
			std::string code_var = lexical_cast<string>(PQgetvalue(db_itk_var, i, PQfnumber(db_itk_var, "code")));
			var.st_sow = lexical_cast<string>(PQgetvalue(db_itk_var, i, PQfnumber(db_itk_var, "st_sow")));
			var.nd_sow = lexical_cast<string>(PQgetvalue(db_itk_var, i, PQfnumber(db_itk_var, "nd_sow")));
			var.st_hr = lexical_cast<string>(PQgetvalue(db_itk_var, i, PQfnumber(db_itk_var, "st_hrv")));
			var.nd_hr = lexical_cast<string>(PQgetvalue(db_itk_var, i, PQfnumber(db_itk_var, "nd_hrv")));
			map_var.insert(pair<std::string , Itk::t_variety>( code_var, var));
		}
		Itk itk(id_itk, code_crop, type_crop, pre, suc, map_var, vect_ds);

		// CropOperation
		sql = "SELECT cop.id, cop.name, cop.type, cop.equipment, cop.event, cop.st, cop.nd, ev.crash_ev FROM (SELECT itk.id as id_itk , cast(regexp_split_to_table(itk.lst_cop, E',') as integer) as ref_cop  FROM proceduralknowledge.itk WHERE itk.id =";
		sql.append(lexical_cast<string>(id_itk));
		sql.append( ") AS foo, proceduralknowledge.cropoperation cop, proceduralknowledge.event ev WHERE ref_cop = cop.id AND ev.id=event");
		PGresult* db_op = cdb.QueryDB(sql.c_str());

		Itk::t_lst_op tmp_op;
		for(int j=0; j < PQntuples(db_op) ; j++) {
			int id = lexical_cast<int>(PQgetvalue(db_op, j, PQfnumber(db_op, "id")));
			std::string name = lexical_cast<string>(PQgetvalue(db_op, j, PQfnumber(db_op, "name")));
			std::string type = lexical_cast<string>(PQgetvalue(db_op, j, PQfnumber(db_op, "type")));
			std::string equ = lexical_cast<string>(PQgetvalue(db_op, j, PQfnumber(db_op, "equipment")));
			std::string st = lexical_cast<string>(PQgetvalue(db_op, j, PQfnumber(db_op, "st")));
			std::string nd = lexical_cast<string>(PQgetvalue(db_op, j, PQfnumber(db_op, "nd")));
			std::string crash_ev = lexical_cast<string>(PQgetvalue(db_op, j, PQfnumber(db_op, "crash_ev")));

			CropOperation cop(id, name, type, equ, st, nd, crash_ev, evts);
			itk.addCropOperation(cop);

		}
		mItk = itk;
	}



}  // namespace crash
DECLARE_DYNAMICS(crash::LittleAgent);

