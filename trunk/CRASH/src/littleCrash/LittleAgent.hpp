/**
 * @file
 * @author RECORD Team,
 *   http://record.toulouse.inra.fr ,
 *   Applied Mathematics and Informatics Research Division &
 *   Environment and Agronomy Research Division,
 *   French National Institute for Agricultural Research
 * @version $Id: $
 */

/*
 * Copyright (C) 2010 - INRA
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
#ifndef LITTLEAGENT_H
#define LITTLEAGENT_H

#include <stdio.h>
#include <string.h>
#include <algorithm>

#include <geos.h>
#include <boost/lexical_cast.hpp>
#include <boost/algorithm/string.hpp>

//Crash utils
#include <utils/CrashDBS.hpp>
#include <utils/Utils.hpp>
#include <utils/PlanFactory.hpp>

// Crash system knowledge
#include <belief/structuralknowledge/systemknowledge/farmland/LandUnit.hpp>
#include <belief/structuralknowledge/systemknowledge/offfarm/Weather.hpp>
// Crash expert knowledge
#include <belief/structuralknowledge/expertknowledge/Crop.hpp>
#include <belief/structuralknowledge/expertknowledge/SoilType.hpp>
// Crash Procedural knowledge
#include <belief/proceduralknowledge/Itk.hpp>

// namespace
using namespace geos;
using namespace std;
using boost::lexical_cast;

namespace ve = vle::extension::decision;
namespace vd = vle::devs;
namespace vu = vle::utils;
namespace vv = vle::value;


namespace crash {
	/**
	 * @brief LittleAgent: Decision Agent.
	 */

	class LittleAgent: public ve::Agent
	{
		public:
			typedef vector< std::string > t_lst_string;

			/*brief Default constructor */
			LittleAgent(const vd::DynamicsInit& mdl, const vd::InitEventList& evts);


			/** @brief Default destructor */
			virtual ~LittleAgent();

			/** @brief Init */
			vd::Time init(const vd::Time& time);

			void initLandUnit(const vd::InitEventList & evts);
			void initCrop(const vd::InitEventList& evts);
			void initSoilType(const vd::InitEventList& evts);
			void initITK(const vd::InitEventList& evts);

			void outSow(const std::string& name, const ve::Activity& activity, vd::ExternalEventList& out);
			void outHrv(const std::string& name, const ve::Activity& activity, vd::ExternalEventList& out);
			void outFrtN(const std::string& name, const ve::Activity& activity, vd::ExternalEventList& out);
			void outSowFrtN(const std::string& name, const ve::Activity& activity, vd::ExternalEventList& out);
			void outIrr(const std::string& name, const ve::Activity& activity, vd::ExternalEventList& out);
			void outNewIrr(const std::string& name, const ve::Activity& activity, vd::ExternalEventList& out);

			void removeActivity(const std::string& name);
			void sendAck(std::string _nm, vd::ExternalEventList& out );

			bool p_bearingCapacitySol(double _th) const; // bearing capacity
			bool p_tmnSow() const;
			bool p_h2oHrv(double _h2o_hrv) const; // grain water content
			bool p_vegetativeStageCrp(int _stg, std::string _op) const;
			bool p_nbLeafCrp(int _nb) const;
			bool p_harvestedOrganeStageCrp(int _stg, std::string _op) const;
			bool p_waterDepletionSol(double _wd) const;
			bool p_atDate(int date) const;
			bool p_swfacCrp(double _nb) const;

			vector<int> getLstLu();
			std::string getCode(std::string name);
			std::string setCode(int _id_itk, int _id_pu, int _yr);

			/** @brief expert knowledge Crop  */
			Crop mCrop;
			std::string mVariety;
			double mDensity;

			/** @brief expert knowledge SoilType  */
			SoilType mSoilType;

			/** @brief Weather knowledge   */
			Weather mWeather;

			/** @brief expert knowledge LandUnit  */
			LandUnit mLandUnit;

			/** @brief procedural knowledge: ItkCrop  */
			Itk mItk;

			PlanFactory pf;


			/* Plot variable*/
			int id;
			std::string newIrrName;
			int nbIrr;
			bool flag;
	};
}  // namespace crash
#endif //LITTLEAGENT_H
