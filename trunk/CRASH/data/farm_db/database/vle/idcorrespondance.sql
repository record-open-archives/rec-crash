-------------------------------------------------
-- Authors: Jérôme Dury
-- Date: 2011-02-04

--               idcorrespondance


-- Comments: thes table are used to keep track of id between crash-vle and the database
-------------------------------------------------

-- Table: cropid
DROP TABLE IF EXISTS  vle.cropid CASCADE;

CREATE TABLE vle.cropid
(
  id_db int,
  id_vle int
);

