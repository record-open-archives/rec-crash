-------------------------------------------------
-- Authors: Pierre Casel & Jérôme Dury
-- Date:

--                soiltype.sql


-- Comments:
-------------------------------------------------

DELETE FROM storage.soiltype;

-- Ajout de la culture
--(id_soiltype, name_soiltype, code_soiltype, th_sawcforsowing)

INSERT INTO storage.soiltype VALUES (1, 'S7','S7',13,23,64, 0.95);
INSERT INTO storage.soiltype VALUES (2, 'S9','S9',30,23,58, 0.95);
INSERT INTO storage.soiltype VALUES (3, 'TPAA','TPAA',16,23,61, 0.95);




