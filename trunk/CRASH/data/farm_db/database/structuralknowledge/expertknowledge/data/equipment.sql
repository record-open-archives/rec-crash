--
-- PostgreSQL database dump
--
SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = off;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET escape_string_warning = off;
SET search_path = storage, pg_catalog;
SET default_tablespace = '';
SET default_with_oids = false;



COPY equipment (id_equipment, name_equipment, code_equipment, type_soil, power_equipment, work_rate, cost_traction, cost_tool, fuel_requirement, description) FROM stdin;
1	combine_drill_MA	combine_drill_MA	hard	130_150	0.9	31	19.5	14.5	semoir herse rotative en combiné
2	combine_drill_MA	combine_drill_MA	easy	130_150	0.7	31	19.5	11.5	semoir herse rotative en combiné
3	3_furrow_plough.	3_furrow_plough.	hard	85_95 	2.5	19.7	18	31	charrue  3 corps. profondeur de travail 25-30 cm (réversible. portée)
4	3_furrow_plough.	3_furrow_plough.	easy	85_95 	2.17	19.7	18	23	charrue  3 corps. profondeur de travail 25-30 cm (réversible. portée)
5	4_5_furrow_plough.	4_5_furrow_plough.	hard	135	1.1	18.9	18.9	22	charrue 4 à 5 corps   (portée  14 ou 16 pouces) 
6	4_5_furrow_plough.	4_5_furrow_plough.	easy	135	0.9	18.9	18.9	14	charrue 4 à 5 corps   (portée  14 ou 16 pouces)
7	6_9_furrow_plough.	6_9_furrow_plough.	hard	170_190 	1.4	45.1	19.1	27	charrue 6 à 9 corps    (portée ou traînée – 14. 16 pouces. varilarge) 
8	6_9_furrow_plough.	6_9_furrow_plough.	easy	170_190 	0.6	45.1	19.1	18	charrue 6 à 9 corps    (portée ou traînée – 14. 16 pouces. varilarge) 
9	12m_sprayer	12m_sprayer		120_140	0.2	29	2	1.5	épandeur engrais DP 12 m
10	decompactor	decompactor	hard	160_185	1	41.7	13.8	20	ameublisseur - décompacteur. profondeur de travail 25-30 cm (3 m. 5-6 dents)
11	decompactor	decompactor	easy	160_185	0.7	41.7	13.8	14	ameublisseur - décompacteur. profondeur de travail 25-30 cm (3 m. 5-6 dents) 
12	boom_sprayer	boom_sprayer		80_120	0.1	22.6	4.4	2	pulvérisateur   (24 - 30 m. traîné. 3400 litres)
13	boom_sprayer	boom_sprayer		80_120	0.1	22.6	4.4	2	pulvérisateur   (24 - 30 m. traîné. 3400 litres)
14	combine_harvester_MA	combine_harvester_MA	hard	290_300	0.6	0	63	32	moissonneuse-batteuse – maïs grain     (barre de coupe : 6 à 8 cueilleurs) 
15	combine_harvester_MA	combine_harvester_MA	easy	290_300	0.4	0	63	22	moissonneuse-batteuse – maïs grain     (barre de coupe : 6 à 8 cueilleurs) 
16	hoeing_machine_6_row	hoeing_machine_6_row		100_120	0.5	24.2	4.7	2.55	bineuse 6 rangs (autoguidée)
\.
