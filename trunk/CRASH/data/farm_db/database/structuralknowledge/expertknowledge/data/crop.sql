--
-- PostgreSQL database dump
--
SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = off;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET escape_string_warning = off;
SET search_path = storage, pg_catalog;
SET default_tablespace = '';
SET default_with_oids = false;


COPY crop(id, name, code, type, rt_rot, tmin_sow, swfac_irr, h2o_hrv, inn_frt) FROM stdin;
1	winter wheat	BH	Winter	2	0	99	99	99
2	spring barley	OP	Summer	3	99	99	99	99
3	winter rape	CH	Winter	4	6	99	99	99
4	spring pea	PP	Summer	6	99	99	99	99
5	maize	MA	Summer	1	6	0.20	0.333	99
6	sunflower	TO	Summer	2	6	99	99	99
7	durum wheat	BA	Winter	2	0	99	99	99
8	soybean	SO	Summer	2	5	99	99	99
\.

COPY cropvariety(id, code_crop, code, st_sow, nd_sow, st_hrv, nd_hrv, density) FROM stdin;
1	CH	conventional	01/01	01/01	01/01	01/01	99
2	BH	conventional	01/10	01/01	01/01	01/01	99
3	BA	conventional	01/01	01/01	01/01	01/01	99
4	PP	conventional	01/01	01/01	01/01	01/01	99
5	MA	hybrid_p	15/03	01/05	01/08	01/10	9.25
6	TO	conventional	01/10	01/01	01/01	01/01	99
\.

COPY cropsuccession(code_crop, code_preceeding, kp) FROM stdin;
BH	BH	1
BH	OP	4
BH	CH	5
BH	PP	6
BH	MA	4
OP	BH	3
OP	OP	1
OP	CH	5
OP	PP	4
OP	MA	4
CH	BH	5
CH	OP	5
CH	CH	1
CH	PP	5
CH	MA	4
PP	BH	5
PP	OP	5
PP	CH	1
PP	PP	1
PP	MA	5
MA	BH	5
MA	OP	5
MA	CH	5
MA	PP	6
MA	MA	2
\.
