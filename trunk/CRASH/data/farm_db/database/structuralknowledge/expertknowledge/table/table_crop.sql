-------------------------------------------------
-- Authors: Jérôme Dury
-- Date:

--                table_crop.sql


-- Comments:
-------------------------------------------------


-- Table: crop

DROP TABLE IF EXISTS  storage.crop CASCADE;
CREATE TABLE storage.crop
(
    id int,
    name character varying,
    code character varying UNIQUE PRIMARY KEY,
    type character varying,
    rt_rot numeric,
    tmin_sow numeric,
    swfac_irr numeric,
    h2o_hrv numeric,
    inn_frt numeric
);

DROP TABLE IF EXISTS  expertknowledge.crop CASCADE;
CREATE TABLE expertknowledge.crop AS TABLE storage.crop;
    ALTER TABLE expertknowledge.crop ALTER COLUMN id SET NOT NULL;
    ALTER TABLE expertknowledge.crop ADD PRIMARY KEY (code);

-- Table: variety
DROP TABLE IF EXISTS  storage.cropvariety CASCADE;
CREATE TABLE storage.cropvariety
(
  id serial NOT NULL,
  code_crop varchar REFERENCES storage.crop,
  code character varying,
  st_sow varchar,
  nd_sow varchar,
  st_hrv varchar,
  nd_hrv varchar,
  density numeric,
  PRIMARY KEY (id, code_crop)
);

DROP TABLE IF EXISTS  expertknowledge.cropvariety CASCADE;
CREATE TABLE expertknowledge.cropvariety AS TABLE storage.cropvariety;
    ALTER TABLE expertknowledge.cropvariety ALTER COLUMN id SET NOT NULL;
    ALTER TABLE expertknowledge.cropvariety ADD FOREIGN KEY (code_crop) REFERENCES storage.crop;
    ALTER TABLE expertknowledge.cropvariety ADD PRIMARY KEY (id, code_crop);

-- Table: cropsuccession
DROP TABLE IF EXISTS  storage.cropsuccession CASCADE;
CREATE TABLE storage.cropsuccession
(
  code_crop varchar REFERENCES storage.crop,
  code_preceeding varchar REFERENCES storage.crop,
  kp numeric,
  PRIMARY KEY (code_crop, code_preceeding)
);

DROP TABLE IF EXISTS  expertknowledge.cropsuccession CASCADE;
CREATE TABLE expertknowledge.cropsuccession AS TABLE storage.cropsuccession;
    ALTER TABLE expertknowledge.cropsuccession ADD FOREIGN KEY (code_crop) REFERENCES storage.crop;
    ALTER TABLE expertknowledge.cropsuccession ADD FOREIGN KEY (code_preceeding) REFERENCES storage.crop;
    ALTER TABLE expertknowledge.cropsuccession ADD PRIMARY KEY (code_crop, code_preceeding);

-- Table: cropproduction
DROP TABLE IF EXISTS  expertknowledge.cropproduction CASCADE;
CREATE TABLE expertknowledge.cropproduction
(
  sim int PRIMARY KEY,
  code_soiltype varchar,
  code_crop varchar,
  ref_variety int,
  ref_itk int,
  yd numeric,
  wi numeric
  );


