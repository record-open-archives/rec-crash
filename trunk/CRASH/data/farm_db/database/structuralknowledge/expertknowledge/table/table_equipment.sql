
-------------------------------------------------
-- Authors: Stéphanie Chanfreau
-- Date: 22/06/11

--                table_equipment.sql


-- Comments:
-------------------------------------------------


-- Table: equipment

DROP TABLE IF EXISTS  storage.equipment CASCADE;

CREATE TABLE storage.equipment
(
  id_equipment serial NOT NULL UNIQUE PRIMARY KEY,
  name_equipment character varying,
  code_equipment character varying,
  type_soil character varying,
  power_equipment character varying, --unit = cv, it is an interval of power
  work_rate numeric, -- hour/ha
  cost_traction numeric, --unit= euros/hour
  cost_tool numeric, -- unit = euros/ha
  fuel_requirement numeric,-- unit = l/ha
  description character varying --details in french about the equipment
);

DROP TABLE IF EXISTS  expertknowledge.equipment CASCADE;
CREATE TABLE expertknowledge.equipment AS TABLE storage.equipment;
    
