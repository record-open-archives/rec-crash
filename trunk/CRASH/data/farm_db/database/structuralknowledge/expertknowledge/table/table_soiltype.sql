-------------------------------------------------
-- Authors: Jérôme Dury
-- Date:

--                table_soiltype.sql


-- Comments:
-------------------------------------------------


-- Table: soiltype

DROP TABLE IF EXISTS  storage.soiltype CASCADE;

CREATE TABLE storage.soiltype
(
  id_soiltype serial NOT NULL UNIQUE PRIMARY KEY,
  name_soiltype character varying,
  code_soiltype character varying,
  clay numeric,
  silt numeric,
  sand numeric,
  d_hucc_sow numeric
);

DROP TABLE IF EXISTS  expertknowledge.soiltype CASCADE;
CREATE TABLE expertknowledge.soiltype AS TABLE storage.soiltype;
    ALTER TABLE expertknowledge.soiltype ALTER COLUMN id_soiltype SET NOT NULL;
    ALTER TABLE expertknowledge.soiltype ADD PRIMARY KEY (id_soiltype);


