--------------------------------------------------------------------------------------------------------------------------------------------------------
--                                perso-rotate( ShapeToRotate geometry,  OriginePoint geometry, angle double precision)
--------------------------------------------------------------------------------------------------------------------------------------------------------

--perso_rotate requires two pieces of information: (1) the origin or coordinate of the axis of rotation; and (2) a rotation angle. ST_Rotate (et al.) only offer the rotation angle. 

CREATE OR REPLACE FUNCTION systemknowledge.perso_rotate(geometry, geometry, double precision)
   RETURNS geometry AS
'SELECT affine($1, cos($3), -sin($3), 0, sin($3), cos($3), 0, 0, 0, 1,
st_x($2)-cos($3)*st_x($2)+sin($3)*st_y($2),
st_y($2)-sin($3)*st_x($2)-cos($3)*st_y($2), 0)' LANGUAGE 'sql' IMMUTABLE
STRICT COST 100; 


--------------------------------------------------------------------------------------------------------------------------------------------------------
--                                merge_small
--------------------------------------------------------------------------------------------------------------------------------------------------------
-- DROP IF EXISTS FUNCTION systemknowledge.merge_small();

-- CREATE OR REPLACE FUNCTION systemknowledge.merge_small()
--   RETURNS character AS
-- $BODY$
--  DECLARE
--  rec_p record; 
--  curs1 refcursor;
	
--  BEGIN

-- DROP TABLE IF EXISTS systemknowledge.temp_grid;
-- CREATE TABLE systemknowledge.temp_grid AS (SELECT * FROM systemknowledge.gridunit);

-- ----------------------------------------------
-- -- Start loop on polygon
-- 	OPEN curs1 FOR SELECT * FROM systemknowledge.temp_gridunit;
-- 	fetch curs1 into rec_p;	
	
-- 	WHILE (FOUND) LOOP -- boucle pour chaque gridunit

-- ----------------------------------------------	
-- SELECT part_1.gid as gid , part_2.gid as gid2, length(ST_Intersection(part_1.the_geom, part_2.the_geom)) as len
-- 	  FROM systemknowledge.gridunit AS part_1,
-- 	       systemknowledge.gridunit AS part_2
-- 	 WHERE part_1.name <> part_2.name 
-- 	   AND ST_Intersects(part_1.the_geom, part_2.the_geom)
-- 	   AND part_1.gid=42
-- ----------------------------------------------
	
		
-- 	fetch curs1 into rec_p;	
-- 	END LOOP;
-- 	CLOSE curs1;

-- ----------------------------------------------

-- RETURN('yeep');

-- END;
-- $BODY$
--   LANGUAGE plpgsql VOLATILE
--   COST 100;



--------------------------------------------------------------------------------------------------------------------------------------------------------
--                               create - grid
--------------------------------------------------------------------------------------------------------------------------------------------------------

CREATE OR REPLACE FUNCTION systemknowledge.create_grid(longu integer, larg integer)
  RETURNS character AS
$BODY$
 DECLARE
 rec_p record; 
 curs1 refcursor;
	
 BEGIN

DROP TABLE IF EXISTS systemknowledge.gridunit CASCADE;
	CREATE TABLE systemknowledge.gridunit
	(
	  gid serial NOT NULL,
	  the_geom geometry
	)
	WITH (
	  OIDS=FALSE
	);
	INSERT INTO systemknowledge.gridunit(the_geom) (SELECT the_geom FROM systemknowledge.plotunit);

-- box for each polygon -> the box is a square and the side equal to the diag of the bounding box
DROP TABLE IF EXISTS systemknowledge.temp_polycell CASCADE;
CREATE TABLE systemknowledge.temp_polycell AS 
(SELECT gid, 	
	round(cast(ST_Distance(ST_Point(ST_xmin(ST_Extent(the_geom)),ST_ymin(ST_Extent(the_geom))),
				ST_Point(ST_xmax(ST_Extent(the_geom)) ,ST_ymax(ST_Extent(the_geom)))) as numeric),0) As diag,
	round(cast((ST_X(Centroid(the_geom)) + (ST_Distance(ST_Point(ST_xmin(ST_Extent(the_geom)),ST_ymin(ST_Extent(the_geom))),
				ST_Point(ST_xmax(ST_Extent(the_geom)) ,ST_ymax(ST_Extent(the_geom))))) ) as numeric),0)as xmaxbox,
	round(cast((ST_X(Centroid(the_geom)) - (ST_Distance(ST_Point(ST_xmin(ST_Extent(the_geom)),ST_ymin(ST_Extent(the_geom))),
				ST_Point(ST_xmax(ST_Extent(the_geom)) ,ST_ymax(ST_Extent(the_geom))))) )as numeric),0)as xminbox,
	round(cast((ST_Y(Centroid(the_geom)) + (ST_Distance(ST_Point(ST_xmin(ST_Extent(the_geom)),ST_ymin(ST_Extent(the_geom))),
				ST_Point(ST_xmax(ST_Extent(the_geom)) ,ST_ymax(ST_Extent(the_geom))))) )as numeric),0)as ymaxbox,
	round(cast((ST_y(Centroid(the_geom)) - (ST_Distance(ST_Point(ST_xmin(ST_Extent(the_geom)),ST_ymin(ST_Extent(the_geom))),
				ST_Point(ST_xmax(ST_Extent(the_geom)) ,ST_ymax(ST_Extent(the_geom))))) )as numeric),0)as yminbox
FROM systemknowledge.gridunit
GROUP BY gid, the_geom ORDER BY gid);


-- create each cell to copy for all polygons
ALTER TABLE systemknowledge.temp_polycell ADD COLUMN cell_geom  geometry ;
UPDATE systemknowledge.temp_polycell SET cell_geom = 
(SELECT  ST_PolygonFromText('POLYGON((	'||xminbox||' '||yminbox||', 	
					'||xminbox+ longu||' '||yminbox||', 
					'||xminbox+ longu||' '||yminbox+ larg||', 
					'||xminbox||' '||yminbox+ larg||', 
					'||xminbox||' '||yminbox||'))')) ;

ALTER TABLE systemknowledge.temp_polycell ADD COLUMN poly_geom  geometry ;
UPDATE systemknowledge.temp_polycell SET poly_geom = gridunit.the_geom 
				FROM  systemknowledge.gridunit 
				WHERE gridunit.gid=temp_polycell.gid;
-- pick up working direction line
ALTER TABLE systemknowledge.temp_polycell ADD COLUMN line geometry;
UPDATE systemknowledge.temp_polycell SET line = workorientation.the_geom 
						FROM systemknowledge.workorientation 
						WHERE intersects(workorientation.the_geom, temp_polycell.poly_geom);
-- Calculate azimut
ALTER TABLE systemknowledge.temp_polycell ADD COLUMN az numeric;
UPDATE systemknowledge.temp_polycell SET az = 90 - degrees(azimuth (ST_StartPoint(line), ST_EndPoint(line))); -- Azimut

-- ALTER TABLE systemknowledge.temp_polycell ADD COLUMN azi numeric;
-- UPDATE systemknowledge.tqgis tetisemp_polycell SET azi = az / ; -- Azimut
-- UPDATE systemknowledge.temp_polycell SET azi = az /(2*pi())*360  WHERE az > pi()/2 AND az < pi() OR az > 3 * pi()/2 ; -- Azimut


----------------------------------------------
-- Start loop on polygon
	OPEN curs1 FOR SELECT * FROM systemknowledge.temp_polycell;
	fetch curs1 into rec_p;	
	
	WHILE (FOUND) LOOP -- boucle pour chaque ligne de découpe

		-- create grid on one polygon 
		DROP TABLE IF EXISTS systemknowledge.throwaway_grid CASCADE;
		CREATE TABLE systemknowledge.throwaway_grid(gid SERIAL PRIMARY KEY, the_geom geometry);
		INSERT INTO systemknowledge.throwaway_grid(the_geom)
		SELECT st_translate(rec_p.cell_geom, x_series, y_series)
		FROM generate_series(0, 
				     (SELECT cast(rec_p.diag *2 as int)), 
				     longu) as x_series,
		generate_series(0 , 
				(SELECT cast(rec_p.diag * 2 as int)) 
				, larg) as y_series,
		(
		   SELECT rec_p.cell_geom
		) AS cell;

		-- rotate polygons based on az
		update systemknowledge.throwaway_grid set the_geom = 
							systemknowledge.perso_rotate(the_geom, Centroid(rec_p.poly_geom) 
										,radians(rec_p.az));
		-- overlay 
		DROP TABLE IF EXISTS systemknowledge.temp_grid CASCADE;
		CREATE TABLE systemknowledge.temp_grid(gid SERIAL PRIMARY KEY, the_geom geometry);
		INSERT INTO systemknowledge.temp_grid(the_geom)
		SELECT
		   Intersection(rec_p.poly_geom, g.the_geom) AS the_geom
		FROM
		   systemknowledge.throwaway_grid g
		WHERE
		   rec_p.poly_geom && g.the_geom
		AND
		   Intersects(rec_p.poly_geom, g.the_geom);

		-- Insert into systemknowledge.grid
		DELETE FROM systemknowledge.gridunit WHERE contains(gridunit.the_geom, pointonsurface(rec_p.poly_geom)); -- on delete le polygon qui a était coupé
		INSERT INTO systemknowledge.gridunit(the_geom) -- insertion des deux polygon créés
		(SELECT the_geom FROM systemknowledge.temp_grid);

		-- merge small polygon 	
		
	fetch curs1 into rec_p;	
	END LOOP;
	CLOSE curs1;

----------------------------------------------
DROP TABLE IF EXISTS systemknowledge.temp_polycell CASCADE;
DROP TABLE IF EXISTS systemknowledge.throwaway_grid CASCADE;
DROP TABLE IF EXISTS systemknowledge.temp_grid CASCADE;

RETURN('yeep');

END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100;


--------------------------------------------------------------------------------------------------------------------------------------------------------
--                               create - grid2
--------------------------------------------------------------------------------------------------------------------------------------------------------

-- Function: systemknowledge.create_grid(integer, integer)

-- DROP FUNCTION systemknowledge.create_grid(integer, integer);

CREATE OR REPLACE FUNCTION systemknowledge.create_grid2(longu integer, larg integer)
  RETURNS character AS
$BODY$
 DECLARE
 rec_p record; 
 curs1 refcursor;
	
 BEGIN

DROP TABLE IF EXISTS systemknowledge.gridunit CASCADE;
	CREATE TABLE systemknowledge.gridunit
	(
	  gid serial NOT NULL,
	  the_geom geometry
	)
	WITH (
	  OIDS=FALSE
	);
	INSERT INTO systemknowledge.gridunit(the_geom) (SELECT the_geom FROM systemknowledge.plotunit);

-- box for each polygon -> the box is a square and the side equal to the diag of the bounding box
	DROP TABLE IF EXISTS systemknowledge.temp_polycell CASCADE;
	CREATE TABLE systemknowledge.temp_polycell AS 
	(SELECT gid,
		the_geom,
		ST_PolygonFromText('POLYGON((	'||ST_xmin(ST_Extent(the_geom))||' '||ST_ymin(ST_Extent(the_geom))||', 	
					'||ST_xmin(ST_Extent(the_geom))||' '||ST_ymax(ST_Extent(the_geom))||', 
					'||ST_xmax(ST_Extent(the_geom))||' '||ST_ymax(ST_Extent(the_geom))||', 
					'||ST_xmax(ST_Extent(the_geom))||' '||ST_ymin(ST_Extent(the_geom))||', 
					'||ST_xmin(ST_Extent(the_geom))||' '||ST_ymin(ST_Extent(the_geom))||'))')as poly_geom,
		ST_PolygonFromText('POLYGON((	'||ST_xmin(ST_Extent(the_geom))||' '||ST_ymin(ST_Extent(the_geom))||', 	
					'||ST_xmin(ST_Extent(the_geom)) + longu ||' '||ST_ymin(ST_Extent(the_geom))||', 
					'||ST_xmin(ST_Extent(the_geom)) + longu ||' '||ST_ymin(ST_Extent(the_geom))+ larg||', 
					'||ST_xmin(ST_Extent(the_geom))||' '||ST_ymin(ST_Extent(the_geom))+ larg||', 
					'||ST_xmin(ST_Extent(the_geom))||' '||ST_ymin(ST_Extent(the_geom))||'))') as cell_geom
	FROM systemknowledge.gridunit
	GROUP BY gid, the_geom ORDER BY gid);

----------------------------------------------
-- Start loop on polygon
	OPEN curs1 FOR SELECT * FROM systemknowledge.temp_polycell;
	fetch curs1 into rec_p;	

	WHILE (FOUND) LOOP -- boucle pour chaque ligne de découpe

		-- create grid on one polygon 
		DROP TABLE IF EXISTS systemknowledge.throwaway_grid CASCADE;
		CREATE TABLE systemknowledge.throwaway_grid(gid SERIAL PRIMARY KEY, the_geom geometry);
		INSERT INTO systemknowledge.throwaway_grid(the_geom)
		SELECT st_translate(rec_p.cell_geom, x_series, y_series)
		FROM generate_series(0, (SELECT cast((ST_xmax(ST_Extent(rec_p.the_geom)) - ST_xmin(ST_Extent(rec_p.the_geom))) as int )),				     
					longu) as x_series,
		generate_series(0 , (SELECT cast((ST_ymax(ST_Extent(rec_p.the_geom)) - ST_ymin(ST_Extent(rec_p.the_geom))) as int)),
					larg) as y_series,
		(
		   SELECT rec_p.cell_geom
		) AS cell;
			


		-- overlay 
		DROP TABLE IF EXISTS systemknowledge.temp_grid CASCADE;
		CREATE TABLE systemknowledge.temp_grid(gid SERIAL PRIMARY KEY, the_geom geometry);
		INSERT INTO systemknowledge.temp_grid(the_geom)
		SELECT
		   Intersection(rec_p.poly_geom, g.the_geom) AS the_geom
		FROM
		   systemknowledge.throwaway_grid g
		WHERE
		   rec_p.poly_geom && g.the_geom
		AND
		   Intersects(rec_p.poly_geom, g.the_geom);

		-- Insert into systemknowledge.grid
		DELETE FROM systemknowledge.gridunit WHERE contains(gridunit.the_geom, pointonsurface(rec_p.poly_geom)); -- on delete le polygon qui a était coupé
		INSERT INTO systemknowledge.gridunit(the_geom) -- insertion des deux polygon créés
		(SELECT the_geom FROM systemknowledge.temp_grid);

		-- merge small polygon 	
		
	fetch curs1 into rec_p;	
	END LOOP;
	CLOSE curs1;

----------------------------------------------
DROP TABLE IF EXISTS systemknowledge.temp_polycell CASCADE;
DROP TABLE IF EXISTS systemknowledge.throwaway_grid CASCADE;
DROP TABLE IF EXISTS systemknowledge.temp_grid CASCADE;

RETURN('yeep');

END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
