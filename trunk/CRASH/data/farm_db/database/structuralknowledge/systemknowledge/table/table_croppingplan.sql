-------------------------------------------------
-- Authors: Jérôme Dury
-- Date: 

--               croppingplan.sql


-- Comments: La table croppingplan est une table de synthèse
-------------------------------------------------


-- Création des tables
    -- croppingplan

-- DROP TABLE IF EXISTS  systemknowledge.croppingplan CASCADE;
-- CREATE TABLE systemknowledge.croppingplan
-- (
--     ref_landunit int,
-- 	    FOREIGN KEY (ref_landunit)
-- 		REFERENCES systemknowledge.farmland
-- 		ON UPDATE CASCADE ON DELETE CASCADE,
-- 	ref_plotunit int,
--     ref_year int,
--     code_crop varchar,
--     ref_cropvariety varchar,
--     code_itk varchar,
--   PRIMARY KEY(ref_landunit, ref_year)
-- );


DROP TABLE IF EXISTS  systemknowledge.cropplan CASCADE;
CREATE TABLE systemknowledge.cropplan
(
    ref_plotunit int,
	    FOREIGN KEY (ref_plotunit)
		REFERENCES systemknowledge.plotunit
		ON UPDATE CASCADE ON DELETE CASCADE,
    year_h int,
    code_crop varchar,
    ref_variety varchar,
    code_itk varchar,
  PRIMARY KEY(ref_plotunit, year_h)
);

