-----------------------------------------------------------


------- waterresource.sql

-----------------------------------------------------------

DROP TABLE IF EXISTS  systemknowledge.waterresource CASCADE;
CREATE TABLE systemknowledge.waterresource
(
  id_waterresource serial PRIMARY KEY,
  ref_quantity int,
  ref_flowrate int
);

DROP TABLE IF EXISTS systemknowledge.weather;
CREATE TABLE systemknowledge.weather
(
	jour int,
	mois int,
	annee int,
	Rg numeric,
	Tx numeric,
	Tn numeric,
	pluie numeric,
	hr numeric,
	vent numeric,
	ETP numeric
);


