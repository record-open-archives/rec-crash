-------------------------------------------------
-- Authors: Jérôme Dury
-- Date:

--               farmland.sql


-- Comments: La table farmland est une table de synthèse
-------------------------------------------------


-- Création des tables

-- capislet
DROP TABLE IF EXISTS  systemknowledge.capislet CASCADE;
CREATE TABLE systemknowledge.capislet
(
  id_capislet serial PRIMARY KEY,
  ref_wr int,
  the_geom geometry
);

-- CroppingBlock
DROP TABLE IF EXISTS  systemknowledge.croppingblock CASCADE;
CREATE TABLE systemknowledge.croppingblock
(
  id_croppingblock int PRIMARY KEY,
  the_geom geometry
);

-- plotunit
DROP TABLE IF EXISTS  systemknowledge.plotunit CASCADE;
CREATE TABLE systemknowledge.plotunit
(
  id_plotunit serial PRIMARY KEY,
  the_geom geometry
);

-- gridunit
DROP TABLE IF EXISTS  systemknowledge.gridunit CASCADE;
CREATE TABLE systemknowledge.gridunit
(
  id_gridunit serial PRIMARY KEY,
  the_geom geometry
);

-- soilunit
DROP TABLE IF EXISTS  systemknowledge.soilunit CASCADE;
CREATE TABLE systemknowledge.soilunit
(
  id_soilunit serial PRIMARY KEY,
  code_soiltype varchar,
  the_geom geometry
);

-- landunit
DROP TABLE IF EXISTS  systemknowledge.landunit CASCADE;
CREATE TABLE systemknowledge.landunit
(
  id_landunit serial PRIMARY KEY,
  the_geom geometry
);

-- workorientation
DROP TABLE IF EXISTS systemknowledge.workorientation;
CREATE TABLE systemknowledge.workorientation(
    id_wo serial NOT NULL,
    the_geom geometry
);

-- farmland
DROP TABLE IF EXISTS  systemknowledge.farmland CASCADE;
CREATE TABLE systemknowledge.farmland
(
  id_landunit int PRIMARY KEY,
  ref_cb int,
  ref_wr int,
  code_soiltype varchar,
  the_geom geometry
);

DROP TABLE IF EXISTS  systemknowledge.cropproduction CASCADE;
CREATE TABLE systemknowledge.cropproduction
(
  id serial PRIMARY KEY,
  ref_soil int,
  ref_crop int,
  ref_variety int,
  ref_itk int,
  yd_mn numeric
  );
