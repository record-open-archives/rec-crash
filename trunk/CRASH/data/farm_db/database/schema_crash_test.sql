-------------------------------------------------
-- Authors: Jérôme Dury
-- Date: 

--                schema_crash_test.sql


-- Comments: create scheam for the crash database
-------------------------------------------------
DROP SCHEMA IF EXISTS gis_schema CASCADE;

DROP SCHEMA IF EXISTS storage CASCADE;
CREATE SCHEMA storage;-- AUTHORIZATION dury;

DROP SCHEMA IF EXISTS expertknowledge CASCADE;
CREATE SCHEMA expertknowledge;-- AUTHORIZATION dury;

DROP SCHEMA IF EXISTS systemknowledge CASCADE;
CREATE SCHEMA systemknowledge;-- AUTHORIZATION dury;

DROP SCHEMA IF EXISTS proceduralknowledge CASCADE;
CREATE SCHEMA proceduralknowledge;-- AUTHORIZATION dury;

DROP SCHEMA IF EXISTS vle CASCADE;
CREATE SCHEMA vle;-- AUTHORIZATION dury;


DROP FUNCTION IF EXISTS concat2(text, text) CASCADE;
CREATE OR REPLACE FUNCTION concat2(text, text) RETURNS text AS '
    SELECT CASE WHEN $1 IS NULL OR $1 = '''' THEN $2
            WHEN $2 IS NULL OR $2 = '''' THEN $1
            ELSE $1 || '','' || $2
            END;
'
 LANGUAGE SQL;

DROP AGGREGATE IF EXISTS concatenate(text, text) CASCADE;
CREATE AGGREGATE concatenate (
  sfunc = concat2,
  basetype = text,
  stype = text,
  initcond = ''
  );
