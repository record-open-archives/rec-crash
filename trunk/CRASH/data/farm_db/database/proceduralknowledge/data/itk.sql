--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = off;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET escape_string_warning = off;
SET search_path = storage, pg_catalog;
SET default_tablespace = '';
SET default_with_oids = false;

COPY itk (id, code_crop, code, domain_variety, domain_soiltype, type_soil, productionsystem, description, code_prec, code_suc, lst_cop) FROM stdin;
1	MA	MA_1	hybrid_p	TPAA	hard	Intensive	mono culture with irrigation	MA	MA	1,2,3,4,5,6,7
2	BH	BH_1	conventional	TPAA	easy	Intensive	rotation with CH without irrigation	CH	CH	13,14,15,16,17
3	CH	CH_1	conventional	TPAA	easy	Intensive	rotation with BH without irrigation	BH	BH	8,9,10,11,12
4	BH	BH_3	conventional	S7	hard	Intensive	rotation with CH,BA,PP,TO without irrigation	PP	CH	18,19,20,21,22
5	BH	BH_3	conventional	S8	easy	Intensive	rotation with CH,BA,PP,TO without irrigation	PP	CH	18,19,20,21,22
6	BH	BH_3	conventional	S9	hard	Intensive	rotation with CH,BA,PP,TO without irrigation	PP	CH	18,19,20,21,22
7	CH	CH_3	conventional	S7	hard	Intensive	rotation with BH,BA,PP,TO without irrigation	BH	BH	23,24,25,26
8	CH	CH_3	conventional	S8	easy	Intensive	rotation with BH,BA,PP,TO without irrigation	BH	BH	23,24,25,26
9	CH	CH_3	conventional	S9	hard	Intensive	rotation with BH,BA,PP,TO without irrigation	BH	BH	23,24,25,26
10	TO	TO_3s	conventional	S7	hard	Intensive	rotation with BH,BA,PP,CH without irrigation	BH	BH	35,39
11	TO	TO_3	conventional	S9	hard	Intensive	rotation with BH,BA,PP,CH with irrigation	BA	BA	35,36,37,38,39
12	TO	TO_3s	conventional	S9	hard	Intensive	rotation with BH,BA,PP,CH without irrigation	BH	BH	35,39
13	PP	PP_3	conventional	S7	hard	Intensive	rotation with BH,BA,TO,CH without irrigation	BH	BH	27,28
14	PP	PP_3	conventional	S8	easy	Intensive	rotation with BH,BA,TO,CH without irrigation	BH	BH	27,28
15	PP	PP_3	conventional	S9	hard	Intensive	rotation with BH,BA,TO,CH without irrigation	BH	BH	27,28
16	BA	BA_3s	conventional	S7	hard	Intensive	rotation with BH,TO,PP,CH without irrigation	CH	CH	29,30,31,32,34
17	BA	BA_3	conventional	S9	hard	Intensive	rotation with BH,TO,PP,CH with irrigation	TO	TO	29,30,31,32,33,34,40
18	BA	BA_3s	conventional	S9	hard	Intensive	rotation with BH,TO,PP,CH without irrigation	CH	CH	29,30,31,32,34
19	MA	MA_1F	hybrid_p	TPAA	hard	Intensive	mono culture with irrigation	MA	MA	41,42,43,44,45,46,47,48,49,50,51,52
20	BH	BH_1F	conventional	TPAA	easy	Intensive	rotation with CH without irrigation	CH	CH	58,59,60,61,62
21	CH	CH_1F	conventional	TPAA	easy	Intensive	rotation with BH without irrigation	BH	BH	53,54,55,56,57
22	BH	BH_3F	conventional	S9	hard	Intensive	rotation with CH,BA,PP,TO without irrigation	PP	CH	63,64,65,66,67
23	CH	CH_3F	conventional	S9	easy	Intensive	rotation with BH,BA,PP,TO without irrigation	BH	BH	68,69,70,71
24	TO	TO_3F	conventional	S9	hard	Intensive	rotation with BH,BA,PP,CH with irrigation	BA	BA	81,82,83,84
25	PP	PP_3F	conventional	S9	hard	Intensive	rotation with BH,BA,TO,CH without irrigation	BH	BH	72,73
26	BA	BA_3F	conventional	S9	hard	Intensive	rotation with BH,TO,PP,CH with irrigation	TO	TO	74,75,76,77,78,79,80
