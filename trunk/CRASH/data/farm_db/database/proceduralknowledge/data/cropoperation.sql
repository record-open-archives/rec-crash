--
-- PostgreSQL database dump
--
SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = off;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET escape_string_warning = off;
SET search_path = storage, pg_catalog;
SET default_tablespace = '';
SET default_with_oids = false;

COPY cropoperation (id, name, type, equipment, st, nd, event, lst_rule) FROM stdin;--MA.1
1	sowing_fert	sowing	combine_drill	01/04	20/04	1	1,7
2	irr_post_lev	irrigating	pivot	08/04	30/04	7	9
3	irrRt	irrigating	pivot	05/05	01/09	6	30,31
4	MA_fert2	fertilization	12m_sprayer	05/05	30/05	3	2
5	MA_fert3	fertilization	12m_sprayer	15/05	10/06	4	3
6	harvesting	harvesting	combine_harvester_MA	10/09	30/10	2	5,8
7	irri_sech	irrigating	pivot	01/05	30/05	8	4
\.
COPY cropoperation (id, name, type, equipment, st, nd, event, lst_rule) FROM stdin;--CH1
8	sowing_fert	sowing	combine_drill	15/08	10/09	9	10,12
9	CH_fert2	fertilization	12m_sprayer	16/01	25/01	10	11
10	CH_fert3	fertilization	12m_sprayer	10/02	25/02	11	11
11	CH_fert4	fertilization	12m_sprayer	10/03	25/03	11	11
12	harvesting	harvesting	combine_harvester_CH	15/06	10/07	12	13,14
\.
COPY cropoperation (id, name, type, equipment, st, nd, event, lst_rule) FROM stdin;--BH1
13	sowing	sowing	combine_drill	15/10	15/11	13	15,17
14	BH_fert1	fertilization	12m_sprayer	16/01	25/01	14	16
15	BH_fert2	fertilization	12m_sprayer	10/02	25/02	14	35
16	BH_fert3	fertilization	12m_sprayer	10/03	25/03	14	36
17	harvesting	harvesting	combine_harvester_BH	20/06	20/07	15	18,19
\.
COPY cropoperation (id, name, type, equipment, st, nd, event, lst_rule) FROM stdin;--BH3
18	sowing	sowing	combine_drill	20/10	15/11	16	20,22
19	BH_fert1	fertilization	12m_sprayer	16/01	25/01	17	21
20	BH_fert2	fertilization	12m_sprayer	20/02	10/03	18	21
21	BH_fert3	fertilization	12m_sprayer	20/03	10/04	19	21
22	harvesting	harvesting	combine_harvester_BH	20/06	15/07	20	23,24
\.
COPY cropoperation (id, name, type, equipment, st, nd, event, lst_rule) FROM stdin;--CH3
23	sowing	sowing	combine_drill	01/09	30/09	21	25,27
24	CH_fert1	fertilization	12m_sprayer	16/01	25/01	22	26
25	CH_fert2	fertilization	12m_sprayer	10/03	25/03	23	26
26	harvesting	harvesting	combine_harvester_CH	20/06	10/07	24	28,29
\.
COPY cropoperation (id, name, type, equipment, st, nd, event, lst_rule) FROM stdin;--PP3
27	sowing	sowing	combine_drill	20/01	15/02	25	37,39
28	harvesting	harvesting	combine_harvester_CH	20/06	10/07	26	38,40
\.
COPY cropoperation (id, name, type, equipment, st, nd, event, lst_rule) FROM stdin;--BA3
29	sowing	sowing	combine_drill	24/10	15/11	27	41,43
30	BA_fert1	fertilization	12m_sprayer	16/01	25/01	28	42
31	BA_fert2	fertilization	12m_sprayer	20/02	10/03	29	42
32	BA_fert3	fertilization	12m_sprayer	25/03	10/04	30	42
33	irri_BA	irrigating	sprinkler	05/04	25/04	31	45
40	irri_BA_2	irrigating	sprinkler	05/04	25/04	31	55
34	harvesting	harvesting	combine_harvester_BH	20/06	25/07	32	47,44
\.
COPY cropoperation (id, name, type, equipment, st, nd, event, lst_rule) FROM stdin;--TO3
35	sowing	sowing	monoseed_drill	20/03	15/04	33	48,49
36	irri_avt_flo_TO	irrigating	sprinkler	20/06	10/07	34	51
37	irri_apres_flo_TO	irrigating	sprinkler	25/07	15/08	34	52
38	irri_ap_flo_2eme_TO	irrigating	sprinkler	30/07	15/08	34	54
39	harvesting	harvesting	combine_harvester_TO	20/08	01/10	35	50,53
\.
COPY cropoperation (id, name, type, equipment, st, nd, event, lst_rule) FROM stdin;--MA1_f
41	sowing_fert	sowing	combine_drill	14/04	15/04	1	0
42	irr_1	irrigating	pivot	15/06	16/06	7	0
43	irr_2	irrigating	pivot	24/06	25/06	7	0
44	irr_3	irrigating	pivot	02/07	03/07	7	0
45	irr_4	irrigating	pivot	10/07	11/07	7	0
46	irr_5	irrigating	pivot	18/07	19/07	7	0
47	irr_6	irrigating	pivot	28/07	29/07	7	0
48	irr_7	irrigating	pivot	07/08	08/08	7	0
49	irr_8	irrigating	pivot	15/08	16/08	7	0
50	MA_fert2	fertilization	12m_sprayer	10/05	11/05	3	0
51	MA_fert3	fertilization	12m_sprayer	21/05	22/06	4	0
52	harvesting	harvesting	combine_harvester_MA	15/10	16/10	2	0
\.
COPY cropoperation (id, name, type, equipment, st, nd, event, lst_rule) FROM stdin;--CH1_f
53	sowing_fert	sowing	combine_drill	01/09	02/09	9	0
54	CH_fert2	fertilization	12m_sprayer	16/01	17/01	10	0
55	CH_fert3	fertilization	12m_sprayer	15/02	16/02	11	0
56	CH_fert4	fertilization	12m_sprayer	15/03	16/03	11	0
57	harvesting	harvesting	combine_harvester_CH	30/06	01/07	12	0
\.
COPY cropoperation (id, name, type, equipment, st, nd, event, lst_rule) FROM stdin;--BH1_F
58	sowing	sowing	combine_drill	29/10	30/10	13	0
59	BH_fert1	fertilization	12m_sprayer	16/01	17/01	14	0
60	BH_fert2	fertilization	12m_sprayer	15/02	16/02	14	0
61	BH_fert3	fertilization	12m_sprayer	15/03	16/03	14	0
62	harvesting	harvesting	combine_harvester_BH	10/07	11/07	15	0
\.
COPY cropoperation (id, name, type, equipment, st, nd, event, lst_rule) FROM stdin;--BH3_f
63	sowing	sowing	combine_drill	29/10	30/10	16	0
64	BH_fert1	fertilization	12m_sprayer	16/01	17/01	17	0
65	BH_fert2	fertilization	12m_sprayer	27/02	28/02	18	0
66	BH_fert3	fertilization	12m_sprayer	01/04	02/04	19	0
67	harvesting	harvesting	combine_harvester_BH	12/07	13/07	20	0
\.
COPY cropoperation (id, name, type, equipment, st, nd, event, lst_rule) FROM stdin;--CH3_f
68	sowing	sowing	combine_drill	01/09	02/09	21	200
69	CH_fert1	fertilization	12m_sprayer	16/01	17/01	22	201
70	CH_fert2	fertilization	12m_sprayer	15/03	16/03	23	202
71	harvesting	harvesting	combine_harvester_CH	22/06	23/06	24	203
\.
COPY cropoperation (id, name, type, equipment, st, nd, event, lst_rule) FROM stdin;--PP3_f
72	sowing	sowing	combine_drill	01/02	02/02	25	0
73	harvesting	harvesting	combine_harvester_CH	29/06	30/06	26	0
\.
COPY cropoperation (id, name, type, equipment, st, nd, event, lst_rule) FROM stdin;--BA3_f
74	sowing	sowing	combine_drill	01/11	02/11	27	0
75	BA_fert1	fertilization	12m_sprayer	16/01	17/01	28	0
76	BA_fert2	fertilization	12m_sprayer	27/02	28/02	29	0
77	BA_fert3	fertilization	12m_sprayer	01/04	02/04	30	0
78	irri_BA	irrigating	sprinkler	05/04	06/04	31	0
79	irri_BA_2	irrigating	sprinkler	10/04	11/04	31	0
80	harvesting	harvesting	combine_harvester_BH	05/07	06/07	32	0
\.
COPY cropoperation (id, name, type, equipment, st, nd, event, lst_rule) FROM stdin;--TO3_f
81	sowing	sowing	monoseed_drill	01/04	02/04	33	0
82	irri_avt_flo_TO	irrigating	sprinkler	23/06	24/07	34	0
83	irri_apres_flo_TO	irrigating	sprinkler	30/07	31/07	34	0
84	harvesting	harvesting	combine_harvester_TO	09/09	10/09	35	0
\.
COPY precedence(first_cop, second_cop, type, tmin, tmax) FROM stdin;
1	6	FS	1	9999
1	4	FS	1	9999
4	5	FS	8	20
1	3	FS	4	9999
1	2	FS	1	9999
3	7	FS	4	9999
8	12	FS	1	9999
8	9	FS	70	9999
9	10	FS	20	9999 
10	11	FS	20	9999
11	12	FS	82	9999
13	17	FS	200	9999
13	14	FS	1	9999
14	15	FS	20	9999 
15	16	FS	20	9999
18	19	FS	1	9999
19	20	FS	30	9999
20	21	FS	20	9999
18	22	FS	0	9999
24	25	FS	40	60
27	28	FS	135	9999
29	30	FS	1	9999
30	31	FS	20	9999
31	32	FS	20	9999
32	33	FS	5	9999
33	40	FS	5	10
29	34	FS	1	9999
35	36	FS	1	9999
36	37	FS	4	9999
35	39	FS	1	9999
37	38	FS	5	9999
41	42	FS	1	9999
42	43	FS	1	9999
43	44	FS	1	9999
44	45	FS	1	9999
45	46	FS	1	9999
47	48	FS	1	9999
48	49	FS	1	9999
49	50	FS	1	9999
50	51	FS	1	9999
51	52	FS	1	9999
53	54	FS	1	9999
54	55	FS	1	9999
55	56	FS	1	9999
56	57	FS	1	9999
58	59	FS	1	9999
59	60	FS	1	9999
60	61	FS	1	9999
61	62	FS	1	9999
63	64	FS	1	9999
64	65	FS	1	9999
65	66	FS	1	9999
66	67	FS	1	9999
68	69	FS	1	9999
69	70	FS	1	9999
70	71	FS	1	9999
72	73	FS	1	9999
74	75	FS	1	9999
75	76	FS	1	9999
76	77	FS	1	9999
77	78	FS	1	9999
78	79	FS	1	9999
79	80	FS	1	9999
81	82	FS	1	9999
82	83	FS	1	9999
83	84	FS	1	9999
\.
