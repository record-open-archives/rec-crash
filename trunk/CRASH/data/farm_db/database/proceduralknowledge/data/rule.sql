--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = off;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET escape_string_warning = off;
SET search_path = storage, pg_catalog;
SET default_tablespace = '';
SET default_with_oids = false;

COPY rule(id, name, lst_pred) FROM stdin;--MA.1
1	sowing	1,3,5
2	Nfert2	1,3,12
3	Nfert3	1,3
4	start_irr	3,9
5	grain_hrv	3,7
7	end_sow_period	10
8	end_hrv_period	11
9	irr_starter	13,3,9
30	stad_harv_0_2_irr	3,44,33
31	stad_harv_sup_2_irr	3,45,34
\.
COPY rule(id, name, lst_pred) FROM stdin;--MA1.3
33	start_swfac_veget_irr	36,37,38
34	start_swfac_repro_irr	39,40,41
\.
COPY rule(id, name, lst_pred) FROM stdin;--CH1
10	sowing	1,3
11	Nfert2	100,101
12	end_sow_period	16
13	end_hrv_period	17
14	grain_hrv	1,101,15,18
\.
COPY rule(id, name, lst_pred) FROM stdin;--BH1
15	sowing	1,3
16	Nfert2	100,101
35	Nfert3	100,101
36	nfert4	100,101
17	end_sow_period	19
18	end_hrv_period	20
19	grain_hrv	1,101,21
\.
COPY rule(id, name, lst_pred) FROM stdin;--BH3
20	sowing	1,3
21	Nfert2	101,100
22	end_sow_period	22
23	end_hrv_period	23
24	grain_hrv	1,3,24
\.
COPY rule(id, name, lst_pred) FROM stdin;--CH3
25	sowing	1,3,14
26	Nfert2	100,101
27	end_sow_period	16
28	end_hrv_period	17
29	grain_hrv	101,3,15,18
\.
COPY rule(id, name, lst_pred) FROM stdin;--PP3
37	sowing	1,3
38	grain_hrv	1,3,48
39	end_sow_period	36
40	end_hrv_period	37
\.
COPY rule(id, name, lst_pred) FROM stdin;--BA3
41	sowing	1,3
42	Nfert2	101,100
43	end_sow_period	38
44	end_hrv_period	39
45	start_irr	3,51
55	irr_2	3,50
47	grain_hrv	1,3,40
\.
COPY rule(id, name, lst_pred) FROM stdin;--TO3
48	sowing	1,3
49	end_sow_period	41
50	end_hrv_period	42
51	start_irr	3,49,44
52	ap_flo_1_irr	3,46,45
54	ap_flo_2_irr	3,45,47
53	grain_hrv	43,1,3
\.
COPY rule(id, name, lst_pred) FROM stdin;--CH3F
200	sowing	200
201	FERT1	201
202	fert2	202
203	harvest	203
\.



COPY predicate(id, name, predicate, param, crash_pred) FROM stdin;--socle
1	Bearing capacity	"NA"	1	p_bearingCapacity_sol
3	No rain	"Na"	4,5	p_rain_wth
100	Bearing capacity	"NA"	100	p_bearingCapacity_sol
101	low rain	"NA"	101,102	p_rain_wth
\.
COPY predicate(id, name, predicate, param, crash_pred) FROM stdin;--MA1.1
5	T min for sowing	"NA"	11	p_tmn_sow
7	Grain humidity	"NA"	7	p_h2o_hrv
9	Water Depletion 40mm	"NA"	3	p_waterDepletion_sol
10	end period	"NA"	8	p_at_date
11	end period	"NA"	9	p_at_date
12	8leaf	"NA"	10	p_nbleaf_crp
13	irr_starter_germ	"NA"	26,27	p_vegetativeStage_crp
33	water_depletion_59mm	"NA"	36	p_waterDepletion_sol
34	water_depletion_72mm	"NA"	37	p_waterDepletion_sol
44	cropphysio_avt_flo	"NA"	48,52	p_vegetativeStage_crp
45	cropphysio_flo	"NA"	49,52	p_vegetativeStage_crp
\.
COPY predicate(id, name, predicate, param, crash_pred) FROM stdin;--MA bonus
2	swfac	"NA"	25	p_swfac_crp
4	CropPhysioMat	"NA"	2	p_harvestedOrganeStage_crp
\.
COPY predicate(id, name, predicate, param, crash_pred) FROM stdin;--CH1
15	CropPhysioMat	"NA"	2,30	p_harvestedOrganeStage_crp
16	end period	"NA"	14	p_at_date
17	end period	"NA"	15	p_at_date
18	Grain humidity	"NA"	16	p_h2o_hrv
\.
COPY predicate(id, name, predicate, param, crash_pred) FROM stdin;--BH1
19	end period	"NA"	17	p_at_date
20	end period	"NA"	18	p_at_date
21	Grain humidity	"NA"	19	p_h2o_hrv
\.
COPY predicate(id, name, predicate, param, crash_pred) FROM stdin;--BH3
22	end period	"NA"	20	p_at_date
23	end period	"NA"	21	p_at_date
24	Grain humidity	"NA"	19	p_h2o_hrv
\.
COPY predicate(id, name, predicate, param, crash_pred) FROM stdin;--CH3
14	Water Depletion_10mm	"NA"	13	p_waterDepletion_sol
25	end period	"NA"	22	p_at_date
26	end period	"NA"	23	p_at_date
27	Grain humidity	"NA"	24	p_h2o_hrv
\.
COPY predicate(id, name, predicate, param, crash_pred) FROM stdin;--MA1.2
28	cropphysio_bef_flow	"NA"	35,26	p_vegetativeStage_crp
29	cropphysio_bef_flow_max	"NA"	27,28	p_vegetativeStage_crp
30	cropphysio_flow_min	"NA"	29,30	p_vegetativeStage_crp
31	cropphysio_flow_max	"NA"	31,32	p_vegetativeStage_crp
32	cropphysio_grain	"NA	33,34	p_vegetativeStage_crp
\.
COPY predicate(id, name, predicate, param, crash_pred) FROM stdin;--PP3
48	Grain humidity	"NA"	39	p_h2o_hrv
36	end period	"NA"	40	p_at_date
37	end period	"NA"	41	p_at_date
\.
COPY predicate(id, name, predicate, param, crash_pred) FROM stdin;--BA3
38	end period	"NA"	42	p_at_date
39	end period	"NA"	43	p_at_date
40	Grain humidity	"NA"	44	p_h2o_hrv
50	Water Depletion_2	"NA"	54	p_waterDepletion_sol
51	Water Depletion_2	"NA"	55	p_waterDepletion_sol
\.
COPY predicate(id, name, predicate, param, crash_pred) FROM stdin;--TO3
41	end period	"NA"	45	p_at_date
42	end period	"NA"	46	p_at_date
43	Grain humidity	"NA"	47	p_h2o_hrv
49	Water Depletion avtflo	"NA"	50	p_waterDepletion_sol
46	Water Depletion flo	"NA"	51	p_waterDepletion_sol
47	Water Depletion flo	"NA"	53	p_waterDepletion_sol
\.
COPY predicate(id, name, predicate, param, crash_pred) FROM stdin;--CH3f
200	end period	"NA"	200	p_at_date
201	end period	"NA"	201	p_at_date
202	end period	"NA"	202	p_at_date
203	end period	"NA"	203	p_at_date
\.
COPY param_predicate(id, name, type, value, unit) FROM stdin;--socle
1	tolerance	none	0	%
4	nDay	tme	0	day
5	limRain	wth	0	mm rain
100	tolerance	none	0.05	%
101	nDay	tme	0	day
102	limRain	wth	10	mm rain
\.
COPY param_predicate(id, name, type, value, unit) FROM stdin;--MA1.1
3	waterDepletion	wat	40	mm
7	h2oHrv	crp	0.25	%
8	date	dte	20/04	nd date
9	date	dte	30/10	nd date
10	nbLeaf	stg	8	NbLeafp
11	tmin_sow	temp	6	degree
26	op	operator	>	op
27	vegetativeStage	stg	2	stg
36	waterDepletion	wat	100	mm
37	waterDepletion	wat	110	mm
48	vegetativeStage	stg	4	stg
49	vegetativeStage	stg	5	stg
52	op	operator	==	op
\.
COPY param_predicate(id, name, type, value, unit) FROM stdin;--CH1
13	waterDepletion	wat	10	mm
14	date	dte	10/09	nd date
15	date+1	dte	10/07	nd date
16	h2oHrv	crp	0.09	%
\.
COPY param_predicate(id, name, type, value, unit) FROM stdin;--BH1
17	date	dte	15/11	nd date
18	date+1	dte	20/07	nd date
19	h2oHrv	crp	0.145	%
\.
COPY param_predicate(id, name, type, value, unit) FROM stdin;--BH3
20	date	dte	15/11	nd date
21	date+1	dte	15/07	nd date
\.
COPY param_predicate(id, name, type, value, unit) FROM stdin;--CH3
22	date	dte	30/09	nd date
23	date+1	dte	10/07	nd date
24	h2oHrv	crp	0.090	%
\.
COPY param_predicate(id, name, type, value, unit) FROM stdin;--MA1.2
2	harvestedOrganeStage	stg	5	harvestedOrganeStage
12	vegetativeStage	stg	2	stg
25	swfac	crp	0.75	%
35	vegetativeStage	stg	0	stg
28	op	operator	<	op
29	vegetativeStage	stg	2	stg
30	op	operator	>=	op
31	vegetativeStage	stg	3	stg
32	op	operator	<	op
33	vegetativeStage	stg	3	stg
34	op	operator	>=	op
\.
COPY param_predicate(id, name, type, value, unit) FROM stdin;--PP3
39	h2oHrv	crp	0.14	%
40	date	dte	15/02	nd date
41	date	dte	10/07	nd date
\.
COPY param_predicate(id, name, type, value, unit) FROM stdin;--BA3
42	date	dte	15/11	nd date
43	date+1	dte	25/07	nd date
44	h2oHrv	crp	0.145	%
54	waterDepletion	wat	15	mm
55	waterDepletion	wat	25	mm
\.
COPY param_predicate(id, name, type, value, unit) FROM stdin;--TO3
45	date	dte	15/04	nd date
46	date	dte	01/10	nd date
47	h2oHrv	crp	0.11	%
50	waterDepletion	wat	60	mm
51	waterDepletion	wat	60	mm
53	waterDepletion	wat	100	mm
\.
COPY param_predicate(id, name, type, value, unit) FROM stdin;--CH3F
200	date	dte	02/09	nd date
201	date+1	dte	17/01	nd date
202	date+1	dte	16/03	nd date
203	date+1	dte	23/06	nd date
\.
