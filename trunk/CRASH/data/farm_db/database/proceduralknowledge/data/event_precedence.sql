--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = off;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET escape_string_warning = off;
SET search_path = storage, pg_catalog;
SET default_tablespace = '';
SET default_with_oids = false;

COPY event(id, name, param, crash_ev) FROM stdin;--MA.1
1	sowfertN	2,3	sow_frtN
2	harvest	1	hrv
3	fertN	4	frtN
4	fertN	5	frtN
6	IrrNew	6	newIrr
7	irrpostleve	7	irr
8	irrsech	8	irr
\.
COPY event(id, name, param, crash_ev) FROM stdin;--CH1
9	sowfertN	9,10	sow_frtN
10	fertN	11	frtN
11	fertN	12	frtN
12	harvest	1	hrv
\.
COPY event(id, name, param, crash_ev) FROM stdin;--BH1
13	sow	13	sow
14	fertN	14	frtN
15	harvest	1	hrv
\.
COPY event(id, name, param, crash_ev) FROM stdin;--BH3
16	sow	15	sow
17	fertN	16	frtN
18	fertN	17	frtN
19	fertN	18	frtN
20	harvest	1	hrv
\.
COPY event(id, name, param, crash_ev) FROM stdin;--CH3
21	sow	19	sow
22	fertN	20	frtN
23	fertN	21	frtN
24	harvest	1	hrv
\.
COPY event(id, name, param, crash_ev) FROM stdin;--PP3
25	sow	22	sow
26	harvest	1	hrv
\.
COPY event(id, name, param, crash_ev) FROM stdin;--BA3
27	sow	23	sow
28	fertN	24	frtN
29	fertN	25	frtN
30	fertN	26	frtN
31	Irr	27	irr
32	harvest	1	hrv
\.
COPY event(id, name, param, crash_ev) FROM stdin;--TO3
33	sow	28	sow
34	Irr	29	irr
35	harvest	1	hrv
\.


COPY param_event(id, name, type, value, unit) FROM stdin;--MA.1
1	action	none	NA	None
2	density	sow	8	gr/sqm
3	N	frtN	23.4	kg N/ha
4	N	frtN	69	kg N/ha
5	N	frtN	69	kg N/ha
6	h2o	irrDose	30	mm/ha
7	h2o	irrDose	30	mm/ha
8	h2o	irrDose	30	mm/ha
\.
COPY param_event(id, name, type, value, unit) FROM stdin;--CH1
9	density	sow	35	gr/sqm
10	N	frtN	23.4	kg N/ha
11	N	frtN	50.6	kg N/ha
12	N	frtN	69	kg N/ha
\.
COPY param_event(id, name, type, value, unit) FROM stdin;--BH1
13	density	sow	240	gr/sqm
14	N	frtN	69	kg N/ha
\.
COPY param_event(id, name, type, value, unit) FROM stdin;--BH3
15	density	sow	300	gr/sqm
16	N	frtN	50.3	kg N/ha
17	N	frtN	61	kg N/ha
18	N	frtN	42.9	kg N/ha
\.
COPY param_event(id, name, type, value, unit) FROM stdin;--CH3
19	density	sow	45	gr/sqm
20	N	frtN	50.3	kg N/ha
21	N	frtN	50	kg N/ha
\.
COPY param_event(id, name, type, value, unit) FROM stdin;--PP3
22	density	sow	100	gr/sqm
\.
COPY param_event(id, name, type, value, unit) FROM stdin;--BA3
23	density	sow	310	gr/sqm
24	N	frtN	50.3	kg N/ha
25	N	frtN	61	kg N/ha
26	N	frtN	42.9	kg N/ha
27	h2o	irrDose	30	mm/ha
\.
COPY param_event(id, name, type, value, unit) FROM stdin;--TO3
28	density	sow	6	gr/sqm
29	h2o	irrDose	30	mm/ha
\.





