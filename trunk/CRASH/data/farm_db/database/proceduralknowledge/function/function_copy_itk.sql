CREATE OR REPLACE FUNCTION proceduralknowledge.copy_itk(varchar)
RETURNS void AS
$BODY$
DECLARE
BEGIN
		-- insert itk
		INSERT INTO proceduralknowledge.itk
		SELECT * FROM storage.itk WHERE code=$1;

		-- SELECT crop operation
		INSERT INTO proceduralknowledge.cropoperation
		SELECT DISTINCT cop.*
		FROM
		(SELECT itk.id as id_itk , cast(regexp_split_to_table(itk.lst_cop, E',') as integer) as ref_cop
				FROM storage.itk WHERE itk.code = $1) AS foo,
		storage.cropoperation cop
		WHERE ref_cop = cop.id;


		--SELECT event from ITK
		INSERT INTO proceduralknowledge.event
		SELECT DISTINCT ev.*
		FROM
		(SELECT fo.id_itk, fo.ref_cop, event as ref_ev
				FROM
				(SELECT itk.id as id_itk , cast(regexp_split_to_table(itk.lst_cop, E',') as integer) as ref_cop
						FROM storage.itk WHERE itk.code = $1) AS fo,
				storage.cropoperation cop
				WHERE ref_cop = cop.id) AS foo,
		storage.event ev;

		INSERT INTO proceduralknowledge.param_event
		SELECT DISTINCT param.* FROM
		(SELECT ev.id, cast(regexp_split_to_table(ev.param, E',') as integer) as ref_cop
				FROM
				(SELECT fo.id_itk, fo.ref_cop, event as ref_ev
						FROM
						(SELECT itk.id as id_itk , cast(regexp_split_to_table(itk.lst_cop, E',') as integer) as ref_cop
								FROM storage.itk WHERE itk.code = $1) AS fo,
						storage.cropoperation cop
						WHERE ref_cop = cop.id) AS foo,
				storage.event ev) as fooo,
		storage.param_event param;



		--SELECT rule from ITK
		INSERT INTO proceduralknowledge.rule
		SELECT DISTINCT rl.*
		FROM
		(SELECT fo.id_itk, fo.ref_cop, cast(regexp_split_to_table(cop.lst_rule, E',') as integer) as ref_rl
				FROM
				(SELECT itk.id as id_itk , cast(regexp_split_to_table(itk.lst_cop, E',') as integer) as ref_cop
						FROM storage.itk WHERE itk.code = $1) AS fo,
				storage.cropoperation cop
				WHERE ref_cop = cop.id) AS foo,
		storage.rule rl
		WHERE rl.id = ref_rl;

		--SELECT unique predicate from ITK
		INSERT INTO proceduralknowledge.predicate
		SELECT DISTINCT pr.*
		FROM
		(SELECT foo.id_itk, foo.ref_cop, foo.ref_rl, cast(regexp_split_to_table(rl.lst_pred, E',') as integer) as ref_pr
				FROM
				(SELECT fo.id_itk, fo.ref_cop, cast(regexp_split_to_table(cop.lst_rule, E',') as integer) as ref_rl
						FROM
						(SELECT itk.id as id_itk , cast(regexp_split_to_table(itk.lst_cop, E',') as integer) as ref_cop
								FROM storage.itk WHERE itk.code = $1) AS fo,
						storage.cropoperation cop
						WHERE ref_cop = cop.id) AS foo,
				storage.rule rl
				WHERE rl.id = ref_rl) AS fooo,
		storage.predicate pr
		WHERE 	pr.id=ref_pr
		ORDER BY id;

		-- param_predicate
		INSERT INTO proceduralknowledge.param_predicate
		SELECT DISTINCT param.*
		FROM
		(SELECT pr.*, cast(regexp_split_to_table(pr.param, E',') as integer) as ref_param
			FROM
			(SELECT foo.id_itk, foo.ref_cop, foo.ref_rl, cast(regexp_split_to_table(rl.lst_pred, E',') as integer) as ref_pr
				FROM
				(SELECT fo.id_itk, fo.ref_cop, cast(regexp_split_to_table(cop.lst_rule, E',') as integer) as ref_rl
					FROM
					(SELECT itk.id as id_itk , cast(regexp_split_to_table(itk.lst_cop, E',') as integer) as ref_cop
						FROM storage.itk WHERE itk.code = $1) AS fo,
					storage.cropoperation cop
					WHERE ref_cop = cop.id) AS foo,
				storage.rule rl
				WHERE rl.id = ref_rl) AS fooo,
			storage.predicate pr
			WHERE 	pr.id=ref_pr) as toto,
		storage.param_predicate param
		WHERE toto.ref_param = param.id;


		INSERT INTO proceduralknowledge.precedence
		SELECT prec.*
		FROM storage.precedence prec, proceduralknowledge.cropoperation cop, proceduralknowledge.cropoperation cop2
		WHERE prec.first_cop = cop.id
		AND prec.second_cop = cop2.id;

END;
$BODY$
LANGUAGE 'plpgsql' VOLATILE
COST 100;


