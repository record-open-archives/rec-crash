-------------------------------------------------
-- Authors: Jérôme Dury
-- Date:

--                function_insert.sql


-- Comments:
-------------------------------------------------

-- custom insert function for the table itkcref
CREATE OR REPLACE FUNCTION proceduralknowledge.insert_itkcref(integer, varchar, varchar, varchar,varchar, character varying, integer, integer)
RETURNS void AS
$BODY$
DECLARE
temp_max_id int;
BEGIN
	SELECT max_id FROM proceduralknowledge.max_id_table WHERE name_table='itkcref' INTO temp_max_id;
	IF (temp_max_id IS NULL) THEN
		temp_max_id :=$1;
	ELSE
		temp_max_id := temp_max_id + 1;
	END IF;
	INSERT INTO proceduralknowledge.itkcref VALUES (temp_max_id,$1,$2,$3,$4,$5,$6,$7,$8);
	INSERT INTO proceduralknowledge.old_new_id VALUES('itkcref',1,temp_max_id);
END;
$BODY$
LANGUAGE 'plpgsql' VOLATILE
COST 100;



-- custom insert function for the table cropoperation
DROP FUNCTION IF EXISTS proceduralknowledge.insert_cropoperation(int , varchar, varchar,varchar,numeric,numeric, numeric, numeric , numeric ,numeric, varchar, varchar);

CREATE OR REPLACE FUNCTION proceduralknowledge.insert_cropoperation(integer, character varying, character varying, character varying, numeric, numeric, numeric, numeric, numeric, numeric, date_start character varying DEFAULT '-inf'::character varying, date_end character varying DEFAULT '+inf'::character varying)
RETURNS void AS
$BODY$
DECLARE
temp_max_id int;
id_itkcref int;
BEGIN
	SELECT max_id FROM proceduralknowledge.max_id_table WHERE name_table='cropoperation' INTO temp_max_id;
	IF (temp_max_id IS NULL) THEN
		temp_max_id :=$1;
	ELSE
		temp_max_id := temp_max_id + $1;
	END IF;
	INSERT INTO proceduralknowledge.cropoperation VALUES (temp_max_id ,$2,$3,$4,$5,$6,$7,$8,$9,$10,date_start,date_end);
	INSERT INTO proceduralknowledge.old_new_id VALUES('cropoperation',$1,temp_max_id);
	SELECT new_id FROM proceduralknowledge.old_new_id WHERE name_table='itkcref' INTO id_itkcref ;
	INSERT INTO proceduralknowledge.l_itkcref_cropoperation VALUES (temp_max_id, id_itkcref);
END;
$BODY$
LANGUAGE 'plpgsql' VOLATILE
COST 100;


-- custom insert function for the table cropoperation_preceedingconstraint
DROP FUNCTION IF EXISTS proceduralknowledge.insert_cropoperation_preceedingconstraint(int ,int, character varying(2), numeric, numeric);

CREATE OR REPLACE FUNCTION proceduralknowledge.insert_cropoperation_preceedingconstraint(int ,int, character varying(2), numeric, numeric)
RETURNS void AS
$BODY$
DECLARE
ref_operation1 int;
ref_operation2 int;
BEGIN
	SELECT new_id FROM proceduralknowledge.old_new_id WHERE name_table='cropoperation' AND old_id=$1 INTO ref_operation1;
	SELECT new_id FROM proceduralknowledge.old_new_id WHERE name_table='cropoperation' AND old_id=$2 INTO ref_operation2;
	INSERT INTO proceduralknowledge.cropoperation_preceedingconstraint VALUES (ref_operation1 ,ref_operation2,$3,$4,$5);
END;
$BODY$
LANGUAGE 'plpgsql' VOLATILE
COST 100;


-- custom insert function for the table operationrule
DROP FUNCTION IF EXISTS proceduralknowledge.insert_operationrule(int, varchar);

CREATE OR REPLACE FUNCTION proceduralknowledge.insert_operationrule(int, varchar) RETURNS void AS
$BODY$
DECLARE
temp_max_id int;
BEGIN
	SELECT max_id FROM proceduralknowledge.max_id_table WHERE name_table='operationrule' INTO temp_max_id;
	IF (temp_max_id IS NULL) THEN
		temp_max_id :=$1;
	ELSE
		temp_max_id := temp_max_id + $1;
	END IF;
	INSERT INTO proceduralknowledge.operationrule VALUES (temp_max_id ,$2);
	INSERT INTO proceduralknowledge.old_new_id VALUES('operationrule',$1,temp_max_id);
END;
$BODY$
LANGUAGE 'plpgsql' VOLATILE;

-- custom insert function for the table l_cropoperation_operationrule
DROP FUNCTION IF EXISTS proceduralknowledge.insert_l_cropoperation_operationrule(int, int);
CREATE OR REPLACE FUNCTION proceduralknowledge.insert_l_cropoperation_operationrule(int, int)
RETURNS void AS
$BODY$
DECLARE
ref_operation int;
ref_rule int;
BEGIN
	SELECT new_id FROM proceduralknowledge.old_new_id WHERE name_table='cropoperation' AND old_id=$1 INTO ref_operation;
	SELECT new_id FROM proceduralknowledge.old_new_id WHERE name_table='operationrule' AND old_id=$2 INTO ref_rule;
	INSERT INTO proceduralknowledge.l_cropoperation_operationrule VALUES (ref_operation ,ref_rule);
END;
$BODY$
LANGUAGE 'plpgsql' VOLATILE
COST 100;



-- custom insert function for the table operationpredicate
DROP FUNCTION IF EXISTS proceduralknowledge.insert_operationpredicate(int, varchar, varchar, varchar);

CREATE OR REPLACE FUNCTION proceduralknowledge.insert_operationpredicate(int, varchar, varchar, varchar) RETURNS void AS
$BODY$
DECLARE
temp_max_id int;
BEGIN
	SELECT max_id FROM proceduralknowledge.max_id_table WHERE name_table='operationpredicate' INTO temp_max_id;
	IF (temp_max_id IS NULL) THEN
		temp_max_id :=$1;
	ELSE
		temp_max_id := temp_max_id + $1;
	END IF;
	INSERT INTO proceduralknowledge.operationpredicate VALUES (temp_max_id ,$2, $3, $4);
	INSERT INTO proceduralknowledge.old_new_id VALUES('operationpredicate',$1,temp_max_id);
END;
$BODY$
LANGUAGE 'plpgsql' VOLATILE;


-- custom insert function for the table l_operationrule_operationpredicate
DROP FUNCTION IF EXISTS proceduralknowledge.insert_l_operationrule_operationpredicate(int, int);
CREATE OR REPLACE FUNCTION proceduralknowledge.insert_l_operationrule_operationpredicate(integer, integer)
RETURNS void AS
$BODY$
DECLARE
ref_predicate int;
ref_rule int;
BEGIN
	SELECT new_id FROM proceduralknowledge.old_new_id WHERE name_table='operationpredicate' AND old_id=$2 INTO ref_predicate;
	SELECT new_id FROM proceduralknowledge.old_new_id WHERE name_table='operationrule' AND old_id=$1 INTO ref_rule;
	INSERT INTO proceduralknowledge.l_operationrule_operationpredicate VALUES (ref_rule, ref_predicate);
END;
$BODY$
LANGUAGE 'plpgsql' VOLATILE
COST 100;



-- custom insert function for the table oprationevent
DROP FUNCTION IF EXISTS proceduralknowledge.insert_operationevent(int,  varchar, varchar, numeric, varchar, numeric, numeric);

CREATE OR REPLACE FUNCTION proceduralknowledge.insert_operationevent(int,  varchar, varchar, numeric, varchar, numeric, numeric) RETURNS void AS
$BODY$
DECLARE
temp_max_id int;
BEGIN
	SELECT max_id FROM proceduralknowledge.max_id_table WHERE name_table='operationevent' INTO temp_max_id;
	IF (temp_max_id IS NULL) THEN
		temp_max_id :=$1;
	ELSE
		temp_max_id := temp_max_id + $1;
	END IF;
	INSERT INTO proceduralknowledge.operationevent VALUES (temp_max_id ,$2,$3,$4,$5,$6,$7);
	INSERT INTO proceduralknowledge.old_new_id VALUES('operationevent',$1,temp_max_id);

END;
$BODY$
LANGUAGE 'plpgsql' VOLATILE;

-- custom insert function for the table l_operation
DROP FUNCTION IF EXISTS proceduralknowledge.insert_l_operationevent(int, int);
CREATE OR REPLACE FUNCTION proceduralknowledge.insert_l_operationevent(integer, integer)
RETURNS void AS
$BODY$
DECLARE
ref_operation int;
ref_event int;
BEGIN
	SELECT new_id FROM proceduralknowledge.old_new_id WHERE name_table='cropoperation' AND old_id=$2 INTO ref_operation;
	SELECT new_id FROM proceduralknowledge.old_new_id WHERE name_table='operationevent' AND old_id=$1 INTO ref_event;
	INSERT INTO proceduralknowledge.l_operationevent VALUES (ref_operation, ref_event);
END;
$BODY$
LANGUAGE 'plpgsql' VOLATILE
COST 100;

