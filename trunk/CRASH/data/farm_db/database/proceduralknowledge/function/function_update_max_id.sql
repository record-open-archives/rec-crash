-------------------------------------------------
-- Authors: Jérôme Dury
-- Date:

--                update_max_id.sql


-- Comments:
-------------------------------------------------


DROP FUNCTION IF EXISTS proceduralknowledge.update_max_id()CASCADE;

CREATE OR REPLACE FUNCTION proceduralknowledge.update_max_id()  RETURNS VOID AS
$BODY$
DECLARE temp_max_id int;
BEGIN
    SELECT max(id) FROM proceduralknowledge.itkcref INTO temp_max_id;
    UPDATE proceduralknowledge.max_id_table SET max_id = temp_max_id  WHERE name_table='itkcref';

    SELECT max(id) FROM proceduralknowledge.cropoperation INTO temp_max_id;
    UPDATE proceduralknowledge.max_id_table SET max_id = temp_max_id  WHERE name_table='cropoperation';

    SELECT max(id) FROM proceduralknowledge.operationrule INTO temp_max_id;
    UPDATE proceduralknowledge.max_id_table SET max_id = temp_max_id  WHERE name_table='operationrule';

    SELECT max(id) FROM proceduralknowledge.operationpredicate INTO temp_max_id;
    UPDATE proceduralknowledge.max_id_table SET max_id = temp_max_id WHERE name_table='operationpredicate';

    SELECT max(id) FROM proceduralknowledge.operationevent INTO temp_max_id;
    UPDATE proceduralknowledge.max_id_table SET max_id = temp_max_id  WHERE name_table='operationevent';

END;
$BODY$
LANGUAGE plpgsql;
