-------------------------------------------------
-- Authors: Pierre Casel Jérôme Dury
-- Date: 

--                max_id.sql


-- Comments: 
-------------------------------------------------

--table max_id_table
DROP TABLE IF EXISTS proceduralknowledge.max_id_table;
CREATE TABLE proceduralknowledge.max_id_table
    (name_table varchar,
    max_id int);
    
 INSERT INTO proceduralknowledge.max_id_table VALUES ('itkcref', 0); 
 INSERT INTO proceduralknowledge.max_id_table VALUES ('cropoperation', 0);
 INSERT INTO proceduralknowledge.max_id_table VALUES ('operationevent', 0); 
 INSERT INTO proceduralknowledge.max_id_table VALUES ('operationrule', 0); 
 INSERT INTO proceduralknowledge.max_id_table VALUES ('operationpredicate', 0); 
  INSERT INTO proceduralknowledge.max_id_table VALUES ('cropoperation_preceedingconstraint', 0);

--table old_new_id
DROP TABLE IF EXISTS proceduralknowledge.old_new_id CASCADE;
CREATE TABLE proceduralknowledge.old_new_id(name_table varchar, old_id int, new_id int);
