-------------------------------------------------
-- Authors: Jérôme Dury
-- Date: 2010-11-03

--               itkcref.sql


-- Comments: Tables used to store ITKcref
-------------------------------------------------

-- Table: ITKcref
-- note: Table crop must  be created before
DROP TABLE IF EXISTS  storage.itk CASCADE;

CREATE TABLE storage.itk
(
	id int NOT NULL UNIQUE PRIMARY KEY,
	code_crop varchar REFERENCES storage.crop,
	code character varying,
	domain_variety character varying,
	domain_soiltype character varying,
	type_soil character varying,
	productionsystem character varying,
	description character varying,
	code_prec varchar,
	code_suc varchar,
	lst_cop varchar
);

DROP TABLE IF EXISTS  proceduralknowledge.itk CASCADE;
CREATE TABLE proceduralknowledge.itk AS TABLE storage.itk;
ALTER TABLE proceduralknowledge.itk ALTER COLUMN id SET NOT NULL;
ALTER TABLE proceduralknowledge.itk ADD PRIMARY KEY (id);
ALTER TABLE proceduralknowledge.itk ADD FOREIGN KEY (code_crop) REFERENCES expertknowledge.crop;



-- Table: cropoperation
DROP TABLE IF EXISTS  storage.cropoperation CASCADE;
CREATE TABLE storage.cropoperation
(
	id serial NOT NULL UNIQUE PRIMARY KEY,
	name character varying,
	type character varying,
	equipment character varying,
	st character varying DEFAULT '-inf',
	nd character varying DEFAULT '+inf',
	event int,
	lst_rule varchar
);
DROP TABLE IF EXISTS  proceduralknowledge.cropoperation CASCADE;
CREATE TABLE proceduralknowledge.cropoperation AS TABLE storage.cropoperation;
ALTER TABLE proceduralknowledge.cropoperation ALTER COLUMN id SET NOT NULL;
ALTER TABLE proceduralknowledge.cropoperation ADD PRIMARY KEY (id);


-- Table: rule
DROP TABLE IF EXISTS  storage.rule CASCADE;
CREATE TABLE storage.rule
(
	id int NOT NULL UNIQUE PRIMARY KEY,
	name character varying,
	lst_pred varchar
);
DROP TABLE IF EXISTS  proceduralknowledge.rule CASCADE;
CREATE TABLE proceduralknowledge.rule AS TABLE storage.rule;
ALTER TABLE proceduralknowledge.rule ALTER COLUMN id SET NOT NULL;
ALTER TABLE proceduralknowledge.rule ADD  PRIMARY KEY (id);

-- Table: predicate
DROP TABLE IF EXISTS  storage.predicate CASCADE;
CREATE TABLE storage.predicate
(
	id int NOT NULL UNIQUE PRIMARY KEY,
	name character varying,
	predicate character varying,
	param varchar,
	crash_pred character varying
);
DROP TABLE IF EXISTS  proceduralknowledge.predicate CASCADE;
CREATE TABLE proceduralknowledge.predicate AS TABLE storage.predicate;
ALTER TABLE proceduralknowledge.predicate ALTER COLUMN id SET NOT NULL;
ALTER TABLE proceduralknowledge.predicate ADD  PRIMARY KEY (id);

-- Table: param_event
DROP TABLE IF EXISTS  storage.param_predicate CASCADE;
CREATE TABLE storage.param_predicate
(
	id int,
	name character varying,
	type character varying,
	value varchar,
	unit varchar,
    PRIMARY KEY (id)
);
DROP TABLE IF EXISTS  proceduralknowledge.param_predicate CASCADE;
CREATE TABLE proceduralknowledge.param_predicate  AS TABLE storage.param_predicate;
ALTER TABLE proceduralknowledge.param_predicate ADD PRIMARY KEY (id);


DROP TABLE IF EXISTS  storage.precedence;
CREATE TABLE storage.precedence
(
	first_cop serial,
	second_cop serial,
	type character(2),
	tmin numeric DEFAULT 0, -- unité de temps en jours
	tmax numeric DEFAULT 99999, -- unité de temps en jours
	FOREIGN KEY (first_cop)
	REFERENCES storage.cropoperation
	ON UPDATE CASCADE ON DELETE CASCADE,
	FOREIGN KEY (second_cop)
	REFERENCES storage.cropoperation
	ON UPDATE CASCADE ON DELETE CASCADE
);
DROP TABLE IF EXISTS  proceduralknowledge.precedence CASCADE;
CREATE TABLE proceduralknowledge.precedence  AS TABLE storage.precedence;
ALTER TABLE proceduralknowledge.precedence ADD PRIMARY KEY (first_cop, second_cop);



-- Table: event
DROP TABLE IF EXISTS  storage.event CASCADE;
CREATE TABLE storage.event
(
	id int,
	name character varying,
	param character varying,
	crash_ev varchar,
    PRIMARY KEY (id)
);
DROP TABLE IF EXISTS  proceduralknowledge.event CASCADE;
CREATE TABLE proceduralknowledge.event  AS TABLE storage.event;
ALTER TABLE proceduralknowledge.event ADD PRIMARY KEY (id);

-- Table: param_event
DROP TABLE IF EXISTS  storage.param_event CASCADE;
CREATE TABLE storage.param_event
(
	id int,
	name character varying,
	type character varying,
	value varchar,
	unit varchar,
    PRIMARY KEY (id)
);
DROP TABLE IF EXISTS  proceduralknowledge.param_event CASCADE;
CREATE TABLE proceduralknowledge.param_event  AS TABLE storage.param_event;
ALTER TABLE proceduralknowledge.param_event ADD PRIMARY KEY (id);
