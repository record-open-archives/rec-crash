
-- insertcrop
INSERT INTO expertknowledge.crop  
    (SELECT * FROM storage.crop WHERE
        code_crop = 'BH' OR
	code_crop = 'OP' OR
	code_crop = 'CH' OR
	code_crop = 'PP' 
--	 OR code_crop = 'MA'
    );


INSERT INTO expertknowledge.cropvariety  
    (SELECT * FROM storage.cropvariety WHERE
        code_variety = 'BH1' OR
	code_variety = 'OP1' OR
	code_variety = 'CH1' OR
	code_variety = 'PP1' 
--	 OR code_variety = 'MA1'
    );	



-- Insert crop sucession
INSERT INTO expertknowledge.cropsuccession
SELECT foo.* 
FROM (SELECT cropsuccession.* 
        FROM storage.cropsuccession, expertknowledge.crop  
        WHERE crop.id_crop = cropsuccession.ref_preceeding ) foo, 
      expertknowledge.crop
WHERE crop.id_crop = foo.ref_crop;

-- Insert soil type
DELETE FROM expertknowledge.soiltype;
INSERT INTO expertknowledge.soiltype  
    (SELECT * FROM storage.soiltype WHERE
        id_soiltype = 1 
    );
 
---------------------------------------------------------
--  ITK
---------------------------------------------------------
INSERT INTO proceduralknowledge.itkcref VALUES (1,1, 'Intensive','BH',1,1);
INSERT INTO proceduralknowledge.itkcref VALUES (2,2, 'Intensive','OP',1,1);
INSERT INTO proceduralknowledge.itkcref VALUES (3,3, 'Intensive','CH',1,1);
INSERT INTO proceduralknowledge.itkcref VALUES (4,4, 'Intensive','PP',1,1);
--INSERT INTO proceduralknowledge.itkcref VALUES (5,5, 'Intensive','MA',1,1);
------------------------------------------------------
-- insert farmland
-----------------------------------------------------
--capislet
--I1
INSERT INTO systemknowledge.capislet(id_capislet, the_geom)
	VALUES ('1', 'POLYGON (( 20 20, 20 520, 820 520, 820 20, 20 20 ))');
--- - --I2
-- INSERT INTO systemknowledge.capislet(id_capislet, the_geom)
--        VALUES ('2','POLYGON (( 860 20, 860 520, 1360 520, 1360 20, 860 20 ))');
--- - --I3
-- INSERT INTO systemknowledge.capislet(id_capislet,  the_geom)
--        VALUES ('3','POLYGON (( 860 -20, 860 -520, 1360 -520, 1360 -20, 860 -20 ))');
-- -- --I4
-- INSERT INTO systemknowledge.capislet(id_capislet, the_geom)
--        VALUES ('4','POLYGON (( 20 -1320, 20 -820, 320 -820, 320 -1320, 20 -1320 ))');
-- -- --I5
-- INSERT INTO systemknowledge.capislet(id_capislet, the_geom)
--        VALUES ('5','POLYGON (( 360 -1220, 360 -820, 610 -820, 610 -1220, 360 -1220 ))');
-- -- --I6
-- INSERT INTO systemknowledge.capislet(id_capislet, the_geom)
--        VALUES ('6','POLYGON (( 2000 -1320, 2000  -920, 2250 -920, 2250 -1320, 2000  -1320 ))');

-- CroppingBlock
INSERT INTO systemknowledge.croppingblock 
    (SELECT id_capislet as id_croppingblock, the_geom FROM systemknowledge.capislet );

-- plotunits
--I1
INSERT INTO systemknowledge.plotunit(id_plotunit,the_geom)
	VALUES ('1', 'POLYGON (( 20 20, 20 520, 420 520, 420 20, 20 20 ))');
INSERT INTO systemknowledge.plotunit(id_plotunit,the_geom)
	VALUES ('2', 'POLYGON (( 420 20, 420 520, 820 520, 820 20, 420 20 ))');
-- -- --I2
-- INSERT INTO systemknowledge.plotunit(id_plotunit, the_geom)
--        VALUES ('3','POLYGON (( 860 20, 860 520, 1110 520, 1110 20, 860 20 ))');
-- INSERT INTO systemknowledge.plotunit(id_plotunit, the_geom)
--        VALUES ('4','POLYGON (( 1110 20, 1110 520, 1360 520, 1360 20, 1110 20 ))');
-- -- --I3
-- INSERT INTO systemknowledge.plotunit(id_plotunit, the_geom)
--        VALUES ('5','POLYGON (( 860 -20, 860 -520, 1110 -520, 1110 -20, 860 -20 ))');
-- INSERT INTO systemknowledge.plotunit(id_plotunit, the_geom)
--        VALUES ('6','POLYGON (( 1110 -20, 1110 -520, 1360 -520, 1360 -20, 1110 -20 ))');
-- -- --I4
-- INSERT INTO systemknowledge.plotunit(id_plotunit, the_geom)
--        VALUES ('7','POLYGON (( 20 -1320, 20 -820, 320 -820, 320 -1320, 20 -1320 ))');
-- -- --I5
-- INSERT INTO systemknowledge.plotunit(id_plotunit, the_geom)
--        VALUES ('8','POLYGON (( 360 -1220, 360 -820, 610 -820, 610 -1220, 360 -1220 ))');
-- -- --I6
-- INSERT INTO systemknowledge.plotunit(id_plotunit, the_geom)
--        VALUES ('9','POLYGON (( 2000 -1320, 2000  -920, 2250 -920, 2250 -1320, 2000  -1320 ))');


-- gridunit
--I1
INSERT INTO systemknowledge.gridunit(id_gridunit, the_geom)
	VALUES ('1', 'POLYGON (( 20 20, 20 520, 153.3 520, 153.3 20, 20 20 ))');
INSERT INTO systemknowledge.gridunit(id_gridunit, the_geom)
	VALUES ('2', 'POLYGON (( 153.3 20, 153.3 520, 286.6 520, 286.6 20, 153.3 20 ))');	
INSERT INTO systemknowledge.gridunit(id_gridunit, the_geom)
	VALUES ('3', 'POLYGON (( 286.6 20, 286.6 520, 420 520, 420 20, 286.6 20 ))');		
INSERT INTO systemknowledge.gridunit(id_gridunit, the_geom)
	VALUES ('4', 'POLYGON (( 420 20, 420 520, 553.3 520, 553.3 20, 420 20 ))');	
INSERT INTO systemknowledge.gridunit(id_gridunit, the_geom)
	VALUES ('5', 'POLYGON (( 553.3 20, 553.3 520, 686.6 520, 686.6 20, 553.3 20 ))');	
INSERT INTO systemknowledge.gridunit(id_gridunit, the_geom)
	VALUES ('6', 'POLYGON (( 686.6 20, 686.6 520, 820 520, 820 20, 686.6 20 ))');
--I2
INSERT INTO systemknowledge.gridunit(id_gridunit, the_geom)
       VALUES ('7','POLYGON (( 860 20, 860 260, 1110 260, 1110 20, 860 20 ))');
INSERT INTO systemknowledge.gridunit(id_gridunit, the_geom)
       VALUES ('8','POLYGON (( 860 260, 860 520, 1110 520, 1110 260, 860 260 ))');
INSERT INTO systemknowledge.gridunit(id_gridunit, the_geom)
       VALUES ('9','POLYGON (( 1110 20, 1110 260, 1360 260, 1360 20, 1110 20 ))');
INSERT INTO systemknowledge.gridunit(id_gridunit, the_geom)   
        VALUES ('10','POLYGON (( 1110 260, 1110 520, 1360 520, 1360 260, 1110 260 ))');
-- -- --I3
-- INSERT INTO systemknowledge.gridunit(id_gridunit,the_geom)
--        VALUES ('11','POLYGON (( 860 -20, 860 -520, 1110 -520, 1110 -20, 860 -20 ))');
-- INSERT INTO systemknowledge.gridunit(id_gridunit,the_geom)
--        VALUES ('12','POLYGON (( 1110 -20, 1110 -520, 1360 -520, 1360 -20, 1110 -20 ))');
-- -- --I4
-- INSERT INTO systemknowledge.gridunit(id_gridunit,the_geom)
--        VALUES ('13','POLYGON (( 20 -1320, 20 -820, 320 -820, 320 -1320, 20 -1320 ))');
-- -- --I5
-- INSERT INTO systemknowledge.gridunit(id_gridunit,the_geom)
--        VALUES ('14','POLYGON (( 360 -1220, 360 -820, 610 -820, 610 -1220, 360 -1220 ))');
-- -- --I6
-- INSERT INTO systemknowledge.gridunit(id_gridunit,the_geom)
--        VALUES ('15','POLYGON (( 2000 -1320, 2000  -920, 2250 -920, 2250 -1320, 2000  -1320 ))');
       

-- soilunit
-- --I1
INSERT INTO systemknowledge.soilunit(id_soilunit, ref_soiltype, the_geom)
    VALUES ('1',1, 'POLYGON (( 20 20, 20 520, 820 520, 820 20, 20 20 ))');
-- INSERT INTO systemknowledge.soilunit(id_soilunit, ref_soiltype, the_geom)
--     VALUES ('2',2, 'POLYGON (( 20 260, 20 520, 420 520, 20 260 ))');
-- INSERT INTO systemknowledge.soilunit(id_soilunit, ref_soiltype, the_geom)
--     VALUES ('3',2, 'POLYGON (( 420 520, 820 520, 820 20, 620 260, 420 520 ))');
-- -- --I2
-- INSERT INTO systemknowledge.soilunit(id_soilunit, ref_soiltype, the_geom)
--        VALUES ('4',2,'POLYGON (( 860 20, 860 520, 1360 520, 1360 20, 860 20 ))');
-- -- --I3
-- INSERT INTO systemknowledge.soilunit(id_soilunit,ref_soiltype, the_geom)
--        VALUES ('5',1,'POLYGON (( 860 -20, 860 -520, 1360 -520, 860 -20 ))');
-- INSERT INTO systemknowledge.soilunit(id_soilunit, ref_soiltype, the_geom)
--        VALUES ('6',2,'POLYGON (( 860 -20, 1360 -520, 1360 -20, 860 -20 ))');
-- -- --I4
-- INSERT INTO systemknowledge.soilunit(id_soilunit,ref_soiltype, the_geom)
-- VALUES ('7',1,'POLYGON (( 20 -1320, 20 -820, 320 -820, 320 -1320, 20 -1320 ))');
-- -- --I5
-- INSERT INTO systemknowledge.soilunit(id_soilunit, ref_soiltype, the_geom)
-- VALUES ('16',1,'POLYGON (( 360 -1220, 360 -820, 610 -820, 610 -1220, 360 -1220 ))');
-- -- --I6
-- INSERT INTO systemknowledge.soilunit(id_soilunit, ref_soiltype, the_geom)
--        VALUES ('17',1,'POLYGON (( 2000 -1320, 2000  -920, 2250 -920, 2250 -1320, 2000  -1320 ))');


-- landunit
 -- Extract the linework from the polygons (you may wish to add any interior rings to this query). 
	DROP TABLE IF EXISTS systemknowledge.all_lines CASCADE;
	CREATE TABLE systemknowledge.all_lines AS
	(SELECT Boundary(the_geom) AS the_geom
	FROM systemknowledge.plotunit
	UNION ALL
	SELECT Boundary(the_geom) AS the_geom
	FROM systemknowledge.gridunit
	UNION ALL
 	SELECT Boundary(the_geom) AS the_geom
 	FROM systemknowledge.soilunit);
	-- Node the linework: 
	DROP TABLE IF EXISTS systemknowledge.noded_lines CASCADE;
	CREATE TABLE systemknowledge.noded_lines AS
	SELECT St_Union(the_geom) AS the_geom
	FROM systemknowledge.all_lines;
	-- Re-polygonize all linework:
	DROP TABLE IF EXISTS systemknowledge.new_polys CASCADE;
	CREATE TABLE systemknowledge.new_polys (id serial PRIMARY KEY, the_geom geometry);
	INSERT INTO systemknowledge.new_polys (the_geom)
	SELECT geom AS the_geom
	FROM St_Dump((
	 SELECT St_Polygonize(the_geom) AS the_geom
	 FROM systemknowledge.noded_lines
	));
	-- UPDATE systemknowledge.new_polys SET the_geom  = ST_SetSRID(the_geom, 27572);

	-- Remove polgyon build from holes
	DELETE FROM systemknowledge.new_polys a
	USING (SELECT st_union(the_geom) FROM systemknowledge.plotunit) i
	WHERE NOT contains(i.st_union, pointonsurface(a.the_geom));
	-- Transfer attributes from original polygons to newly formed polygons
	--     * Create point-in-polygon features. 
	DROP TABLE IF EXISTS systemknowledge.new_polys_pip CASCADE;
	CREATE TABLE systemknowledge.new_polys_pip AS
	SELECT id, ST_PointOnSurface(the_geom) AS the_geom
	FROM systemknowledge.new_polys;
	--    * Overlay points with original polygons, transferring the attributes. 
	DROP TABLE IF EXISTS systemknowledge.pip_with_attributes CASCADE;
	CREATE TABLE systemknowledge.pip_with_attributes AS
	SELECT a.id, id_gridunit, id_plotunit,id_soilunit FROM systemknowledge.new_polys_pip a
	LEFT JOIN systemknowledge.plotunit p ON St_Within(a.the_geom, p.the_geom)
	LEFT JOIN systemknowledge.gridunit g ON St_Within(a.the_geom, g.the_geom)
	LEFT JOIN systemknowledge.soilunit s ON St_Within(a.the_geom, s.the_geom);
	--   * Join the points to the newly formed polygons, transferring the attributes: 
	DROP TABLE IF EXISTS systemknowledge.landunit CASCADE;
	CREATE TABLE systemknowledge.landunit AS
	(SELECT id_gridunit as ref_gridunit , id_plotunit as ref_plotunit, id_soilunit as ref_soilunit, the_geom
	FROM systemknowledge.new_polys a LEFT JOIN systemknowledge.pip_with_attributes b USING (id)
	ORDER BY ref_plotunit);
	ALTER TABLE systemknowledge.landunit ADD COLUMN id_landunit serial;
    --  Drop temporary table
	DROP TABLE IF EXISTS systemknowledge.all_lines;
	DROP TABLE IF EXISTS systemknowledge.noded_lines;
	DROP TABLE IF EXISTS systemknowledge.new_polys;
	DROP TABLE IF EXISTS systemknowledge.new_polys_pip;
	DROP TABLE IF EXISTS systemknowledge.pip_with_attributes;


-- water Ressource
INSERT INTO systemknowledge.waterresource VALUES (1, 300, 25);

--farmland
INSERT INTO systemknowledge.farmland 
(SELECT DISTINCT
 	id_landunit, 
	ref_plotunit, 
	(SELECT id_croppingblock FROM systemknowledge.croppingblock WHERE intersects(croppingblock.the_geom, landunit.the_geom)) as ref_croppingblock,
	(SELECT id_croppingblock FROM systemknowledge.croppingblock WHERE intersects(croppingblock.the_geom, landunit.the_geom)) as ref_waterresource,
	(SELECT name_soiltype FROM expertknowledge.soiltype WHERE ref_soiltype = id_soiltype) as name_soiltype,
	landunit.the_geom		
FROM systemknowledge.croppingblock, systemknowledge.soilunit, systemknowledge.landunit WHERE landunit.ref_soilunit = soilunit.id_soilunit);
--------------------------------------------------------------------------


-- Insert Cropping plan
INSERT INTO systemknowledge.croppingplan VALUES (1,1,2006,'BH','BH1',1);
INSERT INTO systemknowledge.croppingplan VALUES (2,1,2006,'BH','BH1',1);
INSERT INTO systemknowledge.croppingplan VALUES (3,1,2006,'BH','BH1',1);
INSERT INTO systemknowledge.croppingplan VALUES (4,3,2006,'CH','CH1',3);
INSERT INTO systemknowledge.croppingplan VALUES (5,4,2006,'PP','PP1',4);
INSERT INTO systemknowledge.croppingplan VALUES (6,2,2006,'OP','OP1',2);

INSERT INTO systemknowledge.croppingplan VALUES (1,3,2007,'CH','CH1',3);
INSERT INTO systemknowledge.croppingplan VALUES (2,4,2007,'PP','PP1',4);
INSERT INTO systemknowledge.croppingplan VALUES (3,2,2007,'OP','OP1',2);
INSERT INTO systemknowledge.croppingplan VALUES (4,1,2007,'BH','BH1',1);
INSERT INTO systemknowledge.croppingplan VALUES (5,1,2007,'BH','BH1',1);
INSERT INTO systemknowledge.croppingplan VALUES (6,1,2007,'BH','BH1',1);

INSERT INTO systemknowledge.croppingplan VALUES (1,1,2008,'BH','BH1',1);
INSERT INTO systemknowledge.croppingplan VALUES (2,1,2008,'BH','BH1',1);
INSERT INTO systemknowledge.croppingplan VALUES (3,1,2008,'BH','BH1',1);
INSERT INTO systemknowledge.croppingplan VALUES (4,2,2008,'OP','CH1',2);
INSERT INTO systemknowledge.croppingplan VALUES (5,3,2008,'CH','PP1',3);
INSERT INTO systemknowledge.croppingplan VALUES (6,4,2008,'PP','OP1',4);

INSERT INTO systemknowledge.croppingplan VALUES (1,2,2009,'OP','OP1',2);
INSERT INTO systemknowledge.croppingplan VALUES (2,3,2009,'CH','CH1',3);
INSERT INTO systemknowledge.croppingplan VALUES (3,4,2009,'PP','PP1',4);
INSERT INTO systemknowledge.croppingplan VALUES (4,1,2009,'BH','BH1',1);
INSERT INTO systemknowledge.croppingplan VALUES (5,1,2009,'BH','BH1',1);
INSERT INTO systemknowledge.croppingplan VALUES (6,1,2009,'BH','BH1',1);

INSERT INTO systemknowledge.croppingplan VALUES (1,1,2010,'BH','BH1',1);
INSERT INTO systemknowledge.croppingplan VALUES (2,1,2010,'BH','BH1',1);
INSERT INTO systemknowledge.croppingplan VALUES (3,1,2010,'BH','BH1',1);
INSERT INTO systemknowledge.croppingplan VALUES (4,2,2010,'PP','PH1',4);
INSERT INTO systemknowledge.croppingplan VALUES (5,3,2010,'OP','OP1',2);
INSERT INTO systemknowledge.croppingplan VALUES (6,4,2010,'CH','CH1',3);




