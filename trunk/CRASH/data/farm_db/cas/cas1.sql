INSERT INTO expertknowledge.crop
    (SELECT * FROM storage.crop WHERE
        code = 'MA');

INSERT INTO expertknowledge.cropvariety
	(SELECT * FROM storage.cropvariety WHERE
    	code_crop = 'MA' AND code = 'hybrid_p');

-- Insert crop sucession
INSERT INTO expertknowledge.cropsuccession
SELECT foo.*
FROM (SELECT cropsuccession.*
        FROM storage.cropsuccession, expertknowledge.crop
        WHERE crop.code = cropsuccession.code_preceeding ) foo,
      expertknowledge.crop
WHERE crop.code = foo.code_crop;


-- Insert soil type
DELETE FROM expertknowledge.soiltype;

INSERT INTO expertknowledge.soiltype
    (SELECT * FROM storage.soiltype WHERE
        id_soiltype = 3
    );

-- water Ressource
INSERT INTO systemknowledge.waterresource VALUES (1, 300, 25);

-- insert landunit_simple
INSERT INTO systemknowledge.landunit VALUES (1,'POLYGON (( 20 20, 20 520, 410 520, 410 20, 20 20 ))');
INSERT INTO systemknowledge.landunit VALUES (2,'POLYGON (( 410 20, 410 520, 820 520, 820 20, 410 20 ))');
INSERT INTO systemknowledge.landunit VALUES (3,'POLYGON (( 860 20, 860 520, 1360 520, 1360 20, 860 20 ))');

-- insert plotunit
INSERT INTO systemknowledge.plotunit VALUES (1,'POLYGON (( 20 20, 20 520, 820 520, 820 20, 20 20 ))');
INSERT INTO systemknowledge.plotunit VALUES (2,'POLYGON (( 860 20, 860 520, 1360 520, 1360 20, 860 20 ))');

-- insert ilset
INSERT INTO systemknowledge.capislet VALUES (1,1,'POLYGON (( 20 20, 20 520, 820 520, 820 20, 20 20 ))');
INSERT INTO systemknowledge.capislet VALUES (2,1,'POLYGON (( 860 20, 860 520, 1360 520, 1360 20, 860 20 ))');


-- insert cropingblock
INSERT INTO systemknowledge.croppingblock(id_croppingblock, the_geom)
	VALUES ('1', 'MULTIPOLYGON (((20 20, 20 520, 820 520, 820 20, 20 20 )),(( 860 20, 860 520, 1360 520, 1360 20, 860 20)))');

-- insert soilunit
INSERT INTO systemknowledge.soilunit VALUES (1,'S1','POLYGON (( 20 20, 20 520, 410 520, 410 20, 20 20 ))');
INSERT INTO systemknowledge.soilunit VALUES (2,'S2','POLYGON (( 410 20, 410 520, 820 520, 820 20, 410 20 ))');
INSERT INTO systemknowledge.soilunit VALUES (3,'S1','POLYGON (( 860 20, 860 520, 1360 520, 1360 20, 860 20 ))');

-- insert into FARMLAND
INSERT INTO systemknowledge.farmland
SELECT lu.id_landunit,cb.id_croppingblock as ref_cb, ci.ref_wr as ref_wr, su.code_soiltype, lu.the_geom  FROM systemknowledge.landunit lu
	LEFT JOIN systemknowledge.croppingblock cb ON St_Within(lu.the_geom, cb.the_geom)
	LEFT JOIN systemknowledge.capislet ci ON St_Within(lu.the_geom, ci.the_geom)
	LEFT JOIN systemknowledge.soilunit su ON St_Within(lu.the_geom, su.the_geom);

-- Ajout de la culture mais et des ces variété
DELETE FROM proceduralknowledge.predicate;
DELETE FROM proceduralknowledge.rule;
DELETE FROM proceduralknowledge.event;
DELETE FROM proceduralknowledge.precedence;
DELETE FROM proceduralknowledge.cropoperation;
DELETE FROM proceduralknowledge.itk;

--------------------------------------------------
--                  ITK MA
--------------------------------------------------
SELECT * FROM proceduralknowledge.copy_itk('MA_1');


-------
-- Ajout du cropping plan
INSERT INTO systemknowledge.cropplan VALUES (1,2008,'MA','1','MA_hybrid_precoce');
INSERT INTO systemknowledge.cropplan VALUES (2,2008,'MA','1','MA_hybrid_precoce');



