
-- insertcrop
INSERT INTO expertknowledge.crop  
    (SELECT * FROM storage.crop WHERE
    	code_crop = 'MA' OR
        code_crop = 'BH' OR
	code_crop = 'OP' OR
	code_crop = 'CH' OR
	code_crop = 'PP' 
    );


INSERT INTO expertknowledge.cropvariety  
    (SELECT * FROM storage.cropvariety WHERE
    	code_variety = 'MA1' OR
        code_variety = 'BH1' OR
	code_variety = 'OP1' OR
	code_variety = 'CH1' OR
	code_variety = 'PP1' 
    );



-- Insert crop sucession
INSERT INTO expertknowledge.cropsuccession
SELECT foo.* 
FROM (SELECT cropsuccession.* 
        FROM storage.cropsuccession, expertknowledge.crop  
        WHERE crop.id_crop = cropsuccession.ref_preceeding ) foo, 
      expertknowledge.crop
WHERE crop.id_crop = foo.ref_crop;

-- Insert soil type
DELETE FROM expertknowledge.soiltype;

INSERT INTO expertknowledge.soiltype  
    (SELECT * FROM storage.soiltype WHERE
        id_soiltype = 1 OR
        id_soiltype = 2
    );

---------------------------------------------------------
--  ITK
---------------------------------------------------------
INSERT INTO proceduralknowledge.itkcref VALUES (1,1, 'Intensive','BH',1,1);
INSERT INTO proceduralknowledge.itkcref VALUES (2,2, 'Intensive','OP',1,1);
INSERT INTO proceduralknowledge.itkcref VALUES (3,3, 'Intensive','CH',1,1);
INSERT INTO proceduralknowledge.itkcref VALUES (4,4, 'Intensive','PP',1,1);

------------------------------------------------------
-- insert farmland
-----------------------------------------------------
--capislet
--I1
INSERT INTO systemknowledge.capislet(id_capislet, ref_wr, the_geom)
	VALUES (1,1, 'POLYGON (( 20 20, 20 620, 820 620, 820 20, 20 20 ))');
-- --I2
INSERT INTO systemknowledge.capislet(id_capislet, ref_wr, the_geom)
       VALUES (2,1,'POLYGON (( 860 20, 860 620, 1460 620, 1460 20, 860 20 ))');
--I3
INSERT INTO systemknowledge.capislet(id_capislet, ref_wr,  the_geom)
       VALUES (3,1,'POLYGON (( 860 -20, 860 -620, 1460 -620, 1460 -20, 860 -20 ))');
--I4
INSERT INTO systemknowledge.capislet(id_capislet, ref_wr, the_geom)
       VALUES (4,1,'POLYGON (( -80 -1420, -80 -820, 320 -820, 320 -1420, -80 -1420 ))');
--I5
INSERT INTO systemknowledge.capislet(id_capislet, ref_wr, the_geom)
       VALUES (5,1,'POLYGON (( 360 -1420, 360 -820, 760 -820, 760 -1420, 360 -1420 ))');
--I6
INSERT INTO systemknowledge.capislet(id_capislet, ref_wr, the_geom)
       VALUES (6,1,'POLYGON (( 2000 -1520, 2000  -920, 2200 -920, 2200 -1520, 2000  -1520 ))');



-- Cropping Block
--I1
INSERT INTO systemknowledge.croppingblock(id_croppingblock, the_geom)
	VALUES (1, 'MULTIPOLYGON ((( 20 20, 20 620, 820 620, 820 20, 20 20 )))');
-- --I2
INSERT INTO systemknowledge.croppingblock(id_croppingblock, the_geom)
       VALUES (2,'MULTIPOLYGON ((( 860 20, 860 620, 1460 620, 1460 20, 860 20 )))');
--I3
INSERT INTO systemknowledge.croppingblock(id_croppingblock,  the_geom)
       VALUES (3,'MULTIPOLYGON ((( 860 -20, 860 -620, 1460 -620, 1460 -20, 860 -20 )))');
--I4

INSERT INTO systemknowledge.croppingblock(id_croppingblock,  the_geom)
       VALUES (4,'MULTIPOLYGON((( -80 -1420, -80 -820, 320 -820, 320 -1420, -80 -1420 )),(( 360 -1420, 360 -820, 760 -820, 760 -1420, 360 -1420 )),(( 2000 -1520, 2000  -920, 2200 -920, 2200 -1520, 2000  -1520 )))');


-- plotunits
--I1
INSERT INTO systemknowledge.plotunit(id_plotunit,the_geom)
	VALUES ('1','POLYGON (( 20 20, 20 620, 420 620, 420 20, 20 20 ))');
INSERT INTO systemknowledge.plotunit(id_plotunit,the_geom)
	VALUES ('2', 'POLYGON (( 420 20, 420 620, 820 620, 820 20, 420 20 ))');
--I2
INSERT INTO systemknowledge.plotunit(id_plotunit, the_geom)
       VALUES ('3', 'POLYGON (( 860 20, 860 620, 1260 620, 1260 20, 860 20 ))');
INSERT INTO systemknowledge.plotunit(id_plotunit, the_geom)
       VALUES ('4', 'POLYGON (( 1260 20, 1260 620, 1460 620, 1460 20, 1260 20 ))');
--I3
INSERT INTO systemknowledge.plotunit(id_plotunit, the_geom)
       VALUES ('5', 'POLYGON (( 860 -20, 860 -620, 1060 -620, 1060 -20, 860 -20 ))');
INSERT INTO systemknowledge.plotunit(id_plotunit, the_geom)
       VALUES ('6', 'POLYGON (( 1060 -20, 1060 -620, 1460 -620, 1460 -20, 1060 -20 ))');
--I4
INSERT INTO systemknowledge.plotunit(id_plotunit, the_geom)
       VALUES ('7', 'POLYGON (( -80 -1420, -80 -820, 320 -820, 320 -1420, -80 -1420 ))');
--I5
INSERT INTO systemknowledge.plotunit(id_plotunit, the_geom)
       VALUES ('8', 'POLYGON (( 360 -1420, 360 -820, 760 -820, 760 -1420, 360 -1420 ))');
--I6
INSERT INTO systemknowledge.plotunit(id_plotunit, the_geom)
       VALUES ('9', 'POLYGON (( 2000 -1520, 2000  -920, 2200 -920, 2200 -1520, 2000  -1520 ))');


-- WorkOrientation
INSERT INTO systemknowledge.workorientation(id_wo,the_geom)
	VALUES (1, 'LINESTRING (200 40, 200 500)');
INSERT INTO systemknowledge.workorientation(id_wo,the_geom)
	VALUES (2, 'LINESTRING (600 40, 600 500)');
--I2
INSERT INTO systemknowledge.workorientation(id_wo, the_geom)
       VALUES (3,'LINESTRING (985 40, 985 500)');
INSERT INTO systemknowledge.workorientation(id_wo, the_geom)
       VALUES (4,'LINESTRING (1235 40, 1235 500)');
--I3
INSERT INTO systemknowledge.workorientation(id_wo, the_geom)
       VALUES (5,'LINESTRING (985 -40, 985 -500)');
INSERT INTO systemknowledge.workorientation(id_wo, the_geom)
       VALUES (6,'LINESTRING (1235 -40, 1235 -500)');
--I4
INSERT INTO systemknowledge.workorientation(id_wo, the_geom)
       VALUES (7,'LINESTRING (170 -840, 170 -1300)');
--I5
INSERT INTO systemknowledge.workorientation(id_wo, the_geom)
       VALUES (8,'LINESTRING (485 -840, 485 -1200)');
--I6
INSERT INTO systemknowledge.workorientation(id_wo, the_geom)
       VALUES (9,'LINESTRING (2125 -940, 2125 -1300)');


-- gridunit
SELECT * FROM systemknowledge.create_grid2(100,100);


-- soilunit
--I1
INSERT INTO systemknowledge.soilunit(id_soilunit, code_soiltype, the_geom)
	VALUES (1, 'S1', 'POLYGON (( 20 20, 20 620, 420 620, 420 20, 20 20 ))');
INSERT INTO systemknowledge.soilunit(id_soilunit, code_soiltype, the_geom)
	VALUES (2, 'S2', 'POLYGON (( 420 20, 420 620, 820 620, 820 20, 420 20 ))');
--I2
INSERT INTO systemknowledge.soilunit(id_soilunit, code_soiltype, the_geom)
       VALUES (3, 'S2','POLYGON (( 860 20, 860 620, 1260 620, 1260 20, 860 20 ))');
INSERT INTO systemknowledge.soilunit(id_soilunit, code_soiltype, the_geom)
       VALUES (4, 'S1','POLYGON (( 1260 20, 1260 620, 1460 620, 1460 20, 1260 20 ))');
--I3
INSERT INTO systemknowledge.soilunit(id_soilunit, code_soiltype, the_geom)
       VALUES (5, 'S2','POLYGON (( 860 -20, 860 -620, 1060 -620, 1060 -20, 860 -20 ))');
INSERT INTO systemknowledge.soilunit(id_soilunit, code_soiltype, the_geom)
       VALUES (6, 'S1','POLYGON (( 1060 -20, 1060 -620, 1460 -620, 1460 -20, 1060 -20 ))');
--I4
INSERT INTO systemknowledge.soilunit(id_soilunit, code_soiltype, the_geom)
       VALUES (7, 'S1','POLYGON (( -80 -1420, -80 -820, 320 -820, 320 -1420, -80 -1420 ))');
--I5
INSERT INTO systemknowledge.soilunit(id_soilunit, code_soiltype, the_geom)
       VALUES (8, 'S2','POLYGON (( 360 -1420, 360 -820, 760 -820, 760 -1420, 360 -1420 ))');
--I6
INSERT INTO systemknowledge.soilunit(id_soilunit, code_soiltype, the_geom)
       VALUES (9, 'S1','POLYGON (( 2000 -1520, 2000  -920, 2200 -920, 2200 -1520, 2000  -1520 ))');


-- landunit
 -- Extract the linework from the polygons (you may wish to add any interior rings to this query). 
	DROP TABLE IF EXISTS systemknowledge.all_lines CASCADE;
	CREATE TABLE systemknowledge.all_lines AS
	(SELECT Boundary(the_geom) AS the_geom
	FROM systemknowledge.plotunit
	UNION ALL
	SELECT Boundary(the_geom) AS the_geom
	FROM systemknowledge.gridunit
	UNION ALL
 	SELECT Boundary(the_geom) AS the_geom
 	FROM systemknowledge.soilunit
	UNION ALL
 	SELECT Boundary(the_geom) AS the_geom
 	FROM systemknowledge.croppingblock);
 	
	-- Node the linework: 
	DROP TABLE IF EXISTS systemknowledge.noded_lines CASCADE;
	CREATE TABLE systemknowledge.noded_lines AS
	SELECT St_Union(the_geom) AS the_geom
	FROM systemknowledge.all_lines;
	-- Re-polygonize all linework:
	DROP TABLE IF EXISTS systemknowledge.new_polys CASCADE;
	CREATE TABLE systemknowledge.new_polys (id serial PRIMARY KEY, the_geom geometry);
	INSERT INTO systemknowledge.new_polys (the_geom)
	SELECT geom AS the_geom
	FROM St_Dump((
	 SELECT St_Polygonize(the_geom) AS the_geom
	 FROM systemknowledge.noded_lines
	));
	-- UPDATE systemknowledge.new_polys SET the_geom  = ST_SetSRID(the_geom, 27572);

	-- Remove polgyon build from holes
	DELETE FROM systemknowledge.new_polys a
	USING (SELECT st_union(the_geom) FROM systemknowledge.plotunit) i
	WHERE NOT contains(i.st_union, pointonsurface(a.the_geom));
	-- Transfer attributes from original polygons to newly formed polygons
	--     * Create point-in-polygon features. 
	DROP TABLE IF EXISTS systemknowledge.new_polys_pip CASCADE;
	CREATE TABLE systemknowledge.new_polys_pip AS
	SELECT id, ST_PointOnSurface(the_geom) AS the_geom
	FROM systemknowledge.new_polys;
	--    * Overlay points with original polygons, transferring the attributes. 
	DROP TABLE IF EXISTS systemknowledge.pip_with_attributes CASCADE;
	CREATE TABLE systemknowledge.pip_with_attributes AS
	SELECT a.id, gid,id_croppingblock, id_soilunit FROM systemknowledge.new_polys_pip a
	LEFT JOIN systemknowledge.croppingblock cp ON St_Within(a.the_geom, cp.the_geom)
	LEFT JOIN systemknowledge.gridunit g ON St_Within(a.the_geom, g.the_geom)
	LEFT JOIN systemknowledge.soilunit s ON St_Within(a.the_geom, s.the_geom);

	--   * Join the points to the newly formed polygons, transferring the attributes: 
	DROP TABLE IF EXISTS systemknowledge.landunit CASCADE;
	CREATE TABLE systemknowledge.landunit AS
	(SELECT gid as ref_gridunit , id_croppingblock as ref_croppingblock, id_soilunit as ref_soilunit, the_geom
	FROM systemknowledge.new_polys a LEFT JOIN systemknowledge.pip_with_attributes b USING (id)
	ORDER BY ref_croppingblock);
	ALTER TABLE systemknowledge.landunit ADD COLUMN id_landunit serial;
    --  Drop temporary table
	DROP TABLE IF EXISTS systemknowledge.all_lines;
	DROP TABLE IF EXISTS systemknowledge.noded_lines;
	DROP TABLE IF EXISTS systemknowledge.new_polys;
	DROP TABLE IF EXISTS systemknowledge.new_polys_pip;
	DROP TABLE IF EXISTS systemknowledge.pip_with_attributes;


-- water Ressource
INSERT INTO systemknowledge.waterresource VALUES (1, 300, 25);

-- insert into FARMLAND
DELETE FROM systemknowledge.farmland;
INSERT INTO systemknowledge.farmland
SELECT lu.id_landunit,cb.id_croppingblock as ref_cb, ci.ref_wr as ref_wr, su.code_soiltype, lu.the_geom  FROM systemknowledge.landunit lu
	LEFT JOIN systemknowledge.croppingblock cb ON St_Within(lu.the_geom, cb.the_geom)
	LEFT JOIN systemknowledge.capislet ci ON St_Within(lu.the_geom, ci.the_geom)
	LEFT JOIN systemknowledge.soilunit su ON St_Within(lu.the_geom, su.the_geom);


--------------------------------------------------------------------------


-- Insert Cropping plan
---- I1 - P1
--2006
INSERT INTO systemknowledge.cropplan VALUES (1,2006,'BH','1','BH1');
INSERT INTO systemknowledge.cropplan VALUES (1,2007,'MA','1','MA1');
INSERT INTO systemknowledge.cropplan VALUES (1,2008,'BH','1','BH1');
INSERT INTO systemknowledge.cropplan VALUES (1,2009,'MA','1','MA1');
INSERT INTO systemknowledge.cropplan VALUES (1,2010,'BH','1','BH1');

---- I1 - P2
INSERT INTO systemknowledge.cropplan VALUES (2,2006,'MA','1','MA1');
INSERT INTO systemknowledge.cropplan VALUES (2,2007,'BH','1','BH1');
INSERT INTO systemknowledge.cropplan VALUES (2,2008,'MA','1','MA1');
INSERT INTO systemknowledge.cropplan VALUES (2,2009,'BH','1','BH1');
INSERT INTO systemknowledge.cropplan VALUES (2,2010,'MA','1','MA1');
---- I2 - P3
INSERT INTO systemknowledge.cropplan VALUES (3,2006,'BH','1','BH1');
INSERT INTO systemknowledge.cropplan VALUES (3,2007,'CH','1','CH1');
INSERT INTO systemknowledge.cropplan VALUES (3,2008,'BH','1','BH1');
INSERT INTO systemknowledge.cropplan VALUES (3,2009,'OP','1','OP1');
INSERT INTO systemknowledge.cropplan VALUES (3,2010,'BH','1','BH1');

---- I2 - P4
INSERT INTO systemknowledge.cropplan VALUES (4,2006,'CH','1','CH1');
INSERT INTO systemknowledge.cropplan VALUES (4,2007,'BH','1','BH1');
INSERT INTO systemknowledge.cropplan VALUES (4,2008,'OP','1','OP1');
INSERT INTO systemknowledge.cropplan VALUES (4,2009,'BH','1','BH1');
INSERT INTO systemknowledge.cropplan VALUES (4,2010,'PP','1','PP1');

---- I3 - P5
INSERT INTO systemknowledge.cropplan VALUES (5,2006,'BH','1','BH1');
INSERT INTO systemknowledge.cropplan VALUES (5,2007,'MA','1','MA1');
INSERT INTO systemknowledge.cropplan VALUES (5,2008,'BH','1','BH1');
INSERT INTO systemknowledge.cropplan VALUES (5,2009,'MA','1','MA1');
INSERT INTO systemknowledge.cropplan VALUES (5,2010,'BH','1','BH1');

---- I3 - P6
INSERT INTO systemknowledge.cropplan VALUES (6,2006,'MA','1','MA1');
INSERT INTO systemknowledge.cropplan VALUES (6,2007,'BH','1','BH1');
INSERT INTO systemknowledge.cropplan VALUES (6,2008,'MA','1','MA1');
INSERT INTO systemknowledge.cropplan VALUES (6,2009,'BH','1','BH1');
INSERT INTO systemknowledge.cropplan VALUES (6,2010,'MA','1','MA1');

---- I4 - P7
INSERT INTO systemknowledge.cropplan VALUES (7,2006,'BH','1','BH1');
INSERT INTO systemknowledge.cropplan VALUES (7,2007,'OP','1','OP1');
INSERT INTO systemknowledge.cropplan VALUES (7,2008,'BH','1','BH1');
INSERT INTO systemknowledge.cropplan VALUES (7,2009,'PP','1','PP1');
INSERT INTO systemknowledge.cropplan VALUES (7,2010,'BH','1','BH1');

---- I5 - P8
INSERT INTO systemknowledge.cropplan VALUES (8,2006,'OP','1','OP1');
INSERT INTO systemknowledge.cropplan VALUES (8,2007,'BH','1','BH1');
INSERT INTO systemknowledge.cropplan VALUES (8,2008,'PP','1','PP1');
INSERT INTO systemknowledge.cropplan VALUES (8,2009,'BH','1','BH1');
INSERT INTO systemknowledge.cropplan VALUES (8,2010,'CH','1','CH1');

---- I6 - P9
INSERT INTO systemknowledge.cropplan VALUES (9,2006,'BH','1','BH1');
INSERT INTO systemknowledge.cropplan VALUES (9,2007,'PP','1','PP1');
INSERT INTO systemknowledge.cropplan VALUES (9,2008,'BH','1','BH1');
INSERT INTO systemknowledge.cropplan VALUES (9,2009,'CH','1','CH1');
INSERT INTO systemknowledge.cropplan VALUES (9,2010,'BH','1','BH1');
